<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locked extends CI_Controller {

	function __construct() {
        parent::__construct();
		//$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }
	
	public function index() {	
		$this->load->model('settings_model');
		$task_hide = date("Y-m-d H:i:s", strtotime("+5 hours"));
		// hide the task
		$this->session->set_userdata('task_notification',$task_hide);
		
		//print_r($this->session->all_userdata());
		
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$this->load->view('common/main_header', $recentlyviewed);		
		$this->load->view('account_locked');		
		
	}
	
	
} ?>
