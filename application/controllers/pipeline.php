<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pipeline extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

	}

	public function index()	{
		$this->load->model('pipeline_model');
		$user = $this->input->get("user");
		$role = $this->session->userdata("role");

		// set vars for the different things
		$todays_date = date("Y-m-d");
		$month = date("Y-m-01") . " 00:00:00";
		$today = date("Y-m-t") . " 23:59:59";
		$days_360 = date("Y"). "-01-01 00:00:00";
		$first_day_month = date("Y-m-01") . " 00:00:00";
		$last_day_month = date("Y-m-t") . " 23:59:59";
		$start_few_months_ago = new DateTime(date("Y-m-01"));
		$start_few_months_ago->modify('- 3 months');
		$new_start_date = $start_few_months_ago->format("Y-m-d");
		$start_date = new DateTime($new_start_date);
		$end_date = new DateTime($start_date->format("Y-m-d"));
		$how_many_months = 8;
		$chart_array = array();

		if (is_numeric($user)) {
			if (($role==1) && ($this->pipeline_model->getUserMemeberOfGroup($user))) {

				//$data['bymilestone'] = $this->pipeline_model->pipelineByMilestoneUser($user);
				$data['forecast'] = $this->pipeline_model->pipelineForecastUser($user);
				$data['stats'] = $this->pipeline_model->pipelineStatsUser($user);

				$con_30 = $this->pipeline_model->pipelineConversionUser($month, $today, $user);
				if ($con_30['won']!=0) {
					$cal_30 = ($con_30['won']/$con_30['total']*100);
				} else {
					$cal_30 = 0;
				}
				$data['conversion_30days'] = number_format($cal_30);
				$data['conversion_30days_count'] = $con_30['total'];

				// get data for the last 90 days
				$threemonths = date("Y-m-d", strtotime($todays_date ." -90 days")) . " 00:00:00";
				$con_90 = $this->pipeline_model->pipelineConversionUser($threemonths, $today, $user);
				if ($con_90['won']!=0) {
					$cal_90 = ($con_90['won']/$con_90['total']*100);
				} else {
					$cal_90 = 0;
				}
				$data['conversion_90days'] = number_format($cal_90);
				$data['conversion_90days_count'] = $con_90['total'];


				$con_360 = $this->pipeline_model->pipelineConversionUser($days_360, $today, $user);
				if ($con_360['won']!=0) {
					$cal_360 = ($con_360['won']/$con_360['total']*100);
				} else {
					$cal_360 = 0;
				}
				$data['conversion_360days'] = number_format($cal_360);
				$data['conversion_360days_count'] = $con_360['total'];

				$sales_30 = $this->pipeline_model->pipelineSalesUser($first_day_month, $last_day_month, $user);
				$data['sales_30days'] = $sales_30['total'];

				// get sales data from last 30 days
				$sales_90 = $this->pipeline_model->pipelineSalesUser($threemonths, $today, $user);
				$data['sales_90days'] = $sales_90['total'];

				// get sales data from last 30 days
				$sales_360 = $this->pipeline_model->pipelineSalesUser($days_360, $today, $user);
				$data['sales_360days'] = $sales_360['total'];

				for($i=1;$i<=$how_many_months;$i++) {
					$end_date->modify('+ 1 month');
					$sales_for_month = $this->pipeline_model->pipelineSalesMonthUser($start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s"),$start_date->format("m/Y"), $user);
					// add from the query into the array
					$chart_array[] = $sales_for_month;
					// add a month to the start date
					$start_date->modify('+ 1 month');
				}
				$data['sales_by_month'] = $chart_array;


			} else {
				redirect("pipeline");
			}
		} elseif ($user=="all") {
			if ($role==1) {

				// get list of group users
				$users = $this->pipeline_model->getFullGroupUsersList();
				$users_list = implode(',', $users);

				//$data['bymilestone'] = $this->pipeline_model->pipelineByMilestoneUser($user);
				$data['forecast'] = $this->pipeline_model->pipelineForecastUser($users_list);
				$data['stats'] = $this->pipeline_model->pipelineStatsUser($users_list);

				$con_30 = $this->pipeline_model->pipelineConversionUser($month, $today, $users_list);
				if ($con_30['won']!=0) {
					$cal_30 = ($con_30['won']/$con_30['total']*100);
				} else {
					$cal_30 = 0;
				}
				$data['conversion_30days'] = number_format($cal_30);
				$data['conversion_30days_count'] = $con_30['total'];

				// get data for the last 90 days
				$threemonths = date("Y-m-d", strtotime($todays_date ." -90 days")) . " 00:00:00";
				$con_90 = $this->pipeline_model->pipelineConversionUser($threemonths, $today, $users_list);
				if ($con_90['won']!=0) {
					$cal_90 = ($con_90['won']/$con_90['total']*100);
				} else {
					$cal_90 = 0;
				}
				$data['conversion_90days'] = number_format($cal_90);
				$data['conversion_90days_count'] = $con_90['total'];


				$con_360 = $this->pipeline_model->pipelineConversionUser($days_360, $today, $users_list);
				if ($con_360['won']!=0) {
					$cal_360 = ($con_360['won']/$con_360['total']*100);
				} else {
					$cal_360 = 0;
				}
				$data['conversion_360days'] = number_format($cal_360);
				$data['conversion_360days_count'] = $con_360['total'];

				$sales_30 = $this->pipeline_model->pipelineSalesUser($first_day_month, $last_day_month, $users_list);
				$data['sales_30days'] = $sales_30['total'];

				// get sales data from last 30 days
				$sales_90 = $this->pipeline_model->pipelineSalesUser($threemonths, $today, $users_list);
				$data['sales_90days'] = $sales_90['total'];

				// get sales data from last 30 days
				$sales_360 = $this->pipeline_model->pipelineSalesUser($days_360, $today, $users_list);
				$data['sales_360days'] = $sales_360['total'];

				for($i=1;$i<=$how_many_months;$i++) {
					$end_date->modify('+ 1 month');
					$sales_for_month = $this->pipeline_model->pipelineSalesMonthUser($start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s"),$start_date->format("m/Y"), $users_list);
					// add from the query into the array
					$chart_array[] = $sales_for_month;
					// add a month to the start date
					$start_date->modify('+ 1 month');
				}
				$data['sales_by_month'] = $chart_array;


			} else {
				redirect("pipeline");
			}
		} else {

			//$data['bymilestone'] = $this->pipeline_model->pipelineByMilestone();
			$data['forecast'] = $this->pipeline_model->pipelineForecast();
			$data['stats'] = $this->pipeline_model->pipelineStats();

			$con_30 = $this->pipeline_model->pipelineConversion($month, $today);
			if ($con_30['won']!=0) {
				$cal_30 = ($con_30['won']/$con_30['total']*100);
			} else {
				$cal_30 = 0;
			}
			$data['conversion_30days'] = number_format($cal_30);
			$data['conversion_30days_count'] = $con_30['total'];

			// get data for the last 90 days
			$threemonths = date("Y-m-d", strtotime($todays_date ." -90 days")) . " 00:00:00";
			$con_90 = $this->pipeline_model->pipelineConversion($threemonths, $today);
			if ($con_90['won']!=0) {
				$cal_90 = ($con_90['won']/$con_90['total']*100);
			} else {
				$cal_90 = 0;
			}
			$data['conversion_90days'] = number_format($cal_90);
			$data['conversion_90days_count'] = $con_90['total'];


			$con_360 = $this->pipeline_model->pipelineConversion($days_360, $today);
			if ($con_360['won']!=0) {
				$cal_360 = ($con_360['won']/$con_360['total']*100);
			} else {
				$cal_360 = 0;
			}
			$data['conversion_360days'] = number_format($cal_360);
			$data['conversion_360days_count'] = $con_360['total'];

			$sales_30 = $this->pipeline_model->pipelineSales($first_day_month, $last_day_month);
			$data['sales_30days'] = $sales_30['total'];

			// get sales data from last 30 days
			$sales_90 = $this->pipeline_model->pipelineSales($threemonths, $today);
			$data['sales_90days'] = $sales_90['total'];

			// get sales data from last 30 days
			$sales_360 = $this->pipeline_model->pipelineSales($days_360, $today);
			$data['sales_360days'] = $sales_360['total'];

			for($i=1;$i<=$how_many_months;$i++) {
				$end_date->modify('+ 1 month');
				$sales_for_month = $this->pipeline_model->pipelineSalesMonth($start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s"),$start_date->format("m/Y"));
				// add from the query into the array
				$chart_array[] = $sales_for_month;
				// add a month to the start date
				$start_date->modify('+ 1 month');
			}
			$data['sales_by_month'] = $chart_array;

		}

		$data['group'] = $this->pipeline_model->getGroupUsersList();
		$recentlyviewed['viewed'] = $this->pipeline_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->pipeline_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('pipeline', $data);
	}

	public function list_view() {
		$this->load->model('pipeline_model');
		// get status
		$status = $this->input->get('status');
		$tag = $this->input->get('tag');
		$user = $this->input->get("user");
		$role = $this->session->userdata("role");

		if ($role==1) {
			if (is_numeric($user)) {
				// check if user is part of this group
				if ($this->pipeline_model->getUserMemeberOfGroup($user)) {
					$data['group'] = $this->pipeline_model->getGroupUsersList();
					$data['list'] = $this->pipeline_model->pipelineListGroupUser($status, $tag, $user);
				} else {
					$data['list'] = "";
				}
			} elseif ($user=="all") {
				$users = $this->pipeline_model->getFullGroupUsersList();
				$data['group'] = $this->pipeline_model->getGroupUsersList();
				$data['list'] = $this->pipeline_model->pipelineListGroupUser($status, $tag, implode(',', $users));
			} else {
				$data['group'] = $this->pipeline_model->getGroupUsersList();
				$data['list'] =	$this->pipeline_model->pipelineList($status, $tag);
			}
		} else {
			$data['list'] =	$this->pipeline_model->pipelineList($status, $tag);
		}

		$data['selected_status'] = $status;
		$data['selected_tag'] = $tag;
		$data['opp_tags'] = $this->pipeline_model->getOpportunitiesTags();
		$recentlyviewed['viewed'] = $this->pipeline_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->pipeline_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('pipeline_view', $data);
	}
}
