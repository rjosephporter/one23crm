<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calender extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

	}

	public function index($year = null, $month = null) {

		if (empty($year)) { $year = date('Y'); }
		if (empty($month)) { $month = date('m'); }

		$this->load->model('calender_model');
		$this->load->model('client_model');
		$event_data = $this->calender_model->get_cal_data($year, $month, $this->input->get('id'));
		$data['calender'] = $this->calender_model->show_cal($year, $month, $event_data);

		$data['sort_action'] = $this->calender_model->taskListActions();
		$data['group'] = $this->calender_model->getGroupUsersList();

		$recentlyviewed['viewed'] = $this->calender_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->calender_model->getRecentlyViewed('6');
		$data['clients'] = $this->client_model->clientTypeAheadFormatJson();
		$data['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		//var_dump($data['task_setting']);exit();
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('calender', $data);

	}

	public function tasks() {
		$this->load->model('calender_model');
		$selected = $this->input->get('action', TRUE);
		$user = $this->input->get("user");
		$role = $this->session->userdata("role");

		if ($role==1) {
			if (is_numeric($user)) {
				// check if user is part of this group
				if ($this->calender_model->getUserMemeberOfGroup($user)) {
					$data['group'] = $this->calender_model->getGroupUsersList();
					$data['list'] = $this->calender_model->taskListGroupUser($selected, $user);
				} else {
					$data['list'] = "";
				}
			} elseif ($user=="all") {
			$data['list'] = "";
				// get list of users for this group
				$users = $this->calender_model->getFullGroupUsersList();
				$data['list'] = $this->calender_model->taskListGroupUser($selected, implode(',', $users));
				$data['group'] = $this->calender_model->getGroupUsersList();

			} else {
				$data['group'] = $this->calender_model->getGroupUsersList();
				$data['list'] = $this->calender_model->taskList($selected);
			}
		} else {

			$data['list'] = $this->calender_model->taskList($selected);

		}

		$data['sort_action'] = $this->calender_model->taskListActions();
		$data['selected_action'] = $selected;

		$recentlyviewed['viewed'] = $this->calender_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->calender_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('calender_list', $data);
	}

	public function allan_cal_update_task(){
		$this->load->model('calender_model');
		$update_task = $this->calender_model->updateTaskDate($_POST['task_id'], $_POST['new_task_date'], $_POST['end_time']);
	}

	public function cal_update_task() {
		$this->load->model('calender_model');
		// task ref is user id and task id
		$task = $this->input->get('task_ref');
		$date = $this->input->get('task_date');
		if ($task && $date) {
			$task_info = explode('_', $task);
			$task_id = $task_info[1];
			$task_details = $this->calender_model->getTaskDetails($task_id);
			// explode the task date so we can get the time
			$current_task_info = explode(' ', $task_details['date']);
			$new_task_date = $date.' '.$current_task_info['1'];
			$update_task = $this->calender_model->updateTaskDate($task_id, $new_task_date);
			if ($update_task) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function get_current_date(){
		$this->load->model('calender_model');
		$event_data = $this->calender_model->allan_get_cal_task($_GET['start'], $_GET['end']);
		return $event_data;
	}

}
