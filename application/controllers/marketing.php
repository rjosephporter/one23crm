<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marketing extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}
	}

	public function index() {
		$this->load->model('marketing_model');
		$data['tag_list'] = $this->marketing_model->GetAllTags();
		$data['client_list'] = $this->marketing_model->getClientsWithSMS($this->input->get('tag'));
		$recentlyviewed['viewed'] = $this->marketing_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->marketing_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('marketing', $data);
	}

	public function verify() {
		$this->load->model('marketing_model');
		$credits = $this->marketing_model->getSMSCreditCount();
		if ($this->input->post('sms')) {
			$count = count($_POST['sms']);
			// check if they have enough credits
			if ($credits>=$count) {
				$this->session->set_userdata('numbers', json_encode($_POST['sms']));
				redirect('marketing/message');
			} else {
				$this->session->set_flashdata('error', '<div class="alert alert-danger">Sorry you do not have enough credits.</div>');
				redirect('marketing');
			}
		} else {
			$this->session->set_flashdata('error', '<div class="alert alert-danger">No numbers have been selected, please try again.</div>');
			redirect('marketing');
		}
	}

	public function message() {
		$this->load->model('marketing_model');
		$numbers = $this->session->userdata('numbers');
		$recentlyviewed['viewed'] = $this->marketing_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->marketing_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('marketing_message');
	}

	public function verify2() {
		$this->load->model('marketing_model');
		$sms_count = 0;
		$credits = $this->marketing_model->getSMSCreditCount();
		$numbers = json_decode($this->session->userdata('numbers'),true);
		$personalised = $this->input->post('personalised');
		$message = $this->input->post('message');
		$message_characters = strlen($message);

		// they need a message
		if ($message_characters==0) {
			$this->session->set_flashdata('error', '<div class="alert alert-danger">You must enter a message.</div>');
			redirect('marketing/message');
		}

		// loop every message to total the credits needed
		foreach ($numbers as $mob) {
			$characters = 0;
			// get the number and the name
			$explode_data = explode("|", $mob);
			if ($personalised==1) {
				$characters += strlen('Hi '. $explode_data[1] .'. ');
			}
			$characters += $message_characters;
			$sms_count += ceil($characters/160);
		}

		if ($credits>=$sms_count) {

			$sms_session = array(
				'message' => $message,
				'personalised' => $personalised,
				'required_credits' => $sms_count,
				'current_credits' => $credits
			);

			$this->session->set_userdata($sms_session);

			redirect('marketing/summary');

		} else {
			$this->session->set_flashdata('error', '<div class="alert alert-danger">Sorry you do not have enough credits. Please reduce the message size, or purchase additional credits.</div>');
			$this->session->set_flashdata('message', $message);
			redirect('marketing/message');
		}
	}

	public function summary() {
		$this->load->model('marketing_model');
		$data['recipients'] = json_decode($this->session->userdata('numbers'),true);
		$recentlyviewed['viewed'] = $this->marketing_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->marketing_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('marketing_summary', $data);
	}

	public function send() {
		$this->load->model('marketing_model');
		$recipients = json_decode($this->session->userdata('numbers'),true);
		$message = $this->session->userdata('message');
		$personalised = $this->session->userdata('personalised');
		$recipients_count = count($recipients);
		if ($recipients_count==1) {

				$messagetosend = "";
				$explode_data = explode("|", $recipients[0]);
				if ($personalised==1) {
					$messagetosend = 'Hi '. $explode_data[1] .'. '. $message;
				} else {
					$messagetosend = $message;
				}
				$this->marketing_model->sendSms($explode_data[0],$messagetosend);

				$this->session->set_flashdata('error', '<div class="alert alert-success">Successfully delivered to selected recipient.</div>');
				redirect('marketing');

		} elseif ($recipients_count>1) {

			foreach($recipients as $person) {
				$messagetosend = "";
				$explode_data = explode("|", $person);
				if ($personalised==1) {
					$messagetosend = 'Hi '. $explode_data[1] .'. '. $message;
				} else {
					$messagetosend = $message;
				}
				$this->marketing_model->sendSms($explode_data[0],$messagetosend);
			}

			$this->session->set_flashdata('error', '<div class="alert alert-success">Successfully delivered to selected recipients.</div>');
			redirect('marketing');

		} else {
			$this->session->set_flashdata('error', '<div class="alert alert-danger">Error processing your request, please try again later.</div>');
			redirect('marketing');
		}
	}


}
