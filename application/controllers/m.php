<?php class M extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_userdata('mobile_site', 1);
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);	
			redirect('login', 'refresh');			
		}	
	}

	public function index() {	

		// by visiting this controller, the user is saying they want the mobile site
		// we will set the mobile session, and then rest of the site will forward to the mobile style views
		$this->session->set_userdata('mobile_site', 1);
		
		// now that is set ... just send them off to the dashboard!
		redirect('m/dashboard');
	}
	
	public function desktop() {
		// remove the mobile session and send them to the dashboard
		$this->session->unset_userdata('mobile_site');
		redirect('dashboard');	
	}	
	
	public function dashboard() {		
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_dashboard');
			$this->load->view('common/mobile_footer');
		} else {
			redirect('dashboard');
		}
	}
	
	public function clients() {
		$this->load->model('client_model');	
		$data['tag_list'] = $this->client_model->GetAllTags();
		$data['customer_list'] = $this->client_model->getCustomerList($this->input->get('keyword'),$this->input->get('tag'));
		$this->load->view('common/mobile_header');		
		$this->load->view('mobile_clients', $data);
		$this->load->view('common/mobile_footer');					
	}

	public function recent() {
		$this->load->model('dashboard_model');
		$data['update_list'] = $this->dashboard_model->getUpdatesList();
		
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_recent', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect('dashboard');
		}
	}
	
	public function tasks() {
		$this->load->model('dashboard_model');
		$data['tasks_overdue'] = $this->dashboard_model->getTaskListOverdue();
		
		$now = date('Y-m-d H:i:s');
		$end_today = date('Y-m-d') . ' 23:59:59';
		$data['tasks_today'] = $this->dashboard_model->getTaskList($now, $end_today);

		$tomorrow = date("Y-m-d H:i:s ", strtotime("Tomorrow"));
		$next7days = date("Y-m-d H:i:s ", strtotime("+ 8 days"));
		$data['tasks_week'] = $this->dashboard_model->getTaskList($tomorrow, $next7days);			
				
		$data['tasks_other'] = $this->dashboard_model->getTaskList($next7days, date("Y-m-d H:i:s ", strtotime("+ 3 months")));

		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_tasks', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect('dashboard');
		}
	}
	
	public function note_view() {
		$this->load->model('dashboard_model');
		$note_id = $this->input->get('id');
		$data['note'] = $this->dashboard_model->getNoteDetails($note_id);

		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_note_view', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect('clients');
		}
		
	}
	
	public function add_note() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_note_add');
			$this->load->view('common/mobile_footer');
		} else {
			redirect('clients');
		}
	
	}
	
	public function add_task() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');	
			$this->load->view('mobile_task_add');
			$this->load->view('common/mobile_footer');
		} else {
			redirect('clients');
		}	
	}
	
	public function view_quote() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$quote = $this->input->get('quote');
		// grab quote details
		$data['quote_details'] = $this->client_model->getWeblineQuotesDetails($quote);
		$data['quote_results'] = $this->client_model->getWeblineQuotesResults($quote);
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_quote_view', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect('clients');
		}	
	}
	
	public function view_quote_multi() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$quote = $this->input->get('quote');
		// grab quote details
		$data['quote_results'] = $this->client_model->weblineMultiProductQuotesView($quote);
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_quote_view_multi', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect('clients');
		}	
	}
	
	public function quote_term() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$data['client_details'] = $this->client_model->getCustomerData($client);
		if ($data['client_details']) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_team_quote', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect("clients");
		}
	}			

	public function quote_mortgage() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$data['client_details'] = $this->client_model->getCustomerData($client);
		if ($data['client_details']) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_mortgage_quote', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect("clients");
		}
	}
	
	public function quote_familyincome() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$data['client_details'] = $this->client_model->getCustomerData($client);
		if ($data['client_details']) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_familyincome_quote', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect("clients");
		}
	}
	
	public function quote_whole() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$data['client_details'] = $this->client_model->getCustomerData($client);
		if ($data['client_details']) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_whole_quote', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect("clients");
		}
	}
	
	public function quote_ip() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$data['client_details'] = $this->client_model->getCustomerData($client);
		if ($data['client_details']) {
			$this->load->view('common/mobile_header');		
			$this->load->view('mobile_ip_quote', $data);
			$this->load->view('common/mobile_footer');
		} else {
			redirect("clients");
		}
	}	
	
						
	
}
