<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasksetting extends CI_Controller {

	protected $array_icons;

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

	}

	public function index(){
		$data = array();
		$data['viewed'] = false;

		$data['actions'] = $this->Tasksetting_model->getAllTaskLabel('',false);

		$this->load->view('common/main_header', $data);
		$this->load->view('task_setting', $data);
	}

	public function addAction(){
		$data = array(
			'user_id' => $this->session->userdata('user_id'),
			'action_name' => $this->input->post('actionName'),
			'icons' => $this->input->post('icons'),
			'color' => $this->input->post('color_label'),
		);
		$this->db->insert(
			$this->Tasksetting_model->getTable(),
			$data
		);
		if( $this->db->affected_rows() ){
			$last_insert_id = $this->db->insert_id();
			redirect('tasksetting', 'refresh');
		}
		//var_dump($insert);
	}

	public function create_action(){
		$data = array();
		$data['viewed'] = false;
		$data['icons'] = array_keys($this->Tasksetting_model->getIcons());
		$data['color_label'] = $this->Tasksetting_model->getColorLabel();

		$this->load->view('common/main_header', $data);
		$this->load->view('task_setting_create', $data);
	}

	public function edit($id){
		$data = array();
		$data['viewed'] = false;
		$data['icons'] = array_keys($this->Tasksetting_model->getIcons());
		$data['color_label'] = $this->Tasksetting_model->getColorLabel();
		$data['action'] = $this->Tasksetting_model->getTaskSettingById($id);
		$data['id'] = $id;
		if( $data['action']->num_rows == 0 ){
			redirect('tasksetting', 'refresh');
		}else{
			$this->load->view('common/main_header', $data);
			$this->load->view('task_setting_edit', $data);
		}
	}

	public function updateAction($id){
		$data = array(
			'action_name' => $this->input->post('actionName'),
			'icons' => $this->input->post('icons'),
			'color' => $this->input->post('color_label'),
		);
		$this->db->where('id', $id);
		$this->db->update($this->Tasksetting_model->getTable(), $data);
		redirect('tasksetting', 'refresh');
	}

	public function deleteAction($id){
		$this->db->delete($this->Tasksetting_model->getTable(), array('id' => $id));
		redirect('tasksetting', 'refresh');
	}

} // file end
