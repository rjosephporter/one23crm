<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Document_library extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

	}

	public function index()	{
		$this->load->model('dashboard_model');

		$documents = array();

		$category_list = $this->dashboard_model->getDocumentCategory();

		if ($category_list) {

			$counter = 0;

			foreach ($category_list as $category) {

			$provider_counter = 0;

				$documents[] = array('name' => $category['name'], 'cat_id' => $category['id']);

					// get the list of providers
					$provider_list = $this->dashboard_model->getDocumentProviders();

					if ($provider_list) {

						foreach ($provider_list as $provider) {

							$documents[$counter]['providers'][] = array('name' => $provider['name'], 'provider_id' => $provider['id']);

							// now get list of documents for this cat and provider, if any
							$documents_list = $this->dashboard_model->getDocumentList($category['id'],$provider['id']);

							$count_docs = count($documents_list);

							$documents[$counter]['providers'][$provider_counter]['total_docs'] = $count_docs;

							if ($documents_list) {

								foreach ($documents_list as $document) {

									$documents[$counter]['providers'][$provider_counter]['documents'][] = array('test' => $provider_counter, 'name' => $document['name'], 'doc_id' => $document['id'], 'provider_id' => $document['provider'], 'filename' => $document['filename']);

								} // end foreach docs

							}  // if any docs

							$provider_counter++;

						} // end foreach providers

					} // if providers

				$counter++;
			} // end foreach cat

		} // if any cats

		//echo '<pre>';
		//print_r($documents);

		$data['documents'] = $documents;

		$data['uploaded_docs'] = $this->dashboard_model->getUserUploadedDocs();

		$recentlyviewed['viewed'] = $this->dashboard_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->dashboard_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('document_library', $data);

	}

	public function upload() {
		$this->load->model('dashboard_model');
		$this->load->helper('file');
		$filename = substr($_FILES['doc']['name'],0,-4) . '_' . time();
		$config['upload_path'] = './documents/library/own/';
		$config['allowed_types'] = 'pdf';
		$config['file_name'] = $filename;
		$this->load->library('upload', $config);

 		if (!$this->upload->do_upload('doc')) {
			$this->session->set_flashdata('error', $this->upload->display_errors('<div class="alert alert-error">','</div>'));
			redirect('document_library');
		} else {
			$file = $this->upload->data();
			//print_r($file);
			// add to database
			$add_file = array(
				'belongs_to' => $this->session->userdata('user_id'),
				'filename' => $file['file_name'],
				'name' => $this->input->post('name'),
				'active' => 1
			);
			$this->dashboard_model->addUserDoc($add_file);
			$this->session->set_flashdata('error', '<div class="alert alert-success">The file has been successfully added.</div>');
			redirect('document_library');
		}
	}

	public function remove() {
		$this->load->model('dashboard_model');
		$file = $this->input->get('file');
		$this->dashboard_model->removeUserDoc($file);
		$this->session->set_flashdata('error', '<div class="alert alert-success">The file has been successfully removed.</div>');
		redirect('document_library');
	}

}
