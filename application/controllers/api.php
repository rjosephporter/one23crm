<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function vmdShared() {
		$this->load->model('api_model');
		$new_file = array(
			'ref' => $this->input->post('ref'),
			'url' => $this->input->post('url'),
			'name' => $this->input->post('name'),
			'notes' => $this->input->post('notes'),
			'time' => $this->input->post('time'),
			'provider' => $this->input->post('provider')
		);
		$this->api_model->saveVMDFile($new_file);
	}	



	
}
