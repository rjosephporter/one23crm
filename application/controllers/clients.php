<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

	}

	public function delete_selected_clients() {
		$this->load->model('client_model');
		$action = $this->input->post('action');
		if ($action=="delete") {
			if (isset($_POST['client_selected'])) {
				foreach($_POST['client_selected'] as $selected) {
					// delete each of the selected
					$this->client_model->deleteMainClient($selected);
				}
			}
		}
		redirect('clients');
	}

	public function index() {
		$this->load->model('client_model');
		//$data['customer_list'] = $this->client_model->getCustomerList($this->input->get('keyword'),$this->input->get('tag'));
		$data['tag_list'] = $this->client_model->GetAllTags();
		$data['group_users'] = $this->client_model->getGroupUsersList();
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		/*
		if ($this->session->userdata('mobile_site')==1) {
			$data['customer_list'] = $this->client_model->getCustomerList($this->input->get('keyword'),$this->input->get('tag'));
			$this->load->view('common/mobile_header');
			$this->load->view('mobile_clients', $data);
			$this->load->view('common/mobile_footer');
		} else {
		*/
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client', $data);
		//}
	}

	public function ajax_client_details() {
		$this->load->model('client_model');
		$client = $this->input->get('client');
		$details = $this->client_model->getCustomerData($client);
		$full_details = "";
		if ($details) {

			$websites = $this->client_model->getCustomerURLs($client);
			$numbers = $this->client_model->getTelephoneNumberList($client);
			$emails = $this->client_model->getEmailAddressList($client);

			if ($details['account_type']=="2") {
				$full_details .= '<strong>'. $details['company_name'] .' '. $details['companyreg'] .'</strong>';
			} else {
				$full_details .= '<strong>'. $details['title'] .' '. $details['first_name'] .' '. $details['last_name'] .'</strong>';
				$family = $this->client_model->getAssociatedPeople($client);
				if ($family) {
					$full_details .= '<hr style="margin:5px;" /><strong>Family Members:</strong><br />';
					foreach($family as $fam) {
						$full_details .= $fam['title'] .' '. $fam['first_name'] .' '. $fam['last_name'] .' - '. $fam['relationship'] .' - '. date("d/m/Y", strtotime($fam['dob'])) .'<br />';
					}
				}
			}

			if ($details['address_line_1']!="") {
				$full_details .= '<hr style="margin:5px;" /><strong>Address:</strong><br />'. $details['address_line_1'] .' '. $details['town'] .' '. $details['county'] .' '. $details['postcode'];
			}

			if ($numbers) {
				$full_details .= '<hr style="margin:5px;" /><strong>Telephone Numbers:</strong><br />';
				foreach($numbers as $num) {
					$full_details .= $num['number'] .'<br />';
				}
			}

			if ($emails) {
				$full_details .= '<hr style="margin:5px;" /><strong>Email Addresses:</strong><br />';
				foreach($emails as $email) {
					$full_details .= '<a href="mailto:'. $email['email'] .'">'. $email['email'] .'</a><br />';
				}
			}

			if ($websites) {
				$full_details .= '<hr style="margin:5px;" /><strong>Websites:</strong><br />';
				foreach($websites as $url) {
					$full_details .= '<a href="'. $url['url'] .'">'. $url['url'] .'</a><br />';
				}
			}

			echo $full_details;

		} else {

			echo 'Error loading details ...';

		}
	}

	public function ajax_search() {
		$this->load->model('client_model');
		// get the customer list
		$customer_list = $this->client_model->getCustomerList($this->input->get('keyword'),$this->input->get('tag'));

		//if (substr($this->session->userdata('theme_site'),-5)=="clean") {
			$icon_set = '-clean2';
		//} else {
			//$icon_set = '';
		//}

		if ($customer_list) {

			//echo '<pre>';
			//print_r($customer_list);

			$client_list_counter = 0;

			echo '<form action="'. base_url() .'clients/delete_selected_clients" method="post">
				<table class="table" id="client-list">
				  <thead>
					<tr>
					  <th><input type="checkbox" value="1" name="delete_all" id="delete_all" /></th>
					  <th colspan="2">Client / Company Name</th>
					  <th>Action(s)</th>
					</tr>
				  </thead>
				  <tbody>';

				foreach($customer_list as $customer) {

				// reset
				$extra = "";

				if ($customer['type']==2) {
					if ($customer['profile_picture']!="") {
						$list_image = '<img src="'.base_url().'img/profile_images/'.$customer['profile_picture'].'" width="40" height="40" />';
					} else {
						$list_image = '<img src="'.base_url().'img/small_summary_comp.png" />';
					}
				} else {
					if ($customer['profile_picture']!="") {
						$list_image = '<img src="'.base_url().'img/profile_images/'.$customer['profile_picture'].'" width="40" height="40" />';
					} else {
						$list_image = '<img src="'.base_url().'img/small_summary_person.png" />';
					}
				}

				if ($customer['address']!=", , ") {
					$address = $customer['address'];
				} else {
					$address = "";
				}

				if ( (!empty($customer['organisation'])) && (!empty($customer['job_title'])) ) {
					$extra = $customer['job_title'] . ' at <a href="'. base_url() .'clients/view?id='. $customer['associated'] .'"  data-client="'. $customer['associated'] .'" class="client_info">' . $customer['organisation'] . '</a>';
				}

				// if they are associated to someone, then its probs wife or whatever
				if ( (!empty($customer['associated'])) && (!empty($customer['relationship'])) ) {
					// get the partners info
					$partner = $this->client_model->getPartnerInfo($customer['associated']);
					if ($partner) {
						$extra = $customer['relationship'] . ' of <a href="'. base_url() .'clients/view?id='. $partner['partner_id'] .'"  data-client="'. $partner['partner_id'] .'" class="client_info">' . $partner['partner_name'] . '</a>';
					}
				}

				/*
				if (!empty($customer['number'])) {
					$tele_email = $customer['email'] . '<br />' . $customer['number'];
				} else {
					$tele_email = $customer['email'];
				}
				*/

				// set row class
				if ($client_list_counter%2==0) {
					$class = ' class="client-row"';
				} else {
					$class = "";
				}

				echo '<tr'.$class.'>
				<td width="2%" align="center" valign="middle"><input type="checkbox" class="client_selected" name="client_selected['. $customer['id'] .']" value="'. $customer['id'] .'" /></td>
				<td width="40" align="center" valign="middle" class="hidden-phone"><a href="'. base_url() .'clients/view?id='. $customer['id'] .'">'. $list_image .'</a></td>
				<td width="70%" valign="middle"><a href="'. base_url() .'clients/view?id='. $customer['id'] .'" data-client="'. $customer['id'] .'" class="client_info"><span style="font-size:14px; font-weight:bold;">'. $customer['customer_name'] .'</span></a>&nbsp;&nbsp;&nbsp;<a href="'. base_url() .'clients/view?id='. $customer['id'] .'" target="_blank">*</a> '. $extra .'<br />'. $address .'</td>';

				//<td valign="middle" class="hidden-phone">'. $tele_email .'</td>';

				//if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_files")==1) ) {
				if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_delete")==1) ) {
				echo '<td valign="middle" class="hidden-phone"><a href="'.base_url().'clients/delete_client?id='.$customer['id'].'" onClick="return confirm(\'Are you sure you wish to delete this client?\')"><img src="'.base_url().'img/delete-icon'.$icon_set.'.png" /></a></td>';
				}

				echo '</tr>';

				$client_list_counter++;

				}

			echo '</tbody></table><p>
			<label><small>Action for selected clients:</small><br />
			<select name="action" style="padding:0px; height:23px; font-size:12px;">
			<option value="">Please Select</option>
			<option value="delete">Delete Selected Clients</option>
			</select></label>
			<button type="submit" class="btn btn-mini">Submit</button></p></form>';


		} else {

			echo '<h4>Sorry no clients found.</h4>';

		}

	}

	public function ajax_global_search() {
		$this->load->model('client_model');
		$results = $this->client_model->getCustomerListName($this->input->get('keywords'));
		if ($results) {
			echo '<table width="100%" class="table">';
				foreach($results as $result) {
					echo '<tr>
						<td><a href="'. base_url().'clients/view?id='. $result['id'] .'" style="color:#fff;">'. $result['customer_name'] .'</a></td>
						</tr>';
				}
			echo '</table>';
		} else {
			echo "No results found.";
		}
	}

	public function add_tag_client() {
		$this->load->model('client_model');
		$tag = $this->input->post('add_client_tag_ref');
		$client = $this->input->post('client');
		$tagdata = array(
			'tag_id' => $tag,
			'customer_id' => $client
		);
		// check if client has the tag already
		$check = $this->client_model->checkTagClient($tag, $client);
		if (!$check) {
			// add tag to the client
			$this->client_model->addTagClient($tagdata);
		}
	}

	public function remove_tag_client() {
		$this->load->model('client_model');
		$tag = $this->input->post('tag');
		$split_tag = explode('_', $tag);

		$this->client_model->RemoveTagClient($split_tag[0], $split_tag[1]);
	}

	public function edit_client() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getWebsiteAddressList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_edit', $data);
		} else {
			redirect('clients');
		}

	}

	public function delete_client() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			// delete the client
			$this->client_model->deleteMainClient($this->input->get('id'));
			redirect('clients', 'refresh');
		} else {
			redirect('clients');
		}

	}

	public function edit_company() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getWebsiteAddressList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_edit_company', $data);
		} else {
			redirect('clients');
		}

	}

	public function update_company() {
		$this->load->model('client_model');
		$id = $this->input->post('id');

		// clear old telephone numbers before adding new
		$this->client_model->clearAllTelephoneNumbers($id);
		// clear all email addresses
		$this->client_model->clearAllEmailAddresses($id);
		// clear all urls
		$this->client_model->clearAllUrls($id);

		$updated_data = array(
			'companyreg' =>  $this->input->post('companyreg'),
			'companyemployee' =>  $this->input->post('companyemployee'),
			'sector' =>  $this->input->post('sector'),
			'duedil_company_details' =>  $this->input->post('duedil_company')
		);

		$updated_address = array(
			'address_line_1' => $this->input->post('address'),
			'county' => $this->input->post('county'),
			'town' => $this->input->post('town'),
			'postcode' => $this->input->post('postcode')
		);

		// add telephone numbers
		$phone_count = 1;
		foreach ($_POST['telephone'] as $phone) {
			$telephone = array(
				'customer_id' => $id,
				'number' => $phone,
				'type' => $_POST['telephone_type'][$phone_count]
			);
			// add the number if not emtpy
			if (!empty($phone)) {
			$this->client_model->addNewTelephoneToDb($telephone);
			}
		$phone_count++;
		}

		// add emails
		$email_count = 1;
		foreach ($_POST['email'] as $email_address) {
		//echo $email_address;
			$email = array(
				'customer_id' => $id,
				'email' => $email_address,
				'type' => $_POST['email_type'][$email_count]
			);
			// add the number if not emtpy
			if (!empty($email_address)) {
			$this->client_model->addNewEmailToDb($email);
			}
		$email_count++;
		}

		if (isset($_POST['url'])) {
			// add urls
			$url_count = 1;
			foreach ($_POST['url'] as $url) {
				if ($url!="") {
					if ((substr($url,0,7)!="http://") && (substr($url,0,8)!="https://")) {
						$url = "http://".$url;
					}
				}
				$website = array(
					'customer_id' => $id,
					'url' => $url,
					'website' => $_POST['url_site'][$url_count],
					'type' => $_POST['url_type'][$url_count]
				);
				// add the number if not emtpy
				if (!empty($url)) {
				$this->client_model->addNewUrlToDb($website);
				}
			$url_count++;
			}
		}

		$this->client_model->updateClient($updated_data, $id);
		$this->client_model->updateClientAddress($updated_address, $id);

		redirect('clients/view?id=' . $id);

	}

	public function update_client() {
		$this->load->model('client_model');
		$id = $this->input->post('id');
		$haveaddress = $this->client_model->doesClientHaveAddress($id);
		$return = $this->input->post('return');
		$returnid = $this->input->post('returnid');
		// clear old telephone numbers before adding new
		$this->client_model->clearAllTelephoneNumbers($id);
		// clear all email addresses
		$this->client_model->clearAllEmailAddresses($id);
		// clear all urls
		$this->client_model->clearAllUrls($id);

		if ($this->input->post('dob2')!="") {
			$client_dob = explode("/", $this->input->post('dob2'));
			$dob_formatted = $client_dob[2].'-'.$client_dob[1].'-'.$client_dob[0];
		} else {
			$dob_formatted = "0000-00-00";
		}

		$updated_data = array(
			'title' => $this->input->post('title'),
			'first_name' => $this->input->post('fname'),
			'last_name' => $this->input->post('sname'),
			'marital_status' => $this->input->post('marital'),
			'living_status' => $this->input->post('living'),
			'employment_status' => $this->input->post('employment'),
			'occupation' => $this->input->post('job'),
			'dob' => $dob_formatted,
			'smoker' => $this->input->post('smoker')
		);

		$updated_address = array(
			'address_line_1' => $this->input->post('address'),
			'county' => $this->input->post('county'),
			'town' => $this->input->post('town'),
			'postcode' => $this->input->post('postcode')
		);

		$new_address = array(
			'customer_id' => $id,
			'address_line_1' => $this->input->post('address'),
			'county' => $this->input->post('county'),
			'town' => $this->input->post('town'),
			'postcode' => $this->input->post('postcode')
		);

		// add telephone numbers
		$phone_count = 1;
		foreach ($_POST['telephone'] as $phone) {
			$telephone = array(
				'customer_id' => $id,
				'number' => $phone,
				'type' => $_POST['telephone_type'][$phone_count]
			);
			// add the number if not emtpy
			if (!empty($phone)) {
			$this->client_model->addNewTelephoneToDb($telephone);
			}
		$phone_count++;
		}

		// add emails
		$email_count = 1;
		foreach ($_POST['email'] as $email_address) {
		//echo $email_address;
			$email = array(
				'customer_id' => $id,
				'email' => $email_address,
				'type' => $_POST['email_type'][$email_count]
			);
			// add the number if not emtpy
			if (!empty($email_address)) {
			$this->client_model->addNewEmailToDb($email);
			}
		$email_count++;
		}

		if (isset($_POST['url'])) {
			// add urls
			$url_count = 1;
			foreach ($_POST['url'] as $url) {
				if ($url!="") {
					if ((substr($url,0,7)!="http://") && (substr($url,0,8)!="https://")) {
						$url = "http://".$url;
					}
				}
				$website = array(
					'customer_id' => $id,
					'url' => $url,
					'website' => $_POST['url_site'][$url_count],
					'type' => $_POST['url_type'][$url_count]
				);
				// add the number if not emtpy
				if (!empty($url)) {
				$this->client_model->addNewUrlToDb($website);
				}
			$url_count++;
			}
		}

		$this->client_model->updateClient($updated_data, $id);

		if ($haveaddress) { //echo 1;
			$this->client_model->updateClientAddress($updated_address, $id);
		} else { //echo 2;
			$this->client_model->insertAddressData($new_address, $id);
		}

		// if there is a return to, go there
		if ($return) {
			redirect('clients/'.$return.'?id=' . $returnid);
		} else {
			redirect('clients/view?id=' . $id);
		}

	}

	public function view() {
		$this->load->model('client_model');

		$client = $this->input->get('id');

		if ($client) {
			$customer_details = $this->client_model->getCustomerData($client);
			$data['client_id'] = $client;
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($client);
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($client);
			$data['customer_url'] = $this->client_model->getCustomerURLs($client);
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($client);
			$data['customer_policies'] = $this->client_model->summaryLivePolicies($client);
			$data['recent_quotes'] = $this->client_model->summaryRecentQuotes($client);
			$data['upcoming_events'] = $this->client_model->summaryEvents($client);
			$data['recent_messages'] = $this->client_model->summaryRecentMessages($client);
			$data['recent_files'] = $this->client_model->getFilesForClient($client, '', 10);
			$data['associated_people'] = $this->client_model->getAssociatedPeople($client);
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($client);
			$data['customer_email'] = $this->client_model->getEmailAddressList($client);
			$data['tab_messages'] = $this->client_model->tabMessages($client);
			$data['tab_policies'] = $this->client_model->tabPolicies($client);
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($client);
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($client);
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['twitter'] = $this->client_model->getClientTwitterAccount($this->input->get('id'));
			$data['shared'] = $this->client_model->getViewMyDocsSharedList($client);
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($client);
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$this->client_model->addRecentlyViewed($client);
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$data['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			if ($this->session->userdata('mobile_site')==1) {
				$data['medical_records'] = $this->client_model->getMedicalRecords($client);
				$data['income_records'] = $this->client_model->getIncomeReportList($client);
				$data['factfind_records'] = $this->client_model->getFactFindList($client);
				$data['protection_records'] = $this->client_model->getWeblineQuotesMobile($client);
				$this->load->view('common/mobile_header');
				$this->load->view('mobile_clients_view', $data);
				$this->load->view('common/mobile_footer');
			} else {
				$this->load->view('common/main_header', $recentlyviewed);
				$this->load->view('client_view', $data);
			}
		} else {
			redirect('clients');
		}

	}

	public function files() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['sections'] = $this->client_model->getCustomFileSections();
			$data['section1'] = $this->client_model->getFilesForClient($this->input->get('id'), 1, 10);
			$data['section2'] = $this->client_model->getFilesForClient($this->input->get('id'), 2, 10);
			$data['section3'] = $this->client_model->getFilesForClient($this->input->get('id'), 3, 10);
			$data['section4'] = $this->client_model->getFilesForClient($this->input->get('id'), 4, 10);
			$data['section5'] = $this->client_model->getFilesForClient($this->input->get('id'), 5, 10);
			$data['section6'] = $this->client_model->getFilesForClient($this->input->get('id'), 6, 10);
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['vmd_account'] = $this->client_model->getVMDAccount($this->input->get('id'));
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_files', $data);
		} else {
			redirect('clients');
		}

	}

	public function uploadFile() {
		$this->load->model('client_model');
		$config['upload_path'] = './documents/';
		$config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
		$config['file_name'] = rand(9,99) . '_' . time();
		$this->load->library('upload', $config);

 		if (!$this->upload->do_upload('fileupload')) {

		//echo $this->upload->display_errors();

			// some error

		} else {

			$file = $this->upload->data();

			$file_data = array(
					'customer_id' => $this->input->post('client'),
					'added_date' => date('Y-m-d H:i:s'),
					'filename' => $file['file_name'],
					'name' => $this->input->post('name'),
					'type' => $this->input->post('type')
			);

			// add file data to database
			$this->client_model->addNewFileToDb($file_data, $this->input->post('client'));

			$update_info = array(
				'belongs_to' => $this->session->userdata('group_id'),
				'belongs_user' => $this->session->userdata('user_id'),
				'added_date' => date('Y-m-d H:i:s'),
				'customer_id' => $this->input->post('client'),
				'user' => $this->session->userdata('name'),
				'title' => 'new file attached for client ',
				'type' => 3
			);

			$this->client_model->addDashUpdate($update_info);

		}

		redirect('clients/files?id=' . $this->input->post('client') .'&result=');

	}

	public function delete_file() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$file = $this->input->get('file');
		$this->client_model->deleteFile($client, $file);
		redirect('clients/files?id=' . $client);
	}

	public function messages() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['customer_messages'] = $this->client_model->getCustomerMessages($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_mob'] = $this->client_model->getClientMobNumber($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['sms_credits'] = 	$this->client_model->getSMSCredits();
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_messages', $data);
		} else {
			redirect('clients');
		}

	}

	public function create() {
		$this->load->model('client_model');
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_create');
	}

	public function create_company() {
		$this->load->model('client_model');
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_create_company');
	}

	public function client_new_image() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		// new filename
		$new_file = $this->session->userdata('user_id') . '_' . time() . '_' . rand(1,9);
		$config['upload_path'] = './img/profile_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $new_file;
		$this->load->library('upload', $config);
 		if (!$this->upload->do_upload('profileimage')) {
			// error
		} else {
			$file = $this->upload->data();
			// resize the uploaded image
			$resize['image_library'] = 'gd2';
			$resize['source_image']	= $this->upload->upload_path . $this->upload->file_name;
			//$resize['create_thumb'] = TRUE;
			$resize['maintain_ratio'] = FALSE;
			$resize['width'] = 70;
			$resize['height'] = 70;
			$resize['overwrite'] = TRUE;
			$this->load->library('image_lib', $resize);

			if ($this->image_lib->resize()) {
				// image info array
				$image_array = array(
					'customer_id' => $client,
					'image' => $this->upload->file_name
				);
				// add to database and set it
				$this->client_model->clientProfileImage($image_array);
				$imageid = $this->db->insert_id();
				$this->client_model->clientProfileImageSet($imageid, $client);

				redirect('clients/view?id='.$client);
			}

		}// end if upload
	}

	public function client_new_image_url() {
		$this->load->model('client_model');
		$image = $this->input->post('image');
		$client = $this->input->post('client');
		// get the file
		$get_file = file_get_contents($image);
		// new name
		$new_file = $this->session->userdata('user_id') . '_' . time() . '_' . rand(1,9) . '.jpg';
		// save file
		$save_file = file_put_contents('./img/profile_images/'.$new_file, $get_file);

		if ($save_file) {
			// image info array
			$image_array = array(
				'customer_id' => $client,
				'image' => $new_file
			);
			// add to database and set it
			$this->client_model->clientProfileImage($image_array);
			$imageid = $this->db->insert_id();
			$this->client_model->clientProfileImageSet($imageid, $client);
			return true;
		} else {
			return false;
		}
	}

	public function client_blank_image() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		$this->client_model->clientProfileImageSet(0, $client);
	}

	public function create_note() {
		$this->load->model('client_model');
		$customer_id = $this->input->post('notefor');

		// check if any files to upload
		$config['upload_path'] = './documents/';
		$config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
		$config['file_name'] = $this->session->userdata('user_id') . '_' . time();
		$this->load->library('upload', $config);

		$file = "";

 		if (!$this->upload->do_upload('note_file')) {

			// some error

		} else {

			$file = $this->upload->data();

			if ($file['file_name']) {
				$file = $file['file_name'];
			} else {
				$file = "";
			}
		}

		$info_to_add = array(
				'customer_id' => $customer_id,
				'added_date' => date('Y-m-d H:i:s'),
				'added_by' => $this->session->userdata('username'),
				'note' => $this->input->post('note'),
				'file' => $file
		);

		$this->client_model->addCustomerNotes($info_to_add);
		$note_id = $this->db->insert_id();

		$update_info = array(
			'belongs_to' => $this->session->userdata('group_id'),
			'belongs_user' => $this->session->userdata('user_id'),
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $customer_id,
			'user' => $this->session->userdata('name'),
			'title' => 'added note for client ',
			'note_id' => $note_id,
			'type' => 2
		);

		$this->client_model->addDashUpdate($update_info);

		redirect('clients/view?id=' . $customer_id .'&result=note_added');
	}

	public function delete_note() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$note = $this->input->get('note');

		$this->client_model->deleteCustomerNotes($client, $note);

		redirect('clients/view?id=' . $client);

	}

	public function create_task() {
		$this->load->model('client_model');
		if( trim($this->input->post('getclient')) == "" ){
			$customer_id = 0;
		}else{
			$customer_id = $this->input->post('customer_id') ? $this->input->post('customer_id') : 0;
		}
		$taskdate = $this->input->post('task_date');
		$taskendhr = $this->input->post('task_hour_end');
		$taskendmin = $this->input->post('task_min_end');
		$taskhour = $this->input->post('task_hour');
		$taskmin = $this->input->post('task_min');
		$return = $this->input->post('return');
		$remind = $this->input->post('task_remind');

		$date_string = $taskdate . ' ' . $taskhour . ':' . $taskmin . ':00';
		$date_string = str_replace('/', '-', $date_string);

		$task_date = date('Y-m-d H:i:s', strtotime($date_string));
		$date_end = str_replace('/', '-', $taskdate.' '.$taskendhr.':'.$taskendmin.':00');
		$task_date_end = date('Y-m-d H:i:s', strtotime($date_end));

		if ($remind>0) {
			$remind_time = date('Y-m-d H:i:s', strtotime('- '.$remind.' minutes', strtotime($task_date)));
		} else {
			$remind_time = "";
		}

		$task_action = $this->input->post('task_action');
		$action = '';
		$task_setting = 0;
		$get_name_action = $this->Tasksetting_model->getTaskSettingById($task_action);
		if( $get_name_action->num_rows > 0 ){
			$action = $get_name_action->row()->action_name;
			$task_setting = (int)$task_action;
		}else{
			$action = $this->input->post('task_action');
		}

		$task_to_task = array(
				'customer_id' => $customer_id,
				'added_date' => date('Y-m-d H:i:s'),
				'added_by' => $this->session->userdata('username'),
				'name' => $this->input->post('task_name'),
				'date' => $task_date,
				'end_time' => $task_date_end,
				'action' => $action,
				'task_setting' => $task_setting,
				'status' => '1',
				'remind' => $remind_time,
				'remind_mins' => $remind,
				'belongs_to' => $this->session->userdata('user_id')
		);
		//var_dump($this->input->post());
		//var_dump($task_to_task);
		//exit();
		$this->client_model->addClientTask($task_to_task);

		if ($customer_id!=0 && $return == 'client') {
		redirect('clients/view?id=' . $customer_id .'&result=task_added');
		} elseif (isset($return)) {
		redirect($return);
		} else {
		redirect('dashboard');
		}

	}

	public function complete_task() {
		$this->load->model('client_model');
		$value = $this->input->post('task');
		$values = explode("_", $value);
		$task = $values[0];
		$client = $values[1];
		//if ($task) { echo "no task id"; }
		$update = array(
			'status' => 0,
			'completed_date' => date('Y-m-d H:i:s')
		);
		// update the task
		$this->client_model->completeTask($update, $task, $this->session->userdata('user_id'));

		// add some to the updates list
		$update_info = array(
			'belongs_to' => $this->session->userdata('group_id'),
			'belongs_user' => $this->session->userdata('user_id'),
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $client,
			'user' => $this->session->userdata('name'),
			'title' => 'completed task ',
			'task_id' => $value,
			'type' => 4
		);

		$this->client_model->addDashUpdate($update_info);

	}

	public function create_new_client() {
		$this->load->model('client_model');

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
		// personal details
		$this->form_validation->set_rules('title', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('fname', 'first name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sname', 'surname', 'trim|required|xss_clean');
		$this->form_validation->set_rules('dob2', 'date of birth', 'trim|xss_clean');
		$this->form_validation->set_rules('marital', 'marital status', 'trim|xss_clean');
		//$this->form_validation->set_rules('livingstatus', 'living status', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('employmentstatus', 'employment status', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('gender', 'gender', 'trim|required|xss_clean');
		$this->form_validation->set_rules('job', 'job title', 'trim|xss_clean');
		/*
		// partner details
		$this->form_validation->set_rules('partners_title', 'partners title', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_fname', 'partners first name', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_sname', 'partners surname', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_gender', 'partners gender', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_occupation', 'partners occupation', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_dob', 'partners date of birth', 'trim|xss_clean');
		$this->form_validation->set_rules('partners_employmentstatus', 'partners employment status', 'trim|xss_clean');
		// contact details
		$this->form_validation->set_rules('address', 'address line', 'trim|required|xss_clean');
		$this->form_validation->set_rules('address2', 'address line 2', 'trim|xss_clean');
		$this->form_validation->set_rules('town', 'town', 'trim|required|xss_clean');
		$this->form_validation->set_rules('postcode', 'postcode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('daytime', 'daytime telephone', 'trim|xss_clean');
		$this->form_validation->set_rules('evening', 'evening', 'trim|xss_clean');
		$this->form_validation->set_rules('mobile', 'mobile number', 'trim|xss_clean');
		$this->form_validation->set_rules('email', 'email address', 'valid_email|trim|xss_clean');
		*/

		if($this->form_validation->run() == FALSE) {

			//$this->load->view('client_create');
			redirect('clients/create');

		} else {

			// format the dob
			if ($this->input->post('dob2')!="") {
				$client_dob = explode("/", $this->input->post('dob2'));
				$dob_formatted = $client_dob[2].'-'.$client_dob[1].'-'.$client_dob[0];
			} else {
				$dob_formatted = "0000-00-00";
			}

			$client_info = array(
				'ref' => $this->session->userdata('user_id').time(),
				'added_date' => date('Y-m-d H:i:s'),
				'belongs_to' => $this->session->userdata('group_id'),
				'belongs_user' => $this->session->userdata('user_id'),
				'title' => $this->input->post('title'),
				'first_name' => $this->input->post('fname'),
				'last_name' => $this->input->post('sname'),
				'marital_status' => $this->input->post('marital'),
				'living_status' => $this->input->post('living'),
				'employment_status' => $this->input->post('employment'),
				'occupation' => $this->input->post('job'),
				'dob' => $dob_formatted,
				'smoker' => $this->input->post('smoker')
			);

			$this->client_model->insertData($client_info);
			$clientid = $this->db->insert_id();

			// add the address details
			$client_address = array(
				'address_line_1' => $this->input->post('address'),
				'town' => $this->input->post('town'),
				'postcode' => $this->input->post('postcode'),
				'customer_id' => $clientid,
				'type' => $this->input->post('address_type')
			);

			$this->client_model->insertAddressData($client_address, $clientid);

			// add telephone numbers
			$phone_count = 1;
			foreach ($_POST['telephone'] as $phone) {
				$telephone = array(
					'customer_id' => $clientid,
					'number' => $phone,
					'type' => $_POST['telephone_type'][$phone_count]
				);
				// add the number if not emtpy
				if (!empty($phone)) {
				$this->client_model->addNewTelephoneToDb($telephone);
				}
			$phone_count++;
			}

			// add emails
			$email_count = 1;
			foreach ($_POST['email'] as $email_address) {
			//echo $email_address;
				$email = array(
					'customer_id' => $clientid,
					'email' => $email_address,
					'type' => $_POST['email_type'][$email_count]
				);
				// add the number if not emtpy
				if (!empty($email_address)) {
				$this->client_model->addNewEmailToDb($email);
				}
			$email_count++;
			}

			if (isset($_POST['url'])) {
				// add urls
				$url_count = 1;
				foreach ($_POST['url'] as $url) {

					if ($url!="") {
						if ((substr($url,0,7)!="http://") && (substr($url,0,8)!="https://")) {
							$url = "http://".$url;
						}
					}

					$website = array(
						'customer_id' => $clientid,
						'url' => $url,
						'website' => $_POST['url_site'][$url_count],
						'type' => $_POST['url_type'][$url_count]
					);
					// add the number if not emtpy
					if (!empty($url)) {
					$this->client_model->addNewUrlToDb($website);
					}
				$url_count++;
				}
			}

			$partner_name = $this->input->post('partner_fname');

			if ($partner_name!="") {
				// if they have entered partners info
				// format the dob
				if ($this->input->post('partner_dob')!="") {
					$partner_dob = explode("/", $this->input->post('partner_dob'));
					$partner_dob_formatted = $partner_dob[2].'-'.$partner_dob[1].'-'.$partner_dob[0];
				} else {
					$partner_dob_formatted = "0000-00-00";
				}

				$partner_details = array(
					'ref' => $this->session->userdata('user_id') . time() . rand(1,9),
					'belongs_to' => $this->session->userdata('group_id'),
					'belongs_user' => $this->session->userdata('user_id'),
					'added_date' => date('Y-m-d H:i:s'),
					'title' => $this->input->post('partner_title'),
					'first_name' => $this->input->post('partner_fname'),
					'last_name' => $this->input->post('partner_sname'),
					'dob' => $partner_dob_formatted,
					'smoker' => $this->input->post('partner_smoker'),
					'job_title' => $this->input->post('partner_job'),
					'living_status' => $this->input->post('partner_living'),
					'employment_status' => $this->input->post('partner_employment'),
					'associated' => $clientid,
					'relationship' => 'Spouse/Partner'
				);
				// add to database
				$this->client_model->addNewClientPartner($partner_details);
			}

			// see if any kids
			$kids = $this->input->post('dependents');
			if ($kids>0) {
				// loop thru kids and get them to database
				$kid_loop = 1;
				foreach($_POST['kid_fname'] as $kid_fname) {
				// they have a first name
					if ($kid_fname!="") {

						$kid_dob = $_POST['kid_dob'][$kid_loop];
						$kid_dob_explode = explode('/', $kid_dob);
						$kid_dob_formatted = $kid_dob_explode[2]. '-' .$kid_dob_explode[1]. '-' .$kid_dob_explode[0];

						$kid_array = array(
							'ref' => $this->session->userdata('user_id') . time() . rand(1,9),
							'belongs_to' => $this->session->userdata('group_id'),
							'belongs_user' => $this->session->userdata('user_id'),
							'added_date' => date('Y-m-d H:i:s'),
							'first_name' => $kid_fname,
							'last_name' => $_POST['kid_sname'][$kid_loop],
							'dob' => $kid_dob_formatted,
							'relationship' => $_POST['kid_type'][$kid_loop],
							'associated' => $clientid,
							'type' => 4
						);
					// add to database
					$this->client_model->addNewClientPartner($kid_array);
					$kid_loop++;
					}
				}
			}

			// add some to the updates list
			$update_info = array(
				'belongs_to' => $this->session->userdata('group_id'),
				'belongs_user' => $this->session->userdata('user_id'),
				'added_date' => date('Y-m-d H:i:s'),
				'customer_id' => $clientid,
				'user' => $this->session->userdata('name'),
				'title' => 'added a new personal client ',
				'type' => 1
			);

			$this->client_model->addDashUpdate($update_info);

			//echo '<pre>';
			//print_r($client_info);
			//echo '</pre>';
			redirect('clients/view?id=' . $clientid .'&result=added');

		}

	}

	public function create_company_client() {
		$this->load->model('client_model');

			$client_info = array(
					'ref' => $this->session->userdata('user_id').time(),
					'type' => '2',
					'belongs_to' => $this->session->userdata('group_id'),
					'belongs_user' => $this->session->userdata('user_id'),
					'added_date' => date('Y-m-d H:i:s'),
					'company_name' => $this->input->post('company'),
					'companyreg' => $this->input->post('companyreg'),
					'companyemployee' => $this->input->post('companyemployee'),
					'sector' => $this->input->post('sector'),
					'duedil_company_details' => $this->input->post('duedil_company')
			);

			$this->client_model->insertData($client_info);
			$clientid = $this->db->insert_id();

			// add the address details
			$client_address = array(
				'address_line_1' => $this->input->post('address'),
				'town' => $this->input->post('town'),
				'postcode' => $this->input->post('postcode'),
				'customer_id' => $clientid,
				'type' => $this->input->post('address_type')
			);

			$this->client_model->insertAddressData($client_address, $clientid);

		// add telephone numbers
		$phone_count = 1;
		foreach ($_POST['telephone'] as $phone) {
			$telephone = array(
				'customer_id' => $clientid,
				'number' => $phone,
				'type' => $_POST['telephone_type'][$phone_count]
			);
			// add the number if not emtpy
			if (!empty($phone)) {
			$this->client_model->addNewTelephoneToDb($telephone);
			}
		$phone_count++;
		}

		// add emails
		$email_count = 1;
		foreach ($_POST['email'] as $email_address) {
		//echo $email_address;
			$email = array(
				'customer_id' => $clientid,
				'email' => $email_address,
				'type' => $_POST['email_type'][$email_count]
			);
			// add the number if not emtpy
			if (!empty($email_address)) {
			$this->client_model->addNewEmailToDb($email);
			}
		$email_count++;
		}

		if (isset($_POST['url'])) {
			// add urls
			$url_count = 1;
			foreach ($_POST['url'] as $url) {

				if ($url!="") {
					if ((substr($url,0,7)!="http://") && (substr($url,0,8)!="https://")) {
						$url = "http://".$url;
					}
				}

				$website = array(
					'customer_id' => $clientid,
					'url' => $url,
					'website' => $_POST['url_site'][$url_count],
					'type' => $_POST['url_type'][$url_count]
				);
				// add the number if not emtpy
				if (!empty($url)) {
				$this->client_model->addNewUrlToDb($website);
				}
			$url_count++;
			}
		}

		// add some to the updates list
		$update_info = array(
			'belongs_to' => $this->session->userdata('group_id'),
			'belongs_user' => $this->session->userdata('user_id'),
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $clientid,
			'user' => $this->session->userdata('name'),
			'title' => 'added a new company client ',
			'type' => 1
		);

		$is_main_contact = $this->input->post('contact_fname');
		// if they have entered some main contacts details
		if ($is_main_contact!="") {
			$main_contact = array(
				'type' => 1,
				'ref' => $this->session->userdata('user_id').time().rand(1,9),
				'belongs_to' => $this->session->userdata('group_id'),
				'belongs_user' => $this->session->userdata('user_id'),
				'added_date' => date('Y-m-d H:i:s'),
				'title' => $this->input->post('contact_title'),
				'first_name' => $this->input->post('contact_fname'),
				'last_name' => $this->input->post('contact_sname'),
				'associated' => $clientid,
				'organisation' => $this->input->post('company'),
				'job_title' => $this->input->post('contact_job')
			);
			$this->client_model->insertData($main_contact);
			$maincontactid = $this->db->insert_id();
			// if a telephone add it
			$main_contact_phone = $this->input->post('contact_phone');
			if ($main_contact_phone!="") {
				$main_contact_phone_array = array(
					'customer_id' => $maincontactid,
					'number' => $main_contact_phone,
					'type' => 'Work'
				);
				$this->client_model->addNewTelephoneToDb($main_contact_phone_array);
			}
			// if an email address it add
			$main_contact_email = $this->input->post('contact_email');
			if ($main_contact_email!="") {
				$main_contact_email_array = array(
					'customer_id' => $maincontactid,
					'email' => $main_contact_email,
					'type' => 'Work'
				);
				$this->client_model->addNewEmailToDb($main_contact_email_array);
			}
		}

		$this->client_model->addDashUpdate($update_info);

		redirect('clients/view?id=' . $clientid .'&result=added');
	}

	public function send_message() {
		$this->load->model('client_model');
		$customer_id = $this->input->post('client');
		$customer_name = $this->input->post('client_name');

		$data = array(
				'subject' => $this->input->post('subject'),
				'body' => $this->input->post('body')
		);

		$this->client_model->sendMessage($customer_id, $customer_name, $data);

		redirect('clients/messages?id=' . $customer_id .'&result=');
	}

	public function send_reply() {
		$this->load->model('client_model');

		$data = array(
			'subject' => $this->input->post('reply_subject'),
			'body' => $this->input->post('___reply_body'),
			'to' => $this->input->post('reply_to'),
			'client' => $this->input->post('reply_client')
		);

		$this->client_model->sendReplyMessage($data);

		redirect('messages?result=sent');
	}

	public function people() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			// build people list
			$employees = $this->client_model->getListEmployees($this->input->get('id'));
			$main_contacts = $this->client_model->getListMainEmployees($this->input->get('id'));
			$data['employee_list'] = array_merge($main_contacts, $employees);
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_people', $data);
		} else {
			redirect('clients');
		}

	}

	public function new_person() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_people_new', $data);
		} else {
			redirect('clients');
		}

	}

	public function delete_person() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$person = $this->input->get('person');
		$this->client_model->deleteClient($client, $person);

		redirect('clients/view?id=' . $client);

	}

	public function new_family() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);

			$this->load->view('client_family_new', $data);
		} else {
			redirect('clients');
		}

	}

	public function edit_family() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['family_member'] = $this->client_model->getFamilyMemberDetails($this->input->get('id'), $this->input->get('family'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);

			$this->load->view('client_family_edit', $data);
		} else {
			redirect('clients');
		}

	}

	public function update_family() {
		$this->load->model('client_model');

		$family_data = array(
			'title' => $this->input->post('title'),
			'first_name' => $this->input->post('fname'),
			'last_name' => $this->input->post('sname'),
			'dob' => $this->input->post('dob'),
			'relationship' => $this->input->post('relationship')
		);

		$this->client_model->updateFamilyMember($this->input->post('client'), $this->input->post('family'), $family_data);

		redirect('clients/people?id='.$this->input->post('client'));

	}

	public function create_new_person() {
		$this->load->model('client_model');

		$person_data = array(
			'type' => '1',
			'added_date' => date('Y-m-d H:i:s'),
			'ref' =>  $this->session->userdata('user_id').time(),
			'belongs_to' =>  $this->session->userdata('group_id'),
			//'title' => $this->input->post('title'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'job_title' => $this->input->post('job'),
			'associated' => $this->input->post('company_id'),
			'organisation' => $this->input->post('company_name')
			);

		// insert customer data
			$this->client_model->insertData($person_data);
			$clientid = $this->db->insert_id();

			// add the address details
			$client_address = array(
				'address_line_1' => $this->input->post('address'),
				'town' => $this->input->post('town'),
				'postcode' => $this->input->post('postcode'),
				'customer_id' => $clientid,
				'type' => $this->input->post('address_type')
			);

			$this->client_model->insertAddressData($client_address, $clientid);

		// add telephone numbers
		$phone_count = 1;
		foreach ($_POST['telephone'] as $phone) {
			$telephone = array(
				'customer_id' => $clientid,
				'number' => $phone,
				'type' => $_POST['telephone_type'][$phone_count]
			);
			// add the number if not emtpy
			if (!empty($phone)) {
			$this->client_model->addNewTelephoneToDb($telephone);
			}
		$phone_count++;
		}

		// add emails
		$email_count = 1;
		foreach ($_POST['email'] as $email_address) {
		//echo $email_address;
			$email = array(
				'customer_id' => $clientid,
				'email' => $email_address,
				'type' => $_POST['email_type'][$email_count]
			);
			// add the number if not emtpy
			if (!empty($email_address)) {
			$this->client_model->addNewEmailToDb($email);
			}
		$email_count++;
		}

		if (isset($_POST['url'])) {
			// add urls
			$url_count = 1;
			foreach ($_POST['url'] as $url) {
			//echo $url;
				$website = array(
					'customer_id' => $clientid,
					'url' => $url,
					'website' => $_POST['url_site'][$url_count],
					'type' => $_POST['url_type'][$url_count]
				);
				// add the number if not emtpy
				if (!empty($url)) {
				$this->client_model->addNewUrlToDb($website);
				}
			$url_count++;
			}
		}

		$update_info = array(
			'belongs_to' => $this->session->userdata('group_id'),
			'belongs_user' => $this->session->userdata('user_id'),
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $clientid,
			'user' => $this->session->userdata('name'),
			'title' => 'added a new person to a ' . $this->input->post('company_name')
		);

		$this->client_model->addDashUpdate($update_info);

		redirect('clients/people?id=' . $clientid);

	}

	public function create_new_family() {
		$this->load->model('client_model');

		$relationship = $this->input->post('relationship');

		if ($relationship=="Spouse/Partner") {
			$type = 3;
		} else {
			$type = 4;
		}

		// format the dob
		$client_dob = explode("/", $this->input->post('dob'));
		$dob_formatted = $client_dob[2].'-'.$client_dob[1].'-'.$client_dob[0];


		$person_data = array(
			'type' => $type,
			'added_date' => date('Y-m-d H:i:s'),
			'belongs_to' => $this->session->userdata('group_id'),
			'title' => $this->input->post('title'),
			'first_name' => $this->input->post('fname'),
			'last_name' => $this->input->post('sname'),
			'relationship' => $this->input->post('relationship'),
			'associated' => $this->input->post('client'),
			'dob' => $dob_formatted,
			'ref' => $this->session->userdata('user_id').time()
			);

			//echo '|' . $this->input->post('company');

		$this->client_model->insertData($person_data);

		redirect('clients/people?id=' . $this->input->post('client'));

	}

	public function new_opportunity() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['opp_tags'] = $this->client_model->getOpportunitiesTags();
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_opportunity_new', $data);
		} else {
			redirect('clients');
		}

	}

	public function create_new_opportunity() {
		$this->load->model('client_model');

		$value = $this->input->post('value');
		$probability = $this->input->post('probability');
		if ($probability==100) {
			$value_calc = $value;
		} else {
			$value_calc = ($value*('.'.$probability));
		}

		$explode_date = explode('/', $this->input->post('close_date'));
		$new_close_date = $explode_date['2'] . '-' . $explode_date['1'] . '-' . $explode_date['0'];

		$opportunity_data = array(
			'customer_id' => $this->input->post('client'),
			'belongs_to' => $this->session->userdata('user_id'),
			//'belongs_user' => $this->session->userdata('user_id'),
			'milestone' =>  $this->input->post('milestone'),
			'probability' => $this->input->post('probability'),
			'value' => preg_replace('/[^0-9.]/', '', $this->input->post('value')),
			'value_calc' => $value_calc,
			'close_date' => $new_close_date,
			'name' => $this->input->post('name'),
			'text' => $this->input->post('text'),
			'added_date' => date('Y-m-d H:i:s')
		);

		// add to database
		$this->client_model->addNewOpportunity($opportunity_data);
		$opp_id = $this->db->insert_id();

		if (isset($_POST['tag'])) {
			foreach($_POST['tag'] as $tag) {

				$tag_array = array(
					'opp_id' => $opp_id,
					'opp_tag' => $tag['id']
				);
				// add ref tag to database
				$this->client_model->addOpportunityTag($tag_array);
			}
		}
		redirect('clients/opportunities?id=' . $this->input->post('client'));
	}

	public function edit_opportunity() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['opportunity_details'] = $this->client_model->getOpportunity($this->input->get('opportunity'));
			$data['opportunity_tags'] = $this->client_model->getTagsForOpportunity($this->input->get('opportunity'));
			$data['opp_tags'] = $this->client_model->getOpportunitiesTags();
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_opportunity_edit', $data);
		} else {
			redirect('clients');
		}

	}

	public function update_opportunity() {
		$this->load->model('client_model');
		$id = $this->input->post('opportunity');
		// clear tags
		$this->client_model->clearTagOpportunity($id);
		$tags = $this->input->post('tag');
		$value = $this->input->post('value');
		$probability = $this->input->post('probability');

		if ($probability==100) {
			$value_calc = $value;
		} else {
			$value_calc = ($value*('.'.$probability));
		}

		$explode_date = explode('/', $this->input->post('close_date'));
		$new_close_date = $explode_date['2'] . '-' . $explode_date['1'] . '-' . $explode_date['0'];

		$milestone = $this->input->post('milestone');
		$status = $this->input->post('status');

		if ($milestone=="Won") {
			$new_close_date = date('Y-m-d');
			$new_status = 1;
		} elseif ($milestone=="Lost") {
			$new_close_date = date('Y-m-d');
			$new_status = 1;
		} else {;
			$new_status = $this->input->post('status');
		}

		// if the status is closed, then set todays date
		//if ($status==1) {
		//	$new_close_date = date('Y-m-d');
		//}

		$opportunity = array(
			'name' => $this->input->post('name'),
			'text' => $this->input->post('text'),
			'value' => $value,
			'value_calc' => $value_calc,
			'milestone' => $this->input->post('milestone'),
			'probability' => $probability,
			'close_date' => $new_close_date,
			'status' => $new_status
		);

		// see what tags we have
		if ($tags) {
			foreach($_POST['tag'] as $newtag) {
				if($newtag!="") {
					$this->client_model->addOpportunityTag(array('opp_id' => $id, 'opp_tag' => $newtag));
				}
			}
		}

		$this->client_model->updateOpportunity($opportunity, $id);
		redirect('clients/opportunities?id=' . $this->input->post('client'));
	}

	public function delete_opportunity() {
		$this->load->model('client_model');
		$opportunity = $this->input->get('opportunity');
		$this->client_model->deleteOpportunity($opportunity);
		redirect('clients/opportunities?id=' . $this->input->get('id'));
	}

	public function opportunities() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$customer_details = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['list_opportunities'] = $this->client_model->getOpportunityList($this->input->get('id'));
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($this->input->get('id'));
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_opportunities', $data);
		} else {
			redirect('clients');
		}

	}

	public function send_sms() {
		$this->load->model('client_model');
		// get the message
		$message = $this->input->post('message');
		// get current sms credits
		$credits = $data['sms_credits'] = $this->client_model->getSMSCredits();
		// count the total message lenth
		$count = strlen($message);
		// total sms messages needed
		$needed = ceil(($count/160));

		// check they have enough credits to even send the sms
		if ($needed<=$credits) {
			$this->curl->create("https://www.txtlocal.com/sendsmspost.php");
			$topost = array(
				'uname' => 'steve.warden1@btopenworld.com',
				'pword' => 'fuckwit',
				'message' => $this->input->post('message'),
				//'from' => '123InsureMe',
				'from' => $this->session->userdata('sms_display'),
				'selectednums' => $this->input->post('number'),
				'info' => 1
				//'test' => 1
			);
			$this->curl->post($topost);
			$this->curl->execute();
			$message_data = array(
				'added_date' => date('Y-m-d H:i:s'),
				'customer_id' => $this->input->post('client'),
				'sender' => $this->session->userdata('name'),
				'body' => $message,
				'to' => $this->input->post('number'),
				'subject' => 'Text message sent to client',
				'direction' => 1,
				'method' => 2,
				'ref' => 'SMS_' . time()
			);
			$this->client_model->addNewMessageToDb($message_data, $this->input->post('client'));
			// take credits from account
			$this->client_model->removeSMSCredits($credits, $needed);
			// go back to the message page
			redirect('clients/messages?id=' . $this->input->post('client'));
		} else {
			redirect('clients/messages?id=' . $this->input->post('client') . '&results=insuffisant');
		}

	}

	public function new_employee() {
		$this->load->model('client_model');
		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_employee_new', $data);
		} else {
			redirect('clients');
		}

	}

	public function edit_employee() {
		$this->load->model('client_model');
		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['employee'] = $this->client_model->getEmployeeData($this->input->get('id'), $this->input->get('employee'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_employee_edit', $data);
		} else {
			redirect('clients');
		}

	}

	public function edit_employee_save() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		$employee = $this->input->post('employee');
		$dob = $this->input->post('dob');
		if ($dob) {
			$explode = explode('/', $dob);
			$dob = $explode[2] .'-'. $explode[1] .'-'. $explode[0];
		} else {
			$dob = "0000-00-00";
		}

		$update = array(
			'title' => $this->input->post('title'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'dob' => $dob,
			'job_title' => $this->input->post('job'),
			'address' => $this->input->post('street'),
			'town' => $this->input->post('town'),
			'county' => $this->input->post('county'),
			'postcode' => $this->input->post('postcode'),
			'gender' => $this->input->post('gender'),
			'smoker' => $this->input->post('smoker')
		);

		$this->client_model->updateEmployeeData($client, $employee, $update);

		redirect('clients/people?id=' . $client);

	}

	public function delete_employee() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$employee = $this->input->get('employee');
		$this->client_model->deleteEmployee($client, $employee);
		redirect('clients/people?id=' . $client);
	}

	public function delete_family_member() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$family = $this->input->get('family');
		$this->client_model->deleteFamilyMember($client, $family);
		redirect('clients/people?id=' . $client);
	}

	public function create_new_employee() {
		$this->load->model('client_model');
		$client = $this->input->post('client');

		$this->load->library('form_validation');
		//$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
		$this->form_validation->set_rules('title', 'title', 'trim|required|xss_clean');
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('last_name', 'surname name', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('dob', 'date of birth', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('job', 'job title', 'trim|xss_clean');
		//$this->form_validation->set_rules('street', 'address', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('town', 'town', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('postcode', 'postcode', 'trim|required|xss_clean');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('errors', validation_errors());
			redirect('clients/new_employee?id=' . $client);
		} else {

			$dob = $this->input->post('dob');
			if ($dob) {
				$explode = explode('/', $dob);
				$dob = $explode[2] .'-'. $explode[1] .'-'. $explode[0];
			} else {
				$dob = "0000-00-00";
			}

			$employee = array(
				'customer_id' => $client,
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'dob' => $dob,
				'address' => $this->input->post('street'),
				'town' => $this->input->post('town'),
				'county' => $this->input->post('county'),
				'postcode' => $this->input->post('postcode'),
				'title' => $this->input->post('title'),
				'job_title' => $this->input->post('job'),
				'gender' => $this->input->post('gender'),
				'smoker' => $this->input->post('smoker')
			);

			$this->client_model->addNewEmployeeToDb($employee);

			redirect('clients/people?id=' . $client);

		}

	}

	public function getTaskDetails() {
		$this->load->model('client_model');
		$task = $this->client_model->getTask($this->input->post('task'));

		$the_time = substr($task['date'],11);
		$explode_time = explode(':', $the_time);

		$the_date = substr($task['date'],0,10);
		$explode_date = explode('-', $the_date);

		$the_time_end = substr($task['end_time'],11);
		$explode_time_end = explode(':', $the_time_end);

		$the_date_end = substr($task['end_time'],0,10);
		$explode_date_end = explode('-', $the_date_end);
		//var_dump($explode_time_end);exit();
		if( $task['task_setting'] == 0 ){
			$action = $task['action'];
		}else{
			$action = $task['task_setting'];
		}
		$task_array = array(
			'id' => $task['id'],
			'customer_id' => $task['customer_id'],
			'name' => $task['name'],
			'action' => $action,
			'date' => $explode_date[2].'/'.$explode_date[1].'/'.$explode_date[0],
			'end_hr' => isset($explode_time_end[0]) ? $explode_time_end[0]:'',
			'end_min' => isset($explode_time_end[1]) ? $explode_time_end[1]:'',
			'hour' => $explode_time[0],
			'mins' => $explode_time[1],
			'remind' => $task['remind_mins'],
			'customer_name' => !is_null($task['customer_name']) ? $task['customer_name']:''
		);

		echo json_encode($task_array);

	}

	public function update_task() {
		$this->load->model('client_model');

		$taskdate = $this->input->post('edit_task_date');
		if( $this->input->post('edit_customer_id') == 0 && trim($this->input->post('getclient')) != ''){
			//$task_client_task = $this->input->post('edit_client_task');
			$task_client_task = $this->input->post('edit_client_task');
		}elseif( trim($this->input->post('getclient')) == '' && $this->input->post('edit_customer_id') == 0 ){
			$task_client_task = 0;
		}else{
			$task_client_task = $this->input->post('edit_customer_id');
		}

		$task_action = $this->input->post('edit_task_action');
		$action = '';
		$task_setting = 0;
		$get_name_action = $this->Tasksetting_model->getTaskSettingById($task_action);
		if( $get_name_action->num_rows > 0 ){
			$action = $get_name_action->row()->action_name;
			$task_setting = (int)$task_action;
		}else{
			$action = $this->input->post('edit_task_action');
		}

		$taskendhr = $this->input->post('edit_task_hour_end');
		$taskendmin = $this->input->post('edit_task_min_end');
		$taskhour = $this->input->post('edit_task_hour');
		$taskmin = $this->input->post('edit_task_min');
		$return = $this->input->post('return');
		$remind = $this->input->post('edit_task_remind');

		$date_string = $taskdate . ' ' . $taskhour . ':' . $taskmin . ':00';
		$date_string = str_replace('/', '-', $date_string);

		$task_date = date('Y-m-d H:i:s', strtotime($date_string));
		$date_end = str_replace('/', '-', $taskdate.' '.$taskendhr.':'.$taskendmin.':00');
		$task_date_end = date('Y-m-d H:i:s', strtotime($date_end));
		if ($remind>0) {
			$remind_time = date('Y-m-d H:i:s', strtotime('- '.$remind.' minutes', strtotime($task_date)));
		} else {
			$remind_time = "";
		}

		$update = array(
			'name' => $this->input->post('edit_task_name'),
			'customer_id' => $task_client_task,
			'action' => $action,
			'task_setting' => $task_setting,
			'date' => $task_date,
			'end_time' => $task_date_end,
			'remind' => $remind_time,
			'remind_mins' => $remind
		);
		//var_dump($this->input->post());
		//var_dump($update);
		//exit();
		$this->client_model->updateTask($update, $this->input->post('edit_task_id'), $this->session->userdata('user_id'));
		redirect($return);
	}

	public function delete_task() {
		$this->load->model('client_model');
		$this->client_model->deleteTask($this->input->post('task'));
	}

	public function cal_complete_task() {
		$this->load->model('client_model');
		$task = $this->input->post('task');
		$client = 0;
		//if ($task) { echo "no task id"; }
		$update = array(
			'status' => 0,
			'completed_date' => date('Y-m-d H:i:s')
		);
		// update the task
		$this->client_model->completeTask($update, $task, $this->session->userdata('user_id'));

		//print_r($update);

		// add some to the updates list
		$update_info = array(
			'belongs_to' => $this->session->userdata('group_id'),
			'belongs_user' => $this->session->userdata('user_id'),
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $client,
			'user' => $this->session->userdata('name'),
			'title' => 'completed task '
		);

		$this->client_model->addDashUpdate($update_info);

	}

	public function import() {
		$this->load->model('client_model');
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_import');
	}

	public function verify_import() {
		$this->load->model('client_model');
		$this->load->helper('file');
		$config['upload_path'] = './import/';
		$config['allowed_types'] = 'csv';
		$config['file_name'] = $this->session->userdata('user_id') . '_' . time();
		$this->load->library('upload', $config);

 		if (!$this->upload->do_upload('csv_file')) {
			$this->session->set_flashdata('import_error', $this->upload->display_errors('<div class="alert alert-error">','</div>'));
			// redirect
			redirect('clients/import');
		} else {
			$file = $this->upload->data();

			// read the file
			$openfile = fopen($file['full_path'], 'r');

			$headers = $this->input->post('headers');

			$row_count = 1;
			// open the csv file
			while (($thecsv = fgetcsv($openfile, 4096, ",")) !== FALSE) {

				// number of columns
				$numcols = count($thecsv);

				if ( ($headers==1) && ($row_count==2) ) {

					$csv = $thecsv;

				//echo '<pre>';
				//print_r($thecsv);
				//echo '</pre>';

					break;

				} elseif ( (!$headers) && ($row_count==1)) {

					$csv = $thecsv;
					break;

				}

				//$csv = $thecsv;
				//if ($row_count==1) { break; }

				$row_count++;
			}

			// close the file
			fclose($openfile);


		$data['csv_row'] = $csv;
		$data['file'] = $file['file_name'];
		$data['headers'] = $headers;
		$data['columns'] = $numcols;
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_import_verify', $data);
		}


	}

	public function process_import() {
		$this->load->model('client_model');
		$file = $this->input->post('file');
		$column_count = ($this->input->post('columns')-1);
		$headers = $this->input->post('headers');

		if ($file) {

			$filename = './import/' . $file;

			$openfile = fopen($filename, 'r');

			$row_counter = 0;

			while (($thecsv = fgetcsv($openfile, 4096, ",")) !== FALSE) {

			//echo $row_counter.'<br>';

			if (($headers==1) && ($row_counter==0)) {
				$row_counter++;
				continue;
			}

			$row_counter++;

				$import_refs = array(
					'ref' => $this->session->userdata('user_id').rand(10,99).time(),
					'type' => 1,
					'added_date' => date('Y-m-d H:i:s'),
					'belongs_to' => $this->session->userdata('group_id')
				);

				$import_company_refs = array(
					'ref' => $this->session->userdata('user_id').rand(10,99).time(),
					'type' => 2,
					'added_date' => date('Y-m-d H:i:s'),
					'belongs_to' => $this->session->userdata('group_id')
				);

				$import = array();
				$address = array();
				$telephone = array();
				$email = array();
				$company = array();
				$notes = array();

				for ($i = 0; $i <= $column_count; $i++) {

					// do the name details
					if ( ($_POST['column'][$i]=="title") || ($_POST['column'][$i]=="first_name" ) || ($_POST['column'][$i]=="last_name") || ($_POST['column'][$i]=="job_title") || ($_POST['column'][$i]=="dob") && ($_POST['column'][$i]=="marital_status") ) {
						$column_name = $_POST['column'][$i];
						$import[$column_name] = $thecsv[$i];
					}

					// company details
					if ($_POST['column'][$i]=="company_name") {
						if ($thecsv[$i]!="") {
							$company['company_name'] = $thecsv[$i];
						}
					}

					// home address details
					if ( ($_POST['column'][$i]=="address_line_1") || ($_POST['column'][$i]=="town") || ($_POST['column'][$i]=="county") || ($_POST['column'][$i]=="postcode") ) {
						$address_column = $_POST['column'][$i];
						$address[$address_column] = $thecsv[$i];
						if ($_POST['column'][$i]=="address_line_1") {
							$address['type'] = 'Home';
						} elseif ($_POST['column'][$i]=="work_address_line_1") {
							$address['type'] = 'Work';
						}
					}

					// telephone details
					if ( ($_POST['column'][$i]=="telephone") || ($_POST['column'][$i]=="work_telephone") || ($_POST['column'][$i]=="mobile") ) {
						if ($thecsv[$i]!="") {
							$telephone['number'] = $thecsv[$i];

							if ($_POST['column'][$i]=="work_telephone") {
								$telephone['type'] = 'Work';
							} elseif ($_POST['column'][$i]=="mobile") {
								$telephone['type'] = 'Mobile';
							} else {
								$telephone['type'] = 'Home';
							}
						}
					}

					//email addresses
					if ( ($_POST['column'][$i]=="email") || ($_POST['column'][$i]=="work_email") ) {
						if ($thecsv[$i]!="") {
							$email['email'] = $thecsv[$i];
						}
					}

					// customer notes
					if ($_POST['column'][$i]=="notes") {
						if ($thecsv[$i]!="") {
							$notes['note'] = $thecsv[$i];
						}
					}

				} // end loop

				//echo count($company) .' - '. count($import) .'<br>';

				// import the array
				if (count($company)>0) {
					$import_array = array_merge($import_company_refs, $company);
					$this->client_model->insertData($import_array);
					$clientid = $this->db->insert_id();
				} else {
					$import_array = array_merge($import_refs, $import);
					$this->client_model->insertData($import_array);
					$clientid = $this->db->insert_id();
				}

				$personid = array('customer_id' => $clientid);

				// import the address info
				if (count($address)>2) {
					$address_array = array_merge($address, $personid);
					$this->client_model->insertAddressData($address_array, $clientid);
				}

				// import the telephone info
				//echo count($telephone).'<br>';
				if (count($telephone)>0) {
					$telephone_array = array_merge($telephone, $personid);
					$this->client_model->addNewTelephoneToDb($telephone_array, $clientid);
				}

				// import the email addresses
				if (count($email)>0) {
					$email_to = array(
						'customer_id' => $clientid,
						'type' => 'Home'
					);
					$email_array = array_merge($email, $email_to);
					$this->client_model->addNewEmailToDb($email_array, $clientid);
				}

				// import customer notes
				if (count($notes)>0) {

					$note_to = array(
						'customer_id' => $clientid,
						'added_by' => $this->session->userdata('username'),
						'added_date' => date('Y-m-d H:i:s')
					);
					$notes_array = array_merge($notes, $note_to);
					$this->client_model->addCustomerNotes($notes_array);


				//echo '<pre>';
				//print_r($notes);
				//echo '</pre>';


				}

			}

			redirect('clients');

		} else {

			redirect('clients/import');

		}

	}

	public function addresslookup() {
		$this->load->model('addresslookup_model');

		$postcode = $this->input->get('postcode');
		$number = $this->input->get('number');

		// get address data from postcode
		echo $address = $this->addresslookup_model->addressLookup($postcode, $number);
	}

	public function import_employee() {
		$this->load->model('client_model');

		if ($this->input->get('id')) {
			$data['customer_details'] = $this->client_model->getCustomerData($this->input->get('id'));
			$data['customer_notes'] = $this->client_model->getCustomerNotes($this->input->get('id'));
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($this->input->get('id'));
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($this->input->get('id'));
			$data['customer_email'] = $this->client_model->getEmailAddressList($this->input->get('id'));
			$data['associated_people'] = $this->client_model->getAssociatedPeople($this->input->get('id'));
			$data['tab_messages'] = $this->client_model->tabMessages($this->input->get('id'));
			$data['tab_policies'] = $this->client_model->tabPolicies($this->input->get('id'));
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($this->input->get('id'));
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($this->input->get('id'));
			$data['customer_url'] = $this->client_model->getCustomerURLs($this->input->get('id'));
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_employee_import', $data);
		} else {
			redirect('clients');
		}
	}

	public function employee_verify_import() {
		$this->load->model('client_model');
		$this->load->helper('file');
		$config['upload_path'] = './import/';
		$config['allowed_types'] = 'csv';
		$config['file_name'] = $this->session->userdata('user_id') . '_' . time();
		$this->load->library('upload', $config);

 		if (!$this->upload->do_upload('csv_file')) {
			$this->session->set_flashdata('import_error', $this->upload->display_errors('<div class="alert alert-error">','</div>'));
			// redirect
			redirect('clients/import');
		} else {
			$file = $this->upload->data();
			// read the file
			$openfile = fopen($file['full_path'], 'r');
			$headers = $this->input->post('headers');
			$row_count = 1;
			// open the csv file
			while (($thecsv = fgetcsv($openfile, 4096, ",")) !== FALSE) {
				// number of columns
				$numcols = count($thecsv);
				if ( ($headers==1) && ($row_count==2) ) {
					$csv = $thecsv;
					break;
				} elseif ( (!$headers) && ($row_count==1)) {
					$csv = $thecsv;
					break;
				}
				$row_count++;
			}

			// close the file
			fclose($openfile);

		$data['csv_row'] = $csv;
		$data['file'] = $file['file_name'];
		$data['headers'] = $headers;
		$data['columns'] = $numcols;
		$data['client'] = $this->input->post('client');
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_employee_import_verify', $data);
		}
	}

	public function employee_process_import() {
		$this->load->model('client_model');
		$file = $this->input->post('file');
		$column_count = ($this->input->post('columns')-1);
		$headers = $this->input->post('headers');
		$client = $this->input->get('id');

		if ($file) {
			$filename = './import/' . $file;
			$openfile = fopen($filename, 'r');
			$row_counter = 0;
			while (($thecsv = fgetcsv($openfile, 4096, ",")) !== FALSE) {
			if (($headers==1) && ($row_counter==0)) {
				$row_counter++;
				continue;
			}
			$row_counter++;

				// start array
				$import = array(
					'customer_id' => $client
				);

				for ($i = 0; $i <= $column_count; $i++) {

					if ($_POST['column'][$i]!="") {
						$column_name = $_POST['column'][$i];
						$import[$column_name] = $thecsv[$i];
					}

				} // end loop

					if (array_key_exists('house_number', $import)) {
						if (array_key_exists('address', $import)) {
							$import['address'] = $import['house_number'] . ' ' .$import['address'];
						} else {
							$import['address'] = $import['house_number'];
						}

						unset($import['house_number']);
					}

					//echo '<pre>';
					//print_r($import);

				// import array if first name has a value
				if ( ($import['first_name']!="") && ($import['last_name']!="")) {
					$this->client_model->addNewEmployeeToDb($import);
				}

			}
			redirect('clients/people?id='.$client);
		} else {
			redirect('clients/people?id='.$client);
		}
	}

	public function search_company_details() {
		$this->load->model('client_model');
		$name = strtolower($this->input->get('company'));
		if ($name) {
			echo $this->client_model->duedilCompanySearch($name);
		}
	}

	public function get_company_details() {
		$this->load->model('client_model');
		$number = $this->input->get('company_number');
		if ($number) {
			echo $this->client_model->duedilCompanyInfo($number);
		}
	}

	public function view_my_documents() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$customer_details = $this->client_model->getCustomerData($client);
		$data['customer_details'] = $customer_details;
		$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
		$data['customer_notes'] = $this->client_model->getCustomerNotes($client);
		$data['customer_tasks'] = $this->client_model->getCustomerTasks($client);
		$data['associated_people'] = $this->client_model->getAssociatedPeople($client);
		$data['customer_mob'] = $this->client_model->getClientMobNumber($client);
		$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($client);
		$data['customer_email'] = $this->client_model->getEmailAddressList($client);
		$data['tab_messages'] = $this->client_model->tabMessages($client);
		$data['tab_policies'] = $this->client_model->tabPolicies($client);
		$data['tab_opportunities'] = $this->client_model->tabOpportunities($client);
		$data['custom_tabs'] = $this->client_model->getCustomTabsList();
		$data['customer_photo'] = $this->client_model->getCustomerProfileImage($client);
		$data['customer_url'] = $this->client_model->getCustomerURLs($client);
		$data['customer_tags'] = $this->client_model->tagSetForCustomer($client);
		$data['all_tags'] = $this->client_model->GetAllTags();
		$data['email_templates'] = $this->client_model->getEmailTemplates();
		$data['email_signatures'] = $this->client_model->getEmailSignatures();
		$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($client);
		$data['vmd'] = $this->client_model->getVMD();
		$account = $this->client_model->getVMDAccount($client);
		$data['account'] = $account;
		$data['shared'] = $this->client_model->getViewMyDocsSharedList($client);
		$data['uploaded'] = $this->client_model->getViewMyDocsUploadedList($account['vmd']);
		$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('client_view_my_docs', $data);
	}

	public function view_my_documents_activate() {
		$this->load->model('client_model');
		$client = $this->client_model->getCustomerData($this->input->get('id'));
		// get client details so we can create an vmd account
		if ($client) {
			$result = $this->client_model->createVMDAccount($client['customer_id'], $client['title'], $client['first_name'], $client['last_name'], 'test@one23.co.uk', $client['postcode']);

			if ($result) {
				// update client vmd record
				$this->client_model->updateVMDRecord($client['customer_id'], $result['ref'], $result['pin']);
				redirect("clients/view_my_documents?id=". $client['customer_id']);
			} else {
				redirect("clients/view_my_documents?id=". $client['customer_id'] . '&error');
			}
		}
	}

	public function view_my_documents_unlink() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$this->client_model->updateVMDRecordRemove($client);
		redirect("clients/view_my_documents?id=". $client);
	}

	public function view_my_documents_share() {
		$this->load->model('client_model');
		$name = $this->input->post('name');
		$file = $this->input->post('file');
		$client = $this->input->post('client');
		$notes = $this->input->post('notes');
		$filename = $this->input->post('filename');
		if ($filename) {
			// get vmd ref
			$vmd = $this->client_model->getVMDAccount($client);
			// upload file to vmd
			$upload = $this->client_model->uploadVMDDocument($vmd, $filename, $name, $notes);

			if ($upload!="") {
				// save details
				$file_details = array(
					'url' => $upload['url'],
					'ref' => $upload['ref'],
					'time' => date('Y-m-d H:i:s'),
					'user_id' => $client,
					'name' => $name,
					'notes' => $notes
				);
				$this->client_model->createVMDShared($file_details);
				redirect('clients/view_my_documents?id='.$client);
			} else {
				redirect('clients/view_my_documents?id='.$client . '&error');
			}
		} else {
			redirect('clients');
		}
	}

	public function ajax_email_template() {
		$this->load->model('client_model');
		$template = $this->input->get('template');
		$encoded = json_encode($this->client_model->getEmailTemplate($template));
		echo $encoded;
	}

	public function clean_wysiwyg_content() {
		$this->load->helper('htmlpurifier');
		$clean_html = html_purify($this->input->post('___dirty_html'), 'email_content');
		echo json_encode(array('clean_html' => $clean_html));
	}

	public function send_new_email() {
		$this->load->model('client_model');
		// get the posted info
		$to = $this->input->post('to');
		$subject = $this->input->post('subject');
		if ($subject=="") {
			$subject = "No subject";
		}
		$body = $this->input->post('___body'); //we need the tags preserved //$this->input->post('body');
		$signature = $this->input->post('signature');
		$from_name = $this->session->userdata('name');
		$from_email = $this->session->userdata('email');
		$client = $this->input->post('client');
		$client_ref = $this->input->post('client_ref');

		$this->load->library('email');
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'html';

		//$config['send_multipart'] = false;
		//$config['wordwrap'] = false;

		$this->email->initialize($config);
		$this->email->from($from_email, $from_name);
		$this->email->reply_to('dropbox.13554456@123crm.co.uk', $from_name);
		$this->email->to($to);
		$this->email->subject($subject . ' ' . $client_ref);
		// see if we have any attachments
		if (isset($_POST['attach'])) {
			foreach ($_POST['attach'] as $attachment) {
				if ($attachment!="") {
					$this->email->attach($attachment);
				}
			}
		}
		// stick the signature on the end of the body
		if ($signature>0) {
			$the_sig = $this->client_model->getEmailSignature($signature);
			if ($the_sig) {
			$body .= "\n\n------\n" . $the_sig['body'];
			}
		}

		$email_data['content'] = $body;

		//$this->email->message($body);
		$this->email->message($this->load->view('email/simple', $email_data, true));
		// send it!
		$this->email->send();
		// build array to have message

		$new_message = array(
			'subject' => $subject,
			'body' => $body,
			'sender' => $this->session->userdata('name'),
			'direction' => '1',
			'type' => '0',
			'added_date' => date('Y-m-d H:i:s'),
			'customer_id' => $client,
			'to' => $to
		);
		$this->client_model->addNewMessageToDb($new_message, $client);
		$message_id = $this->db->insert_id();
		if (isset($_POST['attach'])) {
			foreach ($_POST['attach'] as $add_attachment) {
				if ($add_attachment!="") {
					$attachment_to_add = array(
						'message_id' => $message_id,
						'file' => $add_attachment
					);
					$this->client_model->addNewMessageToDbAttachments($attachment_to_add);
				}
			}
		}
		redirect('clients/view?id='.$client.'&result=email_sent');
	}

	public function update_file_name() {
		$this->load->model('client_model');
		$name = $this->input->post("value");
		$id = $this->input->post("id");
		$explode_id = explode("_", $id);

		// update the files name
		$data = array(
			'name' => $name
		);

		$this->client_model->updateFilesName($explode_id[1],$explode_id[2],$data);

		echo $name;
	}

	public function custom() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$tab = $this->input->get('custom');
		$tab_details = $this->client_model->getCustomTab($this->session->userdata("user_id"), $tab);

		if ($tab_details) {
			$data['tab'] = $tab_details;

			$data['section1'] = $tab_details['section1_name'];
			$data['section1_type'] = $tab_details['section1'];
			$data['section1_data'] = $this->client_model->getCustomTabData($tab, $client, 1);
			if ($tab_details['section1']==3) {
				$data['section1_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section1_form']);
			}

			$data['section2'] = $tab_details['section2_name'];
			$data['section2_type'] = $tab_details['section2'];
			$data['section2_data'] = $this->client_model->getCustomTabData($tab, $client, 2);
			if ($tab_details['section2']==3) {
				$data['section2_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section2_form']);
			}

			$data['section3'] = $tab_details['section3_name'];
			$data['section3_type'] = $tab_details['section3'];
			$data['section3_data'] = $this->client_model->getCustomTabData($tab, $client, 3);
			if ($tab_details['section3']==3) {
				$data['section3_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section3_form']);
			}

			$data['section4'] = $tab_details['section4_name'];
			$data['section4_type'] = $tab_details['section4'];
			$data['section4_data'] = $this->client_model->getCustomTabData($tab, $client, 4);
			if ($tab_details['section4']==3) {
				$data['section4_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section4_form']);
			}

			$data['section5'] = $tab_details['section5_name'];
			$data['section5_type'] = $tab_details['section5'];
			$data['section5_data'] = $this->client_model->getCustomTabData($tab, $client, 5);
			if ($tab_details['section5']==3) {
				$data['section5_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section5_form']);
			}

			$data['section6'] = $tab_details['section6_name'];
			$data['section6_type'] = $tab_details['section6'];
			$data['section6_data'] = $this->client_model->getCustomTabData($tab, $client, 6);
			if ($tab_details['section6']==3) {
				$data['section6_form'] = $this->client_model->getCustomTabForm($this->session->userdata("user_id"), $tab_details['section6_form']);
			}

		} else {
			redirect("clients/view?id=".$client);
		}

		if ($client) {
			$customer_details = $this->client_model->getCustomerData($client);
			$data['customer_details'] = $customer_details;
			$data['partner_details'] = $this->client_model->getPartnerInfo($customer_details['associated']);
			$data['customer_notes'] = $this->client_model->getCustomerNotes($client);
			$data['customer_photo'] = $this->client_model->getCustomerProfileImage($client);
			$data['customer_url'] = $this->client_model->getCustomerURLs($client);
			$data['customer_tasks'] = $this->client_model->getCustomerTasks($client);
			$data['customer_policies'] = $this->client_model->summaryLivePolicies($client);
			$data['recent_quotes'] = $this->client_model->summaryRecentQuotes($client);
			$data['upcoming_events'] = $this->client_model->summaryEvents($client);
			$data['recent_messages'] = $this->client_model->summaryRecentMessages($client);
			$data['recent_files'] = $this->client_model->getFilesForClient($client, '', 10);
			$data['associated_people'] = $this->client_model->getAssociatedPeople($client);
			$data['customer_telephone'] = $this->client_model->getTelephoneNumberList($client);
			$data['customer_email'] = $this->client_model->getEmailAddressList($client);
			$data['tab_messages'] = $this->client_model->tabMessages($client);
			$data['vmd_account'] = $this->client_model->getVMDAccount($client);
			$data['tab_policies'] = $this->client_model->tabPolicies($client);
			$data['tab_opportunities'] = $this->client_model->tabOpportunities($client);
			$data['customer_tags'] = $this->client_model->tagSetForCustomer($client);
			$data['all_tags'] = $this->client_model->GetAllTags();
			$data['email_templates'] = $this->client_model->getEmailTemplates();
			$data['email_signatures'] = $this->client_model->getEmailSignatures();
			$data['email_attachments'] = $this->client_model->getEmailAttachmentsList($client);
			$data['custom_tabs'] = $this->client_model->getCustomTabsList();
			$this->client_model->addRecentlyViewed($client);
			$recentlyviewed['viewed'] = $this->client_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->client_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('client_custom_tab', $data);
		} else {
			redirect('clients');
		}
	}

	public function custom_add_note() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		$custom = $this->input->post('custom');
		$type = $this->input->post('type');
		$section = $this->input->post('section');

		$add = array(
			'customer_id' => $client,
			'custom' => $custom,
			'type' => $type,
			'section' => $section,
			'entry' => $this->input->post('notes'),
			'added_date' => date('Y-m-d H:i:s')
		);

		$this->client_model->addCustomNotes($add);
		redirect("clients/custom?id=".$client."&custom=". $custom);
	}

	public function custom_save_form() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		$custom = $this->input->post('custom');
		$section = $this->input->post('section');
		$title = $this->input->post('title');
		$entry = "";
		$count = 0;
		foreach($_POST['name'] as $item) {
			$entry .= '<p><strong>' . $_POST['entry'][$count] .':</strong> ' . $item .'</p>';
			$count++;
		}

		$add = array(
			'customer_id' => $client,
			'custom' => $custom,
			'section' => $section,
			'entry' => $title . ' - ' . date('d/m/Y H:i'),
			'entry_value' => $entry,
			'added_date' => date('Y-m-d H:i:s')
		);

		$this->client_model->addCustomNotes($add);
		redirect("clients/custom?id=".$client."&custom=". $custom);
	}

	public function custom_add_file() {
		$this->load->model('client_model');
		$client = $this->input->post('client');
		$custom = $this->input->post('custom');
		$type = $this->input->post('type');
		$section = $this->input->post('section');

		$config['upload_path'] = './documents/';
		$config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
		$config['file_name'] = rand(9,99) . '_' . time();
		$this->load->library('upload', $config);

 		if (!$this->upload->do_upload('file')) {

			// if any errors
			$this->session->set_flashdata('error', '<div class="alert alert-danger">'. $this->upload->display_errors() .'</div>');

		} else {

			$file = $this->upload->data();

			$add = array(
				'customer_id' => $client,
				'custom' => $custom,
				'type' => $type,
				'section' => $section,
				'entry' => $this->input->post('name'),
				'entry_file' => $file['file_name'],
				'added_date' => date('Y-m-d H:i:s')
			);

			$this->client_model->addCustomNotes($add);

		}
		redirect("clients/custom?id=".$client."&custom=". $custom);
	}

	public function custom_remove_entry() {
		$this->load->model('client_model');
		$client = $this->input->get('id');
		$custom = $this->input->get('custom');
		$entry = $this->input->get('entry');
		$this->client_model->removeCustomEntry($entry, $custom);
		redirect("clients/custom?id=".$client."&custom=". $custom);
	}

	public function update_backgound() {
		$this->load->model('client_model');
		$customer_id = $this->input->post('client');
		$data = array(
			'background_info' => $this->input->post('info')
		);
		$this->client_model->updateBackgroundInfo($customer_id, $data);
		redirect('clients/view?id=' . $customer_id .'&result=background_updated');
	}

	public function test_tweets()
	{
		$twitter = $this->input->get('twitter');
		$tweets = '';

		$url = "https://twitter.com/i/profiles/show/". $twitter ."/timeline?count=3";
		// get tweets
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_REFERER, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($curl);
		curl_close($curl);
		// decode stuff
		$decoded = json_decode($result, true);
		$decoded = $decoded['items_html'];

		echo $decoded;
	}

	public function get_tweets() {
		$twitter = $this->input->get('twitter');
		$tweets = '';

		$url = "https://twitter.com/i/profiles/show/". $twitter ."/timeline?count=3";
		// get tweets
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_REFERER, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($curl);
		curl_close($curl);
		// decode stuff
		$decoded = json_decode($result, true);
		$decoded = $decoded['items_html'];

		$this->load->helper('simple_html_dom');

		$html = str_get_html($decoded);

		$tweets .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">';
		foreach($html->find('.StreamItem') as $element) {
			//$tweets .= '<li>'.$element->innertext.'</li>';

			$image = $element->find('img')[0]->src;
			$tweet = $element->find('.ProfileTweet-text')[0]->innertext;
			$date  = $element->find('.ProfileTweet-timestamp')[0]->innertext;

			$tweets .= '<tr><td align="center" width="7%"><a href="#" target="_blank"><img src="'. $image .'"></td></a>
						<td><a href="#" target="_blank">'. strip_tags($tweet) .'</a></td>
						<td align="center" width="7%">'. $date .'</td></tr>';
		}
		$tweets .= '</table>';

		/*
		$get_tweets = '#<p class="js-tweet-text tweet-text">(.*)</p>#';
		preg_match_all($get_tweets,$decoded,$tweet_list);
		$get_avatar = '#<img class="avatar js-action-profile-avatar" src="(.*)" alt="">#';
		preg_match_all($get_avatar,$decoded,$avatar_list);
		$get_date = '#data-long-form="true">(.*)</span></a>#';
		preg_match_all($get_date,$decoded,$date_list);
		$get_url = '#<a class="details with-icn js-details" href="(.*)">#';
		preg_match_all($get_url,$decoded,$url_list);

		if ($tweet_list[0]) {
			$tweets .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">';
			$tweet_count = 0;
			foreach($tweet_list[0] as $tweet) {
				// just leave the image in string
				$image = $avatar_list[0][$tweet_count];
				$image = str_replace('<img class="avatar js-action-profile-avatar" src="', '', $image);
				$image = str_replace('" alt="">', '', $image);
				//sort the date out
				$date = $date_list[0][$tweet_count];
				$date = str_replace('data-long-form="true">', '', $date);
				$date = str_replace('</span></a>', '', $date);
				//sort the url out
				$url = $url_list[0][$tweet_count];
				$url = str_replace('<a class="details with-icn js-details" href="', '', $url);
				$url = str_replace('">', '', $url);
				// tweets
				$tweets .= '<tr><td align="center" width="7%"><a href="https://www.twitter.com/'. $url .'" target="_blank"><img src="'. $image .'"></td></a>
				<td><a href="https://www.twitter.com/'. $url .'" target="_blank">'. strip_tags($tweet) .'</a></td>
				<td align="center" width="7%">'. $date .'</td></tr>';
				$tweet_count++;
			}
			$tweets .= '<table>';
		}
		*/

		echo $tweets;
	}

	public function custom_form_pdf() {
		$this->load->model('client_model');
		$this->load->library('pdf');
		$heading = $this->input->post('customform_name');
		$text = $this->input->post('customform');
		$data['logo'] = $this->client_model->getCompanyLogo();
		$data['page_title'] = $heading;
		$data['page_text'] = $text;
		$style = file_get_contents(base_url().'css/pdf_quote.css');
		$html = $this->load->view('custom_pdf', $data, true);
		$pdf = $this->pdf->load();
		$pdf->shrink_tables_to_fit=0;
		$footer = array (
			'L' => array (
			  'content' => 'Printed: ' . date('d/m/Y'),
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'C' => array (
			  'content' => '',
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'R' => array (
			  'content' => 'Page {PAGENO}',
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'line' => 1,
		);
		$pdf->SetFooter($footer, 'O');
		$pdf->WriteHTML($style,1);
		$pdf->WriteHTML($html);
		$pdf->Output();
	}

	public function custom_form_pdf_save() {
		$this->load->model('client_model');
		$this->load->library('pdf');
		$heading = $this->input->post('customform_name');
		$text = $this->input->post('customform');
		$client = $this->input->post('customform_client');
		$data['logo'] = $this->client_model->getCompanyLogo();
		$data['page_title'] = $heading;
		$data['page_text'] = $text;
		$new_file = $this->session->userdata('user_id') . '_' . time() . '_' . rand(1,9) . '.pdf';
		$style = file_get_contents(base_url().'css/pdf_quote.css');
		$html = $this->load->view('custom_pdf', $data, true);
		$pdf = $this->pdf->load();
		$pdf->shrink_tables_to_fit=0;
		$footer = array (
			'L' => array (
			  'content' => 'Printed: ' . date('d/m/Y'),
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'C' => array (
			  'content' => '',
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'R' => array (
			  'content' => 'Page {PAGENO}',
			  'font-size' => 8,
			  'font-style' => '',
			  'font-family' => 'times',
			  'color'=>'#000000'
			),
			'line' => 1,
		);
		$pdf->SetFooter($footer, 'O');
		$pdf->WriteHTML($style,1);
		$pdf->WriteHTML($html);
		$save = $pdf->Output('./documents/' . $new_file);
			$file_data = array(
					'customer_id' => $client,
					'added_date' => date('Y-m-d H:i:s'),
					'filename' => $new_file,
					'name' => $heading,
					'type' => '1'
			);
			//print_r($file_data);
			// add file data to database
			$this->client_model->addNewFileToDb($file_data, $client);
			redirect('clients/files?id='.$client);
	}

	public function get_typeahead_client(){
		$this->load->model('client_model');
		echo $this->client_model->clientTypeAheadFormatJson();
	}

} // file end
