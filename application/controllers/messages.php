<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			//redirect('locked');
		}

	}

	public function index() {
		$this->load->model('messages_model');
		$data['messages'] = $this->messages_model->listAllMessages();
		$recentlyviewed['viewed'] = $this->messages_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->messages_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');
			$this->load->view('mobile_messages', $data);
			$this->load->view('common/mobile_footer');
		} else {
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('messages', $data);
		}
	}

	public function view() {
		$this->load->model('messages_model');
		$data['message_detail'] = $this->messages_model->getMessageDetails($this->input->get('id'));
		$data['message_attachments'] = $this->messages_model->getMessageAttachments($this->input->get('id'));
		// set message as read
		$this->messages_model->messageRead($this->input->get('id'));
		$recentlyviewed['viewed'] = $this->messages_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->messages_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('common/mobile_header');
			$this->load->view('mobile_messages_view', $data);
			$this->load->view('common/mobile_footer');
		} else {
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('messages_view', $data);
		}
	}

	public function delete() {
		$this->load->model('messages_model');
		$client = $this->input->get('id');
		$ref = $this->input->get('message');
		$this->messages_model->deleteMessage($client, $ref);
		redirect('clients/messages?id=' . $client);
	}

	public function messageUnread() {
		$this->load->model('messages_model');
		// get the count of unread messages
		$count = $this->messages_model->getUnreadMessagesCount();
		echo $count;
	}


}
