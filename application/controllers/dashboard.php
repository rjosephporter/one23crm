<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $sns_list = array('facebook', 'twitter', 'linkedin', 'google');

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		//if ($this->session->userdata('account_expiry_days')<1) {
			//redirect('locked');
		//}

	}

	public function index()	{
		$this->load->model('dashboard_model');
		$this->load->model('pipeline_model');
		$this->load->model('user_model');

		if ($this->session->userdata('account_expiry_days')<1) {
			redirect('locked');
		}

		$data['tasks_overdue'] = $this->dashboard_model->getTaskListOverdue();

		$now = date('Y-m-d H:i:s');
		$end_today = date('Y-m-d') . ' 23:59:59';
		$data['tasks_today'] = $this->dashboard_model->getTaskList($now, $end_today);

		$tomorrow = date("Y-m-d H:i:s ", strtotime("Tomorrow"));
		$next7days = date("Y-m-d H:i:s ", strtotime("+ 8 days"));
		$data['tasks_week'] = $this->dashboard_model->getTaskList($tomorrow, $next7days);

		$data['tasks_other'] = $this->dashboard_model->getTaskList($next7days, date("Y-m-d H:i:s ", strtotime("+ 3 months")));

		$this_month = date("Y-m-01") . " 00:00:00";
		$last_this_month = date("Y-m-t") . " 23:59:59";

		// get sales data
		$sales = $this->pipeline_model->pipelineSales($this_month, $last_this_month);
		$data['sales'] = $sales['total'];

		// get conversion data
		$conversion = $this->pipeline_model->pipelineConversion($this_month, $now);
		if ($conversion['won']!=0) {
			$conversion = ($conversion['won']/$conversion['total']*100);
		} else {
			$conversion = 0;
		}
		$data['dash_conversion'] = $conversion;

		// get pipeline total
		$data['pipeline_totals'] = $this->pipeline_model->pipelineStats();

		$data['update_list_group'] = $this->dashboard_model->getGroupUsersList();
		$update = $this->input->get("updates");
		$role = $this->session->userdata("role");
		if ($update) {
			if (($update=="all") && ($role==1)) {
				$data['update_list'] = $this->dashboard_model->getUpdatesListAll();
			} elseif (($update!="all") && ($role==1)) {
				$data['update_list'] = $this->dashboard_model->getUpdatesListGroup($update);
			}
		} else {
			$data['update_list'] = $this->dashboard_model->getUpdatesList();
		}

		// get client birthdays
		$this_week_monday = date("Y-m-d", strtotime('this week last monday'));
		$this_week_sunday = date("Y-m-d", strtotime('this week next sunday'));
		$this_month_first = date("Y-m-01");
		$this_month_last = date("Y-m-t");

		$data['birthdays_week'] = $this->dashboard_model->getClientBirthDays($this_week_monday, $this_week_sunday);
		$data['birthdays_month'] = $this->dashboard_model->getClientBirthDays($this_month_first, $this_month_last);

		$recentlyviewed['viewed'] = $this->dashboard_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->dashboard_model->getRecentlyViewed('6');

		/* Added data for connected social networks 08.11.2014 @rjosephporter */
		$sns_accounts_raw = $this->user_model->getUserSnsAccounts($this->session->userdata('user_id'));
		$sns_accounts = array();

		$data['other_sns'] = array();
		foreach($sns_accounts_raw as $sa)
			$sns_accounts[] = $sa['sns_type'];

		foreach($this->sns_list as $sl) {
			if(!in_array($sl, $sns_accounts))
				$data['other_sns'][] = $sl;
		}

		$data['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);

		//if ($this->session->userdata('mobile_site')==1) {
			//$this->load->view('common/mobile_header');
			//$this->load->view('mobile_dashboard', $data);
			//$this->load->view('common/mobile_footer');
		//} else {
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('dashboard', $data);
		//}
	}

	public function taskNotification() {
		$this->load->model('dashboard_model');
		$tasks_due = $this->dashboard_model->getTaskNotificationList();
		$task_hidden = $this->session->userdata('task_notification');
		$time = date("Y-m-d H:i:s");

		if ($task_hidden<$time) {

			if ($tasks_due) {

				echo '<table width="100%">';

				foreach($tasks_due as $task) {

					echo '<tr>
					<td width="60%"><strong>' . $task['name'] . '</strong>
					<br />' .date("D jS F Y G:iA", strtotime($task['date'])) .'
					</td>
					<td align="center">
					<div class="dropdown btn-group">
						<a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">
							Task Actions
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" style="text-align:left;">
							<li><a href="#" class="taskNotificationRemind" data-task="'. $task['id'] .'" data-time="1hour">Remind in 1 hour</a></li>
							<li><a href="#" class="taskNotificationRemind" data-task="'. $task['id'] .'" data-time="24hour">Remind in 24 hours</a></li>
							<li><a href="#" class="taskNotificationRemind" data-task="'. $task['id'] .'" data-time="1week">Remind in 1 week</a></li>
							<li><a href="#" class="taskNotificationRemind" data-task="'. $task['id'] .'" data-time="1month">Remind in 1 month</a></li>
							<li><a href="#" class="taskNotificationComplete" data-task="'. $task['id'] .'">Mark as complete</a></li>
						</ul>
					</div>
					</td></tr>';

				}

				echo '</table>';

			} else {
				return false;
			}

		} else {

			return false;

		}


	}

	public function taskNotificationHide() {
		$time_id = $this->input->get('time');
		$time = date("Y-m-d H:i:s");
		// 1=30 mins, 2=60 mins, 3=5hours
		if ($time_id==1) {
			$this->session->set_userdata('task_notification',date("Y-m-d H:i:s", strtotime("+30 minutes")));
		} elseif ($time_id==2) {
			$this->session->set_userdata('task_notification',date("Y-m-d H:i:s", strtotime("+1 hour")));
		} elseif ($time_id==3) {
			$this->session->set_userdata('task_notification',date("Y-m-d H:i:s", strtotime("+5 hours")));
		}
	}

	public function taskNotificationRemind() {
		$this->load->model('dashboard_model');
		$time_id = $this->input->get('time');
		$task_id = $this->input->get('task');
		if ($time_id=="1hour") {
			// update the task with the new time
			$this->dashboard_model->updateTaskTime($task_id, date("Y-m-d H:i:s", strtotime("+1 hour")));
		} elseif ($time_id=="24hour") {
			// update the task with the new time
			$this->dashboard_model->updateTaskTime($task_id, date("Y-m-d H:i:s", strtotime("+24 hours")));
		} elseif ($time_id=="1week") {
			// update the task with the new time
			$this->dashboard_model->updateTaskTime($task_id, date("Y-m-d H:i:s", strtotime("+1 week")));
		} elseif ($time_id=="1month") {
			// update the task with the new time
			$this->dashboard_model->updateTaskTime($task_id, date("Y-m-d H:i:s", strtotime("+1 month")));
		} else {
			return false;
		}

	}

	public function checkEmptyFields()
	{

		$this->session->set_userdata('checked_empty_fields', 1);

		$incomplete_fields = array();
		$required_fields = array(
			'company', 'first_name', 'last_name',
			'address_line', 'address_town', 'address_county', 'address_postcode',
			'email', 'telephone', 'sms'
		);

		$this->load->model('user_model');
		$user_data = $this->user_model->getClientDetails($this->session->userdata('user_id'));

		foreach ($required_fields as $field) {
			if($user_data) {
				if(empty($user_data[$field]))
					$incomplete_fields[] = $field;
			}
		}

		$this->output->set_content_type('application/json')	;
		$this->output->set_output(json_encode(array('incomplete_fields' => $incomplete_fields)));
	}


}
