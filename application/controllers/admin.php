<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		$is_admin = $this->session->userdata('is_admin');
		if ( (!isset($is_logged_in)) || ($is_logged_in!=true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}
		if ($is_admin!=1) {
			redirect("dashboard");
		}
	}

	public function index() {
		$this->load->model('admin_model');
		$data['client_list'] = $this->admin_model->clientList();
		$recentlyviewed['viewed'] = $this->admin_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->admin_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('admin', $data);
	}

	public function user() {
		$this->load->model('admin_model');
		$id = $this->input->get('id');
		$data['user_details'] = $this->admin_model->getClientDetails($id);
		$data['client_list'] = $this->admin_model->clientsClientList($id);
		$recentlyviewed['viewed'] = $this->admin_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->admin_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('admin_user', $data);
	}



} ?>
