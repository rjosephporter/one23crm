<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function test() {
		$this->load->library('facebook');
		echo $this->facebook->get_login_url();
	}

	public function index() {
	
		$this->load->model('user_model');
		$data['loginRedirect'] = $this->session->flashdata('loginRedirect');
		if ($this->session->userdata('mobile_site')==1) {
			$this->load->view('mobile_login', $data);
		} else {
			$this->load->view('login_form', $data);
		}
	}
	
	public function auth()
	{
		$this->load->model('user_model');
		$query = $this->user_model->doauth();
		
		// check if mobile
		$mobile = $this->input->post('mobile');
		
		if ($query) {
			
			// get the group details
			$group = $this->user_model->getGroupDetails($query['id']);
			
			// get the permissions
			$permissions = $this->user_model->getGroupPermissions($query['id']);
			
			// get tab settings
			$tabs = $this->user_model->getTabSettings($query['id']);
			
			// get sms account username
			$sms = $this->user_model->getFullGroupDetails($group['group_id']);
			
			if ($tabs) {
				$set_tabs = array(
					'tab_settings' => 1,
					'files_tab' => $tabs['files_tab'],
					'messages_tab' => $tabs['messages_tab'],
					'people_tab' => $tabs['people_tab'],
					'opportunities_tab' => $tabs['opportunities_tab'],
					'live_tab' => $tabs['live_tab']				
				);
				
				$this->session->set_userdata($set_tabs);
			}
			
			if ($permissions) {
				$set_permissions = array(
					'client_edit' => $permissions['client_edit'],
					'client_opportunities' => $permissions['client_opportunities'],
					'client_files' => $permissions['client_files'],
					'client_notes' => $permissions['client_notes'],
					'client_delete' => $permissions['client_delete']
				);
				$this->session->set_userdata($set_permissions);
			}
			
			// check the group sub status
			$sub = $this->user_model->checkGroupSubscription($group['group_id']);	

			// check if the sub has not ran out
			$today = date('Y-m-d G:i:s');
			$today_stamp = time();		
			$today_string = strtotime($today);
			$expiry_stamp = strtotime($sub['expiry']);
			
			if (!$sub) {
				redirect('login?subscription');
			}
			
			// work out how days until the account expiry
			$sub_time = ($expiry_stamp-$today_stamp);
			
			$expiry_days = ($sub_time/(60*60*24))%365;
			
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true,
				'name' => $query['first_name'] . ' ' . $query['last_name'],
				'user_id' => $query['id'],
				'group_id' => $group['group_id'],
				'role' => $group['role'],
				'sms_display' => $query['sms'],
				'company_name' => $query['company'],
				'telephone' => $query['telephone'],
				'sms_username' => $sms['sms_username'],
				'account_expiry' => $sub['expiry'],
				'email' => $query['email'],
				//'account_expiry_days' => $expiry_days,
				'account_expiry_days' => 100,
				'theme_site' => $query['theme_site'],
				'theme_icons' => $query['theme_icons'],
				'trial' => $query['trial'],
				'is_admin' => $query['admin_account'],
				'is_manager' => $query['manager_account']
			);
			
			//print_r($data);
			
			$this->session->set_userdata($data);
			
			//echo '<pre>';
			//print_r($this->session->all_userdata());
			
			// record the login
			$record_login = array(
				'username' => $this->input->post('username'),
				'time' => date('Y-m-d H:i:s'),
				'ip' => $this->input->ip_address()
			);
			
			$this->user_model->recordLogin($record_login);
			
			$redirect_after_login = $this->input->post('login_redirect');
			
			if ($redirect_after_login) {
				redirect($redirect_after_login);
			} else {
				// go to the dashboard
				if ($mobile==1) {
					redirect('m/dashboard');
				} else {
					redirect('dashboard');
				}
			}
							
		} else {		
			//$this->index();	
			redirect('login?failed');
		}	
		
	}	
	
	public function logout() {
		$this->session->sess_destroy();
		$this->index();
	}

	public function password_request() {
		$this->load->view('login_forgot_password');
	}
	
	public function password_reset() {
		$this->load->model('user_model');
		$email = $this->input->post('email');
		if (!empty($email)) {
			$checkemail = $this->user_model->checkEmail($email);			
				if ($checkemail) {
					$newpassword = rand(10000000,99999999);
					// update the user record with the new password
					$this->user_model->updatePassword($checkemail['id'], $newpassword);
					// send the email with the new password
					$emailbody = '<html><head></head><body>
					Dear Customer,<br /><br />Your new password is <strong>'.$newpassword.'</strong>
					<br /><br />Once you have logged in we recommend you change the password, this can be done in the settings area.
					<br /><br />Thanks,<br />
					The 123CRM Team
					</body></html>';					
					$this->user_model->sendNewPassword($checkemail['email'], $emailbody);
					redirect('login?reset');
				} else {
					redirect('login/password_request?unknown');		
				}						
		} else {
			redirect('login/password_request?unknown');
		}
	}
	
	public function register() {
		$this->load->view('login_register');	
	}	

	public function complete_registration() {
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('telephone', 'Telephone Number', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[users.email]');		
		$this->form_validation->set_rules('company', 'Company', 'trim|required');
		$this->form_validation->set_rules('address', 'Address Line 1', 'trim|required');
		$this->form_validation->set_rules('town', 'Town', 'trim|required');
		$this->form_validation->set_rules('county', 'County', 'trim');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required');
		$this->form_validation->set_rules('sms', 'SMS Display Name', 'trim|required|min_length[6]|max_lenght[11]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');		
		if ($this->form_validation->run()==false) {
			$errors = array(
				'errors' => '<div class="alert alert-danger">' . validation_errors() . '</div>',
				'title' => $this->input->post('title'),
				'firstname' => $this->input->post('firstname'),
				'surname' => $this->input->post('surname'),
				'email' => $this->input->post('email'),
				'telephone' => $this->input->post('telephone'),
				'company' => $this->input->post('company'),				
				'address' => $this->input->post('address'),
				'town' => $this->input->post('town'),
				'county' => $this->input->post('county'),
				'postcode' => $this->input->post('postcode'),
				'username' => $this->input->post('username'),
				'sms' => $this->input->post('sms')
			);
			$this->session->set_flashdata($errors);		
			redirect('login/register');
		} else {
			// make a confirmation code
			$code = md5(rand(100,999).time());
			// put into database
			$new_user = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'first_name' => $this->input->post('firstname'),
				'last_name' => $this->input->post('surname'),
				'email' => $this->input->post('email'),
				'company' => $this->input->post('company'),
				'telephone' => $this->input->post('telephone'),
				'address_line' => $this->input->post('address'),
				'address_town' => $this->input->post('town'),
				'address_county' => $this->input->post('county'),
				'address_postcode' => $this->input->post('postcode'),
				'sms' => $this->input->post('sms'),
				'confirm_code' => $code,
				'active' => '2',
				'created_date' => date('Y-m-d H:i:s')
			);
			// add user
			$new_user_id = $this->user_model->createNewUser($new_user);
			// add group and add user to it
			$new_group_id = $this->user_model->addNewGroup($new_user_id);
			$this->user_model->addUser2Group($new_user_id, $new_group_id);
			// set the sub date
			$this->user_model->createSubscription($new_user_id, '2030-01-01');
			// send an email so they can confirm it
			$emailbody = '<html><head></head><body>';			
			$emailbody .= 'Dear ' . $this->input->post('firstname') . '<br /><br />Thank you for registering with us.<br /><br />Before you can start using your account, you must verify your email address. To do this, simply follow the link displayed below and your account will be activated.';			
			$emailbody .= '<br /><br /><a href="' . base_url() . 'login/confirm?code=' . $code . '">'. base_url() .'login/confirm?code=' . $code . '</a>';
			$emailbody .= "<br /><br />Thanks";
			$emailbody .= '</body></html>';
			// send it
			$this->user_model->sendNormalEmail($this->input->post('email'), 'Please confirm your account', $emailbody);
			$this->session->set_flashdata('showmessage', '<div class="alert alert-success">Account created, please check your inbox.</div>');
			redirect('login');
		}
	}
	
	public function confirm() {
		$this->load->model('user_model');
		$code = $this->input->get('code');	
		if (!empty($code)) {
			// check the code
			$checkcode = $this->user_model->checkCode($code);
			if ($checkcode) {
				// get client details
				$client = $this->user_model->getClientDetails($checkcode['id']);
				// activate the user
				$this->user_model->activateUser($checkcode['id']);
				$this->session->set_flashdata('showmessage', '<div class="alert alert-success">Account activated, please login below.</div>');
				redirect('login');
			} else {
				echo 'Invalid code or account already activated.';
			}		
		} else {
			echo 'Invalid code found, please try again.';
		}
	}

	/**
	 * Account creation via social network site (sns)
	 *
	 * @author 	rjosephporter
	 * @date 	08.07.2014
	 */
	public function create_sns_account()
	{
		$result = array();

		$auth_detail = $this->input->post('auth_detail');
		$sns_detail = $this->input->post('sns_detail');
		$sns_only = $this->input->post('sns_only');

		$first_name = (!empty($sns_detail['first_name'])) ? $sns_detail['first_name'] :  '';
		$last_name = (!empty($sns_detail['last_name'])) ? $sns_detail['last_name'] :  '';
		$email = (!empty($sns_detail['email'])) ? $sns_detail['email'] : '';

		if($auth_detail && $sns_detail) {

			$this->load->model('user_model');

			$network = $auth_detail['network'];
			$sns_id = $sns_detail['id'];

			// make a confirmation code
			$code = md5(rand(100,999).time());
			// put into database
			$new_user = array(
				'username' => preg_replace('/\s+/', '', $first_name.$last_name.time()),
				'password' => md5($code),
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'company' => '',
				'telephone' => '',
				'address_line' => '',
				'address_town' => '',
				'address_county' => '',
				'address_postcode' => '',
				'sms' => '',
				'confirm_code' => $code,
				'active' => '1',
				'created_date' => date('Y-m-d H:i:s')
				//'sns_type' => $network,
				//'sns_id' => $sns_id
			);

			$this->db->trans_start();
			
			$new_user_id = $this->session->userdata('user_id');
			if(!$sns_only) {
				// add user
				$new_user_id = $this->user_model->createNewUser($new_user);
				// add group and add user to it
				$new_group_id = $this->user_model->addNewGroup($new_user_id);
				$this->user_model->addUser2Group($new_user_id, $new_group_id);
				
				// set the sub date
				$this->user_model->createSubscription($new_user_id, '2030-01-01');				
			}

			// add sns account 08.11.2014 @rjosephporter
			$user_sns_id = $this->user_model->createSnsAccount($new_user_id, $network, $sns_id);
			$user_data = $this->user_model->getClientDetails($new_user_id);

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE) {
				$result['status'] = 'error';
				$result['msg'] = 'Database error. Rolled back query.';
			} else {
				$result['status'] = 'success';
				$result['user_id'] = $new_user_id;
				$result['user_data'] = $user_data;
			}
		} else {
			$result['status'] = 'error';
			$result['msg'] = 'Invalid parameters';
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
	}

	/**
	 * Authenticate user via social network site (sns)
	 *
	 * @author 	rjosephporter
	 * @date 	08.07.2014
	 */
	public function auth_sns()
	{
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
	}

	/**
	 * Check if sns user exists
	 *
	 * @author 	rjosephporter
	 * @date 	08.08.2014
	 */
	public function sns_exist()
	{
		$result = array();
		$sns_type = $this->input->get('sns_type');
		$sns_id   = $this->input->get('sns_id');

		$this->load->model('user_model');
		$data =  $this->user_model->isExistSnsAccount($sns_type, $sns_id);


		if($data['count']) {
			$result['sns_exists'] = 1;
			$result['user_data'] = $data['user_data'];
		} else {
			$result['sns_exists'] = 0;
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
	}


}
