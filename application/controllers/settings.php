<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login', 'refresh');
		}

		// check if the subscription expiry
		if ($this->session->userdata('account_expiry_days')<1) {
			//redirect('locked');
		}

	}

	public function index() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings');

	}

	public function account() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$data['account_details'] = $this->settings_model->getAccountDetails($this->session->userdata('user_id'));
		//$data['recurring_payments'] = $this->settings_model->getRecurringPayments($this->session->userdata('user_id'));
		$data['payment_exempt'] = $this->settings_model->isAccountPaymentExempt($this->session->userdata('user_id'));
		$data['sms_credits'] = $this->settings_model->getSMSCreditCount($this->session->userdata('user_id'));
		$data['logo'] = $this->settings_model->getCompanyLogo();
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_account', $data);
	}

	/* 08.15.2014 @rjosephporter */
	private function _edit_username() {
		$this->load->model('user_model');
		$user_id = $this->session->userdata('user_id');
		$sns_accounts = $this->user_model->getUserSnsAccounts($user_id);
		$user_details = $this->user_model->getClientDetails($user_id);

		return (count($sns_accounts) > 0 && $user_details['username_update'] == 0) ? true : false;
	}

	public function edit_account() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$data['account_details'] = $this->settings_model->getAccountDetails($this->session->userdata('user_id'));

		$data['edit_username'] = $this->_edit_username();
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_account_edit', $data);
	}

	public function update_account() {
		$this->load->model('settings_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');

		$this->form_validation->set_rules('company', 'Company', 'required|trim');
		$this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
		$this->form_validation->set_rules('last_name', 'Surname', 'required|trim');
		$this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
		$this->form_validation->set_rules('telephone', 'Telephone Number', 'required|trim');
		$this->form_validation->set_rules('address', 'Address Line', 'required|trim');
		$this->form_validation->set_rules('town', 'Town', 'required|trim');
		$this->form_validation->set_rules('county', 'County', 'required|trim');
		$this->form_validation->set_rules('postcode', 'Postcode', 'required|trim');
		$this->form_validation->set_rules('sms', 'SMS Display Name', 'trim|required|min_length[2]|max_lenght[11]');

		/* 08.15.2014 @rjosephporter */
		$edit_username = 0;
		if($this->_edit_username()) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[2]|max_lenght[30]|is_unique[users.username]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_lenght[50]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|matches[password]|min_length[3]|max_lenght[50]');
			$edit_username = 1;
		}

		if ($this->form_validation->run()==false) {

			//flashdata error
			$errors = array(
				'errors' => validation_errors(),
				'company' => $this->input->post('company'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'telephone' => $this->input->post('telephone'),
				'address' => $this->input->post('address'),
				'town' => $this->input->post('town'),
				'county' => $this->input->post('county'),
				'postcode' => $this->input->post('postcode'),
				'sms' => $this->input->post('sms'),

				/* 08.15.2014 @rjosephporter */
				'username' => $this->input->post('username')
			);

			// session
			$this->session->set_flashdata($errors);

			// redirect
			redirect('settings/edit_account');

		} else {

			// update user
			$update = array(
				'company' => $this->input->post('company'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'telephone' => $this->input->post('telephone'),
				'address_line' => $this->input->post('address'),
				'address_town' => $this->input->post('town'),
				'address_county' => $this->input->post('county'),
				'address_postcode' => $this->input->post('postcode'),
				'sms' => $this->input->post('sms'),

				/* 08.15.2014 @rjosephporter */
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'username_update' => $edit_username
			);
			$this->settings_model->updateUserRecord($update);

			// set ok message
			$this->session->set_flashdata('errors', '<div class="alert alert-success">Account details have been successfully updated.</div>');

			// redirect
			redirect('settings/account');

		}

	}

	public function edit_password() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_account_password_edit');

	}

	public function update_password() {
		$this->load->model('settings_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');

		$this->form_validation->set_rules('password', 'Current Password', 'required|trim');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|trim|matches[conf_new_password]');
		$this->form_validation->set_rules('conf_new_password', 'Confirm New Password', 'required|trim');

		if ($this->form_validation->run()==false) {

			$this->session->set_flashdata('errors', validation_errors());
			redirect('settings/edit_password');

		} else {

			// first check that the current password matches our records
			$pass_check = $this->settings_model->checkPassword($this->input->post('password'));

			if ($pass_check) {

				$this->settings_model->updatePassword(array('password' => md5($this->input->post('new_password'))));

				// set ok message
				$this->session->set_flashdata('errors', '<div class="alert alert-success">Account password has been successfully updated.</div>');
				// redirect
				redirect('settings/account');

			} else {

				$this->session->set_flashdata('errors', '<div class="alert alert-error">Current password does not match our records.</div>');
				redirect('settings/edit_password');

			}
		}
	}

	public function newRecurringPayment() {
		$this->load->library('paypal');
		$this->load->model('settings_model');

		//$paypal = new Paypal();

		// params to send over to paypal
		$params = array(
			'RETURNURL' => base_url() . 'settings/createRecurringPayment',
			'CANCELURL' => base_url() . 'settings/account',
			'PAYMENTREQUEST_0_AMT' => '24.99',
			'PAYMENTREQUEST_0_ITEMAMT' => '24.99',
			'L_PAYMENTREQUEST_0_NAME0' => 'One23 Subscription',
			'L_PAYMENTREQUEST_0_AMT0' => '24.99',
  			'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP',
			'PAYMENTREQUEST_0_DESC' => 'One23 Subscription',
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
			'NOSHIPPING' => '1',
			'L_BILLINGTYPE0' => 'RecurringPayments',
			'L_BILLINGAGREEMENTDESCRIPTION0' => 'Monthly subscription to crm system.'
		);

		// get the response from paypal
		$response = $this->paypal->request('SetExpressCheckout', $params);

		//print_r($response);
		if(is_array($response) && $response['ACK'] == 'Success') { //Request successful
			$token = $response['TOKEN'];
			header('Location: https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
		} else {
			redirect('settings/account?paymenterror');
		}

	}

	public function createRecurringPayment() {
		$this->load->library('paypal');
		$this->load->model('settings_model');

		// get details from paypal
		$getParams = array(
			'TOKEN' => $this->input->get('token')
		);
		$getResponse = $this->paypal->request('GetExpressCheckoutDetails', $getParams);

		//echo '<pre>';
		//print_r($getResponse);
		//echo '</pre>';

		// if there was a problem just die
		if(is_array($getResponse) && $getResponse['ACK'] != 'Success') {
			redirect('settings/account?paymenterror');
			die('Error processing payment.');
		}

		$next_month_date = date('Y-m-d', strtotime('+ 1 month'));

		// lets create the recurring payment profile
		$recurringParams = array(
			'TOKEN' => $getResponse['TOKEN'],
			'PAYERID' => $getResponse['PAYERID'],
			'BILLINGPERIOD' => 'Month',
			'BILLINGFREQUENCY' => 1,
			'PROFILESTARTDATE' => $next_month_date . 'T00:00:00',
			'AMT' => '24.99',
			'INITAMT' => '24.99',
			'FAILEDINITAMTACTION' => 'CancelOnFailure',
			'CURRENCYCODE' => 'GBP',
			'DESC' => 'Monthly subscription to crm system.'
		);
		$recurringResponse = $this->paypal->request('CreateRecurringPaymentsProfile', $recurringParams);

		echo '<pre>';
		print_r($recurringResponse);
		echo '</pre>';

		if(is_array($recurringResponse) && $recurringResponse['ACK'] != 'Success') {
			redirect('settings/account?paymenterror');
			die('Error processing payment.');
		} else {

			// add to transaction logs
			$recurring_log = array(
				'profile_id' => $recurringResponse['PROFILEID'],
				'user_id' => $this->session->userdata('user_id'),
				'status' => $recurringResponse['PROFILESTATUS']
			);
			$this->settings_model->addToRecurringPaymentsLog($recurring_log);
			redirect('settings/account?paymentsuccess');
		}

	}

	public function purchaseSmsCredits() {
		$this->load->library('paypal');
		$this->load->model('settings_model');
		$sms = $this->input->get("sms");

		if ($sms==20) {
			$sms_price = 3;
			$sms_name = "20 SMS Credits";
		} elseif ($sms==50) {
			$sms_price = 6;
			$sms_name = "50 SMS Credits";
		} elseif ($sms==100) {
			$sms_price = 10;
			$sms_name = "100 SMS Credits";
		} elseif ($sms==200) {
			$sms_price = 16;
			$sms_name = "200 SMS Credits";
		} else {
			header('Location: ' . base_url() . 'settings/account');
		}

		// params to send over to paypal
		$params = array(
			'RETURNURL' => base_url() . 'settings/createSmsCredits',
			'CANCELURL' => base_url() . 'settings/account',
			'PAYMENTREQUEST_0_AMT' => $sms_price,
			'PAYMENTREQUEST_0_ITEMAMT' => $sms_price,
			'L_PAYMENTREQUEST_0_NAME0' => $sms_name,
			'L_PAYMENTREQUEST_0_AMT0' => $sms_price,
  			'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP',
			'PAYMENTREQUEST_0_DESC' => $sms_name,
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
			'NOSHIPPING' => '1'
			//'L_BILLINGTYPE0' => 'RecurringPayments',
			//'L_BILLINGAGREEMENTDESCRIPTION0' => 'Monthly subscription to crm system.'
		);

		// get the response from paypal
		$response = $this->paypal->request('SetExpressCheckout', $params);

		//print_r($response);
		if(is_array($response) && $response['ACK'] == 'Success') { //Request successful
			$token = $response['TOKEN'];
			// save stuff in purchase table
			$save_details = array(
				'user' => $this->session->userdata("user_id"),
				'sms_username' => $this->session->userdata("sms_username"),
				'credits' => $sms,
				'price' => $sms_price,
				'sms_ref' => $sms_name,
				'paypal_token' => $token,
				'created' => date("Y-m-d H:i:s")
			);
			$this->settings_model->saveSMSPurchase($save_details);
			header('Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
		} else {
			redirect('settings/account?paymenterror');
		}
	}

	public function createSmsCredits() {
		$this->load->library('paypal');
		$this->load->model('settings_model');
		$token = $this->input->get("token");
		$payer = $this->input->get("PayerID");
		$purchase_details = $this->settings_model->getSMSPurchase($token);
		if ($purchase_details) {
			// params to send over to paypal
			$params = array(
				'TOKEN' => $token,
				'PAYERID' => $payer,
				'PAYMENTREQUEST_0_AMT' => $purchase_details['price'],
				'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP',
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale'
			);
			// get the response from paypal
			$response = $this->paypal->request('DoExpressCheckoutPayment', $params);
			if(is_array($response) && $response['ACK'] == 'Success') {
				// add the credits to the account
				$this->settings_model->addSMSCredits($purchase_details['user'], $purchase_details['credits']);
				// transfer credits from main account
				//$this->settings_model->transferSMSCredits($purchase_details['sms_username'], $purchase_details['credits']);
				// set history to used status
				$this->settings_model->saveSMSPurchaseStatus($purchase_details['id'], 1);
				redirect('settings/account?creditsuccess');
			} else {
				redirect('settings/account?paymenterror');
			}
		} else {
			redirect('settings/account?paymenterror');
		}
	}

	public function export() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_quote');
	}

	public function custom_fields() {
		$this->load->model('settings_model');
		$data['tabs'] = $this->settings_model->getCustomTabsList();
		$data['files'] = $this->settings_model->getCustomFileList();
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_custom_fields', $data);
	}

	public function save_custom_files() {
		$this->load->model('settings_model');
		$sections = array(
			'files_1' => $this->input->post('section1'),
			'files_2' => $this->input->post('section2'),
			'files_3' => $this->input->post('section3'),
			'files_4' => $this->input->post('section4'),
			'files_5' => $this->input->post('section5'),
			'files_6' => $this->input->post('section6')
		);
		$this->settings_model->updateCustomFiles($sections);
		redirect("settings/custom_fields");
	}

	public function new_custom_tab() {
		$this->load->model('settings_model');
		$new = array(
			'user_id' => $this->session->userdata("user_id"),
			'name' => $this->input->post("name")
		);
		$this->settings_model->addCustomTab($new);
		redirect("settings/custom_fields");
	}

	public function delete_custom_tab() {
		$this->load->model('settings_model');
		$tab = $this->input->get('id');
		// delete custom tab
		$this->settings_model->deleteCustomTab($tab);
		redirect("settings/custom_fields");
	}

	public function delete_custom_form() {
		$this->load->model('settings_model');
		$form = $this->input->get('id');
		$this->settings_model->deleteCustomForm($form);
		redirect("settings/custom_forms");
	}

	public function new_custom_form() {
		$this->load->model('settings_model');
		$new = array(
			'user_id' => $this->session->userdata("user_id"),
			'name' => $this->input->post("name"),
			'desc' => $this->input->post("desc"),
			'ref' => rand(1,9) . time()
		);
		$this->settings_model->addCustomForm($new);
		redirect("settings/custom_forms");
	}

	public function custom_forms() {
		$this->load->model('settings_model');
		$data['forms'] = $this->settings_model->getCustomFormsList();
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_custom_forms', $data);
	}

	public function custom_forms_edit() {
		$this->load->model('settings_model');
		$id = $this->input->get('id');
		$data['form_details'] = $this->settings_model->getCustomFormDetails($id);
		$data['form_options'] = $this->settings_model->getCustomFormOptions($id);
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_custom_forms_edit', $data);
	}

	public function custom_forms_update() {
		$this->load->model('settings_model');
		$id = $this->input->post('id');
		// clear current form options
		$this->settings_model->clearCustomFormOptions($id);
		$count = 0;
		foreach($_POST['item_name'] as $name) {
			$values_json = "";
			if ($name!="") {

				if ($_POST['item_type'][$count]==2) {
					$values_list = array();
					$values = $_POST['item_values'][$count];
					if ($values!="") {
						$values_explode = explode(';', $values);
						$values_json = json_encode($values_explode);
					}
				}

				$item = array(
					'form_id' => $id,
					'label' => $_POST['item_name'][$count],
					'type' => $_POST['item_type'][$count],
					'placeholder' => $_POST['item_placeholder'][$count],
					'value' => $values_json
				);

				// add to databse
				$this->settings_model->addCustomFormItem($item);

				$count++;
			}
		}

		redirect("settings/custom_forms");

	}

	public function tags() {
		$this->load->model('settings_model');
		$data['tag_client'] = $this->settings_model->getClientTags();
		$data['tag_opportunities'] = $this->settings_model->getOpportunitiesTags();
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_tags', $data);
	}

	public function new_client_tag() {
		$this->load->model('settings_model');

		$tag = $this->input->post('client_tag');

		$to_add = array(
			'tag' => $tag,
			'belongs_to' => $this->session->userdata('user_id')
		);

		// add to database
		$this->settings_model->addClientTag($to_add);
		redirect('settings/tags');
	}

	public function remove_client_tag() {
		$this->load->model('settings_model');
		$tag = $this->input->get('tag');
		// remove tag
		$remove = $this->settings_model->removeClientTag($tag);
		if ($remove>0) {
			$this->settings_model->removeClientTagUsed($tag);
		}
		redirect('settings/tags');
	}

	public function new_opportunities_tag() {
		$this->load->model('settings_model');

		$tag = $this->input->post('opportunities_tag');

		$to_add = array(
			'tag' => $tag,
			'belongs_to' => $this->session->userdata('user_id')
		);

		// add to database
		$this->settings_model->addOpportunitiesTag($to_add);
		redirect('settings/tags');
	}

	public function remove_opportunities_tag() {
		$this->load->model('settings_model');
		$tag = $this->input->get('tag');
		// remove tag
		$remove = $this->settings_model->removeOpportunitiesTag($tag);
		if ($remove>0) {
			$this->settings_model->removeOpportunitiesTagUsed($tag);
		}
		redirect('settings/tags');
	}

	public function screen() {
		$this->load->model('settings_model');
		$data['selected_theme'] = $this->session->userdata('theme_site');
		$data['selected_icon'] = $this->session->userdata('theme_icons');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_screen', $data);
	}

	public function change_theme() {
		$this->load->model('settings_model');
		$theme = $this->input->get('theme');

		// if they have selected a valid theme update it
		if ( ($theme=="naturalgreen") || ($theme=="blue") || ($theme=="bluedusk") || ($theme=="purple") || ($theme=="pink") || ($theme=="red") || ($theme=="icesteel") || ($theme=="olive") || ($theme=="salmon") ) {

			$icons = 0;

			// update the user record for next time
			$this->settings_model->updateTheme($theme, $icons);

			// update the session so the theme gets applied now
			$this->session->set_userdata(array('theme_site' => $theme, 'theme_icons' => $icons));
		}

		// redirect after
		redirect('settings/screen');
	}

	public function email() {
		$this->load->model('settings_model');
		$this->load->model('client_model');
		$data['signatures'] = $this->settings_model->getEmailSignatures();
		$data['templates'] = $this->settings_model->getEmailTemplates();
		$data['attachments'] = $this->client_model->getEmailAttachmentsList(false);
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_email', $data);
	}

	public function new_email_signature() {
		$this->load->model('settings_model');
		$name = $this->input->post('name');
		$body = $this->input->post('___body');
		$add = array(
			'name' => $name,
			'body' => $body,
			'belongs_to' => $this->session->userdata('user_id')
		);
		$this->settings_model->addEmailSignature($add);
		redirect('settings/email');
	}

	public function new_email_template() {
		$this->load->model('settings_model');
		$name = $this->input->post('name');
		$subject = $this->input->post('subject');
		$body = $this->input->post('___body');

		$attachment_1 = "";
		$attachment_1_name = "";
		$attachment_2 = "";
		$attachment_2_name = "";
		$attachment_3 = "";
		$attachment_3_name = "";
		$attachment_4 = "";
		$attachment_4_name = "";
		$attachment_5 = "";
		$attachment_5_name = "";

		// loop the attachments
		$attachment_count = 1;

		$add = array(
			'name' => $name,
			'subject' => $subject,
			'body' => $body,
			'belongs_to' => $this->session->userdata('user_id')
		);

		foreach($_POST['attachment'] as $attachment) {
			$file_explode = explode('/', $attachment);
			$add['attachment_'. $attachment_count] = $attachment;
			$add['attachment_'. $attachment_count .'_name'] = substr(end($file_explode),0,-4);
			$attachment_count++;
		}

		$this->settings_model->addEmailTemplate($add);
		redirect('settings/email');

	}

	public function email_template() {
		$this->load->model('settings_model');
		$this->load->model('client_model');
		$temp_id = $this->input->get('id');
		// see if we have a template matching that
		$template = $this->settings_model->getThatTemplate($temp_id);
		if ($template) {
			$data['template'] = $template;
			$data['attachments'] = $this->client_model->getEmailAttachmentsList(false);
			$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('settings_email_template', $data);
		} else {
			redirect('settings/email');
		}
	}

	public function email_template_save() {
		$this->load->model('settings_model');
		$id = $this->input->post('id');
		$attachment_count = 1;
		$update = array(
			'name' => $this->input->post('name'),
			'subject' => $this->input->post('subject'),
			'body' => $this->input->post('___body')
		);
		foreach($_POST['attachment'] as $attachment) {
			$file_explode = explode('/', $attachment);
			$update['attachment_'. $attachment_count] = $attachment;
			$update['attachment_'. $attachment_count .'_name'] = substr(end($file_explode),0,-4);
			$attachment_count++;
		}
		$this->settings_model->updateThatTemplate($update, $id);
		$this->session->set_flashdata('alert', '<div class="alert alert-success">Template Successfully Updated.</div>');
		redirect('settings/email');
	}

	public function email_signature() {
		$this->load->model('settings_model');
		$signature_id = $this->input->get('id');
		// see if we have a template matching that
		$signature = $this->settings_model->getThatSignature($signature_id);
		if ($signature) {
			$data['signature'] = $signature;
			$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
			$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
			$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
			$this->load->view('common/main_header', $recentlyviewed);
			$this->load->view('settings_email_signature', $data);
		} else {
			redirect('settings/email');
		}
	}

	public function email_signature_save() {
		$this->load->model('settings_model');
		$id = $this->input->post('id');
		$update = array(
			'name' => $this->input->post('name'),
			'body' => $this->input->post('___body')
		);
		$this->settings_model->updateThatSignature($update, $id);
		$this->session->set_flashdata('alert', '<div class="alert alert-success">Signature Successfully Updated.</div>');
		redirect('settings/email');
	}

	public function email_template_remove() {
		$this->load->model('settings_model');
		$id = $this->input->get('id');
		$this->settings_model->deleteThatTemplate($id);
		$this->session->set_flashdata('alert', '<div class="alert alert-success">Template Successfully Deleted.</div>');
		redirect('settings/email');
	}

	public function email_signature_remove() {
		$this->load->model('settings_model');
		$id = $this->input->get('id');
		$this->settings_model->deleteThatSignature($id);
		$this->session->set_flashdata('alert', '<div class="alert alert-success">Signature Successfully Deleted.</div>');
		redirect('settings/email');
	}

	public function users() {
		$this->load->model('settings_model');
		$this->load->model('user_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		// get list of current additional users
		$data['additional_users'] = $this->settings_model->getAdditionalUsers($this->session->userdata('group_id'), $this->session->userdata('group_id'));

		$data['parent_user'] = $this->user_model->isUserParent($this->session->userdata('user_id'));
		$recentlyviewed['task_setting'] = $this->Tasksetting_model->getAllTaskLabel('',true);
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_users', $data);
	}

	public function users_additional() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_users_additional');
	}

	public function additional_users_add() {
		$this->load->model('settings_model');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
		$this->form_validation->set_rules('surname', 'Last Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[5]|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]');
		$this->form_validation->set_message('is_unique', '%s is taken.');

		if ($this->form_validation->run()==false) {
			$errors = array(
				'errors' => '<div class="alert alert-danger">' . validation_errors() . '</div>',
				'firstname' => $this->input->post('firstname'),
				'surname' => $this->input->post('surname'),
				'email' => $this->input->post('email'),
				'username' => $this->input->post('username')
			);
			$this->session->set_flashdata($errors);
			redirect('settings/users_additional');
		} else {
			$new_user = array(
				'first_name' => $this->input->post('firstname'),
				'last_name' => $this->input->post('surname'),
				'email' => $this->input->post('email'),
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'created_date' => date('Y-m-d H:i:s'),
				'active' => 1
			);

			// create the user
			$this_user = $this->settings_model->addAdditionalUser($new_user);

			// user permissions
			$permissions = array(
				'user_id' => $this_user,
				'client_edit' => $this->input->post('client_edit'),
				//'client_pmi' => $this->input->post('client_pmi'),
				//'client_policies' => $this->input->post('client_policies'),
				'client_opportunities' => $this->input->post('client_opportunities'),
				//'client_medical' => $this->input->post('client_medical'),
				//'client_income' => $this->input->post('client_income'),
				//'client_factfind' => $this->input->post('client_factfind'),
				//'client_compliance' => $this->input->post('client_compliance'),
				'client_files' => $this->input->post('client_files'),
				'client_notes' => $this->input->post('client_notes'),
				'client_delete' => $this->input->post('client_delete')
			);

			$this->settings_model->addAdditionalPermissions($permissions);

			// add user to group
			$this->settings_model->addUser2Group($this_user, $this->session->userdata('group_id'));

			redirect('settings/users');
		}
	}

	public function users_edit_additional() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$data['user'] = $this->settings_model->getAdditionalUserDetails($this->input->get("user"));
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_users_additional_edit', $data);
	}

	public function additional_users_update() {
		$this->load->model('settings_model');
		$user = $this->input->get("user");

		// check if they are in this group and that you should be doing this
		if ($this->session->userdata("role")!=1) {
			redirect("settings/users");
		}

		if (!$this->settings_model->checkAdditionalUserInThisGroup($user)) {
			redirect("settings/users");
		}

		$user_details = array(
			'first_name' => $this->input->post('firstname'),
			'last_name' => $this->input->post('surname'),
			'email' => $this->input->post('email')
		);

		// update user
		$this->settings_model->updateAdditionalUser($user, $user_details);

		$permissions = array(
			'client_edit' => $this->input->post('client_edit'),
			//'client_pmi' => $this->input->post('client_pmi'),
			//'client_policies' => $this->input->post('client_policies'),
			'client_opportunities' => $this->input->post('client_opportunities'),
			//'client_medical' => $this->input->post('client_medical'),
			//'client_income' => $this->input->post('client_income'),
			//'client_factfind' => $this->input->post('client_factfind'),
			//'client_compliance' => $this->input->post('client_compliance'),
			'client_files' => $this->input->post('client_files'),
			'client_notes' => $this->input->post('client_notes'),
			'client_delete' => $this->input->post('client_delete')
		);

		// update permissions
		$this->settings_model->updateAdditionalUserPermissions($user, $permissions);

		redirect("settings/users");

	}

	public function update_tabs() {
		$this->load->model('settings_model');
		// check if they have existing tab settings
		$existing = $this->settings_model->getTabSettings($this->session->userdata("user_id"));

		if ($existing) {
			// update
			$update = array(
				'files_tab' => $this->input->post("files_tab"),
				'messages_tab' => $this->input->post("messages_tab"),
				'pmi_tab' => $this->input->post("pmi_tab"),
				'protection_tab' => $this->input->post("protection_tab"),
				'policies_tab' => $this->input->post("policies_tab"),
				'people_tab' => $this->input->post("people_tab"),
				'opportunities_tab' => $this->input->post("opportunities_tab"),
				'medical_tab' => $this->input->post("medical_tab"),
				'income_tab' => $this->input->post("income_tab"),
				'factfind_tab' => $this->input->post("factfind_tab"),
				'compliance_tab' => $this->input->post("compliance_tab"),
				'live_tab' => $this->input->post("live_tab")
			);
			$this->settings_model->updateTabSettings($existing['id'], $this->session->userdata("user_id"), $update);

		} else {

			// create
			$new = array(
				'user_id' => $this->session->userdata("user_id"),
				'files_tab' => $this->input->post("files_tab"),
				'messages_tab' => $this->input->post("messages_tab"),
				'pmi_tab' => $this->input->post("pmi_tab"),
				'protection_tab' => $this->input->post("protection_tab"),
				'policies_tab' => $this->input->post("policies_tab"),
				'people_tab' => $this->input->post("people_tab"),
				'opportunities_tab' => $this->input->post("opportunities_tab"),
				'medical_tab' => $this->input->post("medical_tab"),
				'income_tab' => $this->input->post("income_tab"),
				'factfind_tab' => $this->input->post("factfind_tab"),
				'compliance_tab' => $this->input->post("compliance_tab"),
				'live_tab' => $this->input->post("live_tab")
			);

			$this->settings_model->addTabSettings($new);

		}

		// set the new session details
		$session = array(
			'tab_settings' => 1,
			'files_tab' => $this->input->post("files_tab"),
			'messages_tab' => $this->input->post("messages_tab"),
			'pmi_tab' => $this->input->post("pmi_tab"),
			'protection_tab' => $this->input->post("protection_tab"),
			'policies_tab' => $this->input->post("policies_tab"),
			'people_tab' => $this->input->post("people_tab"),
			'opportunities_tab' => $this->input->post("opportunities_tab"),
			'medical_tab' => $this->input->post("medical_tab"),
			'income_tab' => $this->input->post("income_tab"),
			'factfind_tab' => $this->input->post("factfind_tab"),
			'compliance_tab' => $this->input->post("compliance_tab"),
			'live_tab' => $this->input->post("live_tab")
		);

		$this->session->set_userdata($session);

		redirect("settings/custom_fields");

	}

	public function custom_fields_tab() {
		$this->load->model('settings_model');
		$id = $this->input->get("id");
		$data['tab'] = $this->settings_model->getCustomTab($id);
		$data['forms_list'] = $this->settings_model->getCustomFormsList();
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_custom_tab_edit', $data);
	}

	public function custom_fields_tab_save() {
		$this->load->model('settings_model');
		$id = $this->input->post("id");

		//section1
		$section1 = $this->input->post("section1");
		$explode1 = explode('_', $section1);
		if ($explode1[0]=="form") {
			$section1 = 3;
			$section1_form = $explode1[1];
		} else {
			$section1 = $explode1[1];
			$section1_form = "";
		}

		//section2
		$section2 = $this->input->post("section2");
		$explode2 = explode('_', $section2);
		if ($explode2[0]=="form") {
			$section2 = 3;
			$section2_form = $explode2[1];
		} else {
			$section2 = $explode2[1];
			$section2_form = "";
		}

		//section3
		$section3 = $this->input->post("section3");
		$explode3 = explode('_', $section3);
		if ($explode3[0]=="form") {
			$section3 = 3;
			$section3_form = $explode3[1];
		} else {
			$section3 = $explode3[1];
			$section3_form = "";
		}

		//section4
		$section4 = $this->input->post("section4");
		$explode4 = explode('_', $section4);
		if ($explode4[0]=="form") {
			$section4 = 3;
			$section4_form = $explode4[1];
		} else {
			$section4 = $explode4[1];
			$section4_form = "";
		}

		//section5
		$section5 = $this->input->post("section5");
		$explode5 = explode('_', $section5);
		if ($explode5[0]=="form") {
			$section5 = 3;
			$section5_form = $explode5[1];
		} else {
			$section5 = $explode5[1];
			$section5_form = "";
		}

		//section6
		$section6 = $this->input->post("section6");
		$explode6 = explode('_', $section6);
		if ($explode6[0]=="form") {
			$section6 = 3;
			$section6_form = $explode6[1];
		} else {
			$section6 = $explode6[1];
			$section6_form = "";
		}

		$update = array(
			'name' => $this->input->post("name"),
			'section1' => $section1,
			'section1_form' => $section1_form,
			'section1_name' => $this->input->post("section1_name"),
			'section2' => $section2,
			'section2_form' => $section2_form,
			'section2_name' => $this->input->post("section2_name"),
			'section3' => $section3,
			'section3_form' => $section3_form,
			'section3_name' => $this->input->post("section3_name"),
			'section4' => $section4,
			'section4_form' => $section4_form,
			'section4_name' => $this->input->post("section4_name"),
			'section5' => $section5,
			'section5_form' => $section5_form,
			'section5_name' => $this->input->post("section5_name"),
			'section6' => $section6,
			'section6_form' => $section6_form,
			'section6_name' => $this->input->post("section6_name")
		);
		$this->settings_model->updateCustomTab($id, $update);
		redirect("settings/custom_fields");

	}

	public function account_logo() {
		$this->load->model('settings_model');
		$config['upload_path'] = './img/company_logos/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = 'logo' .'_'. $this->session->userdata('group_id') . '_' . time();
		$this->load->library('upload', $config);
 		if (!$this->upload->do_upload('logo')) {
			$this->session->set_flashdata('logo_results', '<div class="alert alert-error">'.$this->upload->display_errors().'</div>');
			redirect('settings/account');
		} else {
			$file = $this->upload->data();
			$img_config['image_library'] = 'gd2';
			$img_config['source_image']	= $file['full_path'];
			$img_config['maintain_ratio'] = FALSE;
			$img_config['width'] = 200;
			$img_config['height'] = 50;
			$img_config['quality'] = 100;
			$this->load->library('image_lib', $img_config);
			$this->image_lib->resize();
			$this->settings_model->updateCompanyLogo($file['file_name']);
			$this->session->set_flashdata('logo_results', '<div class="alert alert-success">Logo Has Successfully Been Added.</div>');
			redirect('settings/account');
		}
	}

	public function account_logo_remove() {
		$this->load->model('settings_model');
		$this->settings_model->updateCompanyLogo('');
		$this->session->set_flashdata('logo_results', '<div class="alert alert-success">Logo Has Successfully Been Removed.</div>');
		redirect('settings/account');
	}

	public function help() {
		$this->load->model('settings_model');
		$recentlyviewed['viewed'] = $this->settings_model->getRecentlyViewed('10');
		$recentlyviewed['viewed6'] = $this->settings_model->getRecentlyViewed('6');
		$this->load->view('common/main_header', $recentlyviewed);
		$this->load->view('settings_help');
	}

	public function tasksetting(){
	}

}
