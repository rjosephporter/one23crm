<?php class Cron extends CI_Controller {

	public function index() {	
		echo "<h1>404</h1>";
	}
	
	public function task_reminder() {	
		if($this->input->is_cli_request()) {
		$this->load->model('cron_model');
		
			// get todays date
			$today = date("Y-m-d");
			$today_name = date("j F Y");
		
			// get the list of users
			$users = $this->cron_model->listUsers();
			
			// if we have users ... look for tasks
			if ($users) {
			
				foreach($users as $user) {
				
					// we need the name and email address
					$to_name = $user['name'];
					$to_email = $user['email'];
				
					// get the tasks for each user for today
					$tasks = $this->cron_model->taskByUser($user['id'], $today);

					// if tasks for that day, list them
					if ($tasks) {
					
					// we get to the task stage ... we should start the build up the email
					$email_body = '<html>
					<head></head>
					<body>				
					<p>Hello ' . $to_name .'</p>
					<p>Please find below a list of tasks that are due to be completed today. You can view, edit and complete any of the tasks from your dashboard.</p>
					<ul>';
						
						foreach($tasks as $task) {
						
							//echo $task['due_time'] . ' ' . $task['client_name'] . "\n";
							$email_body .= '<li>Task due ' . $task['due_time'] . ': [' . $task['action'] . '] ' . $task['name'];
							
							if ($task['client_name']) { 
								$email_body .= '<br />for client <a href="http://www.one23.co.uk/clients/view?id='. $task['customer_id'] .'">' . $task['client_name'] . '</a>';
							}
							
							$email_body .= '</li>';
						
						}
						
					$email_body .= '</ul>
					<p>&nbsp;</p>
					<p></p>
					</body></html>';
					
					// get the email sent now!
					//echo $email_body;
					//$config['smtp_host'] = 'smtp.123-reg.co.uk';
					//$config['smtp_user'] = 'no-reply@123-insureme.co.uk';
					//$config['smtp_pass'] = 'ce3U3pug';
					//$config['smtp_port'] = '25';	
					//$config['protocol'] = 'smtp';
					$config['protocol'] = 'sendmail';
					$config['mailtype'] = 'html';	
				
					$this->load->library('email');
					$this->email->initialize($config);		
					$this->email->from('no-reply@one23.co.uk', 'One23');
					$this->email->to($to_email); 
					$this->email->subject('Reminders for ' . $today_name);
					$this->email->message($email_body);	
					$this->email->send();
										
					
					} // if tasks
				
				} // foreach user
			
			}

		} else {
			echo "<h1>404</h1>";		
		}	
	}
	
	/*
	public function db_backup() {
		$this->load->dbutil();
		$this->load->helper('file');
		
		$file_name = date("Ymd_His");

		$prefs = array(
			'tables'      => array(),
			'ignore'      => array(),
			'format'      => 'zip',
			'filename'    => $file_name,
			'add_drop'    => TRUE,
			'add_insert'  => TRUE,
			'newline'     => "\n"
		);
		
		$backup =& $this->dbutil->backup($prefs);		
		// if file is written upload it to the ftp server
		if (write_file('./db_backup/'.$file_name.'.zip', $backup)) {			
			// then upload backup to the ftp server
			$this->load->library('ftp');		
			$config['hostname'] = '94.136.40.103';
			$config['username'] = 'ftp@123-insureme.co.uk';
			$config['password'] = 'R8AVp74l';
			$config['port']     = 21;
			$config['passive']  = FALSE;
			$config['debug']    = TRUE;
			// connect
			$this->ftp->connect($config);				
			// upload the file
			$this->ftp->upload('./db_backup/' . $file_name . '.zip', '/public_html/crm_db_backups/'.$file_name.'.zip', 'auto', 0775);
			// close
			$this->ftp->close();
		}
	}
	*/
	
	public function weblineGetQuotes() {
		$this->load->library('webline');
		$this->load->model('cron_model');		
		$quotes = $this->cron_model->getWeblineListQuotes();
		// loop each quote
		foreach($quotes as $quote) {
			$weblineLoginDetails = $this->cron_model->getWeblineLoginDetails($quote['belongs_to']);
			$token = $this->webline->loginWithParms($weblineLoginDetails['webline_num'],$weblineLoginDetails['webline_user'],$weblineLoginDetails['webline_pass']);
			$this_quote_id = $quote['id'];
			$collection = $quote['collection_type']; // 0=protection, 1=PI, 2=Wholeoflife
			// get quote details
			$quote_details = $this->cron_model->getQuoteDetails($this_quote_id);
			
			if ($collection==1) {				
				$results = $this->webline->IPResults($token, $quote['webline_ref']);
				$this->cron_model->setQuoteStatus($this_quote_id, '2');				
				if ($results->GetIPResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetIPResultsResult->ResponseItemArray->IPResultsItem as $thequote) {					
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						$this->cron_model->addQuoteResults($quote_array);
						// set quote status to received
						$this->cron_model->setQuoteStatus($this_quote_id, '1');
					} // end loop
					// get best price
					$bestprice = $this->cron_model->getQuoteBestPrice($this_quote_id);				
					if ($bestprice) {
						$this->cron_model->setQuoteBestPrice($this_quote_id, $bestprice['provider'], $bestprice['benefit'], $bestprice['premium'], $bestprice['product'], 0);
					}										
				}							
							
			} elseif ($collection==2) {
			
				$results = $this->webline->WholeOfLifeResults($token, $quote['webline_ref']);
				// set quote status to processing
				$this->cron_model->setQuoteStatus($this_quote_id, '2');			
				if ($results->GetWholeOfLifeResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetWholeOfLifeResultsResult->ResponseItemArray->WholeOfLifeResultsItem as $thequote) {
						
						// skip any quotes that are from Skandia
						if (strpos($thequote->ProviderName,"Skandia")!==false) {
								continue; // skip this
						}
					
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						$this->cron_model->addQuoteResults($quote_array);
						// set quote status to received
						$this->cron_model->setQuoteStatus($this_quote_id, '1');
					} // end loop
					// get best price
					$bestprice = $this->cron_model->getQuoteBestPrice($this_quote_id);
					//print_r($bestprice);
					if ($bestprice) {
						$this->cron_model->setQuoteBestPrice($this_quote_id, $bestprice['provider'], $bestprice['benefit'], $bestprice['premium'], $bestprice['product'], 0);
					}					
				}							
			
			} else {
							
				$results = $this->webline->getQuoteResults($token, $quote['webline_ref']);
				// set quote status to processing
				$this->cron_model->setQuoteStatus($this_quote_id, '2');
				if ($results->GetResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetResultsResult->ResponseItemArray->ProtectionResultsItem as $thequote) {
					
						// skip any quotes that are from Skandia
						if (strpos($thequote->ProviderName,"Skandia")!==false) {
								continue; // skip this
						}						

						// if they dont want to see low start, skip it
						if ($quote_details['lowstart']==1) {
							if (strpos($thequote->ProductName,"Low Start Term Assurance")!==false) {
									continue; // skip this
							}						
						}

						// if they dont want to see accelerator products, skip it
						if ($quote_details['accelerator']==1) {
							if (strpos($thequote->ProductName,"Accelerator")!==false) {
									continue; // skip this
							}						
						}						
					
						// reset benefit values
						$deathbenefit = 0;
						$cicbenefit = 0;
						// check if we have any individual benefits
						if (isset($thequote->individualBenefits)) {
							foreach($thequote->individualBenefits->ProtectionBenefitItem as $thebenefit) {						
								if ( (isset($thebenefit->RiskBenefitIndex)) && ($thebenefit->RiskBenefitIndex==0)) {						
									$deathbenefit = $thebenefit->BenefitAmount;
								} elseif ( (isset($thebenefit->RiskBenefitIndex)) && ($thebenefit->RiskBenefitIndex==1) ) {
									$cicbenefit = $thebenefit->BenefitAmount;
								}
							}
						}
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'death_benefit' => $deathbenefit,
							'cic_benefit' => $cicbenefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						//print_r($quote_array);						
						//echo $thequote->ResponseRef . '<br>';
						$this->cron_model->addQuoteResults($quote_array);
						// set quote status to received
						$this->cron_model->setQuoteStatus($this_quote_id, '1');
					} // end loop				
				}				
				// get best price
				$bestprice = $this->cron_model->getQuoteBestPrice($this_quote_id);
				if ($bestprice) {				
					if (($bestprice['death_benefit']>0) && ($bestprice['cic_benefit']>0)) {
						$deathcic = 'Death: &pound;' . $bestprice['death_benefit'] . ' - CIC: &pound;' . $bestprice['cic_benefit'];
					} else {
						$deathcic = 0;
					}					
					$this->cron_model->setQuoteBestPrice($this_quote_id, $bestprice['provider'], $bestprice['benefit'], $bestprice['premium'], $bestprice['product'], $deathcic);
				}
			} // end if		
			
		} // foreach quote
	
	}
	
	public function weblineGetQuotesMulti() {
		$this->load->library('webline');
		$this->load->model('cron_model');		
		$quotes = $this->cron_model->getWeblineListQuotesMulti();
		// loop each quote
		foreach($quotes as $quote) {
			$weblineLoginDetails = $this->cron_model->getWeblineLoginDetails($quote['belongs_to']);
			$token = $this->webline->loginWithParms($weblineLoginDetails['webline_num'],$weblineLoginDetails['webline_user'],$weblineLoginDetails['webline_pass']);
			$this_quote_id = $quote['id'];
			$multi_product = $quote['multiproduct'];
			$collection = $quote['collection_type']; // 0=protection, 1=PI, 2=Wholeoflife
			// get quote details
			$quote_details = $this->cron_model->getQuoteDetailsMulti($this_quote_id);
			if ($collection==1) {				
				$results = $this->webline->IPResults($token, $quote['webline_ref']);
				$this->cron_model->setQuoteStatusMulti($this_quote_id, '2');				
				if ($results->GetIPResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetIPResultsResult->ResponseItemArray->IPResultsItem as $thequote) {					
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						$this->cron_model->addQuoteResultsMulti($quote_array);
						// set quote status to received
						$this->cron_model->setQuoteStatusMulti($this_quote_id, '1');
						// added to processed
						$this->cron_model->setQuoteProcessedMulti($multi_product);
						
					} // end loop
				}							
							
			} elseif ($collection==2) {
			
				$results = $this->webline->WholeOfLifeResults($token, $quote['webline_ref']);
				// set quote status to processing
				$this->cron_model->setQuoteStatusMulti($this_quote_id, '2');			
				if ($results->GetWholeOfLifeResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetWholeOfLifeResultsResult->ResponseItemArray->WholeOfLifeResultsItem as $thequote) {
					
						// skip any quotes that are from Skandia
						if (strpos($thequote->ProviderName,"Skandia")!==false) {
								continue; // skip this
						}
											
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						$this->cron_model->addQuoteResultsMulti($quote_array);
						// set quote status to received
						$this->cron_model->setQuoteStatusMulti($this_quote_id, '1');
						$this->cron_model->setQuoteProcessedMulti($multi_product);
					} // end loop					
				}							
			
			} else {
							
				$results = $this->webline->getQuoteResults($token, $quote['webline_ref']);
				// set quote status to processing
				$this->cron_model->setQuoteStatusMulti($this_quote_id, '2');
				if ($results->GetResultsResult->Success==1) {
					// loop the quote results
					foreach($results->GetResultsResult->ResponseItemArray->ProtectionResultsItem as $thequote) {
					
						// skip any quotes that are from Skandia
						if (strpos($thequote->ProviderName,"Skandia")!==false) {
								continue; // skip this
						}					
					
						// if they dont want to see low start, skip it
						if ($quote_details['lowstart']==1) {
							if (strpos($thequote->ProductName,"Low Start Term Assurance")!==false) {
									continue; // skip this
							}						
						}

						// if they dont want to see accelerator products, skip it
						if ($quote_details['accelerator']==1) {
							if (strpos($thequote->ProductName,"Accelerator")!==false) {
									continue; // skip this
							}						
						}						
					
						// reset benefit values
						$deathbenefit = 0;
						$cicbenefit = 0;
						$quote_array = array(
							'quote_id' => $this_quote_id,
							'product' => $thequote->ProductName,
							'product_id' => $thequote->ProviderID,
							'response_guid' => $thequote->ResponseGuid,
							'response_ref' => $thequote->ResponseRef,
							'provider' => $thequote->ProviderName,
							'provider_logo' => $thequote->ProviderLogoAbsoluteURL,
							'error' => $thequote->ErrorCode,
							'notes' => $thequote->NotesHTML,
							'benefit' => $thequote->Benefit,
							'death_benefit' => $deathbenefit,
							'cic_benefit' => $cicbenefit,
							'premium' => $thequote->Premium,
							'rates' => $thequote->Rates,
							'apply_online' => $thequote->ElectronicApplicationAvailable										
						);
						//echo '<pre>';
						//print_r($quote_array);						
						//echo $thequote->ResponseRef . '<br>';
						$this->cron_model->addQuoteResultsMulti($quote_array);
						$this->cron_model->setQuoteProcessedMulti($multi_product);
					} // end loop
					
						// set quote status to received
						$this->cron_model->setQuoteStatusMulti($this_quote_id, '1');
													
				}				
			} // end if		
			
		} // foreach quote
	
	}
	
	
	
	


}
