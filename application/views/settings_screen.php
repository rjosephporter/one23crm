
<script>
$(document).ready(function(){

	<?php if ($selected_theme=="clean") { echo '$("#icon_box").hide();'; } ?>

	$("#theme").change(function() {
		// get theme value
		var themeSelected = $(this).val();
		// if clean theme is selected hide the icon box
		if (themeSelected=="clean") {
			$("#icon_box").hide();
		} else {
			$("#icon_box").show();
		}
	});

});
</script>
<div id="container_top">
<h4>Screen Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul></div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
                <div class="span12 well"><h5 style="margin-top:0px;">Available Themes</h5>
                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=naturalgreen"><img src="<?php echo base_url(); ?>img/settings_sytle_naturalgreen.png" style="border:#333 1px solid;" /></a><br /><strong>Natural Green</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=blue"><img src="<?php echo base_url(); ?>img/settings_sytle_blue.png" style="border:#333 1px solid;" /></a><br /><strong>Blue</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=bluedusk"><img src="<?php echo base_url(); ?>img/settings_sytle_bluedusk.png" style="border:#333 1px solid;" /></a><br /><strong>Blue Dusk</strong></td>
                      </tr>
                      <tr>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=purple"><img src="<?php echo base_url(); ?>img/settings_sytle_purple.png" style="border:#333 1px solid;" /></a><br /><strong>Purple</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=pink"><img src="<?php echo base_url(); ?>img/settings_sytle_pink.png" style="border:#333 1px solid;" /></a><br /><strong>Pink</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=red"><img src="<?php echo base_url(); ?>img/settings_sytle_red.png" style="border:#333 1px solid;" /></a><br /><strong>Red</strong></td>
                      </tr>
                      <tr>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=icesteel"><img src="<?php echo base_url(); ?>img/settings_sytle_icesteel.png" style="border:#333 1px solid;" /></a><br /><strong>Ice Steel</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=olive"><img src="<?php echo base_url(); ?>img/settings_sytle_olive.png" style="border:#333 1px solid;" /></a><br /><strong>Olive</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=salmon"><img src="<?php echo base_url(); ?>img/settings_sytle_salmon.png" style="border:#333 1px solid;" /></a><br /><strong>Warm Salmon</strong></td>
                      </tr>
                    </table>
                </div>
            </div>


        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">Screen Settings</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
