<?php
if (!$quote_details) {
	redirect('clients');
}

//echo '<pre>';
//print_r($quote_details);
//print_r($quote_results);
//echo '</pre>';

if ($quote_details['status']!=1) { ?>
<script>
$(document).ready(function() {
	setInterval(function(){
	  location.reload();
	}, 5000);
});
</script>
<div style="padding-bottom:10px;"><p align="center"><img src="<?php echo base_url(); ?>img/loading-icon.gif" /></p>
<p align="center">Please Wait, we are currently collecting quotes from our panel of leading insurers.</p></div>

<?php } else { ?>

<h4>Quote Requested: <?php echo $quote_details['product_line']; ?></h4>

<?php if ($quote_results) {
		echo '<ul class="list">';
		// loop and display each quote
		foreach($quote_results as $quote) {
		
			// display benefit amounts
			if ( ($quote['death_benefit']>0) && ($quote['cic_benefit']>0)) {		
				$show_benefit = ' - Death Benefit: &pound;' . number_format($quote['death_benefit'],2) . ' - CIC: &pound;' . number_format($quote['cic_benefit'],2);		
			} else {
				$show_benefit = '- Benefit: &pound;' . number_format($quote['benefit'],2);
			}
		
			if ($quote['apply_online']==1) {
				echo '<li class="arrow"><a href="https://www.webline.co.uk/WeblineNet/OnlineApps/DirectApply.aspx?ResponseGuid='.$quote['response_guid'].'&ResponseRef='.$quote['response_ref'].'&Vnd_Number=023985" target="_blank" title="Click To Purchase"><small><strong>'. $quote['provider'] .' - '. $quote['product'] .' ('. $quote['rates'] .')</strong><br />
				Premium: &pound;'. $quote['premium'] .' '. $show_benefit .'<p style="font-size:11px;">'. $quote['notes'] .'</p></small></a></li>';
			} else {
				echo '<li><small><strong>'. $quote['provider'] .' - '. $quote['product'] .' ('. $quote['rates'] .')</strong><br />
				Premium: &pound;'. $quote['premium'] .'  '. $show_benefit .'<p style="font-size:11px;">'. $quote['notes'] .'</p></small></li>';
			}
		}
		echo '</ul>';
	}

}?>




