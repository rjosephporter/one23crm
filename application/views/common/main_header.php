<?php //blue green yellow purple pink red

$theme = $this->session->userdata('theme_site');

// set the theme colour
if ($theme=="icesteel") {
	$colour = "icesteel";
} elseif ($theme=="naturalgreen") {
	$colour = "naturalgreen";
} elseif ($theme=="blue") {
	$colour = "blue";
} elseif ($theme=="bluedusk") {
	$colour = "bluedusk";
} elseif ($theme=="pink") {
	$colour = "pink";
} elseif ($theme=="olive") {
	$colour = "olive";
} elseif ($theme=="purple") {
	$colour = "purple";
} elseif ($theme=="red") {
	$colour = "red";
} elseif ($theme=="salmon") {
	$colour = "salmon";
} else {
	$colour = "icesteel";
}

$icon_set = "white";

//echo '<pre>';
//print_r($this->session->all_userdata());
//echo '</pre>';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>123CRM</title>

<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/style_<?php echo $colour; ?>.css" rel="stylesheet" type="text/css" />
<link href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<?php echo link_tag('favicon.ico', 'shortcut icon', 'image/ico'); ?>
<script type='text/javascript'>
var baseURL = '<?php echo base_url();?>';
</script>

<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script><script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/common.js"></script>
<script src="<?php echo base_url(); ?>js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<script src="<?php echo base_url(); ?>js/jeditable.mini.js"></script>

<!--<script src="//cdn.ckeditor.com/4.4.4/standard/ckeditor.js"></script>-->
<!-- <script src="//cdn.ckeditor.com/4.4.4/full/ckeditor.js"></script>-->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>plugin/ckfinder/ckfinder.js"></script>-->


<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/social-buttons.css" rel="stylesheet" type="text/css" />

<!-- calendar -->
<link href='<?php echo base_url(); ?>/plugin/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>/plugin/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url(); ?>/plugin/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>/plugin/fullcalendar/fullcalendar.min.js'></script>
<link href='<?php echo base_url(); ?>/css/custom.css' rel='stylesheet' />
<!-- calendar -->

<!-- summernote text editor start -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url('css/summernote.css'); ?>" />
<script src="<?php echo base_url('js/summernote.js'); ?>"></script>
<script src="<?php echo base_url('js/jquery.inlineStyler.src.js'); ?>"></script>
<!-- summernote text editor end -->

</head>
<body>

<?php if(!$this->session->userdata('checked_empty_fields')): ?>
<script>
/* Modal notification for incomplete profile - 08.14.2014 */
$(document).ready(function() {
	$.get(baseURL + '/dashboard/checkEmptyFields', function(data) {
		if(data.incomplete_fields.length > 0) {
			setTimeout(function(){
	            $('#incomplete_profile_modal').modal('show');
	        },3000)
		}
	});
});
</script>

<div id="incomplete_profile_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3>Reminder</h3>
  </div>
  <div class="modal-body">
    <p>Complete your profile to enjoy full features of the system. Remember, it's free!</p>
  </div>
  <div class="modal-footer">
     <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <a href="/settings/edit_account" class="btn">Update Now</a>
  </div>
</div>
<?php endif; ?>

<div id="header_container" class="hidden-phone">
<?php
/*
if (($this->session->userdata('account_expiry_days')>0) && ($this->session->userdata('account_expiry_days')<14)) {
	echo '<div style="padding:10px;"><div class="alert alert-info" style="margin-bottom:0;">Your subscription will expire in '. $this->session->userdata('account_expiry_days') .' days, please <a href="'. base_url() .'settings/account">click here</a> to upgrade your subscription.</div></div>';
} elseif ($this->session->userdata('account_expiry_days')==0) {
	echo '<div style="padding:10px;"><div class="alert alert-info" style="margin-bottom:0;">Your subscription is about to expire, please <a href="'. base_url() .'settings/account">click here</a> to upgrade your subscription.</div></div>';
}


if ($this->session->userdata('trial')==1) {
	echo '<div style="padding:10px;"><div class="alert alert-info" style="margin-bottom:0;">You are currently using a trial account with <strong>'.$this->session->userdata('account_expiry_days').'</strong> days remaining. If you would like to continue using our system, please <a href="'.base_url().'settings/account"><strong>click here</strong></a> to provide payment details.</div></div>';
}*/

?>
<div id="cssmenu">
<ul>
   <li><a href="<?php echo base_url(); ?>dashboard" rel="tooltip" data-placement="bottom" title="Dash"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_home.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>clients" rel="tooltip" data-placement="bottom" title="Clients"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_clients.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>calender" rel="tooltip" data-placement="bottom" title="Calendar"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_cal.png"/></a></li>
   <li><a href="<?php echo base_url(); ?>pipeline" rel="tooltip" data-placement="bottom" title="Sales Pipeline"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_pipeline.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>document_library" rel="tooltip" data-placement="bottom" title="Document Library"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_docs.png" /></a></li>
   <li><a href="#" rel="tooltip" data-placement="bottom" title="Quick Quote"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_calc.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>messages" rel="tooltip" data-placement="bottom" title="Messages"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_messages.png" /><div id="messageUnread" style=" z-index:1; position:absolute;" class="label label-info"></div></a></li>
   <li><a href="<?php echo base_url(); ?>marketing" rel="tooltip" data-placement="bottom" title="Marketing"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_marketing.jpg" style="width:42px"/></a></li>
   <?php /*<li><a href="#" rel="tooltip" data-placement="bottom" title="Marketing"><img src="<?php echo base_url(); ?>img/nav-marketing.png" /></a></li>*/ ?>
</ul>

<div style="float:right; margin-top:5px;  margin-right:5px; padding:0 7px;">
<a href="<?php echo base_url(); ?>login/logout" rel="tooltip" data-placement="bottom" title="Log Out"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_power.png" title="Log Out" /></a>
</div>

<div style="float:right; margin-top:5px; padding:0 7px;">
<a href="<?php echo base_url(); ?>settings" rel="tooltip" data-placement="bottom" title="Settings"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_settings.png" title="Settings" /></a>
</div>

<div style="float:right; margin-top:5px; padding:0 7px;">
<a target="_self" href="#addNew" data-toggle="modal" rel="tooltip" data-placement="bottom" title="Add New"><img src="<?php echo base_url(); ?>img/nav_icons/<?php echo $colour; ?>_new.png" title="Add New" /></a>
</div>

<div id="alert_incomplete_profile" style="float:right; margin-top:5px; padding:0 7px; display:none">
	<div class="alert alert-waring" style="margin-bottom:0px">
	  <strong>Reminder.</strong> <a href="/settings/edit_account">Click here</a> to complete your profile and experience full features of the system.
	</div>
</div>

</div>

<div id="cssmenu2" style="overflow:hidden;">
<ul class="visible-desktop">
<?php if ($viewed) {

	foreach($viewed as $client) {

	echo '<li><a href="'.base_url().'clients/view?id='.$client['customer_id'].'" rel="tooltip" data-placement="bottom" title="'.$client['client_name'].'"><span>';

	if (strlen($client['client_name'])>12) {
		echo substr($client['client_name'],0,12) . '...';
	} else {
		echo $client['client_name'];
	}

	echo '</span></a></li>';

	}

} ?>
</ul>
<ul class="visible-tablet">
<?php if ($viewed6) {

	foreach($viewed6 as $client6) {

	echo '<li><a href="'.base_url().'clients/view?id='.$client6['customer_id'].'" rel="tooltip" data-placement="bottom" title="'.$client6['client_name'].'"><span>';

	if (strlen($client6['client_name'])>12) {
		echo substr($client6['client_name'],0,12) . '...';
	} else {
		echo $client6['client_name'];
	}

	echo '</span></a></li>';

	}

} ?>
</ul>

<div style="float:right; margin-top:1px; margin-right:10px; position:relative;">
<form class="form-inline" action="#" method="post">
  <input type="text" name="search_keyword" id="search_keyword" placeholder="Search ..." style="height:20px; width:200px;">
</form>
</div>

</div>
</div>

<div id="globalSearchResults" class="hide">Search for a client, simply enter a name, phone or email address.</div>


<div class="navbar visible-phone">
<div class="navbar-inner">
<a class="brand" href="#">Menu Bar</a>
<div class="container">
<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</a>

<div class="nav-collapse collapse navbar-responsive-collapse">
<ul class="nav">
<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
<li><a href="<?php echo base_url(); ?>clients">Clients</a></li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Recently Viewed Clients <b class="caret"></b></a>
    <ul class="dropdown-menu">
    	<?php foreach($viewed as $phone_client) {
		echo '<li><a href="'.base_url().'clients/view?id='. $phone_client['customer_id'] .'">'. $phone_client['client_name'] .'</a></li>';
		} ?>
    </ul>
</li>
</li>
<li><a href="<?php echo base_url(); ?>pipeline">Sales Pipeline</a></li>
<li><a href="#">Document Library</a></li>
<li><a href="<?php echo base_url(); ?>messages">Messages <span class="badge badge-important" id="mini_header_message"></span></a></li>
<li><a href="#">Marketing</a></li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
    <ul class="dropdown-menu">
    <li><a href="<?php echo base_url(); ?>login/logout">Log Out</a></li>
    </ul>
</li>
</ul>
</div>

</div>
</div>
</div>


