<?php

//if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$icon_set = '-clean';
//} else {
	//$icon_set = '';
//} ?>

        <div id="selectImage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 id="myModalLabel">Change picture for <?php if (!empty($customer_details['company_name'])) { echo $customer_details['company_name']; } else { echo $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?></h4>
        </div>
        <div class="modal-body">
        <h5>Upload image from computer:</h5>
    	<form action="<?php echo base_url(); ?>clients/client_new_image" method="post" enctype="multipart/form-data">
        <input name="profileimage" type="file" /><br />
        <input name="Submit" type="submit" value="Upload Image" class="btn" />
        <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
        </form>
    	<hr />
    	<h5>Images from social media:</h5>
        <a href="#" id="clientImageDefault" data-client="<?php echo $this->input->get('id'); ?>"><img src="<?php echo base_url(); ?>img/summary_person.png" /></a>&nbsp;&nbsp;
        <?php if ($customer_url) {
			foreach ($customer_url as $url) {
				if ($url['website']=="Facebook") {
					$fb_url = explode("/", $url['url']);
					$fb_count = count($fb_url);
						if ($fb_count>3) {
							$fb_img = 'https://graph.facebook.com/'. $fb_url[3] .'/picture?height=70&width=70';
							echo '<a href="#" id="clientImageFb" data-image="'.$fb_img.'" data-client="'. $this->input->get('id') .'"><img src="'. $fb_img .'" /></a>&nbsp;&nbsp;';
						}
				}
			}
		} ?>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" width="20%" valign="top" style="padding-right:10px; padding-bottom:10px;">
			<?php if ($customer_details['account_type']==2) {
				if (isset($customer_photo['image_name'])) {
					echo '<a href="#selectImage" class="client_image_select" data-toggle="modal"><img src="' . base_url() . 'img/profile_images/'.$customer_photo['image_name'].'" /></a>';
				} else {
					echo '<a href="#selectImage" class="client_image_select" data-toggle="modal"><img src="' . base_url() . 'img/summary_comp.png" /></a>';
				}
			} else {
				if (isset($customer_photo['image_name'])) {
					echo '<a href="#selectImage" class="client_image_select" data-toggle="modal"><img src="' . base_url() . 'img/profile_images/'.$customer_photo['image_name'].'" /></a>';
				} else {
					echo '<a href="#selectImage" class="client_image_select" data-toggle="modal"><img src="' . base_url() . 'img/summary_person.png" /></a>';
				}
			} ?></td>
            <td valign="top"><h5 style="margin-top:0px;"><?php if (!empty($customer_details['company_name'])) { echo $customer_details['company_name']; } else { echo $customer_details['title'] . ' ' . $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?></h5>
            <span class="people_lower_name"><?php if ( (!empty($customer_details['organisation'])) && (!empty($customer_details['job_title'])) ) { echo $customer_details['job_title'] . ' at <a href="'.base_url().'clients/view?id='. $customer_details['associated'] .'" target="_self">' . $customer_details['organisation']. '</a>'; }

			if (($customer_details['relationship']=="Spouse/Partner") && ($customer_details['associated']!=0)) {

			echo 'Spouse/Partner of <a href="'. base_url() .'clients/view?id='. $partner_details['partner_id'] .'">'. $partner_details['partner_name'] .'</a>';

			}

			//if ((isset($partner_details)) && ($customer_details['organisation']!="") && ($customer_details['associated']!=0)) {
			//print_r($partner_details);
				//echo $customer_details['relationship'] . ' of ' . '<a href="'. base_url() .'clients/view?id='.$partner_details['partner_id'].'">'.$partner_details['partner_name'].'</a>';
			//} ?></span>
            <br />
            <?php if ($customer_url) {
				foreach ($customer_url as $show_url) {
					if ($show_url['website']=="Facebook") {
						echo '<a href="' . $show_url['url'] . '" target="_blank"><img src="'.base_url().'img/facebook_small_icon2'.$icon_set.'.png"></a>&nbsp;&nbsp;';
					}
					if ($show_url['website']=="Twitter") {
						echo '<a href="' . $show_url['url'] . '" target="_blank"><img src="'.base_url().'img/twitter_small_icon2'.$icon_set.'.png"></a>&nbsp;&nbsp;';
					}
					if ($show_url['website']=="LinkedIn") {
						echo '<a href="' . $show_url['url'] . '" target="_blank"><img src="'.base_url().'img/linkedin_small_icon2'.$icon_set.'.png"></a>&nbsp;&nbsp;';
					}
				}
			} ?>
            </td>
          </tr>
        </table>
        <ul class="contact_info">
        <?php if ($customer_email) {
			foreach($customer_email as $email) {
				echo '<li class="email" style="word-wrap: break-word;"><a href="mailto:'.$email['email'].'?bcc=dropbox.13554456@123crm.co.uk&subject= **enter your subject here** [REF:'.$customer_details['ref'].']">'. $email['email'] .'</a> <span class="label label-info">'. $email['type'] .'</span></li>';
			}
		}

		if ($customer_url) {
			foreach ($customer_url as $show_this_url) {
				if ($show_this_url['website']=="Website") {
					echo '<li class="telephone"><a href="' . $show_this_url['url'] . '" target="_blank">'. $show_this_url['url'] .'</a> <span class="label label-info">'. $show_this_url['type'] .'</span></li>';
				}
			}
		}

		if ($customer_telephone) {
			foreach($customer_telephone as $telephone) {
				echo '<li class="telephone">'. $telephone['number'] .' <span class="label label-info">'. $telephone['type'] .'</span></li>';
			}
		}

		if ($customer_details['address_line_1']) { ?>
        <li class="address"><?php echo nl2br($customer_details['address_line_1']) . '<br />';
        if (!empty($customer_details['address_line_2'])) { echo $customer_details['address_line_2'] . '<br />'; }
        echo $customer_details['town'] . '<br />';
		 if (!empty($customer_details['county'])) { echo $customer_details['county'] . '<br />'; }

		 echo $customer_details['postcode']; ?><br /><span class="address_lower"><a href="http://maps.google.co.uk/maps?hl=en&safe=off&q=<?php echo $customer_details['postcode']; ?>" target="_blank">show on map</a> | <a href="http://maps.google.co.uk/maps?daddr=<?php echo $customer_details['postcode']; ?>" target="_blank">get directions</a></span></li>
        <?php }
		if ($customer_details['companyreg']) { echo '<li>Company Number: <br /><strong>'. $customer_details['companyreg'] .'</strong></li>'; } ?>
        <?php if ($customer_details['companyemployee']) { echo '<li>Number of Employees: <br /><strong>'. $customer_details['companyemployee'] .'</strong></li>'; } ?>
        <?php if ($customer_details['sector']) { echo '<li>Industry Sector: <br /><strong>'. $customer_details['sector'] .'</strong></li>'; } ?>
        <?php if ($customer_details['dob']!="0000-00-00") { echo '<li>Date of Birth: <br /><strong>'. date("d/m/Y", strtotime($customer_details['dob'])) .'</strong></li>'; } ?>
        <?php if ($customer_details['smoker']==1) { echo '<li>Smoker: <br /><strong>Yes</strong></li>'; } ?>
        <?php if ( (!empty($customer_details['marital_status'])) &&($customer_details['marital_status']!="Please Select") ) { echo '<li>Marital Status: <br /><strong>'. $customer_details['marital_status'] .'</strong></li>'; } ?>
        <?php if ($customer_details['living_status']) { echo '<li>Living Status: <br /><strong>'. $customer_details['living_status'] .'</strong></li>'; } ?>
        <?php if ($customer_details['employment_status']) { echo '<li>Employment Status: <br /><strong>'. $customer_details['employment_status'] .'</strong></li>'; } ?>
        <?php if ($customer_details['occupation']) { echo '<li>Occupation: <br /><strong>'. $customer_details['occupation'] .'</strong></li>'; } ?>
        </ul>

          <?php if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_edit")==1) ) {

		  if ($customer_details['account_type']==2) { ?>
          <p style="margin-top:10px;"><a href="<?php echo base_url(); ?>clients/edit_company?id=<?php echo $this->input->get('id'); ?>">Edit client information</a></p>
          <?php } else {  ?>
          <p style="margin-top:10px;"><a href="<?php echo base_url(); ?>clients/edit_client?id=<?php echo $this->input->get('id'); ?>">Edit client information</a></p>
          <?php }


		  }
		  ?>

          </div>
        </div>

        <div class="row-fluid">
          <div class="span12 well"><h5 style="margin-top:0px;">Tasks</h5>
			<?php if ($customer_tasks) {

            foreach($customer_tasks as $task) {

                echo '<span class="label label-info">' . $task['action_name'] .'</span> - <strong><a href="#" id="'.$task['id'].'" data-toggle="modaledit">' . $task['name'] . '</a></strong><br /> Date: ' . date("d M y G:i", strtotime($task['date'])) . '<br />';

                }

            } else { echo "No tasks for this client found."; } ?>
          <p style="margin-top:10px;"><a href="#createNewTask" data-toggle="modal"><img src="<?php echo base_url(); ?>img/small-add-icon<?php echo $icon_set; ?>.png" />&nbsp;Create new task for client</a></p>
          </div>
        </div>


        <div class="row-fluid">
          <div class="span12 well"><h5 style="margin-top:0px;">
          <?php if ($customer_details['account_type']==2) {

          		echo 'People working at '. $customer_details['company_name'];

			} else {

				echo 'Family';

			} ?></h5>

			<?php if ($associated_people) {

			//echo $customer_details['account_type'];

			echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">';

				foreach ($associated_people as $person) {

					if ($customer_details['account_type']==2) {

					echo '<tr>
						<td><strong><a href="'. base_url().'clients/view?id='. $person['id'] .'">'. $person['first_name']. ' ' .$person['last_name'] . '</a></strong><br />' . $person['job_title'] .'</td>
						<td><a href="'.base_url().'clients/delete_person?id='.$this->input->get('id').'&person='.$person['id'].'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this contact?\')"><img src="'.base_url().'img/delete-icon'.$icon_set.'.png" /></a></td>
					  </tr>';

					  } else {

					echo '<tr>
						<td><strong>';

						if ( ($person['relationship']=="Son") || ($person['relationship']=="Daughter") ) {
							echo $person['first_name']. ' ' .$person['last_name'];
						} else {
							echo '<a href="'. base_url().'clients/view?id='. $person['id'] .'">'. $person['first_name']. ' ' .$person['last_name'] . '</a>';
						}


					echo '</strong>';

					if ($person['dob']!="0000-00-00") {
					 echo ' - ' . date("d/m/Y", strtotime($person['dob']));
					}

					echo ' - '. $person['relationship'];

					if ($person['occupation']) {
						echo '<br />Occupation: ' . $person['occupation'];
					}

					echo '</td>
						<td><a href="'.base_url().'clients/delete_family_member?id='.$this->input->get('id').'&family='.$person['id'].'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this contact?\')"><img src="'.base_url().'img/delete-icon'.$icon_set.'.png" /></a></td>
					  </tr>';

					  }

				}

				echo '</table>';

			} else {

				echo "No people found.";

			} ?>

            <?php if ( ($customer_details['account_type']==1) || ($customer_details['account_type']==3) ) {
            echo '<p style="margin-top:10px;"><a href="'.base_url().'clients/new_family?id='.$this->input->get('id').'"><img src="'.base_url(). 'img/small-add-icon'.$icon_set.'.png" />&nbsp;Create new person</a></p>';
			} else {
            echo '<p style="margin-top:10px;"><a href="'.base_url().'clients/new_person?id='.$this->input->get('id').'"><img src="'.base_url(). 'img/small-add-icon'.$icon_set.'.png" />&nbsp;Create new person</a></p>';
			} ?>
          </div>
        </div>

		<?php if ($customer_details['account_type']==2) {
        echo '<div class="row-fluid">
          <div class="span12 well">
          <h5 style="margin-top:0px;">Company Information</h5>';
		  if ($customer_details['duedil_company_details']) {
			  //build into array
			  $duedil = json_decode($customer_details['duedil_company_details'],true);
			  //print_r($duedil);
			  echo '<strong>Company Name:</strong> ' . $duedil['name_formatted'] . '<br />
			  <strong>Company Number:</strong> ' . $duedil['company_number'] . '<br />
			  <strong>Registered Address</strong> ' . $duedil['registered_address']['string'] . '<br />
			  <strong>Category</strong> ' . $duedil['category'] . '<br />
			  <strong>Trading Status</strong> ' . $duedil['status'] . '<br />
			  <strong>Incorporation Date</strong> ' . date("jS F Y", strtotime($duedil['incorporation_date'])) . '<br />';
		  }
          echo '</div>
        </div>';
		} ?>


        <div class="row-fluid">
          <div class="span12 well">
          <h5 style="margin-top:0px;">Background Information</h5>
          <?php echo $customer_details['background_info'];
		  if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_edit")==1) ) { ?>
          <p style="margin-top:10px;"><a href="#updateBackgound" data-toggle="modal">Edit background information</a></p>
          <?php } ?>
          </div>
        </div>


      </div>
