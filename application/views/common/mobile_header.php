<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Suite</title>
<?php echo link_tag('favicon.ico', 'shortcut icon', 'image/ico'); ?>
<link href="<?php echo base_url(); ?>css/mobile.css" rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div id="container">

<div id="top_bar">
<?php if ($this->router->fetch_class()!="dashboard") {
echo '<a href="#" class="back_button" onclick="javascript:window.history.back(-1);return false;">Back</a>';
} ?>
<h1>One23 CRM</h1>

</div>

<div id="content_container">
