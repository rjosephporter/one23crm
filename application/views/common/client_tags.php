<div id="client_display_name"><?php if (!empty($customer_details['company_name'])) { echo $customer_details['company_name']; } else { echo $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?></div>

<div id="sendClientEmail" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/send_new_email" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4>Send Email</h4>
        </div>
        <?php if ($email_templates) { ?>
        <div class="modal-header">       
		Select Template (if required):<br />
		<select id="sendemail_template" style="padding:0; height:23px;">
		<?php            
            echo '<option value="0">No template required</option>';
        
            foreach($email_templates as $template) {
                echo '<option value="'. $template['id'] .'">'. $template['name'] .'</option>';
            } ?>
                </select>
        </div> 
        <?php } ?>    
       
        <div class="modal-body">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">
                To:<br />
                <input type="text" id="sendemail_to" name="to" value="<?php if ($customer_email) { echo $customer_email[0]['email']; } ?>" />
                </td>
                <td width="50%">
                Subject:<br />
                <input type="text" id="sendemail_subject" name="subject" /> 
                </td>
              </tr>
            </table>
            Email Body:<br />
			<!--
            <textarea name="body" id="sendemail_body"></textarea>
            <script>
                //CKEDITOR.replace( 'sendemail_body' );
				//var editor = CKEDITOR.replace( 'sendemail_body' );
				//CKFinder.setupCKEditor( editor, '/plugin/ckfinder/' );
				CKEDITOR.replace( 'sendemail_body',
				{
					toolbarCanCollapse : true,
					toolbarStartupExpanded : false,
					filebrowserBrowseUrl : '/plugin/ckfinder/ckfinder.html',
					filebrowserImageBrowseUrl : '/plugin/ckfinder/ckfinder.html?type=Images',
					filebrowserFlashBrowseUrl : '/plugin/ckfinder/ckfinder.html?type=Flash',
					filebrowserUploadUrl : '/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					filebrowserImageUploadUrl : '/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
					filebrowserFlashUploadUrl : '/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
					filebrowserWindowWidth : '1000',
					filebrowserWindowHeight : '700'
				});
            </script>
            -->
            <textarea class="wysiwygeditor" name="body" id="sendemail_body" rows="6" style="width:90%;"></textarea>
			<br />
            Attach Files:<br />
            <select id="attach_files_list" style="padding:0; height:23px;">
            <?php if ($email_attachments) {
				echo '<option value="0">Select attachment if required</option>';		
				echo $email_attachments;
			} ?>
            </select> <div class="btn btn-mini" id="attach_files_button" style="margin-top:-10px;">attach this file</div>
            <div id="attached_files"></div>
            <br />
            Select Signature (applied when sent):<br />
            <select id="signature" name="signature" style="padding:0; height:23px;">
            <?php if ($email_signatures) {
				
				echo '<option value="0">No signature required</option>';
			
				foreach($email_signatures as $sig) {
					echo '<option value="'. $sig['id'] .'">'. $sig['name'] .'</option>';
				}
			
			} else {
			
				echo '<option value="">No signatures found</option>';
				
			} ?>
            </select>
            <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
            <input type="hidden" name="client_ref" value="[REF:<?php echo $customer_details['ref']; ?>]" />            
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn" id="sendEmailToClient">Send</button>            
        </div>
    </form>
</div>
    
<div id="client_tag_area">
<img src="<?php echo base_url(); ?>img/small-tag-icon.png" />&nbsp;&nbsp;
<?php if ($customer_tags) {
	foreach($customer_tags as $tag) {	
		echo $tag['tag_name'] . ',&nbsp;&nbsp;&nbsp;';	
	}
} ?>
<a href="#" id="showEditClientTags">Edit Tags</a>
</div>
<div id="client_tag_area_edit">
&nbsp;<img src="<?php echo base_url(); ?>img/small-tag-icon.png" />
<?php if ($customer_tags) {
	foreach($customer_tags as $tag_edit) {	
		echo $tag_edit['tag_name'] . ' <a href="#" title="Remove this tag" class="removeClientTag" data-tag="'. $this->input->get('id'). '_' .$tag_edit['tag_id'] .'"><img src="'.base_url().'img/delete-icon.png" /></a>,&nbsp;&nbsp;&nbsp;';	
	}
} ?>
</div>
<div id="client_tag_selection">
	<form id="add_client_tag" name="add_client_tag" action="#" class="form-inline" style="margin:0;">
    <select id="add_client_tag_ref" name="add_client_tag_ref" class="span2" style="height:24px; padding:0px; margin:0px;">
    <option value=""></option>
	<?php if ($all_tags) {
        foreach($all_tags as $the_tag) {	
            echo '<option value="'. $the_tag['id'] .'">'. $the_tag['tag'] .'</option>';	
        }
    } ?>
    </select>
    <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
    <button type="submit" id="addClientTag">Add Tag</button>
    <a href="#" id="closeClientTags">Close</a>
    </form>
</div>
<div style="padding-top:12px;">
&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url(); ?>img/small-send-email.png" />&nbsp;&nbsp;<a href="#sendClientEmail" data-toggle="modal">Send Email</a>
</div>
