<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Suite</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
<link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
<?php echo link_tag('favicon.ico', 'shortcut icon', 'image/ico'); ?>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/common.js"></script>
<script src="<?php echo base_url(); ?>js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div id="header_container">  
<div id="cssmenu">
<ul>
   <li><a href="<?php echo base_url(); ?>dashboard" rel="tooltip" data-placement="bottom" title="Dash"><img src="<?php echo base_url(); ?>img/nav-home.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>clients" rel="tooltip" data-placement="bottom" title="Clients"><img src="<?php echo base_url(); ?>img/nav-clients.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>calender" rel="tooltip" data-placement="bottom" title="Calendar"><img src="<?php echo base_url(); ?>img/nav-calendar.png"/></a></li>
   <li><a href="<?php echo base_url(); ?>pipeline" rel="tooltip" data-placement="bottom" title="Sales Pipeline"><img src="<?php echo base_url(); ?>img/nav-pipeline.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>document_library" rel="tooltip" data-placement="bottom" title="Document Library"><img src="<?php echo base_url(); ?>img/nav-docs.png" /></a></li>
   <li><a href="#" rel="tooltip" data-placement="bottom" title="Quick Quote"><img src="<?php echo base_url(); ?>img/nav-quote.png" /></a></li>
   <li><a href="<?php echo base_url(); ?>messages" rel="tooltip" data-placement="bottom" title="Messages"><img src="<?php echo base_url(); ?>img/nav-messages.png" /></a></li>
   <li><a href="#" rel="tooltip" data-placement="bottom" title="Marketing"><img src="<?php echo base_url(); ?>img/nav-marketing.png" /></a></li>
</ul>

<div style="float:right; margin-top:5px; margin-right:5px; padding:0 10px;">
<img src="<?php echo base_url(); ?>img/nav-settings.png" title="Settings" />
</div>

<div style="float:right; margin-top:5px; padding:0 10px;">
<a href="<?php echo base_url(); ?>login/logout"><img src="<?php echo base_url(); ?>img/nav-user.png" title="My Account" /></a>
</div>

<div style="float:right; margin-top:5px; padding:0 10px;">
<a target="_self" href="#addNew" data-toggle="modal"><img src="<?php echo base_url(); ?>img/nav-add.png" title="Add New" /></a>
</div>

<div style="float:right; margin-top:5px; margin-right:100px;">
<form class="form-inline" action="<?php echo base_url(); ?>clients" method="get">
  <input type="text" name="search_keyword" id="search_keyword" class="span2" placeholder="Search ..." style="height:30px;">
  <button type="submit" class="btn">Go</button>
</form>
</div>
</div>


</div>

