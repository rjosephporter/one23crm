    <div id="addNew" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel">What would you like to do?</h4>
    </div>
    <div class="modal-body">
    <p><a href="<?php echo base_url(); ?>clients/create_company">Create a new company client</a></p>
    <p><a href="<?php echo base_url(); ?>clients/create">Create a new person/private client</a></p>
    <p><a href="#createNewTask" data-toggle="modal">Create a new task</a></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>

    <div id="taskNotifications" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel">Current Task Notifications</h4>
    </div>
    <div class="modal-body">
		<div id="task_notification_list" style="min-height:200px;">
        </div>
    </div>
    <div class="modal-footer">
    <div class="dropdown btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
            Do Not Show Again
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" style="text-align:left;">
            <li><a href="#" id="taskNotificationHide30">For 30 Mins</a></li>
            <li><a href="#" id="taskNotificationHide60">For 1 Hour</a></li>
			<li><a href="#" id="taskNotificationHide300">For 5 Hours</a></li>
        </ul>
    </div>
    <!--<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>-->
    </div>
    </div>

    <div id="uploadFile" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/uploadFile" method="post" enctype="multipart/form-data" name="newFile" id="newFile">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Upload a new file</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="10" cellpadding="0">
      <tr>
        <td width="15%">Description:</td>
        <td><input name="name" id="name" type="text" /></td>
      </tr>
      <tr>
        <td width="15%">Select a File:</td>
        <td><input name="fileupload" id="fileupload" type="file" /></td>
      </tr>
      <tr>
        <td width="15%">Section:</td>
        <td><select name="type" id="type">
          <option value="0">Please Select</option>
          <!--<option value="1">Policy Documents</option>-->
          <option value="2">Illustrations / Comparisons / Applications</option>
          <option value="3">Compliance Documents</option>
          <option value="4">Miscellaneous Documents</option>
        </select></td>
      </tr>
    </table>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />
    </div>
    <div class="modal-footer">
    <button id="modalUploadNewFile" class="btn">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </form>
    </div>
    </div>

    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>clients/create_note" method="post" enctype="multipart/form-data" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel">Add note to account</h4>
    </div>
    <div class="modal-body">Please enter a note to be added to the client account.
        <div class="control-group" style="padding-top:10px;">
            <label class="control-label"></label>
          <div class="controls">
			<textarea name="note" rows="4" id="note" class="span5"></textarea>
            <input type="hidden" id="notefor" name="notefor" value="<?php echo $this->input->get('id', TRUE); ?>" />
          </div>
          <div class="controls">
			Upload a file: <input name="note_file" type="file" id="note_file" />
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <button id="modalCreateClientNote" class="btn">Add Note</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
    </form>
  </div>

	<?php if (isset($customer_details)) { ?>
    <div id="updateBackgound" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_backgound" method="post" name="updatebkgd" id="updatebkgd">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Update background info</h4>
    </div>
    <div class="modal-body">
 	<textarea name="info" rows="6" id="info" class="span5"><?php echo $customer_details['background_info']; ?></textarea>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />

    </div>
    <div class="modal-footer">
    <button id="modalCreateClientNote" class="btn">Update</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </form>
    </div>
    </div>
    <?php } ?>

    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" />
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task for client </h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td width="20%" valign="middle">Link To:</td>
        <td valign="middle">
		  <div id="link_to_client">
			  <input class="typeahead getclient" name="getclient" type="text" autocomplete="off">
			  <input type="hidden" name="customer_id" id="customer_id" value="0">
   		  </div>
        </td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1" style="padding:0; height:23px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="task_min" id="task_min" class="span1" style="padding:0; height:23px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action" style="padding:0; height:23px;">
          <option value="0">Please Select</option>
          <?php if( $task_setting->num_rows > 0 ){ ?>
					<?php foreach($task_setting->result() as $row){ ?>
						<option value="<?php echo $row->id;?>"><?php echo $row->action_name;?></option>
					<?php } ?>
          <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td>Reminder:</td>
        <td>
        <select name="task_remind" id="task_remind" style="height:22px; padding:0px;">
          <option value="0">Do Not Remind</option>
          <option value="5">Remind me 5 minutes before</option>
          <option value="15">Remind me 15 minutes before</option>
          <option value="30">Remind me 30 minutes before</option>
          <option value="60">Remind me 60 minutes before</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button id="modalCreateClientTask" class="btn">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <input type="hidden" name="return" value="dashboard" />
    </form>
    </div>
    </div>

    <div id="editTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_task" method="post" name="" id="">
    <input type="hidden" id="edit_task_id" name="edit_task_id" />
    <?php if(isset($client_id)) { ?>
		<input type="hidden" name="return" value="clients/view?id=<?php echo isset($client_id) ? $client_id:'';?>">
    <?php } ?>
    <input type="hidden" name="edit_client_task" id="edit_client_task">
	<input type="hidden" name="edit_customer_id" id="edit_customer_id" value="0">
	<input type="hidden" name="getclient" class="editgetclient">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Edit task</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="edit_task_name" id="edit_task_name" class="span4" /></td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="edit_task_date" id="edit_task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="edit_task_hour" id="edit_task_hour" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="edit_task_min" id="edit_task_min" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        <label class="checkbox">
          <input type="checkbox" name="edit_not_required"> Not Required
        </label>

        </td>
      </tr>

      <tr>
        <td>End Time:</td>
        <td>
          <select name="edit_task_hour_end" id="edit_task_hour_end" class="span1">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="edit_task_min_end" id="edit_task_min_end" class="span1">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>

      <tr>
        <td>Action:</td>
        <td>
        <select name="edit_task_action" id="edit_task_action" style="height:22px; padding:0px;">
          <option value="0">Please Select</option>
          <?php if( isset($task_setting) ) { ?>
			  <?php if( $task_setting->num_rows > 0 ){ ?>
						<?php foreach($task_setting->result() as $row){ ?>
							<option value="<?php echo $row->id;?>"><?php echo $row->action_name;?></option>
						<?php } ?>
			  <?php } ?>
          <?php } ?>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    <div class="pull-right">
        <button class="btn" id="completeTask">Complete Task</button>
        <button class="btn" id="deleteTask">Delete</button>
	</div>
    <input type="hidden" name="return" value="clients/view?id=<?php echo $this->input->get('id'); ?>" />
    </form>
    </div>
    </div>
</div>
<audio id="soundFX">
<source src="<?php echo base_url();?>audio/notify.mp3" type="audio/mpeg" />
<source src="<?php echo base_url();?>audio/notify.ogg" type="audio/ogg" />
<source src="<?php echo base_url();?>audio/notify.wav" type="audio/wav" />
Update your browser to enjoy HTML5 audio!
</audio>
<!-- typehead -->
<script src="<?php echo base_url('plugin/typeahead.js/typeahead.bundle.js'); ?>"></script>
<script src="<?php echo base_url('js/allanjs.js'); ?>"></script>
<link href='<?php echo base_url(); ?>/css/typeahead.css' rel='stylesheet' />
<!-- typehead -->
<script>
$('input[name="not_required"], input[name="edit_not_required"]').change(function() {
  console.log(this.checked);
  var prefix = ($(this).attr('name') == 'not_required') ? '' : 'edit_' ;
  if(this.checked) {
    $('#'+prefix+'task_hour').prop('disabled', true).val('00');
    $('#'+prefix+'task_min').prop('disabled', true).val('00');
    $('#'+prefix+'task_hour_end').prop('disabled', true).val('00');
    $('#'+prefix+'task_min_end').prop('disabled', true).val('30');
  } else {
    $('#'+prefix+'task_hour').prop('disabled', false).val('00');
    $('#'+prefix+'task_min').prop('disabled', false).val('00');
    $('#'+prefix+'task_hour_end').prop('disabled', false).val('00');
    $('#'+prefix+'task_min_end').prop('disabled', false).val('00');
  }
});
</script>
</body>
</html>
