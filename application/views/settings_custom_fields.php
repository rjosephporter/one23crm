    <div id="newTab" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_custom_tab" method="post" name="" id="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Custom Client Tab</h4>
    </div>
    <div class="modal-body">
    <label>Tab Name:<br />
    <input type="text" name="name" id="name" class="span4" placeholder="Enter custom tab name" />
    </label>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
    </div>
    </div>

    <div id="container_top">
<h4>Custom Fields</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li class="dropdown active">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Custom Fields <b class="caret"></b></a>
    <ul class="dropdown-menu">
    	<li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Tabs</a></li>
        <li><a href="<?php echo base_url(); ?>settings/custom_forms">Custom Forms</a></li>
    </ul>
    </li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Custom Tabs</h5>
        	<?php if ($tabs) {

				echo '<table width="50%" class="table">';

					foreach($tabs as $tab) {

						echo '<tr>
						<td width="85%"><strong>'. $tab['name'] .'</strong></td>
						<td align="center" style="text-align:center;">
						<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Edit</a> | <a href="'. base_url() .'settings/delete_custom_tab?id='. $tab['tab_ref'] .'" onClick="return confirm(\'Are you sure you wish to delete this custom tab?\')">Remove</a>
						</td>
						</tr>';

					}

				echo '</table>';

			} else {

				echo 'No custom tabs have been created.';

			} ?>
			</div>
        </div>
        <p><a href="#newTab" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add custom tab</a></p>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Client Tabs</h5>
            <p>Select the tabs you wish to appear in the client managment area.</p>
            <form action="<?php echo base_url(); ?>settings/update_tabs" method="post">

            <label class="checkbox">
            <input type="checkbox" name="files_tab" value="1" <?php if ($this->session->userdata("files_tab")==1) { echo 'checked="checked"'; } ?>> Files Tab
            </label>

            <label class="checkbox">
            <input type="checkbox" name="messages_tab" value="1" <?php if ($this->session->userdata("messages_tab")==1) { echo 'checked="checked"'; } ?>> Messages Tab
            </label>

            <label class="checkbox">
            <input type="checkbox" name="people_tab" value="1" <?php if ($this->session->userdata("people_tab")==1) { echo 'checked="checked"'; } ?>> People Tab (shown on company only)
            </label>

            <label class="checkbox">
            <input type="checkbox" name="opportunities_tab" value="1" <?php if ($this->session->userdata("opportunities_tab")==1) { echo 'checked="checked"'; } ?>> Opportunities Tab
            </label>

            <label class="checkbox">
            <input type="checkbox" name="live_tab" value="1" <?php if ($this->session->userdata("live_tab")==1) { echo 'checked="checked"'; } ?>> Live Documents  Tab
            </label>

            <input type="submit" value="Save Tab Settings" class="btn" />
            </form>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Client Files</h5>
            <p>Customise client file sections.</p>
            <form action="<?php echo base_url(); ?>settings/save_custom_files" method="post">
            <label>Section 1 Name:</label>
            <input type="text" name="section1" value="Custom Form Files" placeholder="Enter a name for section 1" readonly="readonly" />
            <label>Section 2 Name:</label>
            <input type="text" name="section2" value="<?php echo $files['files_2']; ?>" placeholder="Enter a name for section 2" />
            <label>Section 3 Name:</label>
            <input type="text" name="section3" value="<?php echo $files['files_3']; ?>" placeholder="Enter a name for section 3" />
            <label>Section 4 Name:</label>
            <input type="text" name="section4" value="<?php echo $files['files_4']; ?>" placeholder="Enter a name for section 4" />
            <label>Section 5 Name:</label>
            <input type="text" name="section5" value="<?php echo $files['files_5']; ?>" placeholder="Enter a name for section 5" />
            <label>Section 6 Name:</label>
            <input type="text" name="section6" value="<?php echo $files['files_6']; ?>" placeholder="Enter a name for section 6" />
            <p><input type="submit" class="btn" value="Save Changes" /></p>
            </form>
            </div>
        </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">Custom Fields</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
