<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name = $customer_details['company_name']; } else { $customer_name = $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?>

<div id="container_top">
<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
    <li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <?php if ($customer_details['account_type']==2) { ?>
    <li class="active"><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
    <?php } ?>
    <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
    <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>    
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  
      <div class="row-fluid">
      	<div class="span12"><p>
        <?php if ($customer_details['account_type']==2) { 
        
			echo '<a target="_self" href="'. base_url() .'clients/new_employee?id='. $this->input->get('id') .'" class="btn"><img class="btn_with_icon" src="'. base_url().'img/add-icon.png" />&nbsp;&nbsp;Add New Employee</a>&nbsp;&nbsp;<a target="_self" href="'. base_url() .'clients/import_employee?id='. $this->input->get('id') .'" class="btn"><img class="btn_with_icon" src="'. base_url().'img/add-icon.png" />&nbsp;&nbsp;Import Employee List</a>';
		
		} else {
		
			echo '<a target="_self" href="'. base_url() .'clients/new_family?id='. $this->input->get('id') .'" class="btn"><img class="btn_with_icon" src="'.base_url().'img/add-icon.png" />&nbsp;&nbsp;Add Family Member</a>';
		
		} ?></p></div>
      </div>  
  
        <div class="row-fluid">
        	<div class="span12 well"><h5 style="margin-top:0px;">People associated with <?php echo $customer_name; ?></h5>
			<?php if ($customer_details['account_type']==1) { 
			
				if ($associated_people) {
				
				echo '<table width="100%" class="table">
					  <thead>
						<tr>
						  <th>Family Member Name</th>
						  <th>Date of Birth</th>
						  <th>Relationship to client</th>
						  <th></th>
						</tr>
					  </thead><tbody>';
					  			  
					foreach ($associated_people as $person) {
					
						echo '<tr>
								<td><a href="'.base_url().'clients/edit_family?id='.$this->input->get('id').'&family='.$person['id'].'">'. $person['title'] .' '. $person['first_name'] .' '. $person['last_name'] .'</a></td>
								<td>'.  date("d/m/Y", strtotime($person['dob'])) .'</td>
								<td>'. $person['relationship'] .'</td>
								<td><a href="'. base_url() .'clients/delete_family_member?id='. $person['associated'] .'&family='. $person['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this family member?\')"><img src="'.base_url().'/img/delete-icon'. $icon_set .'.png" /></a></td>								
							  </tr>';
					
					}
				  
				  echo '</tbody></table>';			
				
				} else {
				
					echo 'Currently no people associated with this client.';			
				
				}
			
			} elseif ($customer_details['account_type']==2) {  
			
			if ($employee_list) {
			
			echo '<table width="100%" class="table">
				  <thead>
					<tr>
					  <th>Employee Name</th>
					  <th>Job Title</th>
					  <th>Date of Birth</th>
					  <th>Address</th>
					  <th></th>
					</tr>
				  </thead><tbody>';
			
				foreach($employee_list as $employee) {
				
					if ($employee['dob']!="") {
						$employee_dob = date("d/m/Y", strtotime($employee['dob']));
					} else {
						$employee_dob = " - ";
					}
				
					echo '<tr><td>'. $employee['first_name'] .' '. $employee['last_name'] .'</td><td>'. $employee['job_title'] .'</td><td>'. $employee_dob .'</td><td>'. $employee['address'] .', '. $employee['town'] .', '. $employee['county'] .', '. $employee['postcode'] .'</td>';					
					if (isset($employee['id'])) {
					echo '<td><a href="'.base_url().'clients/edit_employee?id='. $employee['customer_id'] .'&employee='. $employee['id'] .'"><img src="'. base_url(). 'img/edit-icon.png" /></a>&nbsp;<a href="'. base_url() .'clients/delete_employee?id='. $employee['customer_id'] .'&employee='. $employee['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this employee?\')"><img src="'.base_url().'/img/delete-icon'. $icon_set .'.png" /></a></td>';
					} else { echo '<td><a href="'.base_url().'clients/edit_client?id='.$employee['customer_id'].'&return=people&returnid='. $this->input->get('id') .'"><img src="'. base_url(). 'img/edit-icon.png" /></a></td>'; }
				
				}
			
			echo '</tbody></table>';
			
			} 
			
			} ?>
            </div>
          </div>
      	</div>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
