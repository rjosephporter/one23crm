<?php if ($messages) {
echo '<ul class="wide_list">';
	
	foreach($messages as $message) {
	
		echo '<li class="arrow"><a href="'.base_url().'messages/view?id='. $message['id'] .'">' . $message['subject'] . '<br /><small>Date: '. date("d/m/y H:i", strtotime($message['added_date'])) .' | From: '. $message['sender'] .'</small></a></li>';

	}

echo '</ul>';

} else {

echo '<h3>No messages found.</h3>';

}?>
