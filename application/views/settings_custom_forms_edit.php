<?php if (!$form_details) {
	redirect('settings/custom_forms');
} ?>

<div id="container_top">
<h4>Custom Fields</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li class="dropdown active">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Custom Fields <b class="caret"></b></a>
    <ul class="dropdown-menu">
    	<li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Tabs</a></li>
        <li><a href="<?php echo base_url(); ?>settings/custom_forms">Custom Forms</a></li>
    </ul>
    </li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Custom Forms</h5>
            <form action="<?php echo base_url(); ?>settings/custom_forms_update" method="post">
            <label>Name:<br />
            <input type="text" name="name" placeholder="Enter a name" value="<?php echo $form_details['name']; ?>" />
            </label>
            <label>Description:<br />
            <input type="text" name="desc" placeholder="Enter a description" value="<?php echo $form_details['desc']; ?>" />
            </label>
            <hr />
            <div id="form_parts">
            <?php if ($form_options) {
				$optioncount = 0;
				foreach($form_options as $option) {
					$opt_values = "";
					$values = json_decode($option['value'],true);
					if ($values) {
						foreach($values as $opt_value) {
							$opt_values .= $opt_value .';';
						}
						$opt_values = rtrim($opt_values, ';');
					}

				echo '<div id="item_'. $optioncount .'" class="form_item sortable_item_holder">
				<div class="removeFormItem" data-remove="'. $optioncount .'">remove this form item</div>
				<label>Item Type:<br /><select name="item_type[]" style="padding:0; height:23px;" class="type" data-id="'. $optioncount .'"><option value="1"';
				if ($option['type']==1) { echo ' selected="selected"'; }
				echo '>Text Field</option><option value="4"';
				if ($option['type']==4) { echo ' selected="selected"'; }
				echo '>Textarea - Multi Line Text</option><option value="2"';
				if ($option['type']==2) { echo ' selected="selected"'; }
				echo '>Dropdown Menu</option><option value="3"';
				if ($option['type']==3) { echo ' selected="selected"'; }
				echo '>Checkbox</option><option value="5"';
				if ($option['type']==5) { echo ' selected="selected"'; }
				echo '>Date Field</option><option value="6"';
				if ($option['type']==6) { echo ' selected="selected"'; }
				echo '>Text Line / Heading</option></select></label>
				<label>Item Name:<br /><input type="text" name="item_name[]" placeholder="Enter name for this item" value="'. $option['label'] .'" /></label>
				<label id="item_placeholder_'. $optioncount .'" style="'; if ($option['type']!=1) { echo 'display:none;'; } echo '">Item Placeholder:<br /><input type="text" name="item_placeholder[]" placeholder="Enter placeholder text" value="'. $option['placeholder'] .'" /></label>
				<label id="item_values_'. $optioncount .'" style="'; if ($option['type']!=2) { echo 'display:none;'; } echo '">Item Values (separate values with semicolon):<br /><input type="text" name="item_values[]" placeholder="Enter dropdown values" value="'. $opt_values .'" /></label><br /></div>';



				$optioncount++;
				}
			} ?>
            </div>
            <p id="addFormPart"><img src="<?php echo base_url(); ?>img/add-icon.png" /> add form item</p>
            <input type="hidden" name="id" value="<?php echo $this->input->get('id'); ?>" />
            <input type="submit" value="Save"  class="btn" />
			</form>
            </div>
        </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">Custom Forms</h5>
            <p></p>
        </div>
    </div>

<script>

	var formCounter = <?php if ($form_options) { echo count($form_options); } else { echo 0; } ?>;

	$("#addFormPart").click(function() {
		var newPart = "";
		newPart = '<div id="item_'+formCounter+'" class="form_item sortable_item_holder"><label>Item Type:<br /><select name="item_type[]" style="padding:0; height:23px;" class="type" data-id="'+formCounter+'"><option value="1">Text Field</option><option value="4">Textarea - Multi Line Text</option><option value="2">Dropdown Menu</option><option value="3">Checkbox</option><option value="5">Date Field</option><option value="6">Text Line / Heading</option></select></label><label>Item Name:<br /><input type="text" name="item_name[]" placeholder="Enter name for this item" /></label><label id="item_placeholder_'+formCounter+'">Item Placeholder:<br /><input type="text" name="item_placeholder[]" placeholder="Enter placeholder text" /></label><label id="item_values_'+formCounter+'">Item Values (separate values with semicolon):<br /><input type="text" name="item_values[]" placeholder="Enter dropdown values" /></label></div>';

		$("#form_parts").append(newPart);
		$("#item_values_"+formCounter).hide();
		++formCounter;

	});

	$(document).on('change', '.type', function() {
		var type = $(this).val();
		var id = $(this).data('id');

		if (type==1) {
			$("#item_placeholder_"+id).show();
			$("#item_values_"+id).hide();
		} else if (type==4) {
			$("#item_placeholder_"+id).show();
			$("#item_values_"+id).hide();
		} else if (type==2) {
			$("#item_placeholder_"+id).hide();
			$("#item_values_"+id).show();
		} else if (type==3) {
			$("#item_placeholder_"+id).hide();
			$("#item_values_"+id).hide();
		} else if (type==5) {
			$("#item_placeholder_"+id).show();
			$("#item_values_"+id).hide();
		} else if (type==6) {
			$("#item_placeholder_"+id).hide();
			$("#item_values_"+id).hide();
		}

	});

	$(".removeFormItem").click(function() {
		var id = $(this).data('remove');
		$("#item_"+id).remove();
	});

	$("#form_parts").sortable({
		items: ".form_item",
		distance: 0,
		placeholder: "sortable_placeholder"
	});

</script>

<?php require("common/footer.php"); ?>
