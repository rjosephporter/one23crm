<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Reset</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/ico" />
<style>
body {
background-color:#0b9fe5;
}
.login-logo {
width:411px;
margin:0 auto;
position:relative;
background:url(<?php echo base_url(); ?>img/login_logo.png) top center;
height:208px;
}

.login-page {
width:400px;
margin:0 auto;
position:relative;
margin-top:20px;
}

.login-page h2,h3 {
margin-top:0px;
}

.ip {
margin-top:15px;
color:#999;
}

.forgot {
padding-top:20px;
}

.well {
padding-bottom:0px;
}
</style>
</head>

<body>
<div class="login-logo"></div>
<div class="well login-page">
<?php if (isset($_GET['unknown'])) { echo '<div class="alert alert-error">The email address enter does not match our records.</div>'; } ?>
<h3>Password Reset</h3>
<p>Please enter the email address registered with us.</p>
    <form action="<?php echo base_url(); ?>login/password_reset" method="post">
    <div class="row-fluid">
      <div class="span3">Email Address:</div>
      <div class="span9"><input type="text" name="email" placeholder="Enter your email address" /></div>
    </div>
    <button type="submit" class="btn">Request Password</button>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>login" class="btn">Return to Login</a>
    </form>
</div>
</body>
</html>
