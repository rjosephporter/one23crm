<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!$employee) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name =  $customer_details['company_name']; } else { $customer_name =  $customer_details['first_name'] . ' ' . $customer_details['last_name']; }

if ($this->input->get('result', TRUE)=="added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="note_added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client note has successfully been added.</div>'; } ?>

<div id="container_top">
<h4><?php echo $customer_name; ?></h4>

    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>clients/create_note" method="post" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Add note to account</h3>
    </div>
    <div class="modal-body">Please enter a note to be added to the client account.
        <div class="control-group" style="padding-top:10px;">
            <label class="control-label"></label>
          <div class="controls">
			<textarea name="note" rows="6" id="note"></textarea>
            <input type="hidden" id="notefor" name="notefor" value="<?php echo $this->input->get('id', TRUE); ?>" />       
          </div>
        </div>      
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button id="modalCreateClientNote" class="btn btn-primary">Add Note</button>
    </div>
    </form> 
    </div>
    <div id="updateBackgound" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_backgound" method="post" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Update background info</h4>
    </div>
    <div class="modal-body">
 	<textarea name="info" rows="6" id="info" class="span5"><?php echo $customer_details['background_info']; ?></textarea>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />   
    
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientNote" class="btn btn-primary">Update</button>
    </form>
    </div>
    </div>  
    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" /> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task for client</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>          
        </select>
        
        <select name="task_min" id="task_min" class="span1">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>      
        
        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action">
          <option value="0">Please Select</option>
          <option value="Call">Call</option>
          <option value="Email">Email</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientTask" class="btn btn-primary">Create</button>
    </form>
    </div>
    </div>
    
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages</a></li>
    <li class="active"><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
    <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities</a></li>    
    <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>    
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  		
        <div class="row well">
        <div class="span12">
        <?php if ($this->session->flashdata('errors')) {
		echo "<div class='alert alert-error'>" .  $this->session->flashdata('errors') . "</div>";		
		} ?>
        <form action="<?php echo base_url(); ?>clients/edit_employee_save" method="post" enctype="multipart/form-data">
     	<div class="row-fluid">        
            <div class="span3" style="width:120px;">
            Title:<br />
            <select name="title" style="padding:0px; height:23px; width:120px;">
              <option value="Mr" <?php if ($employee['title']=="Mr") { echo 'selected="selected"'; } ?>>Mr</option>
              <option value="Mrs" <?php if ($employee['title']=="Mrs") { echo 'selected="selected"'; } ?>>Mrs</option>
              <option value="Miss" <?php if ($employee['title']=="Miss") { echo 'selected="selected"'; } ?>>Miss</option>
              <option value="Ms" <?php if ($employee['title']=="Ms") { echo 'selected="selected"'; } ?>>Ms</option>
              <option value="Dr" <?php if ($employee['title']=="Dr") { echo 'selected="selected"'; } ?>>Dr</option>
            </select>
            </div> 
            <div class="span3" style="width:210px;">
            First Name:<br />
            <input type="text" name="first_name" placeholder="Enter first name" value="<?php echo $employee['first_name']; ?>" />
            </div> 
            <div class="span3" style="width:210px;">
            Surname Name:<br />
            <input type="text" name="last_name" placeholder="Enter surname" value="<?php echo $employee['last_name']; ?>" />            
            </div> 
            <div class="span3" style="width:210px;">
            Date of Birth:<br />
            <input type="text" name="dob" id="dob" placeholder="Enter date of birth" value="<?php if ($employee['dob']!="0000-00-00") { echo date("d/m/Y", strtotime($employee['dob'])); } ?>" />
            </div>        
        </div>
        
     	<div class="row-fluid">        
            <div class="span3" style="width:210px;">
            Job Title / Position:<br />
            <input type="text" name="job" placeholder="Enter job title / position" value="<?php echo $employee['job_title']; ?>" />
            </div> 
            <div class="span3" style="width:210px;">
            Gender:<br />
            <select name="gender" style="padding:0px; height:23px; width:120px;">
              <option value="Male" <?php if ($employee['gender']=="Male") { echo 'selected="selected"'; } ?>>Male</option>
              <option value="Female" <?php if ($employee['gender']=="Female") { echo 'selected="selected"'; } ?>>Female</option>
            </select>            
            </div> 
            <div class="span3" style="width:210px;">
            Smoker?<br />
            <select name="smoker" style="padding:0px; height:23px; width:120px;">
              <option value="0" <?php if ($employee['smoker']=="0") { echo 'selected="selected"'; } ?>>No</option>
              <option value="1" <?php if ($employee['smoker']=="1") { echo 'selected="selected"'; } ?>>Yes</option>
            </select>            
            </div>  
            <div class="span3" style="width:210px;">
            </div>        
        </div>        
        
     	<div class="row-fluid">        
            <div class="span3" style="width:200px;">
            Street Address:<br />
			<input type="text" name="street" placeholder="Enter street address" value="<?php echo $employee['address']; ?>" />
            </div> 
            <div class="span3" style="width:200px;">
            Town/City:<br />
            <input type="text" name="town" placeholder="Enter town or city" value="<?php echo $employee['town']; ?>" />
            </div> 
            <div class="span3" style="width:200px;">
            County:<br />
            <input type="text" name="county" placeholder="Enter the county" value="<?php echo $employee['county']; ?>" />            
            </div> 
            <div class="span3">
            Postcode:<br />
            <input type="text" name="postcode" placeholder="Enter postcode" value="<?php echo $employee['postcode']; ?>" style="width:110px;" />
            </div>        
        </div>
        
        </div></div>
        <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
        <input type="hidden" name="employee" value="<?php echo $this->input->get('employee'); ?>" />      
        <button class="btn">Save</button>  
        </form>   
      	</div>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
