<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

//echo '<pre>';
//print_r($opportunity_tags);
//echo '</pre>';
?>

<div id="container_top">
<?php require("common/client_tags.php"); ?>

    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>clients/create_note" method="post" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel">Add note to account</h4>
    </div>
    <div class="modal-body">Please enter a note to be added to the client account.
        <div class="control-group" style="padding-top:10px;">
            <label class="control-label"></label>
          <div class="controls">
			<textarea name="note" rows="4" id="note" class="span5"></textarea>
            <input type="hidden" id="notefor" name="notefor" value="<?php echo $this->input->get('id', TRUE); ?>" />       
          </div>
        </div>      
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button id="modalCreateClientNote" class="btn btn-primary">Add Note</button>
    </div>
    </form> 
    </div>

    <div id="updateBackgound" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_backgound" method="post" name="updatebkgd" id="updatebkgd">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Update background info</h4>
    </div>
    <div class="modal-body">
 	<textarea name="info" rows="6" id="info" class="span5"><?php echo $customer_details['background_info']; ?></textarea>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />   
    
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientNote" class="btn btn-primary">Update</button>
    </form>
    </div>
    </div>
    
    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" /> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task for client</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>          
        </select>
        
        <select name="task_min" id="task_min" class="span1">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>      
        
        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action">
          <option value="0">Please Select</option>
          <option value="Call">Call</option>
          <option value="Email">Email</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientTask" class="btn btn-primary">Create</button>
    </form>
    </div>
    </div>      
    
<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>

        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>                        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>

		<div class="span9">
        
        <div class="row-fluid">
          <div class="span12 well" style="min-height:200px;"><h5 style="margin-top:0px;">Opportunities</h5>
          <form action="<?php echo base_url(); ?>clients/update_opportunity" method="post">
          Opportunity Name:<br />
          <input type="text" name="name" value="<?php echo $opportunity_details['name']; ?>" />
          <br />
          Opportunity Description:<br />
          <textarea name="text" cols="10" rows="3" style="padding:2px;"><?php echo $opportunity_details['text']; ?></textarea>
          <br />
          Expected Value (&pound;):<br />
          <input type="text" name="value" value="<?php echo $opportunity_details['value']; ?>" />
          <br />
          Milestone &amp; Probability of Winning:<br />
          <select name="milestone" id="milestone" style="padding:0px; height:23px; width:100px;">
            <option value="0">Select ...</option>
            <option value="Suspect" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Suspect")) { echo 'selected="selected"'; } ?>>Suspect</option>
            <option value="Prospect" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Prospect")) { echo 'selected="selected"'; } ?>>Prospect</option>
            <option value="Champion" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Champion")) { echo 'selected="selected"'; } ?>>Champion</option>
            <option value="Opportunity" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Opportunity")) { echo 'selected="selected"'; } ?>>Opportunity</option>
            <option value="Proposal" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Proposal")) { echo 'selected="selected"'; } ?>>Proposal</option>
            <option value="Verbal" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Verbal")) { echo 'selected="selected"'; } ?>>Verbal</option>
            <option value="Lost" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Lost")) { echo 'selected="selected"'; } ?>>Lost</option>
            <option value="Won" <?php if ((isset($opportunity_details['milestone'])) && ($opportunity_details['milestone']=="Won")) { echo 'selected="selected"'; } ?>>Won</option>          
          </select>&nbsp;&nbsp;
          <select name="probability" id="probability" style="padding:0px; height:23px; width:100px;">
            <option value="0">Select ...</option>
            <option value="10" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="10")) { echo 'selected="selected"'; } ?>>10%</option>
            <option value="20" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="20")) { echo 'selected="selected"'; } ?>>20%</option>
            <option value="25" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="25")) { echo 'selected="selected"'; } ?>>25%</option>
            <option value="30" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="30")) { echo 'selected="selected"'; } ?>>30%</option>
            <option value="50" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="50")) { echo 'selected="selected"'; } ?>>50%</option>
            <option value="75" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="75")) { echo 'selected="selected"'; } ?>>75%</option>
            <option value="90" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="90")) { echo 'selected="selected"'; } ?>>90%</option>
            <option value="100" <?php if ((isset($opportunity_details['probability'])) && ($opportunity_details['probability']=="100")) { echo 'selected="selected"'; } ?>>100%</option>          
          </select>
          <br />
          Expected Close Date:<br />
          <input type="text" name="close_date" id="close_date" value="<?php echo date("d/m/Y", strtotime($opportunity_details['close_date'])); ?>" />
          <br />
          Opportunity Status:<br />
          <select name="status" style="padding:0px; height:23px; width:100px;">
            <option value="0" <?php if ((isset($opportunity_details['status'])) && ($opportunity_details['status']=="0")) { echo 'selected="selected"'; } ?>>Open</option>
            <option value="1" <?php if ((isset($opportunity_details['status'])) && ($opportunity_details['status']=="1")) { echo 'selected="selected"'; } ?>>Closed</option>
          </select>
          <br />
		  <?php if ($opp_tags) {
          echo 'Assign Tag: <small>(select multiple by holding Crtl)</small><br />
		  <select multiple="multiple" name="tag[]">';
		  
		  	if (isset($opportunity_tags)) {
          
				foreach($opp_tags as $tag) {
				
					if (in_array($tag['id'], $opportunity_tags, TRUE)) {
						$tag_selected = 'selected="selected"';
					} else {
						$tag_selected = '';
					}
								
					echo '<option value="'.$tag['id'].'" '. $tag_selected .'>'.$tag['tag'].'</option>';				
				}	
			
			} else {
			
				foreach($opp_tags as $tag) {				
					echo '<option value="'.$tag['id'].'">'.$tag['tag'].'</option>';				
				}			
			
			}	  
		  
		  echo '</select>';
		  
		  } ?>
          <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
          <input type="hidden" name="opportunity" value="<?php echo $this->input->get('opportunity'); ?>" />
          <p><button class="btn">Save</button>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>clients/opportunities?id=<?php echo $this->input->get('id'); ?>" class="btn">Cancel</a></p>
          </form>
          </div>
        </div>      
      
      </div>
      
    </div>
  </div>
</div>
<?php require("common/footer.php"); ?>
