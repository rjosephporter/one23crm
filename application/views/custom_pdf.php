<?php if (!$page_text) {
	echo 'Error processing PDF, please try again.';
} else {

?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70%">
        <h1 style="font-size:36px;"><?php echo $page_title; ?></h1>   
        </td>
        <td align="right"><?php if ($logo) { echo '<img src="'. base_url() .'img/company_logos/'. $logo .'" alt="" />'; } ?></td>
      </tr>
    </table>
    <hr />

	<?php echo $page_text; ?>
    
<?php } ?>
