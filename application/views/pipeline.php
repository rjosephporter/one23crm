<?php $site_theme = $this->session->userdata('theme_site');

// set the theme colour
if ($site_theme=="green") {
	$pipleline_colour_1 = "#BFD439";
	$pipleline_colour_2 = "#90a410";
} elseif ($site_theme=="yellow") {
	$pipleline_colour_1 = "#F5D927";
	$pipleline_colour_2 = "#e1c301";
} elseif ($site_theme=="purple") {
	$pipleline_colour_1 = "#A35197";
	$pipleline_colour_2 = "#830470";
} elseif ($site_theme=="pink") {
	$pipleline_colour_1 = "#B50649";
	$pipleline_colour_2 = "#78022f";
} elseif ($site_theme=="red") {
	$pipleline_colour_1 = "#D00000";
	$pipleline_colour_2 = "#910000";
} elseif ($site_theme=="clean") {
	$pipleline_colour_1 = "#999999";
	$pipleline_colour_2 = "#666666";
} else {
	$pipleline_colour_1 = "#00DBFF";
	$pipleline_colour_2 = "#009cb6";
} ?>
<script type="text/javascript">
$(function () {

<?php if ($forecast) {

// show forecast months
$forcast_months = "";
foreach ($forecast as $month) {
	$forcast_months .= "'".$month['themonth']."',";
}
$forcast_months = rtrim($forcast_months, ",");

// show the series data
$forecast_build_series = "";
$total_build_series = "";
foreach ($forecast as $value) {
	$forecast_build_series .= $value['thecash'] . ",";
	$total_build_series .= $value['maxcash'] . ",";
}

$forecast_build_series = rtrim($forecast_build_series, ",");
$total_build_series = rtrim($total_build_series, ","); 
?>
	$('#forecast').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [<?php echo $forcast_months; ?>]
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Value in GBP'
			}
		},
		tooltip: {

		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		credits: {
			enabled: false
		},
		legend: {
			enabled: true
		},
		series: [{
			name: 'Pipeline Value',
			data: [<?php echo $forecast_build_series; ?>],
			color: '<?php echo $pipleline_colour_1; ?>'  
		},{
			name: 'Expected Total Value',
			data: [<?php echo $total_build_series; ?>],
			color: '<?php echo $pipleline_colour_2; ?>'   
		}]
	});	
<?php } ?>

<?php if ($sales_by_month) {

// show actual sales months
$sales_months = "";
foreach ($sales_by_month as $salesmonth) {
	$sales_months .= "'".$salesmonth['themonth']."',";
}
$sales_months = rtrim($sales_months, ",");

// show the series data
$sales_build_series = "";
foreach ($sales_by_month as $value) {
	$sales_build_series .= $value['total'] . ",";
}

$sales_build_series = rtrim($sales_build_series, ",");

?>

	$('#salesbymonth').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [<?php echo $sales_months; ?>]
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Value in GBP'
			}
		},
		tooltip: {

		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		credits: {
			enabled: false
		},
		legend: {
			enabled: true
		},
		series: [{
			name: 'Actual Sales Value',
			data: [<?php echo $sales_build_series; ?>],
			color: '<?php echo $pipleline_colour_1; ?>'  
		}]
	});	
<?php } ?>

	
	$("#user").change(function() {
		$("#select_user").submit();
	});

});
</script>

<div id="container_top">
<h4>Sales Pipeline</h4>
<ul class="nav nav-tabs">
    <li class="active"><a href="<?php echo base_url(); ?>pipeline">Chart / Stats View</a></li>
    <li><a href="<?php echo base_url(); ?>pipeline/list_view">List View</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
	<div class="span6">   

		<?php if ($this->session->userdata("role")==1) { ?>
        <div class="row-fluid">
        <div class="span12">
        <form class="form-inline" method="get" id="select_user">
        &nbsp;Pipeline for user:&nbsp;
        <select id="user" name="user" style="padding:0; height:23px;">
        <option value="">Myself Only</option>
        <option value="all" <?php if ($this->input->get("user")=="all") { echo 'selected="selected"'; } ?>>All Users</option>
        <?php if ($group) {
            foreach($group as $user) {
                echo '<option value="'. $user['user_id'] .'"';
                
                if ($user['user_id']==$this->input->get("user")) { echo ' selected="selected"'; }
                
                echo '>'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';
            }
        } ?>
        </select>
        </form>
        </div>
        </div>
		<?php } ?>     
		
        <div class="row-fluid">
        <div class="span12 well">
            <h4 style="margin-top:0px;">Pipeline Forecast</h4>
            <div id="forecast" style="height: 400px; margin: 0 auto">No data to show, opportunities must be added to the system before any data is displayed</div>
        </div>
        </div>

        <div class="row-fluid">
        <div class="span12 well"><!--<div class="pull-right">Show: <a href="#" target="_self">3</a> | <a href="#" target="_self">6</a> | <a href="#" target="_self">9</a> | <a href="#" target="_self">12</a> months</div>-->
          <h4 style="margin-top:0px;">Sales by Month</h4>
            <div id="salesbymonth" style="height: 400px; margin: 0 auto">No data to show, opportunities must be added to the system before any data is displayed</div>
        </div>
        </div>

    </div>    
    <div class="span6">

        <div class="row-fluid">
        <div class="span12 well">
            <h4 style="margin-top:0px;">Pipeline Stats</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%" align="center">
                <h5>Pipeline Value</h5><h4 class="to_much_space">&pound;<?php echo number_format($stats['pipeline'],2); ?></h4></td>
                <td width="50%" align="center">
                <h5>Total Value</h5><h4 class="to_much_space">&pound;<?php echo number_format($stats['total'],2); ?></h4></td>
              </tr>
            </table>    
        </div>
        </div>
        
        <div class="row-fluid">
        <div class="span12 well">
            <h4 style="margin-top:0px;">Conversion Rates</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=conversionthismonth" target="_self">This Month <?php /*(<?php echo $conversion_30days_count; ?>)*/?></a></h5>
                <h4 class="to_much_space"><?php echo $conversion_30days; ?>%</h4></td>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=conversion90days" target="_self">Last 90 Days <?php /*(<?php echo $conversion_90days_count; ?>)*/?></a></h5>
                <h4 class="to_much_space"><?php echo $conversion_90days; ?>%</h4></td>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=conversionwonthisyear" target="_self">Year to Date <?php /*(<?php echo $conversion_360days_count; ?>)*/?></a></h5>
                <h4 class="to_much_space"><?php echo $conversion_360days; ?>%</h4></td>
              </tr>
            </table>    
        </div>
        </div>
        
        <div class="row-fluid">
        <div class="span12 well">
            <h4 style="margin-top:0px;">Actual Sales</h4>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=wonthismonth" target="_self">This Month</a></h5>
                <h4 class="to_much_space">&pound;<?php echo number_format($sales_30days,2); ?></h4></td>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=won90days" target="_self">Last 90 Days</a></h5>
                <h4 class="to_much_space">&pound;<?php echo number_format($sales_90days,2); ?></h4></td>
                <td width="33%" align="center">
                <h5><a href="<?php echo base_url(); ?>pipeline/list_view?status=wonthisyear" target="_self">Year to Date</a></h5>
                <h4 class="to_much_space">&pound;<?php echo number_format($sales_360days,2); ?></h4></td>
              </tr>
            </table>    
        </div>
        </div>

    </div>
</div>

<?php require("common/footer.php"); ?>
