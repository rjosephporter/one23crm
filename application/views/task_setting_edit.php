<div id="container_top">
<h4>Calendar Task Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>
<br clear="all" />
<div id="task-setting" class="container-fluid">
	<div class="create">
		<h1>Update Action - <?php echo $action->row()->action_name;?></h1>
		<form role="form" action="<?php echo base_url(); ?>tasksetting/updateAction/<?php echo $id;?>" method="post">
		  <div class="form-group">
			<label for="actionName">Action Name</label>
			<input type="text" class="form-control input-lg" id="actionName" name="actionName" placeholder="Enter action" value="<?php echo $action->row()->action_name;?>">
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Choose Icons</label>
			<div class="icons-list">
				<ul class="nav nav-pills list-unstyled list-unstyled icons-list-wrapper">
				<?php foreach($icons as $icon ){ ?>
					<li>
						<input type="radio" name="icons" value="<?php echo $icon;?>" <?php echo ($icon == $action->row()->icons) ? 'checked':'';?>>
						<i class="fa <?php echo $icon;?> fa-2x"></i>
					</li>
				<?php } ?>
				</ul>
			</div>
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Choose Color Label</label>
			<div class="color-label-list">
				<ul class="nav nav-pills list-unstyled list-unstyled color-label-wrapper">
				<?php foreach($color_label as $color ){ ?>
					<li>
						<input type="radio" name="color_label" value="<?php echo $color;?>" <?php echo ($color == $action->row()->color) ? 'checked':'';?>>
						<span class="swatch" style="height: 40px;width:50px;display:block;background-color: <?php echo $color;?>"></span>
					</li>
				<?php } ?>
				</ul>
			</div>
		  </div>
		  <button type="submit" class="btn btn-default">Update</button>
		  <a class="btn" href="<?php echo base_url(); ?>tasksetting">Cancel</a>
		</form>
	</div>
</div>
<?php require("common/footer.php"); ?>
