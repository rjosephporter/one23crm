<?php if ($customer_details) {

//echo '<pre>';
//print_r($protection_records);
//echo '</pre>';

?>
<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="10%" align="center" valign="top"><?php if ($customer_details['account_type']==2) { echo '<img src="' . base_url() . 'img/summary_comp.png" width="70" height="70" />'; } else { echo '<img src="' . base_url() . 'img/summary_person.png" width="70" height="70" />'; } ?></td>
    <td align="left" valign="top"><h3><?php if (!empty($customer_details['company_name'])) { echo $customer_details['company_name']; } else { echo $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?></h3><small><?php if ( (!empty($customer_details['organisation'])) && (!empty($customer_details['job_title'])) ) { echo $customer_details['job_title'] . ' at <a href="'.base_url().'clients/view?id='. $customer_details['associated'] .'" target="_self">' . $customer_details['organisation']. '</a>'; } ?></small></td>
  </tr>
</table>
<ul id="client_top_buttons">
<li><a class="btn" href="<?php echo base_url(); ?>m/add_note?id=<?php echo $this->input->get('id'); ?>">Add Note</a></li>
<li><a class="btn" href="<?php echo base_url(); ?>m/add_task?id=<?php echo $this->input->get('id'); ?>">Add Task</a></li>
</ul>
<p>&nbsp;</p>
<h4>Contact Details</h4>
<ul class="list">
<?php if ($customer_email) {		
	foreach($customer_email as $email) {
		echo '<li class="arrow"><a href="mailto:'.$email['email'].'">'. $email['email'] .'</a></li>';
	}
}
if ($customer_telephone) {
	foreach($customer_telephone as $telephone) {
		echo '<li class="arrow"><a href="tel:'.$telephone['number'].'">'. $telephone['number'] .'</a></li>';
	}
}
if (!$customer_email && !$customer_telephone) {
	echo '<li><small>No contact details held for client.</small></li>';
}

if ($customer_details['postcode']!="") {
	echo '<li>'. $customer_details['address_line_1'] . ' ' . $customer_details['town'] . ' ' . $customer_details['county'] . ' ' . $customer_details['postcode'].'</li>';
	echo '<li class="arrow"><a href="http://maps.google.co.uk/maps?hl=en&safe=off&q='. $customer_details['postcode'] .'">View Location On Map</a></li>';
	echo '<li class="arrow"><a href="http://maps.google.co.uk/maps?daddr='. $customer_details['postcode'] .'">Get Directions</a></li>';
} ?>
</ul>
<h4>History / Notes</h4>
<ul class="list">
<?php if ($customer_notes) {
	foreach($customer_notes as $note) {
		echo '<li class="arrow"><a href="'. base_url() .'m/note_view?id='. $note['id'] .'"><small>'. substr($note['note'],0,50) . '...' .'</small></a></li>';
	}
} else {
	echo '<li><small>No history/notes held for this client.</small></li>';
} ?>
</ul>
<h4>Active Policies</h4>
<ul class="list">
<?php if ($customer_policies) {
	foreach($customer_policies as $policies) {
		if (!empty($policies['file'])) {
			echo '<li class="arrow"><a href="'. base_url() .'documents/policy_documents/'. $policies['file'] .'" target="_blank"><small>Policy: '. $policies['policy'] .' | Product: '. $policies['product'] .' | Renewal: '. date("d/m/y", strtotime($policies['renewal_date'])) .'</small></a></li>';
		} else {
			echo '<li><small>Policy: '. $policies['policy'] .' | Product: '. $policies['product'] .' | Renewal: '. date("d/m/y", strtotime($policies['renewal_date'])) .'</small></li>';
		}
	}
} else {
	echo '<li><small>No active policies held for this client.</small></li>';
} ?>
</ul>
<h4>Recent PMI Quotes</h4>
<ul class="list">
<?php if ($recent_quotes) {
	foreach($recent_quotes as $quotes) {
			echo '<li class="arrow"><a href="'. base_url() .'messages/view?id='. $quotes['id'] .'"><small>'. $quotes['subject'] .'</small></a></li>';
	}
} else {
	echo '<li><small>No recent pmi quotes for this client.</small></li>';
} ?>
</ul>

<h4>New Protection Quotes</h4>
<ul class="list">
<li class="arrow"><a href="<?php echo base_url(); ?>m/quote_term?id=<?php echo $this->input->get('id'); ?>"><small> Term Protection Quote</small></a></li>
<li class="arrow"><a href="<?php echo base_url(); ?>m/quote_mortgage?id=<?php echo $this->input->get('id'); ?>"><small>Mortgage Protection Quote</small></a></li>
<li class="arrow"><a href="<?php echo base_url(); ?>m/quote_familyincome?id=<?php echo $this->input->get('id'); ?>"><small>Family Income Benefit Quote</small></a></li>
<li class="arrow"><a href="<?php echo base_url(); ?>m/quote_whole?id=<?php echo $this->input->get('id'); ?>"><small>Whole of Life Quote</small></a></li>
<li class="arrow"><a href="<?php echo base_url(); ?>m/quote_ip?id=<?php echo $this->input->get('id'); ?>"><small>Income Protection Quote</small></a></li>
</ul>

<h4>Recent Protection Quotes</h4>
<ul class="list">
<?php if ($protection_records) {
	foreach($protection_records as $protection) {
		if ($protection['webline_ref']=="MULTIPROD") {
			echo '<li class="arrow"><a href="'. base_url() .'m/view_quote_multi?id='. $protection['customer_id'] .'&quote='. $protection['id'] .'"><small>'. $protection['id'] .'-'. $protection['webline_ref'] .' | '. date("d/m/y H:i", strtotime($protection['added_date'])) .' | '. strip_tags($protection['product_line']) .'</small></a></li>';
		} else {	
			echo '<li class="arrow"><a href="'. base_url() .'m/view_quote?id='. $protection['customer_id'] .'&quote='. $protection['id'] .'"><small>'. $protection['id'] .'-'. $protection['webline_ref'] .' | '. date("d/m/y H:i", strtotime($protection['added_date'])) .' | '. strip_tags($protection['product_line']) .'</small></a></li>';			
		}
	}
} else {
	echo '<li><small>No recent protection quotes for this client.</small></li>';
}?>
</ul>

<h4>Medical History</h4>
<ul class="list">
<?php if ($medical_records) {
	foreach($medical_records as $medical) {
			echo '<li class="arrow"><a href="'. base_url() .'clients/medical_view?id='. $medical['customer_id'] .'&report='. $medical['id'] .'"><small>'. date("d/m/y", strtotime($medical['added_date'])) . ' - ' .  $medical['name'] .'</small></a></li>';
	}
} else {
	echo '<li><small>No medical records held for this client.</small></li>';
} ?>
</ul>
<h4>Income &amp; Expenditure</h4>
<ul class="list">
<?php if ($income_records) {
	foreach($income_records as $income) {
			echo '<li class="arrow"><a href="'. base_url() .'clients/incomeexpenditure_view?id='. $income['customer_id'] .'&report='. $income['id'] .'"><small>'. date("d/m/y", strtotime($income['added_date'])) . ' - ' .  $income['name'] .'</small></a></li>';
	}
} else {
	echo '<li><small>No income records held for this client.</small></li>';
} ?>
</ul>
<h4>Fact Find</h4>
<ul class="list">
<?php if ($factfind_records) {
	foreach($factfind_records as $factfind) {
			echo '<li class="arrow"><a href="'. base_url() .'clients/factfind_view?id='. $factfind['customer_id'] .'&factfind='. $factfind['id'] .'">
			<small>Discussion Date: '. $factfind['discussion_date'] . ' | Type: ' . $factfind['sale_type'] . '</small></a></li>';
	}
} else {
	echo '<li><small>No fact find records held for this client.</small></li>';
} ?>
</ul>
<?php } else {
redirect('clients');
} ?>
