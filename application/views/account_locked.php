<div id="container_top">
</div>
<br clear="all" />
<div class="container-fluid">    
    <div class="row-fluid">
    <div class="span3"></div>
    <div class="span6 well">
    <h3 style="margin-top:0;">Access Restricted</h3>
    <?php if ($this->session->userdata('trial')==1) {
    echo '<h5>Your free trial has now ended. To gain access to your account payment must be made.</h5>
	<h5>Please click the button below to setup your payment.</h5>
	<a href="" class="btn">Setup Payment</a>
	';
    } else {
	echo '<h5>There is a problem with your payment, please contact us.</h5>';
	
	} ?>    
    </div>
    <div class="span3"></div>
    </div>
</div>
<?php require("common/footer.php"); ?>
