<div id="container_top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" align="left"><h4>Client Import</h4></td>
    <td align="right"><p><a target="_self" href="<?php echo base_url(); ?>clients/create_company" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Company</a>&nbsp;&nbsp;&nbsp;<a target="_self" href="<?php echo base_url(); ?>clients/create" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Person</a>&nbsp;&nbsp;&nbsp;<a target="_self" href="<?php echo base_url(); ?>clients/import" class="btn">Import</a></p></td>
  </tr>
</table>
</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
	<div class="span7 well">
    <?php echo $this->session->flashdata('import_error'); ?>
    <form action="<?php echo base_url(); ?>clients/verify_import" method="post" enctype="multipart/form-data">
    <label>Please Select CSV File:<br />
    <input name="csv_file" type="file" />
    </label>
    <label class="checkbox">
    <input name="headers" type="checkbox" value="1" /> Does the first row of the csv file contain headers (names of each column)?
    </label>
    <p>&nbsp;</p>
    <button type="submit" class="btn">Import File</button>
    </form>
    </div>    
    <div class="span5 well helpbox">
    <h5 style="margin-top:0px;">Importing Clients - Step 1</h5>
    <p></p>    
    </div>
</div>


<?php require("common/footer.php"); ?>
