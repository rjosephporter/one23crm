<?php if (!$account_details) {
redirect('settings');
}

// address
if ($account_details['address_line']!="") {

	$address_line = $account_details['address_line'] .', '. $account_details['address_town'] .', '. $account_details['address_county'] .', '. $account_details['address_postcode'];

} else {

	$address_line = "Details not provided, please update account.";

} ?>
<div id="container_top">
<h4>Account Information</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

			<?php if (isset($_GET['paymenterror'])) { echo '<div class="alert alert-error">Error processing your payment request, please contact support.</div>'; } ?>
            <?php if (isset($_GET['paymentsuccess'])) { echo '<div class="alert alert-success">Thank you, we are now processing your payment and any changes will be applied to your account shortly.</div>'; } ?>

            <div class="row-fluid">
            	<div class="span12 well">
                <?php echo $this->session->flashdata('errors'); ?>
                <h5 style="margin-top:0px;">Account Information</h5>
                <p>Company: <strong><?php echo $account_details['company']; ?></strong></p>
				<p>Owner: <strong><?php echo $account_details['first_name'] . ' ' . $account_details['last_name']; ?></strong></p>
                <p>Address: <strong><?php echo $address_line; ?></strong></p>
                <p>Email: <strong><?php echo $account_details['email']; ?></strong></p>
                <p>Telephone: <strong><?php echo $account_details['telephone']; ?></strong></p>
                <p>SMS Name: <strong><?php echo $account_details['sms']; ?></strong></p>
                <p><a href="<?php echo base_url(); ?>settings/edit_account">Update Account Details</a> or <a href="<?php echo base_url(); ?>settings/edit_password">Change Password</a></p>
                </div>
            </div>

            <?php /*
            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Subscription Information</h5>
                <p>Current Package: <strong><?php echo strtoupper($account_details['package']); ?></strong>
                <?php if ($account_details['package']!="power") { echo '<a href="#">upgrade your account</a>'; } ?>
                </p>
                <?php if ($payment_exempt) {
					echo '<div class="alert alert-success" style="margin-bottom:0px;">This account is payment exempt, therefore no subscription payment is required / or can be setup.</div>';
				} else { ?>
				<p>Subscription Expiry: <strong><?php echo date("d/m/Y", strtotime($this->session->userdata('account_expiry'))); ?></strong></p>
                <p>Current Payment Method: <strong>PayPal</strong></p>
                <a href="<?php echo base_url(); ?>settings/newRecurringPayment" class="btn">New</a>
                <?php } ?>
                </div>
            </div>
			*/ ?>

            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">SMS/Text Credits</h5>
                <p>Available Credits: <?php echo $sms_credits['sms']; ?></p>
                <p>If you wish to purchase additional sms credits, simply select the required quantity below and following the instructions. If you require more than 200 credits, please contact us for a price.</p>
                <form action="<?php echo base_url(); ?>settings/purchaseSmsCredits" method="get">
                <input type="hidden" name="sms" value="20" />
                <input type="submit" value="Purchase 20 SMS Credits - &pound;3.00" class="btn" />
                </form>

                <form action="<?php echo base_url(); ?>settings/purchaseSmsCredits" method="get">
                <input type="hidden" name="sms" value="50" />
                <input type="submit" value="Purchase 50 SMS Credits - &pound;6.00" class="btn" />
                </form>

                <form action="<?php echo base_url(); ?>settings/purchaseSmsCredits" method="get">
                <input type="hidden" name="sms" value="100" />
                <input type="submit" value="Purchase 100 SMS Credits - &pound;10.00" class="btn" />
                </form>

                <form action="<?php echo base_url(); ?>settings/purchaseSmsCredits" method="get">
                <input type="hidden" name="sms" value="200" />
                <input type="submit" value="Purchase 200 SMS Credits - &pound;16.00" class="btn" />
                </form>
                </div>
            </div>

            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Company Logo</h5>
                <?php echo $this->session->flashdata('logo_results'); ?>
                <p>To display your company logo on comparison reports, simply upload your logo below. Please note only jpg, gif and png image files are allowed.</p>
                <?php if ($logo) {
					echo '<img src="'. base_url() .'img/company_logos/'. $logo .'" alt="" />&nbsp;&nbsp;<a href="'. base_url() .'settings/account_logo_remove">Remove logo</a>';
				} else { ?>
                    <form action="<?php echo base_url(); ?>settings/account_logo" method="post" enctype="multipart/form-data">
                    <label>Select Your Logo:<br /></label>
                    <input type="file" name="logo" />
                    <br />
                    <input type="submit" value="Upload Logo" class="btn" />
                    </form>
                <?php } ?>
                </div>
            </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">Account Information</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
