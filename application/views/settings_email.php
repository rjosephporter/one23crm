<script>
$(document).ready(function() {

	var attachmentCount = 1;
	var attachmentCountLimit = 5;
	$("#add_attachment").click(function() {
		if (attachmentCount<=attachmentCountLimit) {
			var newAttachment = '<label>Attachment '+attachmentCount+':<br /><select name="attachment[]" style="padding:0; height:23px;"><option value="0">No attachment required</option><?php echo $attachments; ?></select></label>';
			$("#attachment").append(newAttachment);
			attachmentCount++;
		} else {
			alert("You have reached the max of 5 attachments.");
		}
	});

});
</script>
<div id="newTemplate" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_email_template" method="post" name="" id="">
  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Email Template</h4>
    </div>
    <div class="modal-body">
    <label>Template Name:<br />
    <input type="text" name="name" id="name" class="span4" placeholder="Enter template name" />
    </label>
    <label>Template Subject:<br />
    <input type="text" name="subject" id="subject" class="span4" placeholder="Enter template subject" />
    </label>
    <label>Template Body:<br />
    <textarea class="wysiwygeditor" name="body" id="body" rows="6" style="width:90%;" placeholder="Enter template body"></textarea>
    </label>
    <div id="attachment">

    </div>
    <div id="add_attachment" style="cursor:pointer;"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add attachment</div>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
</div>
    </div>

    <div id="newSignature" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_email_signature" method="post" name="" id="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Email Signature</h4>
    </div>
    <div class="modal-body">
    <label>Signature Name:<br />
    <input type="text" name="name" id="name" class="span4" placeholder="Enter signature name" />
    </label>
    <label>Signature Body:<br />
    <?php /*<textarea name="body" id="body" rows="6" style="width:90%;" placeholder="Enter signature body text" class="wysiwygeditor"></textarea>*/ ?>
	<textarea name="body" id="body_signature" placeholder="Enter signature body text"></textarea>
	<script>
        $(document).ready(function(){
            $('#body_signature').summernote();
        });
	</script>
    </label>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
    </div>
    </div>

<div id="container_top">
<h4>Email Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

		<?php /*
        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Quick Parts</h5>

            </div>
        </div>
		*/ ?>

        <?php echo $this->session->flashdata('alert'); ?>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Templates</h5>
            <?php if ($templates) {

		echo '<table width="50%" class="table">';

			foreach($templates as $template) {

				echo '<tr>
				<td width="85%"><strong>'. $template['name'] .' - '. $template['subject'] .'</strong></td>
				<td align="center" style="text-align:center;">
				<a href="'.base_url().'settings/email_template?id='. $template['id'] .'">Edit</a> | <a href="'.base_url().'settings/email_template_remove?id='. $template['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this template?\')">Remove</a>
				</td>
				</tr>';

			}

		echo '</table>';


			} else {

				echo 'You have not created any templates yet.';

			} ?>
            </div>
        </div>
        <p><a href="#newTemplate" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add email template</a></p>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Signatures</h5>
            <?php if ($signatures) {

		echo '<table width="50%" class="table">';

			foreach($signatures as $sig) {

				echo '<tr>
				<td width="85%"><strong>'. $sig['name'] .'</strong></td>
				<td align="center" style="text-align:center;">
				<a href="'.base_url().'settings/email_signature?id='. $sig['id'] .'">Edit</a> | <a href="'.base_url().'settings/email_signature_remove?id='. $sig['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this signature?\')">Remove</a></td>
				</tr>';

			}

		echo '</table>';


			} else {

				echo 'You have not created any signatures yet.';

			} ?>
            </div>
        </div>
		<p><a href="#newSignature" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add email signature</a></p>

        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">Email Settings</h5>
            <p></p>
        </div>


    </div>

<?php require("common/footer.php"); ?>
