<?php

if (!$customer_details) {
redirect('clients');
}

?>

<script>
$(function() {
	$('.vmdShareModal').click(function() {
		var nameVDM  = $(this).data('name')
		var filenameVDM  = $(this).data('filename');
		$('#vmdname').val(nameVDM);
		$('#vmdfilename').val(filenameVDM);
	});	
	
	$("#Section1FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section1FormViewBody").html(details);
		$("#Section1FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);		
	});
	
	$("#Section2FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section2FormViewBody").html(details);
		$("#Section2FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);		
	});
	
	$("#Section3FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section3FormViewBody").html(details);
		$("#Section3FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);
	});
	
	$("#Section4FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section4FormViewBody").html(details);
		$("#Section4FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);		
	});
	
	$("#Section5FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section5FormViewBody").html(details);
		$("#Section5FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);		
	});
	
	$("#Section6FormViewDetails").click(function() {
		var details = $(this).data('details');
		var details_name = $(this).data('name');
		$("#Section6FormViewBody").html(details);
		$("#Section6FormViewHeader").html(details_name);
		$(".customform_data").val(details);
		$(".customform_name").val(details_name);		
	});
	
	$('.date').each(function() {
		$(this).datepicker({ dateFormat: 'dd/mm/yy' });
	});					

});
</script>

<div id="container_top">
<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li';
			if ($tab['tab_ref']==$this->input->get("custom")) {
				echo '  class="active"';
			}
			echo '><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
                
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>                        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>


</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">

	<?php require("common/client_left_menu.php"); ?>

      <div class="span9">
       
       <?php echo $this->session->flashdata("error"); ?>
       
        <div class="row-fluid">
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section1) {
			
			if ($section1_type==1) {
			
				echo '<div id="section1Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section1 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="1" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section1 .' - <a target="_self" href="#section1Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section1_data) {
					echo '<table class="table">';
					foreach($section1_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section1_type==2) {
			
				echo '<div id="section1Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="1" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="5"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section1 .' - <a target="_self" href="#section1Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section1_data) {
					echo '<table class="table">';
					foreach($section1_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section1_type==3) {
			
				echo '<div id="Section1Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section1_form) {
				
					foreach($section1_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="1" />
				<input type="hidden" name="title" value="'. $section1 .'" />				
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section1FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section1FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section1FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section1 .' - <a target="_self" href="#Section1Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section1_data) {				
					echo '<table width="100%" class="table">';
					foreach($section1_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section1FormView" id="Section1FormViewDetails" data-toggle="modal" data-name="'. $section1 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section2) {
			
			if ($section2_type==1) {
			
				echo '<div id="section2Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section2 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="2" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section2 .' - <a target="_self" href="#section2Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section2_data) {
					echo '<table class="table">';
					foreach($section2_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section2_type==2) {
			
				echo '<div id="section2Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="2" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="4"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section2 .' - <a target="_self" href="#section2Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section2_data) {
					echo '<table class="table">';
					foreach($section2_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section2_type==3) {
			
				echo '<div id="Section2Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section2_form) {
				
					foreach($section2_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="2" />
				<input type="hidden" name="title" value="'. $section2 .'" />			
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section2FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section2FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section2FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section2 .' - <a target="_self" href="#Section2Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section2_data) {				
					echo '<table width="100%" class="table">';
					foreach($section2_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section2FormView" id="Section2FormViewDetails" data-toggle="modal" data-name="'. $section2 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>          
        </div>

        <div class="row-fluid">
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section3) {
			
			if ($section3_type==1) {
			
				echo '<div id="section3Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section3 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="3" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section3 .' - <a target="_self" href="#section3Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section3_data) {
					echo '<table class="table">';
					foreach($section3_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section3_type==2) {
			
				echo '<div id="section3Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="3" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="4"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section3 .' - <a target="_self" href="#section3Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section3_data) {
					echo '<table class="table">';
					foreach($section3_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section3_type==3) {
			
				echo '<div id="Section3Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section3_form) {
				
					foreach($section3_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="3" />
				<input type="hidden" name="title" value="'. $section3 .'" />			
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section3FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section3FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section3FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section3 .' - <a target="_self" href="#Section3Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section3_data) {				
					echo '<table width="100%" class="table">';
					foreach($section3_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section3FormView" id="Section3FormViewDetails" data-toggle="modal" data-name="'. $section3 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section4) {
			
			if ($section4_type==1) {
			
				echo '<div id="section4Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section4 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section4_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="4" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section4 .' - <a target="_self" href="#section4Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section4_data) {
					echo '<table class="table">';
					foreach($section4_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section4_type==2) {
			
				echo '<div id="section4Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="4" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="4"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section4 .' - <a target="_self" href="#section4Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section4_data) {
					echo '<table class="table">';
					foreach($section4_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section4_type==3) {
			
				echo '<div id="Section4Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section4_form) {
				
					foreach($section4_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="4" />
				<input type="hidden" name="title" value="'. $section4 .'" />			
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section4FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section4FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section4FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section4 .' - <a target="_self" href="#Section4Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section4_data) {				
					echo '<table width="100%" class="table">';
					foreach($section4_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section4FormView" id="Section4FormViewDetails" data-toggle="modal" data-name="'. $section4 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>          
        </div>
        
        <div class="row-fluid">
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section5) {
			
			if ($section5_type==1) {
			
				echo '<div id="section5Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section5 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="5" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section5 .' - <a target="_self" href="#section5Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section5_data) {
					echo '<table class="table">';
					foreach($section5_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section5_type==2) {
			
				echo '<div id="section5Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section1_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="5" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="5"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section5 .' - <a target="_self" href="#section5Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section5_data) {
					echo '<table class="table">';
					foreach($section5_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section5_type==3) {
			
				echo '<div id="Section5Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section5_form) {
				
					foreach($section5_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="5" />
				<input type="hidden" name="title" value="'. $section5 .'" />				
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section5FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section5FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section5FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section5 .' - <a target="_self" href="#Section5Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section5_data) {				
					echo '<table width="100%" class="table">';
					foreach($section5_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section5FormView" id="Section5FormViewDetails" data-toggle="modal" data-name="'. $section5 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;">
          <?php if ($section6) {
			
			if ($section6_type==1) {
			
				echo '<div id="section6Notes" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New Note</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_note" method="post">
				<textarea style="width:90%;" rows="8" placeholder="Enter notes for \''. $section6 .'\' here ..." name="notes"></textarea><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Add Note" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section6_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="6" />
				</form>
				</div>
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section6 .' - <a target="_self" href="#section6Notes" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Note</a></h5>';
				
				if ($section6_data) {
					echo '<table class="table">';
					foreach($section6_data as $data) {
						echo '<tr><td width="90%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br />' . nl2br($data['entry']) . '</td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a></td></tr>';
					}
					echo '</table>';
				} else {
					echo 'No data has been added.';
				}
								
		  	} elseif ($section6_type==2) {
			
				echo '<div id="section6Files" class="modal hide fade">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4>Add New File</h4>
				</div>
				<div class="modal-body">
				<form action="'. base_url() .'clients/custom_add_file" method="post" enctype="multipart/form-data">
				<input type="text" placeholder="Enter a name ..." name="name" /><br />
				<input type="file" name="file" /><br />
				</div>
				<div class="modal-footer">				
				<input type="submit" value="Upload File" class="btn" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<input type="hidden" name="custom" value="'. $this->input->get("custom") .'" />
				<input type="hidden" name="type" value="'. $section6_type .'" />
				<input type="hidden" name="client" value="'. $this->input->get("id") .'" />
				<input type="hidden" name="section" value="6" />
				</form>
				</div>
				</div>';
				
				echo '<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/view_my_documents_share" method="post" name="share" id="share">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">Share this file with client</h4>
				</div>
				<div class="modal-body">
				<label>Name of file:<br />
				<input type="text" name="name" id="vmdname" class="input-xlarge" />
				</label>
				<label>Notes:<br />
				<textarea name="notes" class="input-xlarge" rows="5"></textarea>
				</label>
				</div>
				<div class="modal-footer">
				<button class="btn">Share File</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				<input type="hidden" name="user" id="vmduser" />
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="file" id="vmdfile" />
				<input type="hidden" name="filename" id="vmdfilename" />
				</form> 
				</div>';
								
				echo '<h5 style="margin-top:0px;">'. $section6 .' - <a target="_self" href="#section6Files" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New File</a></h5>';
				
				if ($section6_data) {
					echo '<table class="table">';
					foreach($section6_data as $data) {
						echo '<tr><td width="85%">Added: ' . date("d/m/y H:i", strtotime($data['added_date'])) . '<br /><a href="'. base_url() .'documents/'. $data['entry_file'] .'" target="_blank">' . $data['entry'] . '</a></td><td valign="middle"><a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $data['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this?\')"><img src="'. base_url() .'img/delete-icon'. $icon_set .'.png" /></a>';
					
					if ($vmd_account) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="'. $vmd_account['vmd'] .'" data-filename="'.$data['entry_file'].'" data-name="'.$data['entry'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
						
					echo '</td></tr>';
					}
					echo '</table>';
				} else {				
					echo 'No data has been added.';
				}			

		  	} elseif ($section6_type==3) {
			
				echo '<div id="Section6Form" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<form action="'. base_url() .'clients/custom_save_form" method="post">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="myModalLabel">New Form</h4>
				</div>
				<div class="modal-body">';
				
				if ($section6_form) {
				
					foreach($section6_form as $formpart) {
						
						echo '<label>'. $formpart['label'] . '<br />';
						
						if ($formpart['type']==1) {
							echo '<input type="text" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==4) {
							echo '<textarea rows="4" name="name[]" placeholder="'. $formpart['placeholder'] .'"></textarea>';
						} else if ($formpart['type']==5) {
							echo '<input type="text" class="date" name="name[]" placeholder="'. $formpart['placeholder'] .'" />';
						} else if ($formpart['type']==6) {
							echo '<input type="hidden" name="name[]" value="&nbsp;" />';
						} else if ($formpart['type']==2) {
							$values = json_decode($formpart['value']);
							echo '<select name="name[]" style="padding:0; height:23px;">';
								foreach($values as $value=>$id) {							
									echo '<option>'. $id .'</option>';
								}
							echo '</select>';
						} else if ($formpart['type']==3) {
							echo '<input name="name[]" type="checkbox" value="Checked" />';		
						}			
						
						echo'<input type="hidden" name="entry[]" value="'. $formpart['label'] .'" /></label>';
					
					}
				
				} else {
				
					echo 'Error loading custom form, please check form settings.';
				
				}
								
				echo '</div>
				<div class="modal-footer">
				<input type="hidden" name="client" value="'. $this->input->get('id') .'"/>
				<input type="hidden" name="custom" value="'. $this->input->get('custom') .'" />
				<input type="hidden" name="section" value="6" />
				<input type="hidden" name="title" value="'. $section6 .'" />				
				<button class="btn">Save</button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
				</form> 
				</div>';
				
				echo '<div id="Section6FormView" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 id="Section6FormViewHeader">View Details</h4>
				</div>
				<div class="modal-body">
				<div id="Section6FormViewBody"></div>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf" target="_blank" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="View as PDF" class="btn btn-mini" />
				</form>
				<form style="display:table-cell; padding:5px;" action="'. base_url() .'clients/custom_form_pdf_save" method="post">
				<textarea name="customform" style="display:none;" class="customform_data"></textarea>
				<input type="hidden" name="customform_name" class="customform_name" />
				<input type="hidden" name="customform_client" class="customform_client" value="'. $this->input->get('id') .'" />
				<input type="submit" value="Save as PDF" class="btn btn-mini" />
				</form>				
				</div>
				<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div> 
				</div>';	
			
				echo '<h5 style="margin-top:0px;">'. $section6 .' - <a target="_self" href="#Section6Form" data-toggle="modal"><img src="'. base_url() .'img/small-add-icon'. $icon_set .'.png" /> Add New Form</a></h5>';
				
				if ($section6_data) {				
					echo '<table width="100%" class="table">';
					foreach($section6_data as $formdata) {
						echo '<tr><td width="75%">'. $formdata['entry'] .'</td>
						<td><a href="#Section6FormView" id="Section6FormViewDetails" data-toggle="modal" data-name="'. $section6 .'" data-details="'. $formdata['entry_value'] .'">View</a> | <a href="'. base_url() .'clients/custom_remove_entry?id='. $this->input->get("id") .'&custom='. $this->input->get("custom") .'&entry='. $formdata['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this?\')">Delete</a></td>
						</tr>';
					}
					echo '</table>';
				} else {
					echo 'No records found.';
				}
				
			} else {
			
				echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
			
			}
			
		  } else {
		  
		  	echo '<a href="'. base_url() .'settings/custom_fields_tab?id='. $tab['tab_ref'] .'">Click to configure this section.</a>';
		  	
		  } ?>
          </div>          
        </div>                
              
       
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
