<?php

if (!$customer_details) {
redirect('clients');
}

?>

<script>
$(function() {
	$('.vmdShareModal').click(function() {
		var fileVDM  = $(this).data('file');
		var nameVDM  = $(this).data('name')
		var filenameVDM  = $(this).data('filename');
		$('#vmdfile').val(fileVDM);
		$('#vmdname').val(nameVDM);
		$('#vmdfilename').val(filenameVDM);
	});
	
	$(".edit_filename").editable("<?php echo base_url(); ?>clients/update_file_name", {
		submit: 'Save',
		cancel: 'Cancel',
		event: 'dblclick'	
	});
	
	
});
</script>

<div id="uploadNewFile" class="modal hide fade">
<form action="<?php echo base_url(); ?>clients/uploadFile" method="post" enctype="multipart/form-data" name="newFile" id="newFile">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h4>Upload a new file</h4>
</div>
<div class="modal-body">    
<table width="100%" border="0" cellspacing="10" cellpadding="0">
<tr>
<td width="15%">Description:</td>
<td><input name="name" id="name" type="text" /></td>
</tr>
<tr>
<td width="15%">Select a File:</td>
<td><input name="fileupload" id="fileupload" type="file" /></td>
</tr>
<tr>
<td width="15%">Section:</td>
<td><select name="type" id="type" style="padding:0px; height:23px;">
  <option value="0">Please Select</option>
  <?php if ($sections) {
  	$newcount = 1;
  	foreach($sections as $thesecion) {
		if ($sections['files_'.$newcount]!="") {
			echo '<option value="'. $newcount .'">'. $sections['files_'.$newcount] .'</option>';
		}
		$newcount++;
  	}  
  } ?>
</select></td>
</tr>  
</table>
<input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />   
</div>
<div class="modal-footer">
<button id="modalUploadNewFile" class="btn">Save</button>
<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
</form>
</div>
</div>

<div id="vmdShare" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form action="<?php echo base_url(); ?>clients/view_my_documents_share" method="post" name="share" id="share">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel">Share this file with client</h4>
</div>
<div class="modal-body">
<label>Name of file:<br />
<input type="text" name="name" id="vmdname" class="input-xlarge" />
</label>
<label>Notes:<br />
<textarea name="notes" class="input-xlarge" rows="5"></textarea>
</label>
<label>Send Client Notification:<br />
<select name="notification" style="padding:0px; height:23px;">
<option value="no">No Notification</option>
<option value="email">Email Notification</option>
</select>
</label>
</div>
<div class="modal-footer">
    <button class="btn">Share File</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
</div>
<input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>"/>
<input type="hidden" name="file" id="vmdfile" />
<input type="hidden" name="filename" id="vmdfilename" />
</form> 
</div>

<?php if ($this->input->get('result', TRUE)=="added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="note_added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client note has successfully been added.</div>'; } ?>

<div id="container_top">

<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?> 

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>                        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li class="active"><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>


</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
      
      	<?php if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_files")==1) ) { ?>
        <div class="row-fluid">
      		<div class="span12"><p><a href="#uploadNewFile" class="btn" data-toggle="modal"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add File</a></p></div>
      	</div>
        <?php } ?>
      
        <div class="row-fluid">
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_1']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_1'] .'</h5>';
			if ($section1) {
				echo '<table class="table">';
				foreach($section1 as $files1) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files1['filename'] .'" target="_blank">'. $files1['name'] .'</a></td>
					<td width="25%">'. date("d/m/y H:i", strtotime($files1['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files1['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files1['filename'].'" data-name="'.$files1['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';				
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}		  
		  } else { 
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  } ?>
          </div>
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_2']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_2'] .'</h5>';
			if ($section2) {
				echo '<table class="table">';
				foreach($section2 as $files2) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files2['filename'] .'" target="_blank">'. $files2['name'] .'</a></td>
					<td width="30%">'. date("d/m/y H:i", strtotime($files2['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files2['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files2['filename'].'" data-name="'.$files2['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';				
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}		  } else { 
		  
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  
		  } ?>
          </div>
        </div>      
        <div class="row-fluid">
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_3']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_3'] .'</h5>';
			if ($section3) {
				echo '<table class="table">';
				foreach($section3 as $files3) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files3['filename'] .'" target="_blank">'. $files3['name'] .'</a></td>
					<td width="30%">'. date("d/m/y H:i", strtotime($files3['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files3['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files3['filename'].'" data-name="'.$files3['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';				
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}
		  } else { 
		  
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  
		  } ?>
          </div>
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_4']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_4'] .'</h5>';
			if ($section4) {
				echo '<table class="table">';
				foreach($section4 as $files4) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files4['filename'] .'" target="_blank">'. $files4['name'] .'</a></td>
					<td width="30%">'. date("d/m/y H:i", strtotime($files4['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files4['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files4['filename'].'" data-name="'.$files4['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';			
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}
		  } else { 
		  
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  
		  } ?>
          </div>
        </div>       
        <div class="row-fluid">
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_5']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_5'] .'</h5>';
			if ($section5) {
				echo '<table class="table">';
				foreach($section5 as $files5) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files5['filename'] .'" target="_blank">'. $files5['name'] .'</a></td>
					<td width="30%">'. date("d/m/y H:i", strtotime($files5['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files5['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files5['filename'].'" data-name="'.$files5['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';			
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}		  
		  
		  } else { 
		  
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  
		  } ?>
          </div>
          <div class="span6 well" style="min-height:200px;">
          <?php if ($sections['files_6']!="") {
          	echo '<h5 style="margin-top:0px;">'. $sections['files_6'] .'</h5>';
			if ($section6) {
				echo '<table class="table">';
				foreach($section6 as $files6) {
					echo '<tr>
					<td width="60%"><a href="'. base_url() .'documents/'. $files6['filename'] .'" target="_blank">'. $files6['name'] .'</a></td>
					<td width="30%">'. date("d/m/y H:i", strtotime($files6['added_date'])) .'</td>
					<td style="text-align:right;"><a href="'. base_url() .'clients/delete_file?id='. $this->input->get('id') .'&file='. $files6['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this file?\')"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a>';
					
					if ($vmd_account['vmd']) {
					echo '&nbsp;<a href="#vmdShare" class="vmdShareModal" data-toggle="modal" data-user="" data-filename="'.$files6['filename'].'" data-name="'.$files6['name'].'" target="_self"><img src="'.base_url().'img/share-icon.png" /></a>';
					}					
					
					echo '</td>
					</tr>';			
				}
				echo '</table>';
			} else {
				echo 'No files found.';
			}	  
		  } else { 
		  	echo 'You have not yet configured this section, please <a href="'. base_url() .'settings/custom_fields">click here</a>.';
		  } ?>
          </div>
        </div>       
      
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
