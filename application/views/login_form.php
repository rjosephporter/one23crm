<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/ico" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<script src="<?php echo base_url(); ?>js/hello.js"></script>
<style>
body {
/*background-color:#0b9fe5;*/
}
.login-logo {
width:411px;
margin:0 auto;
position:relative;
background:url(http://i.imgur.com/VDqyxR6.jpg) top center;
height:208px;
}

.login-page {
width:400px;
margin:0 auto;
position:relative;
margin-top:20px;
}

.login-page h2,h3 {
margin-top:0px;
}

.ip {
margin-top:15px;
color:#999;
}

.forgot {
padding-top:20px;
}

.well {
padding-bottom:0px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	//$("#username").focus();
});
</script>

<!--<link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet" type="text/css" />-->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/social-buttons.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div class="login-logo"></div>
<div class="well login-page">
<?php if (isset($_GET['failed'])) { echo '<div class="alert alert-error">Invalid login details provided, please try again.</div>'; } ?>
<?php if (isset($_GET['activated'])) { echo '<div class="alert alert-success">Your account has now been activated, you may now login below.</div>'; } ?>
<?php if (isset($_GET['reset'])) { echo '<div class="alert alert-success">Your password has been reset, check your inbox.</div>'; } ?>
<?php if (isset($_GET['expired'])) { echo '<div class="alert alert-error">Your subscription has now expired, please click here to extend/renew the subscription.</div>'; } ?>
<?php if (isset($_GET['subscription'])) { echo '<div class="alert alert-error">There is a problem with your subscription, please contact the accounts department.</div>'; }
echo $this->session->flashdata('showmessage'); ?>
<div id="login_loader" class="alert alert-info" style="display:none">
  <img src="/img/ajax-loader.gif" style="height: 18px;">
  <span>Logging in with Facebook...</span>
</div>
<h3>Please Login</h3>
<?php echo form_open('login/auth'); ?>
<div class="row-fluid">
  <div class="span3">Username/Email:</div>
  <div class="span9"><?php echo
  form_input(array(
  'name' => 'username',
  'id' => 'username',
  'value' => '',
  'placeholder' => 'Username or Email',
)); ?></div>
</div>
<div class="row-fluid">
  <div class="span3">Password:</div>
  <div class="span9"><?php echo form_input(array(
  'name' => 'password',
  'value' => '',
  'type' => 'password',
  'placeholder' => 'Password',
)); ?></div>
</div>
<?php if ($loginRedirect) {
	echo '<input type="hidden" name="login_redirect" value="'. $loginRedirect .'">';
} ?>

<div class="row">
  <div class="span9"><button type="submit" class="btn"><i class="icon-lock"></i>&nbsp;&nbsp;Login Securely</button></div>
</div>

<div class="row" style="margin-top: 15px">
	<div class="span5 pull-center">
    <a class="btn btn-block" href="<?php echo base_url(); ?>login/register"><i class="fa fa-pencil"></i> Create new account</a>
		<button class="btn btn-block btn-facebook btn-social" for="facebook"><i class="fa fa-facebook"></i> | Login with Facebook</button>
		<button class="btn btn-block btn-twitter btn-social" for="twitter"><i class="fa fa-twitter"></i> | Login with Twitter</button>
		<button class="btn btn-block btn-linkedin btn-social" for="linkedin"><i class="fa fa-linkedin"></i> | Login with LinkedIn</button>
    <button class="btn btn-block btn-google-plus btn-social" for="google"><i class="fa fa-google-plus"></i> | Login with Google+</button>
	</div>
</div>

<div class="row forgot">
<div class="span9"><a href="<?php echo base_url(); ?>login/password_request">Forgotten your password?</a>
</div>
</div>
<div class="row ip">
  <div class="span9"><small><strong>Your IP Address has been logged for security reasons.</strong></small></div>
</div>
<?php echo form_close(); ?>
</div>

<script>
var redirect_url = location.protocol + '//' + location.host + '/login'; 

hello.init({
  facebook: '1486594334912497',
  twitter: '2kipt4DdbDk7JinY3w8xWjaZm',
  linkedin: '75u5l0mp1oasn3',
  google: '521600025771-ipi0gi1g82b468vbn8ag8td2bvb4atar.apps.googleusercontent.com'
});

$('.btn-social').click(function(e){
  e.preventDefault();
  var social_network = $(this).attr('for');
  hello.login(social_network, {
    display: 'popup',
    scope: null,
    redirect_uri: redirect_url,
    response_type: 'token',
    force: true,
    oauth_proxy: 'https://auth-server.herokuapp.com/proxy'
  }, function(auth){
      console.log(auth);
      if(!auth.error) {
        hello(auth.network).api('/me').success(function(r){
          console.log(r);
          $('#login_loader span').text('Logging in with '+auth.network+'...');
          $('#login_loader').show();
          $.get('/login/sns_exist', { sns_type : auth.network, sns_id : r.id }, function(res){
            console.log(res);
            if(res.sns_exists) {
              // Proceed to auth_sns
              console.log(res.user_data);
              ajax_auth(res.user_data.username, res.user_data.confirm_code);
            } else {
              // Create account then auth_sns
              $.post('/login/create_sns_account', { auth_detail : auth, sns_detail : r }, function(data){
                console.log(data);
                ajax_auth(data.user_data.username, data.user_data.confirm_code);
              });
            }
          });
          
        });
      } else {
        console.log(auth.error.message);
      }
  });
});

function ajax_auth(username, password) {
  var url = '/login/auth';
  var form = $('body').append('<form id="auth_form_hidden" action="' + url + '" method="post">' +
    '<input type="hidden" name="username" value="' + username + '" />' +
    '<input type="hidden" name="password" value="' + password + '" />' +
    '</form>');
  ///$(form).submit();
  //$(form).trigger('submit');
  $('#auth_form_hidden').trigger('submit');
}

</script>

</body>
</html>
