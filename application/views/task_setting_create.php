<div id="container_top">
<h4>Calendar Task Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>
<br clear="all" />
<div id="task-setting" class="container-fluid">
	<div class="create">
		<h1>Create Action</h1>
		<form role="form" action="<?php echo base_url(); ?>tasksetting/addAction" method="post">
		  <div class="form-group">
			<label for="actionName">Action Name</label>
			<input type="text" class="form-control input-lg" id="actionName" name="actionName" placeholder="Enter action">
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Choose Icons</label>
			<div class="icons-list">
				<ul class="nav nav-pills list-unstyled list-unstyled icons-list-wrapper">
				<?php foreach($icons as $icon ){ ?>
					<li>
						<input type="radio" name="icons" value="<?php echo $icon;?>">
						<i class="fa <?php echo $icon;?> fa-2x"></i>
					</li>
				<?php } ?>
				</ul>
			</div>
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Choose Color Label</label>
			<div class="color-label-list">
				<ul class="nav nav-pills list-unstyled list-unstyled color-label-wrapper">
				<?php foreach($color_label as $color ){ ?>
					<li>
						<input type="radio" name="color_label" value="<?php echo $color;?>">
						<span class="swatch" style="height: 40px;width:50px;display:block;background-color: <?php echo $color;?>"></span>
					</li>
				<?php } ?>
				</ul>
			</div>
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		  <a class="btn" href="<?php echo base_url(); ?>tasksetting">Cancel</a>
		</form>
	</div>
</div>
<?php require("common/footer.php"); ?>
