<div id="container_top">
<h4>Settings</h4>
<ul class="nav nav-tabs">
    <li class="active"><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/help">Help Video</a></li>*/ ?>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span6 well">
        <h5>Account</h5>
        <p>Manage your account subscription, including settings for recurring payments. You can also purchase additonal sms/text credits.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/account">Account settings ></a></small></p>

        <h5>Tags</h5>
        <p>Tags can be attached to your clients, both private and company clients. Once a tag is attached you can filter clients by tag, helping you find relevant clients easily.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/tags">Tags ></a></small></p>

        <h5>Email Settings</h5>
        <p>some nice info needs to go here.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/email">Email settings ></a></small></p>


        </div>
        <div class="span6 well">
        <h5>User Settings</h5>
        <p>Control who has access to your account.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/users">User settings ></a></small></p>


        <h5>Custom Fields</h5>
        <p>Custom fields allows users to collect information that is relevent to their needs. You can create custom fields to hold any data you require.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/custom_fields">Custom fields ></a></small></p>

        <h5>Screen Settings</h5>
        <p>You can easily change the colour theme used within the system. Select from 6 available themes.</p>
        <p style="border-bottom:#E3E3E3 1px solid; padding-bottom:10px;"><small><a href="<?php echo base_url(); ?>settings/screen">Screen settings ></a></small></p>

        </div>
    </div>

<?php require("common/footer.php"); ?>
