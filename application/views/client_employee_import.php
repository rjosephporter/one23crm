<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name = $customer_details['company_name']; } else { $customer_name = $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?>

<div id="container_top">
<h4><?php echo $customer_name; ?></h4>
    
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
    <?php if ($customer_details['account_type']==2) { ?>
    <li class="active"><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  
      <div class="row-fluid">
      	<div class="span12"><p><a href="<?php echo base_url(); ?>clients/people?id=<?php echo $this->input->get('id'); ?>" class="btn">Return</a></p></div>
      </div>  
  

<div class="row-fluid">
	<div class="span7 well">
    <?php echo $this->session->flashdata('import_error'); ?>
    <form action="<?php echo base_url(); ?>clients/employee_verify_import" method="post" enctype="multipart/form-data">
    <label>Please Select CSV File:<br />
    <input name="csv_file" type="file" />
    </label>
    <label class="checkbox">
    <input name="headers" type="checkbox" value="1" /> Does the first row of the csv file contain headers (names of each column)?
    </label>
    <p>&nbsp;</p>
    <button type="submit" class="btn">Import File</button>
    <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
    </form>
    </div>    
    <div class="span5 well helpbox">
    <h5 style="margin-top:0px;">Importing Employees - Step 1</h5>
    <p>Proin dui est, porttitor fermentum semper vel, elementum ut mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean fermentum sagittis elementum.</p>    
    </div>
</div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
