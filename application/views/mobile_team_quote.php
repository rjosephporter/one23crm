<?php //print_r($client_details); ?>
<script>
$(document).ready(function() {

	$("#term_prem_amount").hide();
	$("#death_cic_opts").hide();
	$("#cic_opts").hide();
	$("#term_prem_ci_amount").hide();
	$("#term_death_ci_prem").hide();	
	
	$("#term_type").change(function() {
		var term_benefit_type = $(this).val();
		if (term_benefit_type=="DEATH_BENEFIT") {
			$("#death_opts").show();
			$("#cic_opts").hide();
			$("#death_cic_opts").hide();
		} else if (term_benefit_type=="CRITICAL_ILLNESS") {		
			$("#cic_opts").show();
			$("#death_opts").hide();
			$("#death_cic_opts").hide();		
		} else if (term_benefit_type=="DEATH_OR_EARLIER_CIC") {
			$("#death_cic_opts").show();
			$("#death_opts").hide();
			$("#cic_opts").hide();				
		}
	});	
	
	$("input:radio[name=driven]").click(function() {
		var term_driven = $(this).val();
		if (term_driven=="Benefit") {			
			$("#term_prem_amount").hide();
			$("#term_ben_amount").show();
		} else {
			$("#term_ben_amount").hide();
			$("#term_prem_amount").show();
		}
	});
	
	$("input:radio[name=cic_driven]").click(function() {
		var term_driven = $(this).val();
		if (term_driven=="Benefit") {			
			$("#term_prem_ci_amount").hide();
			$("#term_ben_ci_amount").show();
		} else {
			$("#term_ben_ci_amount").hide();
			$("#term_prem_ci_amount").show();
		}
	});
	
	$("input:radio[name=death_cic_driven]").click(function() {
		var term_driven = $(this).val();
		if (term_driven=="Benefit") {			
			$("#term_death_ci_prem").hide();
			$("#term_death_ci_amount").show();
			$("#term_death_ben_amount").show();
		} else {
			$("#term_death_ben_amount").hide();
			$("#term_death_ci_amount").hide();
			$("#term_death_ci_prem").show();
			$("#death_cic_multi_select").hide();
		}
	});
	
	$("#occupation_search").click(function() {
		var occ_value = $('#lifea_occupation').val();
		if (occ_value!="") {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url(); ?>clients/webline_occupation_list",
				cache: false,
				data: 'job='+occ_value,
				dataType: "html",
				success: function(list){
					$("#OccList").hide().html(list).fadeIn('Slow');
				}
			});	
		}
	});			

});
</script>
<form action="<?php echo base_url(); ?>clients/webline_protection_save?id=<?php echo $this->input->get('id'); ?>" method="post">
<h4 style="font-size:26px; padding-bottom:10px;">New Term Protection Quote</h4>
<h4>Client Details</h4>
<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5" id="lifea_box">
  <tr>
    <td width="30%" align="right">Title:</td>
    <td width="70%" align="left">
    <select name="lifea_title" style="padding:10px;">
    <option value="Mr" <?php if ($client_details['title']=="Mr") { echo 'selected="selected"'; } ?>>Mr</option>
    <option value="Mrs" <?php if ($client_details['title']=="Mrs") { echo 'selected="selected"'; } ?>>Mrs</option>
    <option value="Miss" <?php if ($client_details['title']=="Miss") { echo 'selected="selected"'; } ?>>Miss</option>
    <option value="Ms" <?php if ($client_details['title']=="Ms") { echo 'selected="selected"'; } ?>>Ms</option>
    </select>            
    </td>
  </tr>
  <tr>
    <td width="30%" align="right">First Name:</td>
    <td align="left"><input name="lifea_name" type="text" value="<?php echo $client_details['first_name']; ?>" /></td>
  </tr>
  <tr>
    <td width="30%" align="right">Surname:</td>
    <td align="left"><input name="lifea_surname" type="text" value="<?php echo $client_details['last_name']; ?>" /></td>
  </tr>
  <tr>
    <td width="30%" align="right">Date of Birth:(dd/mm/yyyy)</td>
    <td align="left"><input name="lifea_dob" type="text" id="lifea_dob" value="<?php if ($client_details['dob']!="0000-00-00") { echo date("d/m/Y", strtotime($client_details['dob'])); } ?>" /></td>
  </tr>
  <tr>
    <td width="30%" align="right">Gender/Sex:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="lifea_sex" value="Male" <?php if ($client_details['title']=="Mr") { echo 'checked="checked"'; } ?> /> Male</label><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="lifea_sex" <?php if ($client_details['title']!="Mr") { echo 'checked="checked"'; } ?> value="Female" /> Female</label></td>
  </tr>          
  <tr>
    <td width="30%" align="right">Smoker:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="lifea_smoker" value="No" checked="checked" /> No</label><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="lifea_smoker" value="Yes" /> Yes</label></td>
  </tr>          
  <tr>
    <td width="30%" align="right">Occupation:</td>
    <td align="left"><div id="OccList"></div>
    <input name="lifea_occupation" id="lifea_occupation" type="text" value="<?php echo $client_details['occupation']; ?>"/>
    <button id="occupation_search" type="button">Find Occupation</button></td>
  </tr>
</table>
<br clear="all" />

<h4>Product Options</h4>
<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="30%" align="right">Type:</td>
    <td width="70%" align="left">
    <select id="term_type" name="type" style="padding:10px;">
        <option value="DEATH_BENEFIT">Death Benefit Only</option>
        <option value="CRITICAL_ILLNESS">Critical Illness Only</option>
        <option value="DEATH_OR_EARLIER_CIC">Death with Critical Illness</option>
    </select>            
    </td>
  </tr>
</table>

<table id="death_opts" align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="30%" align="right">Driven:</td>
    <td width="70%" align="left"><label class="radio inline" style="margin-top:-10px;"><input name="driven" type="radio" checked="checked" value="Benefit" /> Benefit </label><label class="radio inline" style="margin-top:-10px;"><input name="driven" type="radio" value="Premium" /> Premium</label></td>
  </tr>   
  <tr id="term_ben_amount">
    <td width="30%" align="right">Death Benefit Amount:</td>
    <td align="left"><input name="term_benefit_amount[1]" type="text" value="" /></td>
  </tr> 
  <tr id="term_prem_amount">
    <td width="30%" align="right">Premium:</td>
    <td align="left"><input name="term_premium_amount[1]" type="text" value="" /></td>
  </tr>
</table>

<table id="cic_opts" align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="30%" align="right">Driven:</td>
    <td width="70%" align="left"><label class="radio inline" style="margin-top:-10px;"><input name="cic_driven" type="radio" checked="checked" value="Benefit" /> Benefit </label><label class="radio inline" style="margin-top:-10px;"><input name="cic_driven" type="radio" value="Premium" /> Premium</label></td>
  </tr>           
  <tr>
    <td width="30%" align="right">CIC Options:</td>
    <td align="left">
    <select name="cic_opt" style="padding:10px;">
        <option value="1">CIC Only</option>
        <option value="16">CIC & TPD (own occ)</option>
        <option value="32">CIC & TPD (any occ)</option>
        <option value="64">CIC & TPD (suited occ)</option>                                
    </select>
    </td>
  </tr>
  <tr id="term_ben_ci_amount">
    <td width="30%" align="right">CI Benefit Amount:</td>
    <td align="left"><input name="term_ben_amount[1]" type="text" value="" /></td>
  </tr> 
  <tr id="term_prem_ci_amount">
    <td width="30%" align="right">Premium:</td>
    <td align="left"><input name="term_prem_amount[1]" type="text" value="" /></td>
  </tr>
</table>

<table id="death_cic_opts" align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="30%" align="right">Driven:</td>
    <td width="70%" align="left"><label class="radio inline" style="margin-top:-10px;"><input name="death_cic_driven" type="radio" checked="checked" value="Benefit" /> Benefit </label><label class="radio inline" style="margin-top:-10px;"><input name="death_cic_driven" type="radio" value="Premium" /> Premium</label></td>
  </tr>                     
  <tr>
    <td width="30%" align="right">CIC Options:</td>
    <td align="left">
    <select name="death_cic_opt" style="padding:10px;">
        <option value="1">CIC Only</option>
        <option value="16">CIC & TPD (own occ)</option>
        <option value="32">CIC & TPD (any occ)</option>
        <option value="64">CIC & TPD (suited occ)</option>                                
    </select>
    </td>
  </tr>
  <tr id="term_death_ben_amount">
    <td width="30%" align="right">Death Benefit Amount:</td>
    <td align="left"><input name="term_death_ben_amount" type="text" value="" /></td>
  </tr>
  <tr id="term_death_ci_amount">
    <td width="30%" align="right">CI Benefit Amount:</td>
    <td align="left"><input name="term_ci_ben_amount" type="text" value="" /></td>
  </tr>                    
  <tr id="term_death_ci_prem">
    <td width="30%" align="right" valign="top">Premium:</td>
    <td align="left"><input name="term_death_prem_amount[1]" type="text" value="" /></td>
  </tr>         
</table>

<table align="left" width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="30%" align="right">Term in Years:</td>
    <td width="70%" align="left"><input name="term[1]" type="text" maxlength="2" value="" /></td>
  </tr>        
  <tr>
    <td width="30%" align="right">Payment Frequency:</td>
    <td align="left">
    <select name="payment" style="padding:10px;">
        <option value="Monthly">Monthly</option>
        <option value="Annually">Annually</option>
    </select>
    </td>
  </tr>
  <tr>
    <td width="30%" align="right">Increasing Benefit?:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="increasing" checked="checked" value="No" /> No</label><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="increasing" value="Yes" /> Yes</label></td>
  </tr>
  <tr>
    <td width="30%" align="right">Waiver of Premium:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="waiver" checked="checked" value="None" /> No</label><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="waiver" value="Yes" /> Yes</label></td>
  </tr>
  <tr>
    <td width="30%" align="right">Low Start Products:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="lowstart" value="1" /> No</label><label class="radio inline" style="margin-top:-10px;"><input name="lowstart" type="radio" value="0" checked="checked" /> 
    Yes</label></td>
  </tr>
  <tr>
    <td width="30%" align="right">Accelerator Products:</td>
    <td align="left"><label class="radio inline" style="margin-top:-10px;"><input type="radio" name="accelerator" value="1" /> No</label><label class="radio inline" style="margin-top:-10px;"><input name="accelerator" type="radio" value="0" checked="checked" /> 
    Yes</label></td>
  </tr>          
  <tr>
    <td width="30%" align="right">Policy Type:</td>
    <td align="left">
    <select name="policy" style="padding:10px;">
        <option value="Both">Reviewable &amp; Guaranteed</option>
        <option value="No">Guaranteed</option>
        <option value="Yes">Reviewable</option>
    </select>            
    </td>
  </tr>        
</table>

<br clear="all" />
<input type="hidden" name="policy_type" value="single" />
<input name="" type="submit" style="margin:10px; padding:10px;" value="Save Quote" />
</form>
