<?php if ($report_details) {
//print_r($report_details);
 ?>
<div class="title_bar">Income Details</div>
<ul class="wide_list">
<?php echo '<li><strong>Total Gross Income:</strong> &pound;' . number_format($report_details['income'],2) . '</li>';
if (!empty($report_details['client_income'])) { echo '<li><strong>Client Income:</strong> &pound;' . number_format($report_details['client_income'],2) . '</li>'; }
if (!empty($report_details['occupation'])) { echo '<li><strong>Occupation:</strong> ' . $report_details['occupation'] . '</li>'; }
if (!empty($report_details['partner_name'])) {
	if (!empty($report_details['partner_income'])) { echo '<li><strong>Partner Income:</strong> ' . number_format($report_details['partner_income'],2) . '</li>'; }
	if (!empty($report_details['partner_occupation'])) { echo '<li><strong>Partner Occupation:</strong> ' . $report_details['partner_occupation'] . '</li>'; }
}
?>
</ul>

<div class="title_bar">Mortgage Details</div>
<ul class="wide_list">
<?php if ($report_details['mortgage']!="0.00") { 
if (!empty($report_details['mortgage'])) { echo '<li><strong>Amount Remaining:</strong> &pound;' . number_format($report_details['mortgage'],2) . '</li>'; }
if (!empty($report_details['remaining'])) { echo '<li><strong>Term Remaining:</strong> ' . $report_details['remaining'] . '</li>'; }
if (!empty($report_details['mortgage_payment'])) { echo '<li><strong>Monthly Payment:</strong> &pound;' . number_format($report_details['mortgage_payment'],2) . '</li>'; }
} else {
	echo '<li>No mortgage details held.</li>';
}?>
</ul>

<div class="title_bar">Expenditure Details</div>
<ul class="wide_list">
<?php if ($report_details['mortgage_payment']!="0.00") { echo '<li><strong>Monthly Payment:</strong> &pound;' . number_format($report_details['mortgage_payment'],2) . '</li>'; } 
if ($report_details['rent']!="0.00") { echo '<li><strong>Rent:</strong> &pound;' . number_format($report_details['rent'],2) . '</li>'; }
if ($report_details['council_tax']!="0.00") { echo '<li><strong>Council Tax:</strong> &pound;' . number_format($report_details['council_tax'],2) . '</li>'; }
if ($report_details['food']!="0.00") { echo '<li><strong>Food/Shopping:</strong> &pound;' . number_format($report_details['food'],2) . '</li>'; }
if ($report_details['gas']!="0.00") { echo '<li><strong>Gas Bill:</strong> &pound;' . number_format($report_details['gas'],2) . '</li>'; }
if ($report_details['electric']!="0.00") { echo '<li><strong>Electric Bill:</strong> &pound;' . number_format($report_details['electric'],2) . '</li>'; }
if ($report_details['water']!="0.00") { echo '<li><strong>Water Rates:</strong> &pound;' . number_format($report_details['water'],2) . '</li>'; }
if ($report_details['petrol']!="0.00") { echo '<li><strong>Petrol:</strong> &pound;' . number_format($report_details['petrol'],2) . '</li>'; }
if ($report_details['loan']!="0.00") { echo '<li><strong>Secured Loan(s):</strong> &pound;' . number_format($report_details['loan'],2) . '</li>'; }
if ($report_details['loan2']!="0.00") { echo '<li><strong>Unsecured Loan(s):</strong> &pound;' . number_format($report_details['loan2'],2) . '</li>'; }
if ($report_details['motor_finance']!="0.00") { echo '<li><strong>Motor Finance:</strong> &pound;' . number_format($report_details['motor_finance'],2) . '</li>'; }
if ($report_details['hp']!="0.00") { echo '<li><strong>Hire Purchase:</strong> &pound;' . number_format($report_details['hp'],2) . '</li>'; }
if ($report_details['credit_cards']!="0.00") { echo '<li><strong>Credit Cards:</strong> &pound;' . number_format($report_details['credit_cards'],2) . '</li>'; }
if ($report_details['school']!="0.00") { echo '<li><strong>School Fees:</strong> &pound;' . number_format($report_details['school'],2) . '</li>'; }
if ($report_details['childcare']!="0.00") { echo '<li><strong>Childcare:</strong> &pound;' . number_format($report_details['childcare'],2) . '</li>'; }
if ($report_details['skycable']!="0.00") { echo '<li><strong>Sky / Cable Subscription:</strong> &pound;' . number_format($report_details['skycable'],2) . '</li>'; }
if ($report_details['pension']!="0.00") { echo '<li><strong>Pension:</strong> &pound;' . number_format($report_details['pension'],2) . '</li>'; }
if ($report_details['life']!="0.00") { echo '<li><strong>Life Insurance:</strong> &pound;' . number_format($report_details['life'],2) . '</li>'; }
if ($report_details['home']!="0.00") { echo '<li><strong>Home Insurance:</strong> &pound;' . number_format($report_details['home'],2) . '</li>'; }
if ($report_details['car']!="0.00") { echo '<li><strong>Car Insurance:</strong> &pound;' . number_format($report_details['car'],2) . '</li>'; }
if ($report_details['medical']!="0.00") { echo '<li><strong>Medical Insurance:</strong> &pound;' . number_format($report_details['medical'],2) . '</li>'; }
if ($report_details['landline']!="0.00") { echo '<li><strong>Landline &amp; Internet:</strong> &pound;' . number_format($report_details['landline'],2) . '</li>'; }
if ($report_details['tv']!="0.00") { echo '<li><strong>TV Licence:</strong> &pound;' . number_format($report_details['tv'],2) . '</li>'; }
if ($report_details['mobile']!="0.00") { echo '<li><strong>Mobile Phone(s):</strong> &pound;' . number_format($report_details['mobile'],2) . '</li>'; }
if ($report_details['gym']!="0.00") { echo '<li><strong>Gym Membership:</strong> &pound;' . number_format($report_details['gym'],2) . '</li>'; }
if ($report_details['breakdown']!="0.00") { echo '<li><strong>Breakdown Cover:</strong> &pound;' . number_format($report_details['breakdown'],2) . '</li>'; }
if ($report_details['savings']!="0.00") { echo '<li><strong>Savings:</strong> &pound;' . number_format($report_details['savings'],2) . '</li>'; }
if ($report_details['goingout']!="0.00") { echo '<li><strong>Going Out / Socialising:</strong> &pound;' . number_format($report_details['goingout'],2) . '</li>'; }
if ($report_details['other']!="0.00") { echo '<li><strong>Other:</strong> &pound;' . number_format($report_details['other'],2) . '</li>'; }

echo '<li><strong>Total Outgoings:</strong> &pound;' . number_format($report_details['outgoings'],2) . '</li>';

?>
</ul>

<div class="title_bar">Bank Details</div>
<ul class="wide_list">
<?php if ($report_details['bank_acc']!="") { 
echo '<li><strong>Account Name:</strong> ' . $report_details['bank_acc_name'] . '</li>';
echo '<li><strong>Account Number:</strong> ' . $report_details['bank_acc'] . '</li>';
echo '<li><strong>Sort Code:</strong> ' . $report_details['bank_sort'] . '</li>';
echo '<li><strong>Bank Name:</strong> ' . $report_details['bank_name'] . '</li>';
} else {
	echo '<li>No bank details held.</li>';
} ?>
</ul>
<?php } else {
redirect('clients');
} ?>
