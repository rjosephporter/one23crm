
<script>
$(document).ready(function(){
	$("#keyword").focus();

	$("#client_search").submit(function() {
		return false;
	});
	
	// on page load just get the full list
	$.ajax({
		type: "GET",
		url: "<?php echo base_url(); ?>clients/ajax_search",
		cache: false,
		data: {
			'keyword' : '', 'tag' : ''
		},
		dataType: "html",
		beforeSend: function() {
			$('#list_of_clients').html('<h2>Loading ...</h2>');
		},
		success: function(msg){                    
			$('#list_of_clients').html(msg);
		}
	});

	var searchMinLength = 3;
	
	$("#keyword, #tag, #user").bind("change keyup",function () {
		var value = $("#keyword").val();
		var tag = $("#tag").val();
		var user = $("#user").val();
		if ((value.length >= searchMinLength) || (value.length==0)) {
		
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>clients/ajax_search",
				cache: false,
                data: {
                    'keyword' : value, 'tag' : tag, 'user' : user
                },
                dataType: "html",
                success: function(msg){                    
					$('#list_of_clients').hide().html(msg).fadeIn('Slow');
                }
            });		
		
		}
	});
	
	$("#tag, #user").bind("change keyup",function () {
		var tag = $("#tag").val();
		var user = $("#user").val();
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>clients/ajax_search",
				cache: false,
                data: {
                    'tag' : tag, 'user' : user
                },
                dataType: "html",
                success: function(msg){                    
					$('#list_of_clients').hide().html(msg).fadeIn('Slow');
                }
            });		
	});
	
	$("#list_of_clients").on("click", "#delete_all", function() {
		$(".client_selected").prop('checked', this.checked);
	});
	
	$(document).on("mouseover", ".client_info", function(event) {
		if (event.type === "mouseover") {
			var e = $(this);
			var client = e.data('client');
			var infoContent = '';
			
				$.ajax({
					type: "GET",
					url: "<?php echo base_url(); ?>clients/ajax_client_details",
					cache: false,
					async: false,
					data: {
						'client' : client
					},
					dataType: "html",
					success: function(msg){                    
						infoContent = '<div style="color:#000;">'+msg+'</div>';
					}
				});	

			e.popover({ title: '<div style="color:#000;width:400px;">Quick Client Overview</div>', html: true, content: infoContent });
			e.popover("show");
		}
	});
	
	$(document).on("mouseout", ".client_info", function(event) {
		infoContent = '';
		$(this).popover("hide");			
	});
		
});
</script>
<div id="container_top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" align="left"><h4>Clients</h4></td>
    <td align="right"><p><a target="_self" href="<?php echo base_url(); ?>clients/create_company" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Company</a>&nbsp;&nbsp;&nbsp;<a target="_self" href="<?php echo base_url(); ?>clients/create" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Person</a>&nbsp;&nbsp;&nbsp;<a target="_self" href="<?php echo base_url(); ?>clients/import" class="btn">Import</a></p></td>
  </tr>
</table>
</div>

<br clear="all" />

<div class="container-fluid">

<!--<form class="form-inline" action="<?php //echo base_url(); ?>clients" method="get">-->
<form class="form-inline" action="#" id="client_search">
  <label style="font-size:12px;">Find a person or company by typing their name or phone number<br />
  <input type="text" name="keyword" id="keyword" style="width:300px;" autocomplete="off"></label>
  <label style="font-size:12px; margin-left:30px;">Filter by tag<br />
  <select name="tag" id="tag" style="height:23px; padding:0px;width:110px;">
  <option value="">No tag filter</option>
<?php if ($tag_list) {
	foreach($tag_list as $tag) {
		echo '<option value="'. $tag['id'] .'">' . $tag['tag'] .'</option>';	
	}
} ?>  
  </select></label>
  <?php if ($this->session->userdata("role")==1) { ?>
  <label style="font-size:12px; margin-left:30px;">Filter by added user<br />
  <select name="user" id="user" style="height:23px; padding:0px;">
  <option value="">Myself Only</option>
  <?php if ($group_users) {
  	foreach ($group_users as $user) {
		echo '<option value="'. $user['user_id'] .'">'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';	
	}
  } ?> 
  </select></label>
  <?php } ?>
</form>
<div id="list_of_clients">
<?php /* if ($customer_list) {

	echo '<table class="table" id="client-list"><tbody>';

		foreach($customer_list as $customer) { 
		
		if (!empty($customer['company_name'])) { 
			$display_name = '<a href="'. base_url() .'clients/view?id='. $customer['id'] .'"><span style="font-size:14px; font-weight:bold;">' . $customer['company_name'] . '</span></a>';
		} else {
			$display_name = '<a href="'. base_url() .'clients/view?id='. $customer['id'] .'"><span style="font-size:14px; font-weight:bold;">' .$customer['first_name'] . ' ' . $customer['last_name'] . '</span></a>';		
		}
		
		if ( (!empty($customer['organisation'])) && (!empty($customer['job_title'])) ) {
		
			$display_name = '<a href="'. base_url() .'clients/view?id='. $customer['id'] .'"><span style="font-size:14px; font-weight:bold;">'. $customer['first_name'] . ' ' . $customer['last_name'] . '</span></a> ' . $customer['job_title'] . ' at <a href="'. base_url() .'clients/view?id='. $customer['associated'] .'">' . $customer['organisation'] . '</a>';
		
		}
		
		if ($customer['type']==2) { $list_image = "small_summary_comp.png"; } else { $list_image = "small_summary_person.png"; }
		
		if ($customer['address']!=", , ") {
			$address = $customer['address'];
		} else {
			$address = "";		
		}
		
		
		echo '<tr>
		<td width="40" align="center" valign="middle"><a href="'. base_url() .'clients/view?id='. $customer['id'] .'"><img src="'. base_url() .'img/'. $list_image .'" width="40" /></a></td>
		<td width="60%" valign="middle">'. $display_name .'<br />'. $address. '</td>
		<td valign="middle">';
		
		if (!empty($customer['number'])) {
		
			echo $customer['email'] . '<br />' . $customer['number']; 
		
		} else {
				
			echo $customer['email'];
		
		}
		 
		 echo '</td>
		 <td valign="middle"><a href="'.base_url().'clients/delete_client?id='.$customer['id'].'" onClick="return confirm(\'Are you sure you wish to delete this client?\')"><img src="'.base_url().'img/delete-icon.png" /></a></td>
		</tr>';
		
		}
		
	echo '</tbody></table>';	


} else {

	echo '<h4>Sorry no clients found.</h4>';

} */?>

</div>

<?php require("common/footer.php"); ?>
