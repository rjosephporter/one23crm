<script>
$(document).ready(function(){
	
	$("#message").keyup(function() {
		var smsCount = $("#message").val().length;
		var smsNeeded = Math.ceil(smsCount/160);
		$("#sms_message_counter").html("Character count without personalised message is <strong>"+smsCount+"</strong> and will use "+smsNeeded+" credits per message.");	
	});
});
</script>

<div id="container_top">
<h4>SMS Marketing</h4>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

			 <?php echo $this->session->flashdata('error'); ?>

            <div class="row-fluid">
                <div class="span12 well">
                	<h5 style="margin-top:0px;">Personalised Message</h5>
                    If you would like each message to start "Hi <i>First Name</i>." please tick the box below. Please remember this will increase the character count, so may require additional sms credits to send. We will give you a summary on the next page.
                    <form action="<?php echo base_url(); ?>marketing/verify2" method="post">
                    <input type="checkbox" name="personalised" value="1" />
                </div>       
            </div>
            
            <div class="row-fluid">
                <div class="span12 well">
                	<h5 style="margin-top:0px;">Enter your message below</h5>
                	<textarea rows="7" style="width:70%;" id="message" name="message" placeholder="Enter your message ..."><?php echo $this->session->flashdata('message'); ?></textarea>
                    <p id="sms_message_counter"></p>
                    <p><button type="submit" class="btn">Next Step</button></p>
                    </form>
                </div>       
            </div>
        
        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">SMS Marketing</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis orci nec volutpat eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec hendrerit ultricies nulla at consequat.</p>
            <p>Nunc sollicitudin eros malesuada ante egestas, dictum egestas felis venenatis. Praesent id justo dui.</p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
