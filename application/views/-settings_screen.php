
<script>
$(document).ready(function(){

	<?php if ($selected_theme=="clean") { echo '$("#icon_box").hide();'; } ?>

	$("#theme").change(function() {
		// get theme value
		var themeSelected = $(this).val();
		// if clean theme is selected hide the icon box
		if (themeSelected=="clean") {
			$("#icon_box").hide();
		} else {
			$("#icon_box").show();
		}
	});

});
</script>
<div id="container_top">
<h4>Screen Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
                <div class="span12 well"><h5 style="margin-top:0px;">Available Themes</h5>
                    <table width="100%" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=blue&icons=0"><img src="<?php echo base_url(); ?>img/settings_blue_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Blue</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=green&icons=0"><img src="<?php echo base_url(); ?>img/settings_green_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Green</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=yellow&icons=0"><img src="<?php echo base_url(); ?>img/settings_yellow_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Yellow</strong></td>
                      </tr>
                      <tr>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=purple&icons=0"><img src="<?php echo base_url(); ?>img/settings_purple_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Purple</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=pink&icons=0"><img src="<?php echo base_url(); ?>img/settings_pink_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Pink</strong></td>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=red&icons=0"><img src="<?php echo base_url(); ?>img/settings_red_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Red</strong></td>
                      </tr>
                      <tr>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="33%" align="center"><a href="<?php echo base_url(); ?>settings/change_theme?theme=clean&icons=0"><img src="<?php echo base_url(); ?>img/settings_clean_theme.png" style="border:#333 1px solid;" /></a><br /><strong>Clean</strong></td>
                        <td width="33%" align="center">&nbsp;</td>
                        <td width="33%" align="center">&nbsp;</td>
                      </tr>
                    </table>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12 well"><h5 style="margin-top:0px;">Select Theme</h5>
                <form action="<?php echo base_url(); ?>settings/change_theme" method="get">
                <table width="100%" border="0" cellspacing="5" cellpadding="5">
                  <tr>
                    <td width="50" align="right"><strong>Theme:</strong></td>
                    <td align="left"><select name="theme" id="theme" style="padding:0; height:23px;">
                    <optgroup label="Dark Backgrounds">
                    <option value="blue" <?php if ($selected_theme=="blue") { echo 'selected="selected"'; } ?>>Blue</option>
                    <option value="green" <?php if ($selected_theme=="green") { echo 'selected="selected"'; } ?>>Green</option>
                    <option value="yellow" <?php if ($selected_theme=="yellow") { echo 'selected="selected"'; } ?>>Yellow</option>
                    <option value="purple" <?php if ($selected_theme=="purple") { echo 'selected="selected"'; } ?>>Purple</option>
                    <option value="pink" <?php if ($selected_theme=="pink") { echo 'selected="selected"'; } ?>>Pink</option>
                    <option value="red" <?php if ($selected_theme=="red") { echo 'selected="selected"'; } ?>>Red</option>
                    </optgroup>
                    <optgroup label="Clean/Light Backgrounds">
                    <option value="clean" <?php if ($selected_theme=="clean") { echo 'selected="selected"'; } ?>>Clean</option>
                    <option value="blue_clean" <?php if ($selected_theme=="blue_clean") { echo 'selected="selected"'; } ?>>Blue</option>
                    <option value="purple_clean" <?php if ($selected_theme=="purple_clean") { echo 'selected="selected"'; } ?>>Purple</option>
                    <option value="pink_clean" <?php if ($selected_theme=="pink_clean") { echo 'selected="selected"'; } ?>>Pink</option>
                    <option value="red_clean" <?php if ($selected_theme=="red_clean") { echo 'selected="selected"'; } ?>>Red</option>
                    </optgroup>
                    </select></td>
                  </tr>
                  <tr id="icon_box">
                    <td width="50" align="right"><strong>Icons:</strong></td>
                    <td align="left"><select name="icons" id="icons" style="padding:0; height:23px;">
                    <option value="0" <?php if ($selected_icon==0) { echo 'selected="selected"'; } ?>>White styled navigation icons</option>
                    <option value="1" <?php if ($selected_icon==1) { echo 'selected="selected"'; } ?>>Colour-coded navigation icons</option>
                    </select></td>
                  </tr>
                </table>
                <input name="save" type="submit" class="btn" value="Save" />
                </form>
                </div>
            </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">Screen Settings</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
