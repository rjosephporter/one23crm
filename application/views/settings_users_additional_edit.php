<?php if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$icon_set = '-clean';
} else {
	$icon_set = '';
}

if (!$user) {
	redirect("settings/users");
}

?>

<div id="container_top">
<h4>User Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Add Additional User</h5>
                <?php echo $this->session->flashdata('errors'); ?>
                <p>Please provide updated details below.</p>
                <form action="<?php echo base_url(); ?>settings/additional_users_update?user=<?php echo $this->input->get("user"); ?>" method="post">
                <label>NEW Password <small>(leave blank to keep existing)</small>:</label>
                <input type="password" name="password" placeholder="Enter a new password" />
                <label>First Name:</label>
                <input type="text" name="firstname" value="<?php echo $user['first_name']; ?>" placeholder="Enter a first name" />
                <label>Surname:</label>
                <input type="text" name="surname" value="<?php echo $user['last_name']; ?>" placeholder="Enter a surname" />
                <label>Email Address:</label>
                <input type="text" name="email" value="<?php echo $user['email']; ?>" placeholder="Enter an email address" />

                <h5 style="margin-top:0px;">Set User Permissions</h5>

                <table width="60%" border="0" cellspacing="0" cellpadding="5">
                  <tr style="border-bottom:#fff 1px solid;">
                    <td width="60%"><strong>Function</strong></td>
                    <td width="20%" align="center"><strong>Read Only</strong></td>
                    <td align="center"><strong>Full Permission</strong></td>
                  </tr>
                  <tr>
                    <td><strong>Client - Edit Details</strong></td>
                    <td align="center"><input name="client_edit" type="radio" value="0" <?php if ($user['client_edit']==0) { echo 'checked="checked"'; } ?> /></td>
                    <td align="center"><input name="client_edit" type="radio" value="1" <?php if ($user['client_edit']==1) { echo 'checked="checked"'; } ?> /></td>
                  </tr>
                  <tr>
                    <td><strong>Client - Delete</strong></td>
                    <td align="center"><input name="client_delete" type="radio" value="0" <?php if ($user['client_delete']==0) { echo 'checked="checked"'; } ?> /></td>
                    <td align="center"><input name="client_delete" type="radio" value="1" <?php if ($user['client_delete']==1) { echo 'checked="checked"'; } ?> /></td>
                  </tr>
                  <tr>
                    <td><strong>Client - Notes</strong></td>
                    <td align="center"><input name="client_notes" type="radio" value="0" <?php if ($user['client_notes']==0) { echo 'checked="checked"'; } ?> /></td>
                    <td align="center"><input name="client_notes" type="radio" value="1" <?php if ($user['client_notes']==1) { echo 'checked="checked"'; } ?> /></td>
                  </tr>
                  <tr>
                    <td><strong>Client - Files</strong></td>
                    <td align="center"><input name="client_files" type="radio" value="0" <?php if ($user['client_files']==0) { echo 'checked="checked"'; } ?> /></td>
                    <td align="center"><input name="client_files" type="radio" value="1" <?php if ($user['client_files']==1) { echo 'checked="checked"'; } ?> /></td>
                  </tr>
                  <tr>
                    <td><strong>Client - Opportunities</strong></td>
                    <td align="center"><input name="client_opportunities" type="radio" value="0" <?php if ($user['client_opportunities']==0) { echo 'checked="checked"'; } ?> /></td>
                    <td align="center"><input name="client_opportunities" type="radio" value="1" <?php if ($user['client_opportunities']==1) { echo 'checked="checked"'; } ?> /></td>
                  </tr>
                </table>

                <p style="margin-top:10px;">
                <input name="" type="submit" value="Save" class="btn" />
                </p>
				</form>
                </div>
            </div>

        </div>

        <div class="span4">

        </div>
    </div>

<?php require("common/footer.php"); ?>
