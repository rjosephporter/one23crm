<?php //if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$icon_set = '-clean';
//} else {
	//$icon_set = '';
//}

?>

    <div id="newClientTag" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_client_tag" method="post" name="" id="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Client Tag</h4>
    </div>
    <div class="modal-body">
    <label>Tag Name:<br />
    <input type="text" name="client_tag" id="client_tag" class="span4" placeholder="Enter the tag name" />
    </label>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
    </div>
    </div>

    <div id="newOpportunitiesTag" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_opportunities_tag" method="post" name="" id="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Opportunities Tag</h4>
    </div>
    <div class="modal-body">
    <label>Tag Name:<br />
    <input type="text" name="opportunities_tag" id="opportunities_tag" class="span4" placeholder="Enter the tag name" />
    </label>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
    </div>
    </div>

<div id="container_top">
<h4>Tag Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Client Tags</h5>
                <?php if ($tag_client) {

					echo '<table width="100%" class="table">';

						foreach($tag_client as $client) {

							echo '<tr>
							<td width="90%">'.$client['tag'].'</td>
							<td><a href="'.base_url().'settings/remove_client_tag?tag='. $client['id'] .'"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a></td>
							</tr>';

						}

					echo '</table>';

				} else {

					echo "No client tag have been added yet.";

				} ?>
                </div>
            </div>
            <p><a href="#newClientTag" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add client tag</a></p>
            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Opportunities Tag</h5>
                <?php if ($tag_opportunities) {

					echo '<table width="100%" class="table">';

						foreach($tag_opportunities as $opportunities) {

							echo '<tr>
							<td width="90%">'.$opportunities['tag'].'</td>
							<td><a href="'.base_url().'settings/remove_opportunities_tag?tag='. $opportunities['id'] .'"><img src="'. base_url() .'img/delete-icon'.$icon_set.'.png" /></a></td>
							</tr>';

						}

					echo '</table>';

				} else {

					echo "No tags for opportunities have been added yet.";

				} ?>
                </div>
            </div>
            <p><a href="#newOpportunitiesTag" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add opportunities tag</a></p>
        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">About Tag Settings</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
