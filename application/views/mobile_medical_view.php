<?php if ($medical_details) { ?>
<div class="title_bar">Lifestyle Details</div>
<ul class="wide_list">
<?php if (!empty($medical_details['height'])) { echo '<li><strong>Height:</strong> ' . $medical_details['height'] . '</li>'; }            
if (!empty($medical_details['weight'])) { echo '<li><strong>Weight:</strong> ' . $medical_details['weight'] . '</li>'; }
if (!empty($medical_details['waist'])) { echo '<li><strong>Waist Size:</strong> ' . $medical_details['waist'] . '</li>'; }
if (!empty($medical_details['participate'])) { echo '<li><strong>Participate in any hazardous pursuits?</strong> ' . $medical_details['participate'] . '</li>'; }
if (!empty($medical_details['abroad'])) { echo '<li><strong>Lived/travelled abroad?</strong> ' . $medical_details['abroad'] . '</li>'; }
if (!empty($medical_details['lifestyle_info'])) { echo '<li><strong>Additional Information:</strong><br />' . nl2br($medical_details['lifestyle_info']) . '</li>'; } ?>
</ul>
<div class="title_bar">Doctors Details</div>
<ul class="wide_list">
<?php if (!empty($medical_details['doctor'])) {
if (!empty($medical_details['doctor'])) { echo '<li><strong>Doctor\'s Name:</strong> ' . $medical_details['doctor'] . '</li>'; }
if (!empty($medical_details['surgery'])) { echo '<li><strong>Surgery Name:</strong> ' . $medical_details['surgery'] . '</li>'; }
if (!empty($medical_details['surgery_address'])) { echo '<li><strong>Surgery Address:</strong><br />' . nl2br($medical_details['surgery_address']) . '</li>'; }
if (!empty($medical_details['surgery_telephone'])) { echo '<li><strong>Surgery Telephone:</strong> ' . $medical_details['surgery_telephone'] . '</li>'; }
if (!empty($medical_details['time_with_doc'])) { echo '<li><strong>Time with current doc:</strong> ' . $medical_details['time_with_doc'] . '</li>'; }			
} else {
	echo "<li>No doctor details held for this client.</li>";			
} ?> 
</ul>
<div class="title_bar">Medical History 1</div>
<ul class="wide_list">
<?php if ($medical_details['med_1_heart']==1) { echo "<li><strong>Heart Disease:</strong> Yes</li>";  } ?>
<?php if ($medical_details['med_1_stroke']==1) { echo "<li><strong>Stroke:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_1_diabetes']==1) { echo "<li><strong>Diabetes:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_1_cancer']==1) { echo "<li><strong>Cancer:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_1_sclerosis']==1) { echo "<li><strong>Multiple Sclerosis:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_1_depression']==1) { echo "<li><strong>Depression:</strong> Yes</li>"; } ?>
<?php if (!empty($medical_details['med_1_details'])) { echo '<li><strong>Additional Information:</strong><br />' . nl2br($medical_details['med_1_details']) . '</li>'; } ?>
</ul>
<div class="title_bar">Medical History 2</div>
<ul class="wide_list">
<?php if ($medical_details['med_2_stress']==1) { echo "<li><strong>Stress/Anxiety:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_pressure']==1) { echo "<li><strong>High Blood Pressure:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_cholesterol']==1) { echo "<li><strong>High Cholesterol:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_arthritis']==1) { echo "<li><strong>Arthritis:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_fits']==1) { echo "<li><strong>Fits/Fainting/Blackouts:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_digestive']==1) { echo "<li><strong>Digestive Disorders:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_kidney']==1) { echo "<li><strong>Kidney/Bladder:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_fatigue']==1) { echo "<li><strong>Fatigue/Tiredness:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_visual']==1) { echo "<li><strong>Visual Disturbance:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_std']==1) { echo "<li><strong>Had an STD:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_hiv']==1) { echo "<li><strong>HIV:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_gynological']==1) { echo "<li><strong>Gynological Disorder:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_joint']==1) { echo "<li><strong>Joint Problems:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_blood']==1) { echo "<li><strong>Blood Disorders:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_gout']==1) { echo "<li><strong>Gout:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_hearing']==1) { echo "<li><strong>Hearing Problems:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_skin']==1) { echo "<li><strong>Skin Disorders:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_xray']==1) { echo "<li><strong>Xrays or Scans:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_investigations']==1) { echo "<li><strong>Any Investigations:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_surgery']==1) { echo "<li><strong>Surgery in 5 yrs:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_abscence']==1) { echo "<li><strong>2 weeks abscence +:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_medication']==1) { echo "<li><strong>On any medication:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_parents']==1) { echo "<li><strong>Parents ill before 60:</strong> Yes</li>"; } ?>
<?php if ($medical_details['med_2_siblings']==1) { echo "<li><strong>Siblings ill before 60:</strong> Yes</li>"; } ?>
<?php if (!empty($medical_details['med_2_details'])) { echo '<li><strong>Additional Information:</strong><br />' . nl2br($medical_details['med_2_details']) . '</li>'; } ?>
</ul>
<?php } else {
redirect('clients');
} ?>
