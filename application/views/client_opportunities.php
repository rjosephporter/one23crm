<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
} ?>

<div id="container_top">
<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>


</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
      	
        <?php if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_opportunities")==1) ) { ?>
        <div class="row-fluid">
      		<div class="span12"><p><a href="<?php echo base_url(); ?>clients/new_opportunity?id=<?php echo $this->input->get('id'); ?>" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add New Opportunity</a></p></div>
      	</div>
        <?php } ?>
        
        <div class="row-fluid">
          <div class="span12 well" style="min-height:200px;"><h5 style="margin-top:0px;">Opportunities</h5>
		<?php if ($list_opportunities) {
		
        echo '<table width="100%" class="table">
              <thead>
                <tr>
                  <th>Opportunity</th>
                  <th align="center" width="15%">Milestone</th>
                  <th align="center" width="10%">Value</th>
                  <th align="center" width="10%">Close Date</th>
				  <th width="5%"></th>
                </tr>
              </thead><tbody>';
			  
		foreach ($list_opportunities as $opportunity) {
		
			if ($opportunity['status']==0) {
				$status = "<strong>Open</strong>";
			} else {
				$status = "<strong>Closed</strong>";
			}		
		
			if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_opportunities")==1) ) {
			echo '<tr><td><strong><a href="'. base_url() .'clients/edit_opportunity?id='.$this->input->get('id').'&opportunity='.$opportunity['id'].'" target="_self">'. $opportunity['name'] . '</a></strong><br />' . $status;
			} else {
			echo '<tr><td><strong>'. $opportunity['name'] . '</strong><br />' . $status;
			
			
			}
			if (!empty($opportunity['text'])) { echo ' <i>' . $opportunity['text'] . '</i>'; }
			
			echo '</td>
				<td align="center">'. $opportunity['milestone'] .' ('. $opportunity['probability'] .'%)</td>
				<td align="center">&pound;'. $opportunity['value'] .'</td>
				<td align="center">'. date("d/m/Y", strtotime($opportunity['close_date'])) .'</td>';
				
				if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_opportunities")==1) ) {
				echo '<td><a href="'. base_url() .'clients/delete_opportunity?id='.$this->input->get('id').'&opportunity='.$opportunity['id'].'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this opportunity?\')"><img src="'.base_url().'img/delete-icon'. $icon_set .'.png" /></a></td>';
				}
			
			echo '</tr>';		
		
		}			  
			  
		echo '</tbody></table>';        
        
        } else {
		
			echo 'Currently no opportunities for this client.';
		
		}?>                  
          </div>
        </div>      
      
      </div>
      
    </div>
  </div>
</div>
<?php require("common/footer.php"); ?>
