<?php if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$icon_set = '-clean';
} else {
	$icon_set = '';
}

?>

<div id="container_top">
<h4>User Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <?php if($parent_user): ?>
        <div class="span8">

            <div class="row-fluid">
            	<div class="span12 well"><h5 style="margin-top:0px;">Additional Users</h5>

                <?php if ($additional_users) {

					echo '<table width="100%" class="table">';

						foreach($additional_users as $user) {

							echo '<tr>
							<td width="10%">'. $user['username'] .'</td>
							<td width="75%">'. $user['first_name'] .' '. $user['last_name'] .'</td>
							<td width="15%"><a href="'.base_url().'settings/users_edit_additional?user='. $user['user_id'] .'">Edit</a> | <a href="'.base_url().'settings/users_delete_additional?user='. $user['user_id'] .'">Delete</a></td>
							</tr>';

						}

					echo '</table>';

				} else {

					echo 'You currently do not have any additional users linked to your account.';

				} ?>

                </div>
            </div>
            <p><a href="<?php echo base_url(); ?>settings/users_additional"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add additional user</a></p>
        </div>

        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">About User Settings</h5>
            <p></p>
        </div>
        <?php else: ?>
        <div class="span12">
            This is not available for sub users.
        </div>
        <?php endif; ?>
    </div>

<?php require("common/footer.php"); ?>
