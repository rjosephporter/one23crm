<?php if (!$customer_details) {
redirect('clients');
} ?>
<script>
$(function(){
});
</script>

<div id="container_top">
<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		}
	}
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>        
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>                        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li class="active"><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">

<?php require("common/client_left_menu.php"); ?>

      <div class="span9">

<?php if (!$vmd) {

       echo '<div class="row-fluid">
          <div class="span12">
            <div class="alert alert-error">No View My Documents provider account has been setup. Please contact our support department.</div>
			</div>
			</div>';

} else {

if ($account['vmd']=="") {

       echo '<div class="row-fluid">
          <div class="span12">
            <div class="alert alert-error">';
			
			if ($customer_details['postcode']!="") {
				echo 'No \'View my Documents\' account setup for this client. To enable this service, simply <a href="'.base_url().'clients/view_my_documents_activate?id='.$this->input->get('id').'&enable=yes">click here to activate</a>.';
			} else {
				echo 'No \'View my Documents\' account setup for this client. Before you can enable this service, please ensure you have entered the full address including postcode.';
			}
						
			echo '</div>
		</div>
        </div>';			
		} else { //print_r($account);?>
        <div class="row-fluid">
          <div class="span6 well"><h5 style="margin-top:0px;">Client Account Settings</h5>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td width="20%">Status:</td>
                <td>Active <small>(<a href="<?php echo base_url(); ?>clients/view_my_documents_unlink?id=<?php echo $customer_details['customer_id']; ?>&enable=no">unlink client</a>)</small></td>
              </tr>
              <tr>
                <td width="20%">PIN:</td>
                <td><?php echo $account['vmd_pin']; /* ?> <small>(<a href="<?php echo base_url(); ?>">request new pin</a>)</small><?php */ ?></td>
              </tr>
            </table>
          </div>
        </div>         
            
        <div class="row-fluid">
          <div class="span12 well"><h5 style="margin-top:0px;">Documents Shared with Client</h5>
          <?php if ($shared) {
		  	
			echo '<table class="table">';
		  
		  		foreach($shared as $shared_file) {
				
					echo '<tr>
					<td width="70%"><a href="'.$shared_file['url'] .'" target="_blank">'. $shared_file['name'] .'</a>';
					
					if ($shared_file['notes']!="") {
						echo '<em> - '. $shared_file['notes'] .'</em>';				
					}
					
					echo '</td>
					<td width="15%" style="text-align:center;">'. date("d/m/y H:i", strtotime($shared_file['time'])) .'</td>
					<td align="center" style="text-align:center;"><a href="'.$shared_file['url'] .'" target="_blank">View Document</a>';
					
					//&nbsp;|&nbsp;<a href="#" target="_self" onClick="return confirm(\'Are you sure you wish to unshare this document with the client?\')">Unshare</a>
					
					echo '</td>
					</tr>';
				
				}
		  
		  	echo '</table>';
		  
		  } else {		  	
			echo 'No files/documents have been shared with the client.';
		  } ?>
          </div>
        </div>      
        <div class="row-fluid">
          <div class="span12 well"><h5 style="margin-top:0px;">Documents Uploaded by Client</h5>
          <?php if ($uploaded) { 
		  
			echo '<table class="table">';
		  
		  		foreach($uploaded as $uploaded_file) {
				
					echo '<tr>
					<td width="70%"><a href="'.$uploaded_file['url'] .'" target="_blank">'. $uploaded_file['name'] .'</a>';
					
					if ($uploaded_file['notes']!="") {
						echo '<em> - '. $uploaded_file['notes'] .'</em>';				
					}
					
					echo '</td>
					<td width="15%" style="text-align:center;">'. date("d/m/y H:i", strtotime($uploaded_file['time'])) .'</td>
					<td align="center" style="text-align:center;"><a href="'. $uploaded_file['url'] .'" target="_blank">View Document</a>';
					/*&nbsp;|&nbsp;<a href="'.base_url().'clients/view_my_documents_unshare?id='.$this->input->get('id').'&file='.$uploaded_file['id'].'&user='.$account['id'].'&type=uploaded" target="_self" onClick="return confirm(\'Are you sure you wish to delete this document uploaded by the client?\')">Unshare</a>
					*/
					echo '</td>
					</tr>';
				
				}
		  
		  	echo '</table>';
		  
		  } else {		  	
			echo 'The client has not uploaded any files/documents.';
		  } ?>
          </div>
        </div>
		<?php } 
		} ?>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
