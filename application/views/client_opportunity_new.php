<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
} ?>

<div id="container_top">
<?php require("common/client_tags.php"); ?>    
    
<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>        
                        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li class="active"><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
        
        <div class="row-fluid">
          <div class="span12 well" style="min-height:200px;"><h5 style="margin-top:0px;">Opportunities</h5>
          <form action="<?php echo base_url(); ?>clients/create_new_opportunity" method="post">
          Opportunity Name:<br />
          <input type="text" name="name" />
          <br />
          Opportunity Description:<br />
          <textarea name="text" cols="10" rows="3" style="padding:2px;"></textarea>
          <br />
          Expected Value (&pound;):<br />
          <input type="text" name="value" />
          <br />
          Milestone &amp; Probability of Winning:<br />
          <select name="milestone" id="milestone" style="padding:0px; height:23px; width:100px;">
            <option value="0">Select ...</option>
            <option value="Suspect">Suspect</option>
            <option value="Prospect">Prospect</option>
            <option value="Champion">Champion</option>
            <option value="Opportunity">Opportunity</option>
            <option value="Proposal">Proposal</option>
            <option value="Verbal">Verbal</option>
            <option value="Lost">Lost</option>
            <option value="Won">Won</option>          
          </select>&nbsp;&nbsp;
          <select name="probability" id="probability" style="padding:0px; height:23px; width:100px;">
            <option value="0">Select ...</option>
            <option value="10">10%</option>
            <option value="20">20%</option>
            <option value="25">25%</option>
            <option value="30">30%</option>
            <option value="50">50%</option>
            <option value="75">75%</option>
            <option value="90">90%</option>
            <option value="100">100%</option>          
          </select>
          <br />
          Expected Close Date:<br />
          <input type="text" name="close_date" id="close_date" value="<?php echo date("d/m/Y", strtotime(date("Y-m-d") ." +14 day")); ?>" />
          <br />
		  <?php if ($opp_tags) {
          echo 'Assign Tag: <small>(select multiple by holding Crtl)</small><br />
		  <select multiple="multiple" name="tag[]">';
          
		  	foreach($opp_tags as $tag) {
			
				echo '<option value="'.$tag['id'].'">'.$tag['tag'].'</option>';
			
			
			}		  
		  
		  echo '</select>';
		  
		  } ?>
          <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />
          <p><button class="btn">Save</button>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>clients/opportunities?id=<?php echo $this->input->get('id'); ?>" class="btn">Cancel</a></p>
          </form>
          </div>
        </div>      
      
      </div>
      
    </div>
  </div>
</div>
<?php require("common/footer.php"); ?>
