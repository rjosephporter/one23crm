<script>
$(document).ready(function() {

	$(".completeTask").click(function() {
	// get the checkbox data
	var checkData = $(this).val();
	//alert(checkData);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>clients/complete_task",
			data: { task: checkData },
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not complete this task");
			}
		});

	});

	$("#action, #user").change(function() {
		$("#action_sort").submit();
	});

});
</script>
    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" />
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="task_min" id="task_min" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action" style="height:22px; padding:0px;">
          <option value="0">Please Select</option>
          <option value="Call">Call</option>
          <option value="Email">Email</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button id="modalCreateClientTask" class="btn">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <input type="hidden" name="return" value="calender/tasks" />
    </form>
    </div>
    </div>

<div id="container_top">
<h4>Calendar and Tasks<a href="#createNewTask" class="btn pull-right" data-toggle="modal" style="font-weight:normal;"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Task</a></h4>

<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>calender">Calendar View</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>calender/tasks">List View</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

<?php if ($sort_action) { ?>
<div row-fluid>
<div class="span12">
<form class="form-inline" method="get" id="action_sort">
Sort by action required:
<select id="action" name="action" style="padding:0; height:23px;">
<option value=""></option>
<?php foreach($sort_action as $action) {
	if ($selected_action==$action['action']) {
		$selected = 'selected="selected"';
	} else { $selected = ''; }
	echo '<option value="'.$action['action'].'" '.$selected.'>'.$action['action'].'</option>';
} ?>
</select>

<?php if ($this->session->userdata("role")==1) { ?>
&nbsp;Show tasks for:
<select id="user" name="user" style="padding:0; height:23px;">
<option value="">Myself Only</option>
<option value="all" <?php if ($this->input->get("user")=="all") { echo 'selected="selected"'; } ?>>All Users</option>
<?php if ($group) {
	foreach($group as $user) {
		echo '<option value="'. $user['user_id'] .'"';

		if ($user['user_id']==$this->input->get("user")) { echo ' selected="selected"'; }

		echo '>'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';
	}
} ?>
</select>
<?php } ?>
<button type="submit" class="btn btn-mini">Sort List</button>
</form>
</div>
</div>
<?php } ?>

<div row-fluid>
<div class="span12 well">
<h5 style="margin-top:0;">Upcoming Tasks:</h5>
<?php if ($list) {

	echo '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">';

	foreach ($list as $task) {

	// see its overdue
	if ($task['date'] <= date("Y-m-d H:i:s")) {
	$overdue = '<span class="label label-important">Overdue</span> ';
	} else {
	$overdue = "";
	}

	if ($task['client_name']=="") {
		$client = "";
	} else {
		$client = ' for <a href="'. base_url() .'clients/view?id='. $task['client'] .'">' . $task['client_name'] . '</a>';
	}

	echo '<tr>
	<td width="5%"><input type="checkbox" class="completeTask" value="'.$task['id'] .'_'.$task['client'] .'" /></td>
	<td>'. $overdue .'<span class="label label-info">'. $task['action_name'] .'</span>&nbsp;<strong>'. $task['name'] .'</strong>'. $client .'</td>
	<td width="15%" style="text-align:left;">'. date("d M Y", strtotime($task['date'])) .'<br />at '. date("H:i", strtotime($task['date'])) .'
	<br />Assigned: '. $task['username'] .'</td>
	</tr>';

	}

	echo '</table>';

} else {
	echo "No tasks to show.";
} ?>
</div>
</div>

<?php require("common/footer.php"); ?>
