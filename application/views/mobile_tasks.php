<div class="title_bar">Overdue Tasks</div>
<?php if ($tasks_overdue) {
	echo '<ul class="list">';
	foreach($tasks_overdue as $overdue) {
		
		echo '<li><a href="#">'. $overdue['action'] . ' - ' . $overdue['name'] . '<br /><small>'.date("d F Y G:i", strtotime($overdue['date'])) .'</small></a></li>';
	}
	echo '</ul>';
} else {
	echo '<div class="task_empty">No overdue tasks</div>';
} ?>
<div class="title_bar">Due Today</div>
<?php if ($tasks_today) {
	echo '<ul class="list">';
	foreach($tasks_today as $today) {
		
		echo '<li><a href="#">'. $today['action'] . ' - ' . $today['name'] . '<br /><small>At '.date("G:i", strtotime($today['date'])) .'</small></a></li>';
	}
	echo '</ul>';
} else {
	echo '<div class="task_empty">No tasks due today</div>';
} ?>
<div class="title_bar">Next 7 Days</div>
<?php if ($tasks_week) {
	echo '<ul class="list">';
	foreach($tasks_week as $week) {
		
		echo '<li><a href="#">'. $week['action'] . ' - ' . $week['name'] . '<br /><small>At '.date("d F Y G:i", strtotime($week['date'])) .'</small></a></li>';
	}
	echo '</ul>';
} else {
	echo '<div class="task_empty">No tasks due in the next 7 days</div>';
} ?>
<div class="title_bar">Upcoming</div>
<?php if ($tasks_other) {
	echo '<ul class="list">';
	foreach($tasks_other as $other) {
		
		echo '<li><a href="#">'. $other['action'] . ' - ' . $other['name'] . '<br /><small>At '.date("d F Y G:i", strtotime($other['date'])) .'</small></a></li>';
	}
	echo '</ul>';
} else {
	echo '<div class="task_empty">No upcoming tasks</div>';
} ?>
