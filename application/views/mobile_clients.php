<form class="title_bar" style="text-align:center;" action="<?php echo base_url(); ?>clients" method="get">
<input name="keyword" type="text" placeholder="Enter search keywords" value="<?php echo $this->input->get('keyword'); ?>" />
<input name="" type="submit" value="Search" class="button" />
</form>
<?php if ($customer_list) { ?>

<?php echo '<ul class="wide_list">';
	
	foreach($customer_list as $customer) {
	
		if ($customer['address']!=", , ") {
			$address = '<br /><small>' . $customer['address'] . '</small>';
		} else {
			$address = "";		
		}	
	
		echo '<li class="arrow"><a href="'.base_url().'clients/view?id='. $customer['id'] .'"><strong>'. $customer['customer_name'] . '</strong>' . $address . '</a></li>';

	}

echo '</ul>';

} else {

echo '<div class="task_empty"><h3>No clients found.</h3></div>';

}?>
