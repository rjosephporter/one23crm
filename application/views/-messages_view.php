<?php //require("common/header.php");

if (!$message_detail) {
redirect('messages');
} ?>

    <div id="replyMessage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>clients/send_reply" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel">Reply</h4>
    </div>
    <div class="modal-body">
	<textarea style="width:95%; height:200px;" name="reply_body">


-----------------------------
<?php echo $message_detail['body']; ?></textarea>
	<input type="hidden" name="reply_to" value="<?php echo $message_detail['sender']; ?>" /><input type="hidden" name="reply_subject" value="<?php echo $message_detail['subject']; ?>" />
    </div>
    <div class="modal-footer">
    <button class="btn" type="submit">Send Email</button>
	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>    
    </div>
    </form> 
  </div>

<div id="container_top">
<h4>Messages</h4>
<p><?php echo $message_detail['subject']; ?></p>
</div>

<br clear="all" />

<div class="container-fluid">

	
    <div class="row-fluid">
    <div class="span12">
    <?php if ($this->input->get('quoteclient')) { ?>
    <p><a href="<?php echo base_url(); ?>clients/quotes?id=<?php echo $this->input->get('quoteclient'); ?>" class="btn">Return to client</a>
    <?php } ?>
    
    <?php if ($this->input->get('return')) { ?>
    <p><a href="<?php echo base_url(); ?>messages" class="btn">Return to messages</a>
	<?php } ?>

	<?php if ($this->input->get('messageclient')) { ?>
    <p><a href="<?php echo base_url(); ?>clients/messages?id=<?php echo $this->input->get('messageclient'); ?>" class="btn">Return to client</a>
    <?php } ?>

	<?php if ($this->input->get('messagecentre')) { ?>
    <p><a href="<?php echo base_url(); ?>clients/messages?id=<?php echo $this->input->get('messagecentre'); ?>" class="btn">Return to client</a>
    <?php } ?> 

    &nbsp;&nbsp;<a href="#replyMessage" data-toggle="modal" class="btn">Reply</a></p>

    </div>
    </div>

    <div class="row-fluid">
    
    	<div class="span6 well">
        <h5 style="margin-top:0px;">Message Details</h5>
		<?php if(!empty($message_detail['sender'])) { echo $message_detail['sender']; } ?><br />
        <?php echo date("d/m/y H:i", strtotime($message_detail['messagedate'])); ?><br />
        <?php echo $message_detail['subject']; ?><br />        
        </div>
        <?php if ($message_detail['method']==1) { ?>
        <div class="span6 well">
        <h5 style="margin-top:0px;">Attachments</h5>
		<?php if ($message_attachments) {
        
            foreach ($message_attachments as $attachment) {
			
				// have a look at the first bit of the filename
				$explode_file = explode('/', $attachment['file']);
				
				if ($explode_file[0]=="documents") {
                	echo '<a href="'. base_url() . $attachment['file'] .'" target="_blank">'. end($explode_file) .'</a><br />';
				} else {
                	echo '<a href="'. base_url() .'documents/'. $attachment['file'] .'" target="_blank">'. end($explode_file) .'</a><br />';
				}
            
            }
        
        } else {
		
			echo 'No attachments for this message.';
		
		} ?>        
        </div>
        <?php } ?> 
    
    </div>

	<div class="row-fluid">
    	<div class="span12 well"><h5 style="margin-top:0px;">Message Body</h5>
		<?php echo nl2br($message_detail['body']); ?></div>    
    </div>
</div>
<?php require("common/footer.php"); ?>
