<div id="container_top">
<h4>Custom Fields</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">
        <iframe src="http://www.screenr.com/embed/qQKH" width="650" height="396" frameborder="0"></iframe>

        </div>
        <div class="span4 well" style="background:#B50649;">
        	<h5 style="color:#FFFFFF; margin-top:0px;"></h5>
            <p style="color:#FFFFFF;">Proin dui est, porttitor fermentum semper vel, elementum ut mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean fermentum sagittis elementum. In mollis suscipit velit vitae fringilla. In hac habitasse platea dictumst. Sed sapien orci, pellentesque in accumsan quis, facilisis ut enim.</p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
