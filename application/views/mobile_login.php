<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/ico" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<style>
body {
padding-top: 40px;
padding-bottom: 40px;
background-color: #f5f5f5;
}

.form-signin {
max-width: 300px;
padding: 19px 29px 29px;
margin: 0 auto 20px;
background-color: #fff;
border: 1px solid #e5e5e5;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
-moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
box-shadow: 0 1px 2px rgba(0,0,0,.05);
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
margin-bottom: 10px;
}
.form-signin input[type="text"],
.form-signin input[type="password"] {
font-size: 16px;
height: auto;
margin-bottom: 15px;
padding: 7px 9px;
}
</style>
</head>

<body>

<div class="container">

<form action="<?php echo base_url(); ?>login/auth" method="post" class="form-signin">
<?php if (isset($_GET['failed'])) { echo '<div class="alert alert-error">Invalid login details provided, please try again.</div>'; } ?>
<?php if (isset($_GET['activated'])) { echo '<div class="alert alert-success">Your account has now been activated, you may now login below.</div>'; } ?>
<?php if (isset($_GET['reset'])) { echo '<div class="alert alert-success">Your password has been reset, check your email.</div>'; } ?>
<?php if (isset($_GET['expired'])) { echo '<div class="alert alert-error">Your subscription has now expired, please click here to extend/renew the subscription.</div>'; } ?>
<?php if (isset($_GET['subscription'])) { echo '<div class="alert alert-error">There is a problem with your subscription, please contact the accounts department.</div>'; } ?>

<h2 class="form-signin-heading">Please Login</h2>
<input type="text" name="username" class="input-block-level" placeholder="Username">
<input type="password" name="password" class="input-block-level" placeholder="Password">
<input type="hidden" name="mobile" value="1" />
<button class="btn" type="submit"><i class="icon-lock"></i>&nbsp;&nbsp;Login Securely</button>
</form>

</div> 




</body>
</html>
