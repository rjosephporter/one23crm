<div id="container_top">
<h4>Calendar Task Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>
<br clear="all" />
<div id="task-setting" class="container-fluid">
	<div class="list">
		<h1>List Action</h1>
		<div>
			<h3>Items : <?php echo $actions->num_rows;?></h3>
		</div>
		<p></p>
		<a target="_self" href="<?php echo base_url(); ?>tasksetting/create_action" class="btn">
				<img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Action
		</a>
		<p></p>
		<div class="list-task-settings">
			<table class="table table-hover table-bordered">
			  <thead>
				<tr>
				  <th>Action Name</th>
				  <th>Icon</th>
				  <th>Color</th>
				  <th>Action</th>
				</tr>
			  </thead>
			  <tbody>
				<?php if($actions->num_rows > 0) {?>
					<?php foreach($actions->result() as $row) { ?>
							<tr>
							  <td><h5><a href="<?php echo base_url(); ?>tasksetting/edit/<?php echo $row->id;?>"><?php echo $row->action_name;?></a></h5></td>
							  <td><span><i class="fa <?php echo $row->icons;?> fa-2x"></i></span></td>
							  <td><span class="swatch" style="height: 40px;width:50px;display:inline-block;background-color: <?php echo $row->color;?>"></span></td>
							  <td><h5><a href="<?php echo base_url(); ?>tasksetting/deleteAction/<?php echo $row->id;?>" onclick="return confirm('Are you sure you want to Delete this action?')">Delete</a></h5></td>
							</tr>
					<?php }?>
				<?php }?>
			  </tbody>
			</table>
		</div>
	</div>
</div>
<?php require("common/footer.php"); ?>
