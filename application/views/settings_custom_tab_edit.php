<div id="container_top">
<h4>Custom Fields</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li class="active"><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <form action="<?php echo base_url(); ?>settings/custom_fields_tab_save" method="post">
        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Edit Custom Tab</h5>
			<label>Tab Name:</label>
            <input type="text" name="name" value="<?php echo $tab['name']; ?>" placeholder="Enter custom tab name" />
			</div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 1</h5>
			<label>Section 1 Name:</label>
            <input type="text" name="section1_name" value="<?php echo $tab['section1_name']; ?>" placeholder="Section 1 name" />
            <label>Use:</label>
            <select name="section1" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section1']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section1']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section1']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section1_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 2</h5>
			<label>Section 2 Name:</label>
            <input type="text" name="section2_name" value="<?php echo $tab['section2_name']; ?>" placeholder="Section 2 name" />
            <label>Use:</label>
            <select name="section2" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section2']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section2']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section2']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section2_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
			</div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 3</h5>
			<label>Section 3 Name:</label>
            <input type="text" name="section3_name" value="<?php echo $tab['section3_name']; ?>" placeholder="Section 3 name" />
            <label>Use:</label>
            <select name="section3" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section3']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section3']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section3']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section3_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
			</div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 4</h5>
			<label>Section 4 Name:</label>
            <input type="text" name="section4_name" value="<?php echo $tab['section4_name']; ?>" placeholder="Section 4 name" />
            <label>Use:</label>
            <select name="section4" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section4']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section4']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section4']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section4_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
			</div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 5</h5>
			<label>Section 5 Name:</label>
            <input type="text" name="section5_name" value="<?php echo $tab['section5_name']; ?>" placeholder="Section 5 name" />
            <label>Use:</label>
            <select name="section5" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section5']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section5']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section5']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section5_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
			</div>
        </div>

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Section 6</h5>
			<label>Section 6 Name:</label>
            <input type="text" name="section6_name" value="<?php echo $tab['section6_name']; ?>" placeholder="Section 6 name" />
            <label>Use:</label>
            <select name="section6" style="padding:0; height:23px;">
            <option value="generic_0" <?php if ($tab['section6']==0) { echo 'selected="selected"'; } ?>>Empty</option>
            <option value="generic_1" <?php if ($tab['section6']==1) { echo 'selected="selected"'; } ?>>Notes</option>
            <option value="generic_2" <?php if ($tab['section6']==2) { echo 'selected="selected"'; } ?>>Files</option>
            <?php if ($forms_list) {
            echo '<optgroup label="Custom Forms">';
            	foreach ($forms_list as $form) {
					echo '<option value="form_'. $form['id'] .'"';

					if ($tab['section6_form']==$form['id']) {
						echo ' selected="selected" ';
					}

					echo '>'. $form['name'] .' - '. $form['desc'] .'</option>';
				}
            echo '</optgroup>';
			} ?>
            </select>
			</div>
        </div>

        <input type="hidden" name="id" value="<?php echo $tab['id']; ?>" />
        <input type="submit" value="Save Changes" class="btn" />
        </form>


        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">Custom Fields</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
