<?php if (!$account_details) {
redirect('settings');
} ?>
<div id="container_top">
<h4>Account Information</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
            	<div class="span12 well">
                <?php echo $this->session->flashdata('errors'); ?>
                <h5 style="margin-top:0px;">Account Information</h5>
                <p>Please provide updated details below.</p>
                <form action="<?php echo base_url(); ?>settings/update_account" method="post">
                <label>Company Name:</label>
                <input type="text" name="company" value="<?php if ($this->session->flashdata('company')) { echo $this->session->flashdata('company'); } else { echo $account_details['company']; } ?>" />
                <label>First Name:</label>
                <input type="text" name="first_name" value="<?php if ($this->session->flashdata('first_name')) { echo $this->session->flashdata('first_name'); } else { echo $account_details['first_name']; } ?>" />
                <label>Surname:</label>
                <input type="text" name="last_name" value="<?php if ($this->session->flashdata('last_name')) { echo $this->session->flashdata('last_name'); } else { echo $account_details['last_name']; } ?>" />
                <label>Email Address:</label>
                <input type="text" name="email" value="<?php if ($this->session->flashdata('email')) { echo $this->session->flashdata('email'); } else { echo $account_details['email']; } ?>" />
                <label>Telephone Number:</label>
                <input type="text" name="telephone" value="<?php if ($this->session->flashdata('telephone')) { echo $this->session->flashdata('telephone'); } else { echo $account_details['telephone']; } ?>" />
                <label>Address Line:</label>
                <input type="text" name="address" value="<?php if ($this->session->flashdata('address')) { echo $this->session->flashdata('address'); } else { echo $account_details['address_line']; } ?>" />
                <label>Town:</label>
                <input type="text" name="town" value="<?php if ($this->session->flashdata('town')) { echo $this->session->flashdata('town'); } else { echo $account_details['address_town']; } ?>" />
                <label>County:</label>
                <input type="text" name="county" value="<?php if ($this->session->flashdata('county')) { echo $this->session->flashdata('county'); } else { echo $account_details['address_county']; } ?>" />
                <label>Postcode:</label>
                <input type="text" name="postcode" value="<?php if ($this->session->flashdata('postcode')) { echo $this->session->flashdata('postcode'); } else { echo $account_details['address_postcode']; } ?>" />
                <label>SMS Display Name:</label>
                <input type="text" name="sms" value="<?php if ($this->session->flashdata('sms')) { echo $this->session->flashdata('sms'); } else { echo $account_details['sms']; } ?>" maxlength="11" />

                <?php if($edit_username): ?>
                <label>Username:</label>
                <input type="text" name="username" value="<?php if ($this->session->flashdata('username')) { echo $this->session->flashdata('username'); } else { echo $account_details['username']; } ?>" maxlength="30" />
                <div class="alert alert-info" style="display:inline">
                    <strong>Note:</strong>  You can only change your username once.
                </div>
                <label>Password:</label>
                <input type="password" name="password" value="" maxlength="50" />
                <label>Confirm Password:</label>
                <input type="password" name="confirm_password" value="" maxlength="50" />
                <?php endif;  ?>

				<p style="margin-top:10px;">
                <input name="" type="submit" value="Save Details" class="btn" />
                </p>
				</form>
                </div>
            </div>

        </div>
        <div class="span4">
        </div>
    </div>

<?php require("common/footer.php"); ?>
