<div id="container_top">
<h4>Admin</h4>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span10 well">
		<h5 style="margin-top:0px;">Client List - <?php echo count($client_list); ?></h5>
		<?php if ($client_list) {
			
		  echo '<table width="100%" class="table">
				  <thead>
					<tr>
					  <th>Client Name</th>
					  <th>Username</th>
					  <th>Company Name</th>
					  <th>Account Expiry</th>
					  <th width="10%">Actions</th>
					</tr>
				  </thead><tbody>';
		
				foreach($client_list as $client) {
					
					echo '<tr>
					<td>'. $client['first_name'] .' '. $client['last_name'] .'</td>
					<td>'. $client['username'] .'</td>
					<td>'. $client['company'] .'</td>
					<td>'. date("D j M Y", strtotime($client['expiry'])) .'</td>
					<td><a href="'. base_url() .'admin/user?id='. $client['id'] .'">View</a> | <a href="#">Edit</a></td>
					</tr>';				
				
				}		
		
			echo '</tbody></table>';
		
		 } ?>
        </div>
    </div>

<?php require("common/footer.php"); ?>
