<?php //require("common/header.php");
$overdue_count = 0;
$today_count = 0;
$week_count = 0;
$other_count = 0;
$overdue_list="";
$today_list="";
$week_list="";
$other_list="";

// if theme is clean, then dash icons need to end _clean
//if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$dash_icon = '_clean';
//} else {
	//$dash_icon = '';
//} ?>
<script>
$(document).ready(function() {

	$(".completeTask").click(function() {
	// get the checkbox data
	var checkData = $(this).val();
	//alert(checkData);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>clients/complete_task",
			data: { task: checkData },
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not complete this task");
			}
		});

	});

	$("#updates").change(function() {
		 $("#show_updates").submit();
	});


});
</script>
<script src="<?php echo base_url(); ?>js/hello.js"></script>
    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" />
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task </h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td width="20%" valign="middle">Link To:</td>
        <td valign="middle">
		  <div id="link_to_client">
			  <input class="typeahead getclient" name="getclient" type="text" autocomplete="off">
			  <input type="hidden" name="customer_id" id="customer_id" value="0">
   		  </div>
        </td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1" style="padding:0; height:23px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="task_min" id="task_min" class="span1" style="padding:0; height:23px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action" style="padding:0; height:23px;">
          <option value="0">Please Select</option>
          <?php if( $task_setting->num_rows > 0 ){ ?>
					<?php foreach($task_setting->result() as $row){ ?>
						<option value="<?php echo $row->id;?>"><?php echo $row->action_name;?></option>
					<?php } ?>
          <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td>Reminder:</td>
        <td>
        <select name="task_remind" id="task_remind" style="height:22px; padding:0px;">
          <option value="0">Do Not Remind</option>
          <option value="5">Remind me 5 minutes before</option>
          <option value="15">Remind me 15 minutes before</option>
          <option value="30">Remind me 30 minutes before</option>
          <option value="60">Remind me 60 minutes before</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button id="modalCreateClientTask" class="btn">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <input type="hidden" name="return" value="dashboard" />
    </form>
    </div>
    </div>

<div id="container_top">
<h4>Dashboard</h4>
</div>

<?php //print_r($this->session->all_userdata()); ?>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
	<div class="span4">

	<?php if ($this->session->userdata('is_admin')==1) { echo '<p><a href="' . base_url() . 'admin">Admin Panel</a></p>'; } ?>

    <div class="row-fluid">
	<div class="span12 well">
        <a href="#createNewTask" class="btn pull-right" data-toggle="modal"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Task</a>
        <h5 style="margin-top:0px;">Your Current Tasks</h5>
        <?php if ($tasks_overdue) {
        $overdue_list .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        foreach($tasks_overdue as $overdue) {

            if (!empty($overdue['company_name'])) {
                $overdue_name = $overdue['company_name'];
            } else {
                $overdue_name = $overdue['client_name'];
            }

            $overdue_count++;
            $overdue_list .= '<tr>
            <td width="5%" valign="top"><input type="checkbox" class="completeTask" value="'.$overdue['id'] .'_'.$overdue['customer_id'] .'" /></td>
            <td><a href="'. base_url() .'clients/view?id='. $overdue['client'] .'" target="_self">'. $overdue_name . '</a> - ' .$overdue['name'] .'<br /><span style="color:#999; line-height:1em; padding-bottom:5px;">'. date("D jS F Y G:iA", strtotime($overdue['date'])) .'</span></td>
            </tr>';
        }
        $overdue_list .= '</table>';
        } ?>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Overdue <span class="badge badge-important"><?php echo $overdue_count; ?></span></h6>
        <?php echo $overdue_list; ?>

        <?php if ($tasks_today) {
        $today_list .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        foreach($tasks_today as $today) {

            if (!empty($today['company_name'])) {
                $today_name = $today['company_name'];
            } else {
                $today_name = $today['client_name'];
            }

            $today_count++;
            $today_list .= '<tr>
            <td width="5%" valign="top"><input type="checkbox" class="completeTask" value="'.$today['id'] .'_'.$today['customer_id'] .'" /></td>
            <td><a href="'. base_url() .'clients/view?id='. $today['client'] .'" target="_self">'. $today_name . '</a> - '. $today['name'] .'<br /><span style="color:#999; line-height:1em; padding-bottom:5px;">'. date("D jS F Y G:iA", strtotime($today['date'])) .'</span></td>
            </tr>';
        }
        $today_list .= '</table>';
        } ?>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Due Today <span class="badge badge-warning"><?php echo $today_count; ?></span></h6>
        <?php echo $today_list; ?>

        <?php if ($tasks_week) {
        $week_list .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        foreach($tasks_week as $week) {

            if (!empty($week['company_name'])) {
                $week_name = $week['company_name'];
            } else {
                $week_name = $week['client_name'];
            }

            $week_count++;
            $week_list .= '<tr>
            <td width="5%" valign="top"><input type="checkbox" class="completeTask" value="'.$week['id'] .'_'.$week['customer_id'] .'" /></td>
            <td><a href="'. base_url() .'clients/view?id='. $week['client'] .'" target="_self">'. $week_name . '</a> - '. $week['name'] .'<br /><span style="color:#666; line-height:1em; padding-bottom:5px;">'. date("D jS F Y G:iA", strtotime($week['date'])) .'</span></td>
            </tr>';
        }
        $week_list .= '</table>';
        } ?>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Next 7 Days <span class="badge badge-info"><?php echo $week_count; ?></span></h6>
        <?php echo $week_list; ?>

        <?php if ($tasks_other) {
        $other_list .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        foreach($tasks_other as $other) {

            if (!empty($other['company_name'])) {
                $other_name = $other['company_name'];
            } else {
                $other_name = $other['client_name'];
            }

            $other_count++;
            $other_list .= '<tr>
            <td width="5%" valign="top"><input type="checkbox" class="completeTask" value="'.$other['id'] .'_'.$other['customer_id'] .'" /></td>
            <td><a href="'. base_url() .'clients/view?id='. $other['client'] .'" target="_self">'. $other_name . '</a> - '. $other['name'] .'<br /><span style="color:#999; line-height:1em; padding-bottom:5px;">'. date("D jS F Y G:iA", strtotime($other['date'])) .'</span></td>
            </tr>';
        }
        $other_list .= '</table>';
        } ?>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Future <span class="badge"><?php echo $other_count; ?></span></h6>
        <?php echo $other_list; ?>
        </div>
        </div>

		<div class="row-fluid">
		<div class="span12 well">
       	<h5 style="margin-top:0px;">Client Birthdays</h5>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Birthdays This Week</h6>
        <?php if ($birthdays_week) {
			foreach($birthdays_week as $birth_week) {
				echo '<a href="'.base_url().'clients/view?id='. $birth_week['id'] .'">'. $birth_week['customer_name'] . '</a> - '. $birth_week['birtday'] .'<br />';
			}
		} else {
			echo 'None of your clients have a birthday this week.';
		} ?>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Birthdays This Month</h6>
        <?php if ($birthdays_month) {
			foreach($birthdays_month as $birth_month) {
				echo '<a href="'.base_url().'clients/view?id='. $birth_month['id'] .'">'. $birth_month['customer_name'] . '</a> - '. $birth_month['birtday'] .'<br />';
			}
		} else {
			echo 'None of your clients have a birthday this month.';
		} ?>
        </div>

    <?php if(count($other_sns) > 0): ?>
    <div class="row-fluid">
      <div class="span12 well">
        <h5 style="margin-top:0px;">Connect more</h5>
        <h6 style="margin-top:10px; border-bottom:#E3E3E3 1px solid; padding-bottom:5px;">Connect with your other social network accounts</h6>
        <div class="alert alert-block" id="sns_alert" style="display:none">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <span>&nbsp;</span>
        </div>
          <?php foreach($other_sns as $sa): ?>
            <?php if($sa == 'google'): ?>
              <button class="btn btn-block btn-google-plus btn-social" for="google"><i class="fa fa-google-plus"></i> | Connect with Google+</button>
            <?php else:  ?>
              <button class="btn btn-block btn-<?php echo $sa ?> btn-social" for="<?php echo $sa ?>"><i class="fa fa-<?php echo $sa ?>"></i> | Connect with <?php echo ucfirst($sa) ?></button>
            <?php endif; ?>
          <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>

        </div>

    </div>

	<div class="span8">

		<div class="row-fluid">
		<div class="span12 well">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%" align="center" valign="top"><h4 style="margin-top:0px;">Sales Month To Date</h4></td>
            <td width="25%" align="center" valign="top"><h4 style="margin-top:0px;">Conversion Month To Date</h4></td>
            <td width="25%" align="center" valign="top"><h4 style="margin-top:0px;">Pipeline Total Value</h4></td>
            <td width="25%" align="center" valign="top"><h4 style="margin-top:0px;">Pipeline Expected Value</h4></td>
          </tr>
          <tr>
          <td align="center" valign="middle"><h3 style="color:#00dbff;">&pound;<?php echo number_format($sales,2); ?></h3></td>
          <td align="center" valign="middle"><h3 style="color:#00dbff;"><?php echo number_format($dash_conversion,2); ?>%</h3></td>
          <td align="center" valign="middle"><h3 style="color:#00dbff;">&pound;<?php echo number_format($pipeline_totals['total'],2); ?></h3></td>
          <td align="center" valign="middle"><h3 style="color:#00dbff;">&pound;<?php echo number_format($pipeline_totals['pipeline'],2); ?></h3></td>
          </tr>
        </table>
        </div>
        </div>

		<div class="row-fluid">
		<div class="span12 well">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="60%"><h5 style="margin-top:0px;">Latest Updates</h5></td>
            <td align="center">
            <?php if ($this->session->userdata("role")==1) { ?>
            <form class="form-inline" action="" method="get" id="show_updates">
            <span>Show updates for:&nbsp;&nbsp;</span>
            <select id="updates" name="updates" style="padding:0; height:23px;">
            <option value="">Myself Only</option>
            <?php if (count($update_list_group)) {
				echo '<option value="all"';
				if ($this->input->get("updates")=="all") { echo ' selected="selected"'; }
				echo '>All Users</option>';
				foreach ($update_list_group as $user) {
					echo '<option value="'. $user['user_id'] .'"';
					if ($this->input->get("updates")==$user['user_id']) { echo ' selected="selected"'; }
					echo '>'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';
				}
            } ?></select>
            </form>
            <?php } ?>
            </td>
          </tr>
        </table>

			<?php if ($update_list) {

            echo '<table width="100%" border="0" cellspacing="5" cellpadding="0" class="updates">';

                foreach($update_list as $update) {

                    if (!empty($update['company_name'])) {
                        $client_name = $update['company_name'];
                    } else {
                        $client_name = $update['first_name'] . ' ' . $update['last_name'];
                    }

                    // sort out the icons
                    if ($update['type']==1) {
                        $selected_icon = 'dash_client';
                    } elseif ($update['type']==3) {
                        $selected_icon = 'dash_file';
                    } elseif ($update['type']==4) {
                        $selected_icon = 'dash_tick';
                    } else {
                        $selected_icon = 'dash_note';
                    }

                     echo '<tr class="updates_list">
                        <td width="5%" valign="middle" align="center"><img src="'.base_url().'img/'.$selected_icon.$dash_icon.'.png" /></td>
                        <td align="left" valign="top"><strong>'. $update['user'] .'</strong> '. $update['title'] .' <a href="'.base_url().'clients/view?id='. $update['the_customer'] .'" target="_self"><strong>'. $client_name .'</strong></a><br />';

                        if ($update['type']==2) {
                            echo $update['note'];
                        } elseif ($update['type']==4) {
                            echo  '(' .$update['task_action'].') ' . $update['task_name'];
                        }

                        echo '</td>
                        <td width="20%" align="right" valign="top">'. date("d/m/y H:i", strtotime($update['update_date'])) .'</td>
                      </tr>';

                }

            echo '</table>';
            } else {

                echo 'Currently no updates to show.';

            } ?>

        </div>
        </div>

    </div>

</div>

<script>
// Added for connecting to other social network accounts - 08.12.2014 @rjosephporter

var redirect_url = location.protocol + '//' + location.host + '/dashboard';

hello.init({
  facebook: '1486594334912497',
  twitter: '2kipt4DdbDk7JinY3w8xWjaZm',
  linkedin: '75u5l0mp1oasn3',
  google: '521600025771-b6ggmpeu3qbdv0ols4dpib3v36sspc15.apps.googleusercontent.com'
});

$('.btn-social').click(function(e){
  e.preventDefault();
  $('#sns_alert span').text('');
  $('#sns_alert').removeClass('alert-error alert-success').hide();
  var social_network = $(this).attr('for');
  var selected_btn = $(this);
  hello.login(social_network, {
    display: 'popup',
    scope: null,
    redirect_uri: redirect_url,
    response_type: 'token',
    force: true,
    oauth_proxy: 'https://auth-server.herokuapp.com/proxy'
  }, function(auth){
      console.log(auth);
      if(!auth.error) {
        hello(auth.network).api('/me').success(function(r){
          console.log(r);
          $.get('/login/sns_exist', { sns_type : auth.network, sns_id : r.id }, function(res){
            console.log(res);
            if(res.sns_exists) {
              // Proceed to auth_sns
              var alert_msg = auth.network + ' already used by other account. Contact us if you want to merge these accounts.';
              console.log(alert_msg);
              $('#sns_alert span').text(alert_msg);
              $('#sns_alert').addClass('alert-error').show();
              console.log(res.user_data);
            } else {
              // Create account
              $.post('/login/create_sns_account', { auth_detail : auth, sns_detail : r, sns_only : 1 }, function(data){
                var alert_msg = 'Successfully connected your ' + auth.network + ' account.';
                console.log(data);
                console.log(alert_msg);
                $('#sns_alert span').text(alert_msg);
                $('#sns_alert').addClass('alert-success').show();
                selected_btn.remove();
              });
            }
          });

        });
      } else {
        console.log(auth.error.message);
      }
  });
});
</script>

<?php require("common/footer.php"); ?>
