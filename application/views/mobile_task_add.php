<form action="<?php echo base_url(); ?>clients/create_task" method="post">
<textarea style="margin:10px; width:80%; height:100px; padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif;" placeholder="Description" name="task_name"></textarea>
<br />
<label style="margin:10px;">Category:<br />
<select name="task_action" style="margin:10px; padding:10px;">
    <option value="Call">Call</option>
    <option value="Email">Email</option>
    <option value="Follow-Up">Follow-Up</option>
    <option value="Meeting">Meeting</option>
    <option value="Milestone">Milestone</option>
    <option value="Send">Send</option>
</select>
</label>
<br />
<label style="margin:10px;">Due:<br />
<select name="task_date" style="margin:10px; padding:10px;">
	<option value="<?php echo date("d/m/Y"); ?>">Today</option>
	<option value="<?php echo date("d/m/Y", strtotime("Tomorrow")); ?>">Tomorrow</option>
	<option value="<?php echo date("d/m/Y", strtotime('+ 7 days')); ?>">7 Days</option>
    <option value="<?php echo date("d/m/Y", strtotime('+ 30 days')); ?>">30 Days</option>
</select>
</label>
<br />
<input type="hidden" id="task_hour" name="task_hour" value="23" />
<input type="hidden" id="task_min" name="task_min" value="45" />
<input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id'); ?>" />
<input name="" type="submit" style="margin:10px; padding:10px;" value="Save" />
</form>
