
<div id="container_top">
<h4>Document Library</h4>
</div>

<br clear="all" />

<div class="container-fluid">

<script>
$(document).ready(function() {

	$(".provider_holder").hide();
	$(".library").hide();
	
	$(".section").click(function() {
		$(this).siblings('.provider_holder').toggle("slow");
	});	
	
	$(".provider").click(function() {
		$(this).siblings('.table').toggle("slow");
	});	

});
</script>

    <div id="addDocuments" class="modal hide fade">
    <form action="<?php echo base_url(); ?>document_library/upload" method="post" enctype="multipart/form-data">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Upload File To Library</h4>
    </div>
    <div class="modal-body">
    Name of Document:<br />
 	<input type="text" id="name" name="name" />
    <br />
    Select Document:<br />
    <input name="doc" type="file" />
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn">Upload</button>
    </form>
    </div>
    </div>  


<div class="row-fluid">
    <div class="span8 well">
    <?php echo $this->session->flashdata('error'); ?>
    <h3 style="margin-top:0px;cursor:pointer;" class="section">My Uploaded Documents</h3>
    <div class="provider_holder">
    <?php if ($uploaded_docs) {
	
		echo '<table width="50%" class="table">';
		
			foreach($uploaded_docs as $user_doc) {
			
				echo '<tr>
				<td width="75%"><a href="'.base_url().'documents/library/own/'. $user_doc['filename'] .'" target="_blank">'. $user_doc['name'] .'</a></td>
				<td><a href="'.base_url().'documents/library/own/'. $user_doc['filename'] .'" target="_blank">View Document</a> | <a href="'.base_url().'document_library/remove?file='. $user_doc['id'] .'">Remove</a></td>
				</tr>';
			
			}		
		
		echo '</table>';
	
	} else {
	
		echo '<p>You have not uploaded any of your own documents.</p>';
	
	} ?>
    <a href="#addDocuments" data-toggle="modal" class="btn" style="margin-top:10px;">Upload Document</a>
    </div>
    </div>
</div>

<?php if ($documents) {

	//echo '<pre>';
	//print_r($documents);
	//echo '</pre>';
	
	$counter = 0;
	foreach ($documents as $document_category) {

	echo '<div class="row-fluid"><div class="span8 well"><h3 style="margin-top:0px;cursor:pointer;" class="section">'. $document_category['name'] .'</h3>
	<div class="provider_holder">';
			
		if ($document_category['providers']) {
		
			foreach($document_category['providers'] as $provider) {
				
				// see if any files for this provider
				if (isset($provider['documents'])) {
		
					echo '<div class="row-fluid"><div class="span12 well"><h5 style="margin-top:0px;color:#0088cc;cursor:pointer" class="provider">'. $provider['name'] .' - click to show '. $provider['total_docs'] .' documents</h5>';
					
					
						echo '<table class="table library" style="margin-bottom:0;">';
						
						// list files for this provider
						foreach($provider['documents'] as $doc) {
						
							echo '<tr><td><a href="'.base_url().'documents/library/'.$doc['filename'].'" target="_blank">' .$doc['name'] . '</a></td></tr>';
						
						}
						
						echo '</table>';					
					
					echo '</div></div>';
				}
			
			}
		
		}			
		
	echo '</div></div></div>';
	$counter++;	
	}

}

 require("common/footer.php"); ?>
