    <div id="newTab" class="modal hide fade">
    <form action="<?php echo base_url(); ?>settings/new_custom_form" method="post" name="" id="">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Add Custom Form</h4>
    </div>
    <div class="modal-body">
    <label>Form Name:<br />
    <input type="text" name="name" id="name" class="span4" placeholder="Enter custom form name" />
    </label>
    <label>Description:<br />
    <input type="text" name="desc" id="desc" class="span4" placeholder="Enter a description" />
    </label>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    </form>
    </div>
    </div>

    <div id="container_top">
<h4>Custom Fields</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li class="dropdown active">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Custom Fields <b class="caret"></b></a>
    <ul class="dropdown-menu">
    	<li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Tabs</a></li>
        <li><a href="<?php echo base_url(); ?>settings/custom_forms">Custom Forms</a></li>
    </ul>
    </li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Custom Forms</h5>
        	<?php if ($forms) {

				echo '<table width="50%" class="table">';

					foreach($forms as $form) {

						echo '<tr>
						<td width="85%"><strong>'. $form['name'] .'</strong>';

						if ($form['desc']!="") { echo ' - <em>'. $form['desc'] .'</em>'; }

						echo '</td>
						<td align="center" style="text-align:center;">
						<a href="'. base_url() .'settings/custom_forms_edit?id='. $form['id'] .'">Edit</a> | <a href="'. base_url() .'settings/delete_custom_form?id='. $form['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this custom form?\')">Remove</a>
						</td>
						</tr>';

					}

				echo '</table>';

			} else {

				echo 'No custom forms have been created.';

			} ?>
			</div>
        </div>
        <p><a href="#newTab" data-toggle="modal"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add custom form</a></p>


        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">Custom Forms</h5>
            <p></p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
