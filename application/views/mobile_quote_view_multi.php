<?php
if (!$quote_results) {
	redirect('clients');
}

//echo '<pre>';
//print_r($quote_results);
//echo '</pre>';

if ($quote_results) {

	// loop and display each quote
	foreach($quote_results as $quote) {
	
	echo '<h4>Quote Requested: '. $quote['product_line'] .'</h4>';
		
		echo '<ul class="list">';
		
			if (isset($quote['quotes'])) {
			
				foreach($quote['quotes'] as $result) {
		
					if ($result['apply_online']==1) {
					
						echo '<li class="arrow"><a href="https://www.webline.co.uk/WeblineNet/OnlineApps/DirectApply.aspx?ResponseGuid='.$result['response_guid'].'&ResponseRef='.$result['response_ref'].'&Vnd_Number=023985" target="_blank" title="Click To Purchase"><small><strong>'. $result['provider'] .' - '. $result['product'] .' ('. $result['rates'] .')</strong><br />
						Premium: &pound;'. $result['premium'] .' - Benefit: &pound; '. number_format($result['benefit'],2) .'<p style="font-size:11px;">'. $result['notes'] .'</p></small></a></li>';
					
					} else {
					
						echo '<li><small><strong>'. $result['provider'] .' - '. $result['product'] .' ('. $result['rates'] .')</strong><br />
						Premium: &pound;'. $result['premium'] .' - Benefit: &pound; '. number_format($result['benefit'],2) .'<p style="font-size:11px;">'. $result['notes'] .'</p></small></li>';
					
					}
				
				}
				
			} else {
				echo '<li><small><strong>No quotes for this product</strong></small></li>';
			}
		
		echo '</ul>';
	
	}

} ?>




