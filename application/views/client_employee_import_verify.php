<div id="container_top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" align="left"><h4>Client Employee Import - Verify</h4></td>
    <td align="right"></td>
  </tr>
</table>
</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
	<div class="span7 well">
	<?php if ($csv_row) {
	
		echo 'Some information here blah blah
		<form action="'. base_url() .'clients/employee_process_import?id='. $client .'" method="post">';
	
		echo '<table width="100%" class="table">
		 <thead>
			<tr>
			  <th>Select column</th>
			  <th>Example data</th>
			</tr>
		  </thead><tbody>';
		  
		  $row_count = 0;
		  
		  foreach($csv_row as $row) {
		  
		  	echo '<tr>
					<td><select name="column['. $row_count .']">
					<option value="">Do not import / Unknown</option>
					<optgroup label="Employee Details">
						<option value="title">Title</option>
						<option value="first_name">First name</option>
						<option value="last_name">Surname</option>
						<option value="job_title">Job title</option>
						<option value="dob">Date of birth</option>
						<option value="house_number">House Number / Name</option>
						<option value="address">Address Line</option>
						<option value="town">Town</option>
						<option value="county">County</option>
						<option value="postcode">Postcode</option>
					</optgroup>
					</select></td>
					<td>'. $row .'</td>
				  </tr>';
				  
			$row_count++;
		  
		  }		
		
		echo '</tbody></table>';
		echo '<input type="hidden" name="file" value="'. $file .'" />';
		echo '<input type="hidden" name="columns" value="'. $columns .'" />';
		//echo '<input type="hidden" name="client" value="'. $client .'" />';
		if ($headers) {
			echo '<input type="hidden" name="headers" value="1" />';
		} else {
			echo '<input type="hidden" name="headers" value="0" />';
		}
		echo '<p><button type="submit" class="btn">Continue</button></p><form>';	
	
	} else {
	
	
	} ?>
    </div>   
    <div class="span5 well helpbox">
    <h5 style="margin-top:0px;">Importing Employees - Step 2</h5>
    <p>Proin dui est, porttitor fermentum semper vel, elementum ut mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean fermentum sagittis elementum. In mollis suscipit velit vitae fringilla. In hac habitasse platea dictumst. Sed sapien orci, pellentesque in accumsan quis, facilisis ut enim.</p>    
    </div>
</div>


<?php require("common/footer.php"); ?>
