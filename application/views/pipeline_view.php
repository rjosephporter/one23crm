<script>
$(document).ready(function() {
	
	$("#status,#tag").change(function() {
		$("#status_sort").submit();
	});

});
</script>
<div id="container_top">
<h4>Sales Pipeline</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>pipeline">Chart / Stats View</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>pipeline/list_view">List View</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

<div row-fluid>
	<div class="span12">
    <form class="form-inline" method="get" id="status_sort">
    Filter by status or milestone:
    <select id="status" name="status" style="padding:0; height:23px;">
    <optgroup label="Status">
    <option value="">All (open &amp; closed)</option>
    <option value="open" <?php if ($selected_status=="open") { echo 'selected="selected"'; } ?>>Open</option>
    <option value="closed" <?php if ($selected_status=="closed") { echo 'selected="selected"'; } ?>>Closed</option>
    <option value="closed30" <?php if ($selected_status=="closed30") { echo 'selected="selected"'; } ?>>Closed in last 30 days</option>
    <option value="closed90" <?php if ($selected_status=="closed90") { echo 'selected="selected"'; } ?>>Closed in last 90 days</option>
    <option value="closedyear" <?php if ($selected_status=="closedyear") { echo 'selected="selected"'; } ?>>Closed in the last year</option>
    </optgroup>
    <optgroup label="Milestone">
    <option value="suspect" <?php if ($selected_status=="suspect") { echo 'selected="selected"'; } ?>>Milestone is Suspect</option>
    <option value="prospect" <?php if ($selected_status=="prospect") { echo 'selected="selected"'; } ?>>Milestone is Prospect</option>
    <option value="champion" <?php if ($selected_status=="champion") { echo 'selected="selected"'; } ?>>Milestone is Champion</option>
    <option value="opportunity" <?php if ($selected_status=="opportunity") { echo 'selected="selected"'; } ?>>Milestone is Opportunity</option>
    <option value="proposal" <?php if ($selected_status=="proposal") { echo 'selected="selected"'; } ?>>Milestone is Proposal</option>
    <option value="verbal" <?php if ($selected_status=="verbal") { echo 'selected="selected"'; } ?>>Milestone is Verbal</option>
    <option value="lost" <?php if ($selected_status=="lost") { echo 'selected="selected"'; } ?>>Milestone is Lost</option>
    <option value="won" <?php if ($selected_status=="won") { echo 'selected="selected"'; } ?>>Milestone is Won</option>
    </optgroup>
    </select>
    <?php if ($opp_tags) {
	echo 'Tag:
	<select name="tag" id="tag" style="padding:0; height:23px;">
		<option value=""></option>';
    	foreach ($opp_tags as $tag) {
			if ($selected_tag==$tag['id']) {
				$selected = 'selected="selected"';
			} else {
				$selected = "";
			}
			echo '<option value="'.$tag['id'].'" '.$selected.'>'.$tag['tag'].'</option>';
		}
	echo '</select>';
	} ?>
    
	<?php if ($this->session->userdata("role")==1) { ?>
    &nbsp;User:
    <select id="user" name="user" style="padding:0; height:23px;">
    <option value="">Myself Only</option>
	<option value="all" <?php if ($this->input->get("user")=="all") { echo 'selected="selected"'; } ?>>All Users</option>
    <?php if ($group) {
        foreach($group as $user) {
            echo '<option value="'. $user['user_id'] .'"';
            
            if ($user['user_id']==$this->input->get("user")) { echo ' selected="selected"'; }
            
            echo '>'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';
        }
    } ?>
    </select>
    <?php } ?>    
    <button type="submit" class="btn btn-mini">Apply</button>
    </form>
    </div>
</div>

<div row-fluid>
	<div class="span12 well">
	<h5 style="margin-top:0;">Opportunities:</h5>
	<?php if ($list) {
	
	echo '<table width="100%" class="table">
			  <thead>
				<tr>
				  <th>Client</th>
				  <th>Opportunity</th>
				  <th>Milestone</th>
				  <th>Expected Value</th>
				  <th>Expected Close Date</th>
				  <th>Status</th>
				</tr>
			  </thead><tbody>';
				  	
		foreach($list as $opp) {
		
			if ($opp['status']==0) {
				$status = "Open";
			} else {
				$status = "Closed";
			}
		
			echo '<tr>
				<td><a href="'.base_url().'clients/opportunities?id='.$opp['customer_id'].'">'.$opp['client'].'</a></td>
				<td>'.$opp['name'].'</td>
				<td>'.$opp['milestone'].' ('.$opp['probability'].'%)</td>
				<td>&pound;'.$opp['value_calc'].'</td>
				<td>'.date("jS F Y", strtotime($opp['close_date'])).'</td>
				<td>'.$status.'</td>
				</tr>';
				
		}
		
		echo '</tbody></table>';
	
	} else {
	
		echo 'No opportunities found matching any selected criteria.';
	
	} ?>
    </div>
</div>

<?php require("common/footer.php"); ?>
