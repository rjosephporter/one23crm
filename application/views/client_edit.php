<?php 

if (substr($this->session->userdata('theme_site'),-5)=="clean") {
	$icon_set = '-clean';
} else {
	$icon_set = '';
} 

if (!$customer_details) {
redirect('clients');
}

?>

<div id="container_top">
<h4>Edit</h4>
</div>

<br clear="all" />
<div class="container-fluid">

<?php echo form_open('clients/update_client'); ?>

<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Personal Details:</h5>
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Title:<br />
    <select name="title" id="title" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Mr" <?php if ($customer_details['title']=="Mr") { echo 'selected="selected"'; } ?>>Mr</option>
      <option value="Mrs" <?php if ($customer_details['title']=="Mrs") { echo 'selected="selected"'; } ?>>Mrs</option>
      <option value="Miss" <?php if ($customer_details['title']=="Miss") { echo 'selected="selected"'; } ?>>Miss</option>
      <option value="Ms" <?php if ($customer_details['title']=="Ms") { echo 'selected="selected"'; } ?>>Ms</option>
      <option value="Dr" <?php if ($customer_details['title']=="Dr") { echo 'selected="selected"'; } ?>>Dr</option>
    </select>
    </div>
  	<div class="span3" style="width:210px;">
	First Name:<br /><input name="fname" type="text" id="fname" placeholder="Enter the first name" value="<?php echo $customer_details['first_name']; ?>" />
    </div>    
  	<div class="span3" style="width:210px;">
	Surname:<br /><input name="sname" type="text" id="sname" placeholder="Enter the surname" value="<?php echo $customer_details['last_name']; ?>" />
    </div> 
    <div class="span3" style="width:210px;">
    Date of Birth: DD/MM/YYYY<br /><input name="dob2" type="text" id="dob2" placeholder="Enter date of birth" value="<?php if ($customer_details['dob']!="0000-00-00") { echo date("d/m/Y", strtotime($customer_details['dob'])); } ?>" />
    </div> 
  	</div> 
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Job Title:<br /><input name="job" type="text" id="job" placeholder="Enter a job title" value="<?php echo $customer_details['occupation']; ?>" />
    </div>
  	<div class="span3" style="width:210px;">
	Marital Status:<br />
    <select name="marital" id="marital" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Married" <?php if ($customer_details['marital_status']=="Married") { echo 'selected="selected"'; } ?>>Married</option>
      <option value="Single" <?php if ($customer_details['marital_status']=="Single") { echo 'selected="selected"'; } ?>>Single</option>
      <option value="Widowed" <?php if ($customer_details['marital_status']=="Widowed") { echo 'selected="selected"'; } ?>>Widowed</option>
      <option value="Seperated" <?php if ($customer_details['marital_status']=="Seperated") { echo 'selected="selected"'; } ?>>Seperated</option>
      <option value="Divorced" <?php if ($customer_details['marital_status']=="Divorced") { echo 'selected="selected"'; } ?>>Divorced</option>
      <option value="Co-habiting" <?php if ($customer_details['marital_status']=="Co-habiting") { echo 'selected="selected"'; } ?>>Co-habiting</option>
      <option value="Living together" <?php if ($customer_details['marital_status']=="Living together") { echo 'selected="selected"'; } ?>>Living together</option>
    </select>     
    </div>    
  	<div class="span3" style="width:210px;">
	Employment Status:<br />
    <select name="employment" id="employment" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Employed - Full Time" <?php if ($customer_details['employment_status']=="Employed - Full Time") { echo 'selected="selected"'; } ?>>Employed - Full Time</option>
      <option value="Employed - Part Time" <?php if ($customer_details['employment_status']=="Employed - Part Time") { echo 'selected="selected"'; } ?>>Employed - Part Time</option>
      <option value="Self-Employed" <?php if ($customer_details['employment_status']=="Self-Employed") { echo 'selected="selected"'; } ?>>Self-Employed</option>
      <option value="Retired" <?php if ($customer_details['employment_status']=="Retired") { echo 'selected="selected"'; } ?>>Retired</option>
      <option value="Unemployed" <?php if ($customer_details['employment_status']=="Unemployed") { echo 'selected="selected"'; } ?>>Unemployed</option>
      <option value="House Person" <?php if ($customer_details['employment_status']=="House Person") { echo 'selected="selected"'; } ?>>House Person</option>
    </select>    
    </div> 
    <div class="span3" style="width:210px;">
	Living Status:<br />
    <select name="living" id="living" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Owner - Outright" <?php if ($customer_details['living_status']=="Owner - Outright") { echo 'selected="selected"'; } ?>>Owner - Outright</option>
      <option value="Owner - Mortgage" <?php if ($customer_details['living_status']=="Owner - Mortgage") { echo 'selected="selected"'; } ?>>Owner - Mortgage</option>
      <option value="Rented" <?php if ($customer_details['living_status']=="Rented") { echo 'selected="selected"'; } ?>>Rented</option>
      <option value="Living With Parents" <?php if ($customer_details['living_status']=="Living With Parents") { echo 'selected="selected"'; } ?>>Living With Parents</option>
    </select>    
    </div>    
	</div>    
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Smoker?<br />
    <input name="smoker" type="checkbox" id="smoker" value="1" <?php if ($customer_details['smoker']=="1") { echo 'checked="checked"'; } ?> />
    </div>
  	<div class="span3" style="width:210px;">
    </div>    
  	<div class="span3" style="width:210px;">
    </div> 
    <div class="span3" style="width:210px;">
    </div>    
	</div>    
     
  </div>  
</div>

<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Contact Details:</h5>
  <h6>Telephone Numbers</h6>
  <?php if ($customer_telephone) {
  		
		$phone_count = 1;
		
		foreach($customer_telephone as $telephone) {
		
			echo '<input name="telephone['. $phone_count .']" type="text" id="telephone['. $phone_count .']" placeholder="enter telephone number" value="'. $telephone['number'] .'" />&nbsp;<select name="telephone_type['. $phone_count .']" style="padding:0px; height:23px; width:100px;">';
			echo '<option value="Home"'; if ($telephone['type']=="Home") { echo 'selected="selected"'; } echo '>Home</option>';
			echo '<option value="Work"'; if ($telephone['type']=="Work") { echo 'selected="selected"'; } echo '>Work</option>';
			echo '<option value="Direct"'; if ($telephone['type']=="Direct") { echo 'selected="selected"'; } echo '>Direct</option>';
			echo '<option value="Mobile"'; if ($telephone['type']=="Mobile") { echo 'selected="selected"'; } echo '>Mobile</option>';
			echo '<option value="Fax"'; if ($telephone['type']=="Fax") { echo 'selected="selected"'; } echo '>Fax</option></select><br />';
		
		$phone_count++;
		
		}
  
  } else { ?>
  <input name="telephone[1]" type="text" id="telephone[1]" placeholder="enter telephone number" />&nbsp;<select name="telephone_type[1]" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select>
  <?php } ?>
  <div id="telephoneNumbers">
  </div>
  <p><a href="#" id="addTelephone"><img src="<?php echo base_url(); ?>img/small-add-icon<?php echo $icon_set; ?>.png" />&nbsp;add telephone number</a></p>
  <h6>Email Addresses</h6>
  <?php if ($customer_email) {
  		
		$email_count = 1;
		
		foreach($customer_email as $email) {
		
			echo '<input name="email['.$email_count.']" type="text" id="email['.$email_count.']" placeholder="enter email address" value="'. $email['email'] .'" />&nbsp;<select name="email_type['.$email_count.']" style="padding:0px; height:23px; width:100px;">';
			echo '<option value="Home"'; if ($email['type']=="Home") { echo 'selected="selected"'; } echo '>Home</option>';
			echo '<option value="Work"'; if ($email['type']=="Work") { echo 'selected="selected"'; } echo '>Work</option></select><br />';
		
		$email_count++;
		
		}
  
  } else { ?> 
  <input name="email[1]" type="text" id="email[1]" placeholder="enter email address" />&nbsp;<select name="email_type[1]" style="padding:0px; height:23px; width:100px;">
  <option value="Home">Home</option>
  <option value="Work">Work</option></select><br />
  <?php } ?>
  <div id="emailAddresses">
  </div>  
  <p><a href="#" id="addEmailAddress"><img src="<?php echo base_url(); ?>img/small-add-icon<?php echo $icon_set; ?>.png" />&nbsp;add email address</a></p>  
  <h6>Websites</h6>
  <?php if ($customer_url) {
  		
		$url_count = 1;
		
		foreach($customer_url as $url) {
		
			echo '<input name="url['.$url_count.']" type="text" id="url['.$url_count.']" placeholder="enter web address" value="'. $url['url'] .'" />&nbsp;';
			echo '<select name="url_site['.$url_count.']" style="padding:0px; height:23px; width:100px;">';
			echo '<option value="Website"'; if ($url['website']=="Website") { echo 'selected="selected"'; } echo '>Website</option>';
			echo '<option value="Twitter"'; if ($url['website']=="Twitter") { echo 'selected="selected"'; } echo '>Twitter</option>';
			echo '<option value="Skype"'; if ($url['website']=="Skype") { echo 'selected="selected"'; } echo '>Skype</option>';
			echo '<option value="Xing"'; if ($url['website']=="Xing") { echo 'selected="selected"'; } echo '>Xing</option>';
			echo '<option value="Google+"'; if ($url['website']=="Google+") { echo 'selected="selected"'; } echo '>Google+</option>';
			echo '<option value="Facebook"'; if ($url['website']=="Facebook") { echo 'selected="selected"'; } echo '>Facebook</option>';
			echo '<option value="YouTube"'; if ($url['website']=="YouTube") { echo 'selected="selected"'; } echo '>YouTube</option>';
			echo '<option value="GitHub"'; if ($url['website']=="Github") { echo 'selected="selected"'; } echo '>GitHub</option>';
			echo '<option value="LinkedIn"'; if ($url['website']=="LinkedIn") { echo 'selected="selected"'; } echo '>LinkedIn</option>';
			echo '<option value="Blog"'; if ($url['website']=="Blog") { echo 'selected="selected"'; } echo '>Blog</option></select>&nbsp;';
			echo '<select name="url_type['.$url_count.']" style="padding:0px; height:23px; width:100px;">';
			echo '<option value="Personal"'; if ($url['type']=="Personal") { echo 'selected="selected"'; } echo '>Personal</option>';
			echo '<option value="Work"'; if ($url['type']=="Work") { echo 'selected="selected"'; } echo '>Work</option></select><br />';
		
		$url_count++;
		
		}
  
  } else { ?> 
<input name="url[1]" type="text" id="url[1]" placeholder="enter web address" />&nbsp;<select name="url_site[1]" style="padding:0px; height:23px; width:100px;"><option value="Website">Website</option><option value="Twitter">Twitter</option><option value="Skype">Skype</option><option value="Xing">Xing</option><option value="Google+">Google+</option><option value="Facebook">Facebook</option><option value="YouTube">YouTube</option><option value="GitHub">GitHub</option><option value="LinkedIn">LinkedIn</option><option value="Blog">Blog</option></select>&nbsp;<select name="url_type[1]" style="padding:0px; height:23px; width:100px;"><option value="Personal">Personal</option><option value="Work">Work</option></select>
<?php } ?>  
  <div id="webAddressess">  
  </div> 
  <p><a href="#" id="addURL"><img src="<?php echo base_url(); ?>img/small-add-icon<?php echo $icon_set; ?>.png" />&nbsp;add web address</a></p>   
  <h6>Addresses</h6>
  <div class="form-inline">
  <input type="text" id="find_address_1" class="form-control" placeholder="House Number" />
  <input type="text" id="find_address_2" class="form-control" placeholder="Postcode" />
  <button type="button" id="find_address_submit" class="btn btn-mini">Find Address</button>
  <div id="address_lookup_results"></div>
  </div>
  <br clear="all" />    
  <textarea name="address" id="address" cols="" rows="2" style="width:300px; padding:2px;"><?php echo $customer_details['address_line_1']; ?></textarea>&nbsp;<select name="address_type" style="padding:0px; height:23px; width:100px; margin-bottom:30px;"><option value="Home">Home</option><option value="Work">Work</option></select><br /> 
  <input name="town" id="town" type="text" style="width:160px;" placeholder="town" value="<?php echo $customer_details['town']; ?>" />&nbsp;<input name="county" id="county" type="text" style="width:120px;" placeholder="county" value="<?php echo $customer_details['county']; ?>" />&nbsp;<input name="postcode" id="postcode" type="text" style="width:100px;" placeholder="postcode" value="<?php echo $customer_details['postcode']; ?>" />
  </div>  
</div>
<script>
$(document).ready(function(){
	var telephoneCounter = <?php if (isset($phone_count)) { echo $phone_count; } else { echo 2; } ?>;
	var emailCounter = <?php if (isset($email_count)) { echo $email_count; } else { echo 2; } ?>;
	var webCounter = <?php if (isset($url_count)) { echo $url_count; } else { echo 2; } ?>;;
		
	$("#addTelephone").click(function(){
	var newTelephone = "";	
	
	newTelephone = '<input name="telephone['+telephoneCounter+']" type="text" id="telephone['+telephoneCounter+']" placeholder="enter telephone number" />&nbsp;<select name="telephone_type['+telephoneCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select><br />';
	
	$("#telephoneNumbers").append(newTelephone);
	++telephoneCounter;
	});

	$("#addEmailAddress").click(function(){
	var newEmail = "";	
	newEmail = '<input name="email['+emailCounter+']" type="text" id="email['+emailCounter+']" placeholder="enter email address" />&nbsp;<select name="email_type['+emailCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option></select><br />';
	
	$("#emailAddresses").append(newEmail);
	++emailCounter;
	});

	$("#addURL").click(function(){
	var newWeb = "";	
	newWeb = '<input name="url['+webCounter+']" type="text" id="url['+webCounter+']" placeholder="enter web address"  />&nbsp;<select name="url_site['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Website">Website</option><option value="Twitter">Twitter</option><option value="Skype">Skype</option><option value="Xing">Xing</option><option value="Google+">Google+</option><option value="Facebook">Facebook</option><option value="YouTube">YouTube</option><option value="GitHub">GitHub</option><option value="LinkedIn">LinkedIn</option><option value="Blog">Blog</option></select>&nbsp;<select name="url_type['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Personal">Personal</option><option value="Work">Work</option></select><br />';
	
	$("#webAddressess").append(newWeb);
	++emailCounter;
	});

	// address finder
	$("#find_address_submit").click(function() {
		// get the number and postcode
		var findnumber = $("#find_address_1").val();
		var findpostcode = $("#find_address_2").val();
		var info = [];
		$.ajax({
			dataType: 'json',
			data: 'postcode='+findpostcode+'&number='+findnumber,
			url: '<?php echo base_url(); ?>clients/addresslookup',
			success: function(data) {
				var line;
				$("#address_lookup_results").html('<select id="address_lookup_results_list" multiple="multiple" style="width:500px;"></select>');
				for(var i in data) {
					info[i] = data[i];
					line = data[i].address+", "+data[i].town+", "+data[i].county+", "+data[i].postcode;
					$("#address_lookup_results_list").append('<option value="'+i+'">'+line+'</option>')
				}
				// select and populate form
				$("#address_lookup_results_list").click(function() {
					var address_val = (this.value);
					$("#address").val(info[address_val].address);
					$("#town").val(info[address_val].town);
					$("#county").val(info[address_val].county);
					$("#postcode").val(info[address_val].postcode);
				});
			},
			cache: false	
		});	
	});

});
</script>
<div class="row">
  <div class="span4"><button class="btn" type="submit">Save</button>&nbsp;<a class="btn" href="<?php echo base_url(); ?>clients/view?id=<?php echo $this->input->get("id"); ?>">Cancel</a>
</div>
</div>
<input name="id" type="hidden" id="id" value="<?php echo $this->input->get('id', TRUE); ?>" />
<input name="return" type="hidden" id="return" value="<?php echo $this->input->get('return', TRUE); ?>" />
<input name="returnid" type="hidden" id="returnid" value="<?php echo $this->input->get('returnid', TRUE); ?>" />
<?php echo form_close(); ?>
</div>
<?php require("common/footer.php"); ?>
