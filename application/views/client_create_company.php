

<div id="container_top">
<h4>New Company</h4>
</div>

<br clear="all" />
<div class="container-fluid">

<?php echo form_open('clients/create_company_client'); ?>

<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Organisation:</h5>
	<div class="row"> 
  	<div class="span12">
    Company Name:<br /><input name="company" type="text" id="company" placeholder="Enter company name" />&nbsp;&nbsp;<div id="searchCompanyInfo" class="btn btn-mini" style="margin-bottom:10px;">search company information</div>
    <div id="company_lookup_results"></div>
    </div>     
  	</div>
        
	<div class="row"> 
  	<div class="span3" style="width:210px;">
	Company Registration Number:<br /><input name="companyreg" type="text" id="companyreg" placeholder="Enter company reg number" />
    </div>    
  	<div class="span3" style="width:210px;">
	Number Employees:<br /><input name="companyemployee" type="text" id="companyemployee" placeholder="Enter number of employees" />
    </div> 
    <div class="span3" style="width:210px;">
    Company Industry Sector:<br /><input name="sector" type="text" id="sector" placeholder="Enter business sector" />
    </div>
  	<div class="span3" style="width:210px;"></div>    
  	</div>    
      
  </div>  
</div>

<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Organisation Contact Details:</h5>
  <h6>Telephone Numbers</h6>
  <input name="telephone[1]" type="text" id="telephone[1]" placeholder="enter telephone number" />&nbsp;<select name="telephone_type[1]" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select>
  <div id="telephoneNumbers">
  </div>
  <p><a href="#" id="addTelephone"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add telephone number</a></p>
  <h6>Email Addresses</h6>
  <input name="email[1]" type="text" id="email[1]" placeholder="enter email address" />&nbsp;<select name="email_type[1]" style="padding:0px; height:23px; width:100px;">
  <option value="Home">Home</option>
  <option value="Work">Work</option></select><br />
  <div id="emailAddresses">
  </div>  
  <p><a href="#" id="addEmailAddress"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add email address</a></p>  
  <h6>Websites</h6>
  <div id="webAddressess">  
  </div> 
  <p><a href="#" id="addURL"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add web address</a></p>   
  <h6>Addresses</h6>
  <div class="form-inline">
  <input type="text" id="find_address_1" class="form-control" placeholder="House Number" />
  <input type="text" id="find_address_2" class="form-control" placeholder="Postcode" />
  <button type="button" id="find_address_submit" class="btn btn-mini">Find Address</button>
  <div id="address_lookup_results"></div>
  </div>
  <br clear="all" />  
  <textarea name="address" id="address" cols="" rows="2" style="width:300px; padding:2px;"></textarea>&nbsp;<select name="address_type" style="padding:0px; height:23px; width:100px; margin-bottom:30px;"><option value="Home">Home</option><option value="Work">Work</option></select><br /> 
  <input name="town" id="town" type="text" style="width:160px;" placeholder="town" />&nbsp;<input name="county" id="county" type="text" style="width:120px;" placeholder="county" />&nbsp;<input name="postcode" id="postcode" type="text" style="width:100px;" placeholder="postcode" />
  </div>  
</div>

<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Main Contact Details (if required):</h5>
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Title:<br />
    <select name="contact_title" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Mr">Mr</option>
      <option value="Mrs">Mrs</option>
      <option value="Miss">Miss</option>
      <option value="Ms">Ms</option>
      <option value="Dr">Dr</option>
    </select>
    </div>
  	<div class="span3" style="width:210px;">
	First Name:<br /><input name="contact_fname" type="text" id="contact_fname" placeholder="Enter the first name" />
    </div>    
  	<div class="span3" style="width:210px;">
	Surname:<br /><input name="contact_sname" type="text" id="contact_sname" placeholder="Enter the surname" />
    </div> 
    <div class="span3" style="width:210px;">
    Job Title:<br /><input name="contact_job" type="text" id="contact_job" placeholder="Enter a job title" />
    </div> 
  	</div> 
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Email Address:<br /><input name="contact_email" type="text" id="contact_email" placeholder="Enter email address" />
    </div>
  	<div class="span3" style="width:210px;">
    Telephone Number:<br /><input name="contact_phone" type="text" id="contact_phone" placeholder="Enter telephone number" />
    </div> 
    <div class="span3" style="width:210px;">
    </div>    
	</div>
  </div>  
</div>

<script>
$(document).ready(function(){
	var telephoneCounter = 2;
	var emailCounter = 2;
	var webCounter = 1;
		
	$("#addTelephone").click(function(){
	var newTelephone = "";	
	
	newTelephone = '<input name="telephone['+telephoneCounter+']" type="text" id="telephone['+telephoneCounter+']" placeholder="enter telephone number" />&nbsp;<select name="telephone_type['+telephoneCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select><br />';
	
	$("#telephoneNumbers").append(newTelephone);
	++telephoneCounter;
	});

	$("#addEmailAddress").click(function(){
	var newEmail = "";	
	newEmail = '<input name="email['+emailCounter+']" type="text" id="email['+emailCounter+']" placeholder="enter email address" />&nbsp;<select name="email_type['+emailCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option></select><br />';
	
	$("#emailAddresses").append(newEmail);
	++emailCounter;
	});

	$("#addURL").click(function(){
	var newWeb = "";	
	newWeb = '<input name="url['+webCounter+']" type="text" id="url['+webCounter+']" placeholder="enter web address"  />&nbsp;<select name="url_site['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Website">Website</option><option value="Twitter">Twitter</option><option value="Skype">Skype</option><option value="Xing">Xing</option><option value="Google+">Google+</option><option value="Facebook">Facebook</option><option value="YouTube">YouTube</option><option value="GitHub">GitHub</option><option value="LinkedIn">LinkedIn</option><option value="Blog">Blog</option></select>&nbsp;<select name="url_type['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Personal">Personal</option><option value="Work">Work</option></select><br />';
	
	$("#webAddressess").append(newWeb);
	++emailCounter;
	});
	
	// address finder
	$("#find_address_submit").click(function() {
		// get the number and postcode
		var findnumber = $("#find_address_1").val();
		var findpostcode = $("#find_address_2").val();
		var info = [];
		$.ajax({
			dataType: 'json',
			data: 'postcode='+findpostcode+'&number='+findnumber,
			url: '<?php echo base_url(); ?>clients/addresslookup',
			success: function(data) {
				var line;
				$("#address_lookup_results").html('<select id="address_lookup_results_list" multiple="multiple" style="width:500px;"></select>');
				for(var i in data) {
					info[i] = data[i];
					line = data[i].address+", "+data[i].town+", "+data[i].county+", "+data[i].postcode;
					$("#address_lookup_results_list").append('<option value="'+i+'">'+line+'</option>')
				}
				// select and populate form
				$("#address_lookup_results_list").click(function() {
					var address_val = (this.value);
					$("#address").val(info[address_val].address);
					$("#town").val(info[address_val].town);
					$("#county").val(info[address_val].county);
					$("#postcode").val(info[address_val].postcode);
				});
			},
			cache: false	
		});	
	});
	
	$("#searchCompanyInfo").click(function() {
		var company_info = [];
		var company = $("#company").val();
		$.ajax({
			dataType: 'json',
			data: 'company='+company,
			url: '<?php echo base_url(); ?>clients/search_company_details',
			success: function(data) {
				var line;
				$("#company_lookup_results").html('<select id="company_lookup_results_list" multiple="multiple" style="width:500px;"></select>');
				for(var i in data) {
					company_info[i] = data[i];
					line = data[i].name;
					$("#company_lookup_results_list").append('<option value="'+data[i].number+'">'+line+'</option>')
				}
				$("#company_lookup_results_list").click(function() {
					var company_number = (this.value);
						$.ajax({
							dataType: 'json',
							data: 'company_number='+company_number,
							url: '<?php echo base_url(); ?>clients/get_company_details',
							success: function(company_data) {
								var att = $.parseJSON(company_data);
								$("#duedil_company").text(company_data);
								$("#company").val(att.name_formatted);
								$("#companyreg").val(att.company_number);
								//$("#sector").val(att.company_number);
								$("#find_address_1").val(att.registered_address['full_address'][0]);
								$("#find_address_2").val(att.registered_address['postcode']);
								$("#address").val(att.registered_address['full_address'][0]);
								//$("#town").val(att.registered_address['full_address'][2]);
								//$("#county").val(att.registered_address['full_address'][3]);
								$("#postcode").val(att.registered_address['postcode']);
							},
							cache: false	
						});						
				});
			},
			cache: false	
		});	
	});
	
	

});
</script>
<div class="row">
  <div class="span4"><button class="btn" type="submit">Save</button>&nbsp;<a class="btn" href="<?php echo base_url(); ?>clients">Cancel</a>
</div>
</div>
<textarea name="duedil_company" id="duedil_company" style="display:none;"></textarea>
<input name="id" type="hidden" id="id" value="<?php echo $this->input->get('id', TRUE); ?>" />
<?php echo form_close(); ?>
</div>
<?php require("common/footer.php"); ?>
