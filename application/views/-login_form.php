<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/ico" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<style>
body {
background-color:#0b9fe5;
}
.login-logo {
width:411px;
margin:0 auto;
position:relative;
background:url(<?php echo base_url(); ?>img/login_logo.png) top center;
height:208px;
}

.login-page {
width:400px;
margin:0 auto;
position:relative;
margin-top:20px;
}

.login-page h2,h3 {
margin-top:0px;
}

.ip {
margin-top:15px;
color:#999;
}

.forgot {
padding-top:20px;
}

.well {
padding-bottom:0px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	//$("#username").focus();
});
</script>
</head>

<body>
<div class="login-logo"></div>
<div class="well login-page">
<?php if (isset($_GET['failed'])) { echo '<div class="alert alert-error">Invalid login details provided, please try again.</div>'; } ?>
<?php if (isset($_GET['activated'])) { echo '<div class="alert alert-success">Your account has now been activated, you may now login below.</div>'; } ?>
<?php if (isset($_GET['reset'])) { echo '<div class="alert alert-success">Your password has been reset, check your inbox.</div>'; } ?>
<?php if (isset($_GET['expired'])) { echo '<div class="alert alert-error">Your subscription has now expired, please click here to extend/renew the subscription.</div>'; } ?>
<?php if (isset($_GET['subscription'])) { echo '<div class="alert alert-error">There is a problem with your subscription, please contact the accounts department.</div>'; } ?>
<h3>Please Login</h3>
<?php echo form_open('login/auth'); ?>
<div class="row-fluid">
  <div class="span3">Username:</div>
  <div class="span9"><?php echo 
  form_input(array(
  'name' => 'username',
  'id' => 'username',
  'value' => '',
  'placeholder' => 'Username',
)); ?></div>
</div>
<div class="row-fluid">
  <div class="span3">Password:</div>
  <div class="span9"><?php echo form_input(array(
  'name' => 'password',
  'value' => '',
  'type' => 'password',
  'placeholder' => 'Password',
)); ?></div>
</div>
<?php if ($loginRedirect) {
	echo '<input type="hidden" name="login_redirect" value="'. $loginRedirect .'">';
} ?>

<div class="row">
  <div class="span9"><button type="submit" class="btn"><i class="icon-lock"></i>&nbsp;&nbsp;Login Securely</button></div>
</div>
<div class="row forgot">
<div class="span9"><a href="<?php echo base_url(); ?>login/password_request">Forgotten your password?</a></div>
</div>
<div class="row ip">
  <div class="span9"><small><strong>Your IP Address has been logged for security reasons.</strong></small></div>
</div>
<?php echo form_close(); ?>
</div>
</body>
</html>
