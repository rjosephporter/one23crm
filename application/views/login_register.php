<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/ico" />
<script src="<?php echo base_url(); ?>js/placeholders.min.js"></script>
<style>
body {
background-color:#0b9fe5;
}
.login-logo {
width:411px;
margin:0 auto;
position:relative;
background:url(<?php echo base_url(); ?>img/login_logo.png) top center;
height:208px;
}

.login-page {
width:500px;
margin:0 auto;
position:relative;
margin-top:20px;
}

.login-page h2,h3 {
margin-top:0px;
}

.ip {
margin-top:15px;
color:#999;
}

.forgot {
padding-top:20px;
}

.well {
padding-bottom:0px;
}
</style>
</head>

<body>
<div class="login-logo"></div>
<div class="well login-page">
<?php echo $this->session->flashdata('errors'); ?>
<h3>Account Registration</h3>
<p><strong>Please enter your details in the fields provided below.</strong></p>
<form class="form-horizontal" action="<?php echo base_url(); ?>login/complete_registration" method="post">

 <div style="background:#d1d1d1; padding:10px; margin:5px;">Your Contact Infomation</div>
    <table width="100%" border="0" cellpadding="5">
      <tr>
        <td width="25%" align="left"><strong>Title:</strong></td>
        <td>
        <select name="title">
        <option value="">Please Select</option>
        <option value="Mr" <?php if ($this->session->flashdata('title')=="Mr") { echo 'selected="selected"'; } ?>>Mr</option>
        <option value="Mrs" <?php if ($this->session->flashdata('title')=="Mrs") { echo 'selected="selected"'; } ?>>Mrs</option>
        <option value="Ms" <?php if ($this->session->flashdata('title')=="Ms") { echo 'selected="selected"'; } ?>>Ms</option>
        <option value="Miss" <?php if ($this->session->flashdata('title')=="Miss") { echo 'selected="selected"'; } ?>>Miss</option>
        </select>
        </td>
      </tr>
      <tr>
        <td align="left"><strong>First Name:</strong></td>
        <td><input type="text" name="firstname" placeholder="First Name" value="<?php echo $this->session->flashdata('firstname'); ?>" /></td>
      </tr>                  
      <tr>
        <td align="left"><strong>Surname:</strong></td>
        <td><input type="text" name="surname" placeholder="Surname" value="<?php echo $this->session->flashdata('surname'); ?>" /></td>
      </tr>
      <tr>
        <td align="left"><strong>Email:</strong></td>
        <td><input type="text" name="email" placeholder="Email Address" value="<?php echo $this->session->flashdata('email'); ?>" /></td>
      </tr>                                   
      <tr>
        <td align="left"><strong>Telephone:</strong></td>
        <td><input type="text" name="telephone" placeholder="Telephone Number" value="<?php echo $this->session->flashdata('telephone'); ?>" /></td>
      </tr>             
    </table>
 <div style="background:#d1d1d1; padding:10px; margin:5px;">Company Information</div>
    <table width="100%" border="0" cellpadding="5">
      <tr>
        <td width="25%" align="left"><strong>Company:</strong></td>
        <td><input type="text" name="company" placeholder="Company Name" value="<?php echo $this->session->flashdata('company'); ?>" /></td>
      </tr>                 
      <tr>
        <td align="left"><strong>Address Line 1:</strong></td>
        <td><input type="text" name="address" placeholder="Address Line 1" value="<?php echo $this->session->flashdata('address'); ?>" /></td>
      </tr>
      <tr>
        <td align="left"><strong>Town:</strong></td>
        <td><input type="text" name="town" placeholder="Town" value="<?php echo $this->session->flashdata('town'); ?>" /></td>
      </tr>
      <tr>
        <td align="left"><strong>County:</strong></td>
        <td><input type="text" name="county" placeholder="County" value="<?php echo $this->session->flashdata('county'); ?>" /></td>
      </tr>
      <tr>
        <td align="left"><strong>Postcode:</strong></td>
        <td><input type="text" name="postcode" placeholder="Postcode" value="<?php echo $this->session->flashdata('postcode'); ?>" /></td>
      </tr>                                                                        
    </table>             
 <div style="background:#d1d1d1; padding:10px; margin:5px;">Website Preferences</div>             
    <table width="100%" border="0" cellpadding="5">
      <tr>
        <td width="25%" align="left"><strong>Username:</strong></td>
        <td><input type="text" name="username" placeholder="Username" value="<?php echo $this->session->flashdata('username'); ?>" /></td>
      </tr>
      <tr>
        <td align="left"><strong>Password:</strong></td>
        <td><input type="password" name="password" placeholder="Password" /></td>
      </tr>                                   
      <tr>
        <td align="left"><strong>SMS Display Name:</strong></td>
        <td><input type="text" name="sms" placeholder="SMS Display Name" maxlength="11" value="<?php echo $this->session->flashdata('sms'); ?>" /></td>
      </tr>
    </table>             

<p>&nbsp;</p>



<button type="submit" class="btn" style="background:#BC064D; color:#FFFFFF;">Complete Registration</button>
</form>

</div>
</body>
</html>
