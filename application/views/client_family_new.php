<?php //require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name =  $customer_details['company_name']; } else { $customer_name =  $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?>

<div id="container_top">
<h4><?php echo $customer_name; ?></h4>

    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo base_url(); ?>clients/create_note" method="post" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Add note to account</h3>
    </div>
    <div class="modal-body">Please enter a note to be added to the client account.
        <div class="control-group" style="padding-top:10px;">
            <label class="control-label"></label>
          <div class="controls">
			<textarea name="note" rows="6" id="note"></textarea>
            <input type="hidden" id="notefor" name="notefor" value="<?php echo $this->input->get('id', TRUE); ?>" />       
          </div>
        </div>      
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button id="modalCreateClientNote" class="btn btn-primary">Add Note</button>
    </div>
    </form> 
    </div>
    <div id="updateBackgound" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_backgound" method="post" name="create" id="create">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Update background info</h4>
    </div>
    <div class="modal-body">
 	<textarea name="info" rows="6" id="info" class="span5"><?php echo $customer_details['background_info']; ?></textarea>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />   
    
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientNote" class="btn btn-primary">Update</button>
    </form>
    </div>
    </div>  
    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <input type="hidden" id="clienttask" name="clienttask" value="<?php echo $this->input->get('id', TRUE); ?>" /> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task for client</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>          
        </select>
        
        <select name="task_min" id="task_min" class="span1">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>      
        
        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action">
          <option value="0">Please Select</option>
          <option value="Call">Call</option>
          <option value="Email">Email</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalCreateClientTask" class="btn btn-primary">Create</button>
    </form>
    </div>
    </div>
    
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages</a></li>
    <li class="active"><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  		<form action="<?php echo base_url(); ?>clients/create_new_family" method="post">
        <div class="row-fluid">
        	<div class="span12 well"><h5 style="margin-top:0px;">Personal Details</h5>
            
            <table width="50%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><label>Title:</label>
                    <select name="title" id="title" style="padding:0px; height:23px; width:120px;">
                          <option value="">Please Select</option>
                          <option value="Mr">Mr</option>
                          <option value="Mrs">Mrs</option>
                          <option value="Miss">Miss</option>
                          <option value="Ms">Ms</option>
                          <option value="Sir">Sir</option>
                          <option value="Dr">Dr</option>
                    </select>    
                </td>
                <td><label>First Name:</label><input name="fname" type="text" id="fname" placeholder="Enter the first name" /></td>
                <td><label>Last Name:</label><input name="sname" type="text" id="sname" placeholder="Enter the surname" /></td>
              </tr>
            </table>
            
            <table width="50%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><label>Date of Birth:</label><input name="dob" type="text" id="dob" placeholder="Enter the date of birth" /></td>
                <td><label>Relationship to Client:</label>
                <select name="relationship" id="relationship" style="padding:0px; height:23px;">
                  <option value="">Please Select</option>
                  <option value="Spouse/Partner">Spouse/Partner</option>
                  <option value="Son">Son</option>
                  <option value="Daughter">Daughter</option>
                    </select></td>
              </tr>
            </table>            

          </div>

          <input type="hidden" name="client" value="<?php echo $this->input->get('id') ?>" />
          <button class="btn">Save</button>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>clients/people?id=<?php echo $this->input->get('id') ?>" class="btn">Cancel</a>          
		</form>         
      	</div>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
