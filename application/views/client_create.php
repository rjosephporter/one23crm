
<?php echo form_open('clients/create_new_client'); ?>
<div id="container_top">
<h4>New Person</h4>
</div>

<br clear="all" />
<div class="container-fluid">

<?php echo validation_errors(); ?>
<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Personal Details:</h5>
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Title:<br />
    <select name="title" id="title" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Mr">Mr</option>
      <option value="Mrs">Mrs</option>
      <option value="Miss">Miss</option>
      <option value="Ms">Ms</option>
      <option value="Dr">Dr</option>
    </select>
    </div>
  	<div class="span3" style="width:210px;">
	First Name:<br /><input name="fname" type="text" id="fname" placeholder="Enter the first name" />
    </div>    
  	<div class="span3" style="width:210px;">
	Surname:<br /><input name="sname" type="text" id="sname" placeholder="Enter the surname" />
    </div> 
    <div class="span3" style="width:210px;">
    Date of Birth: DD/MM/YYYY<br /><input name="dob2" type="text" id="dob2" placeholder="Enter date of birth" />
    </div>
  	</div> 
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Job Title:<br /><input name="job" type="text" id="job" placeholder="Enter a job title" />
    </div>
  	<div class="span3" style="width:210px;">
	Marital Status:<br />
    <select name="marital" id="marital" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Married">Married</option>
      <option value="Single">Single</option>
      <option value="Widowed">Widowed</option>
      <option value="Seperated">Seperated</option>
      <option value="Divorced">Divorced</option>
      <option value="Co-habiting">Co-habiting</option>
      <option value="Living together">Living together</option>
    </select>     
    </div>    
  	<div class="span3" style="width:210px;">
    Number of Children:<br />
    <select name="dependents" id="dependents" style="height:23px; padding:0px;">
      <option value="0">None</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
    </select>    
    </div> 
    <div class="span3" style="width:210px;">
	Living Status:<br />
    <select name="living" id="living" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Owner - Outright">Owner - Outright</option>
      <option value="Owner - Mortgage">Owner - Mortgage</option>
      <option value="Rented">Rented</option>
      <option value="Living With Parents">Living With Parents</option>
    </select>    
    </div>    
	</div>
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
	Employment Status:<br />
    <select name="employment" id="employment" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Employed - Full Time">Employed - Full Time</option>
      <option value="Employed - Part Time">Employed - Part Time</option>
      <option value="Self-Employed">Self-Employed</option>
      <option value="Retired">Retired</option>
      <option value="Unemployed">Unemployed</option>
      <option value="House Person">House Person</option>
    </select>    
    </div>
  	<div class="span3" style="width:210px;">
    Smoker?<br />
    <input name="smoker" type="checkbox" id="smoker" value="1" />
    </div>    
  	<div class="span3" style="width:210px;">
    </div> 
    <div class="span3" style="width:210px;">
    </div>    
	</div>
    
  </div>  
</div>

<div class="row" id="partner_details">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Partners Details:</h5>
	<div class="row"> 
  	<div class="span3" style="width:210px;">
    Title:<br />
    <select name="partner_title" id="partner_title" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Mr">Mr</option>
      <option value="Mrs">Mrs</option>
      <option value="Miss">Miss</option>
      <option value="Ms">Ms</option>
      <option value="Dr">Dr</option>
    </select>
    </div>
  	<div class="span3" style="width:210px;">
	First Name:<br /><input name="partner_fname" type="text" id="partner_fname" placeholder="Enter the first name" />
    </div>    
  	<div class="span3" style="width:210px;">
	Surname:<br /><input name="partner_sname" type="text" id="partner_sname" placeholder="Enter the surname" />
    </div> 
    <div class="span3" style="width:210px;">
    Date of Birth: DD/MM/YYYY<br /><input name="partner_dob" type="text" id="partner_dob" placeholder="Enter date of birth" />
    </div> 
  	</div> 
    
	<div class="row"> 
  	<div class="span3" style="width:210px;">
	Living Status:<br />
    <select name="partner_living" id="partner_living" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Owner - Outright">Owner - Outright</option>
      <option value="Owner - Mortgage">Owner - Mortgage</option>
      <option value="Rented">Rented</option>
      <option value="Living With Parents">Living With Parents</option>
    </select>    
    </div>
  	<div class="span3" style="width:210px;">
	Employment Status:<br />
    <select name="partner_employment" id="partner_employment" style="height:23px; padding:0px;">
      <option>Please Select</option>
      <option value="Employed - Full Time">Employed - Full Time</option>
      <option value="Employed - Part Time">Employed - Part Time</option>
      <option value="Self-Employed">Self-Employed</option>
      <option value="Retired">Retired</option>
      <option value="Unemployed">Unemployed</option>
      <option value="House Person">House Person</option>
    </select>
    </div> 
    <div class="span3" style="width:210px;">
    Job Title:<br /><input name="partner_job" type="text" id="partner_job" placeholder="Enter a job title" />
    </div>
    <div class="span3" style="width:210px;">
    Smoker?<br />
    <input name="partner_smoker" type="checkbox" id="partner_smoker" value="1" />
    </div>        
	</div>
  </div>  
</div>

<div class="row" id="children_details">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Childrens Details:</h5>
	<div id="kids"></div>
  </div>  
</div>


<div class="row">
  <div class="span12 well" style="width:940px;">
  <h5 style="margin-top:0px;">Contact Details:</h5>
  <h6>Telephone Numbers</h6>
  <input name="telephone[1]" type="text" id="telephone[1]" placeholder="enter telephone number" />&nbsp;<select name="telephone_type[1]" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select>
  <div id="telephoneNumbers">
  </div>
  <p><a href="#" id="addTelephone"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add telephone number</a></p>
  <h6>Email Addresses</h6>
  <input name="email[1]" type="text" id="email[1]" placeholder="enter email address" />&nbsp;<select name="email_type[1]" style="padding:0px; height:23px; width:100px;">
  <option value="Home">Home</option>
  <option value="Work">Work</option></select><br />
  <div id="emailAddresses">
  </div>  
  <p><a href="#" id="addEmailAddress"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add email address</a></p>  
  <h6>Websites</h6>
  <div id="webAddressess">  
  </div> 
  <p><a href="#" id="addURL"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add web address</a></p>   
  <h6>Addresses</h6>
  <div class="form-inline">
  <input type="text" id="find_address_1" class="form-control" placeholder="House Number" />
  <input type="text" id="find_address_2" class="form-control" placeholder="Postcode" />
  <button type="button" id="find_address_submit" class="btn btn-mini">Find Address</button>
  <div id="address_lookup_results"></div>
  </div>
  <br clear="all" />  
  <textarea name="address" id="address" cols="" rows="2" style="width:300px; padding:2px;"></textarea>&nbsp;<select name="address_type" style="padding:0px; height:23px; width:100px; margin-bottom:30px;"><option value="Home">Home</option><option value="Work">Work</option></select><br /> 
  <input name="town" id="town" type="text" style="width:160px;" placeholder="town" />&nbsp;<input name="county" id="county" type="text" style="width:120px;" placeholder="county" />&nbsp;<input name="postcode" id="postcode" type="text" style="width:100px;" placeholder="postcode" />
  </div>  
</div>
<script>
$(document).ready(function(){
	// hide partner on page load
	$("#partner_details").hide();
	$("#children_details").hide();
	
	// they select married, co-hab or living, show the partner box .. else keep it hidden
	$("#marital").change(function(){
		var maritalStatus = $(this).val();
		if (maritalStatus=="Married") {
			$("#partner_details").fadeIn("slow");
		} else if (maritalStatus=="Co-habiting") {
			$("#partner_details").fadeIn("slow");			
		} else if (maritalStatus=="Living together") {
			$("#partner_details").fadeIn("slow");			
		} else {
			$("#partner_details").fadeOut("slow");
		}
	});
	
	// if they select dependents, then show selected number of boxes
	$("#dependents").change(function(){
		var dependentsNumber = $(this).val();
		$("#kids").empty();
		if (dependentsNumber>0) {
			// loop number of kids
			for(var kids=1; kids<=dependentsNumber; kids++) {
				var newKid = "";
				
				newKid = '<strong>Child '+kids+'</strong><br /><table width="50%" border="0" cellspacing="0" cellpadding="2"><tr><td><label>First Name:</label><input name="kid_fname['+kids+']" type="text" placeholder="First name" /></td><td><label>Last Name:</label><input name="kid_sname['+kids+']" type="text" placeholder="Surname" /></td><td><label>Date of Birth:</label><input name="kid_dob['+kids+']" type="text" placeholder="Date of birth" /></td></tr><tr><td><label>Relationship to Client:</label><select name="kid_type['+kids+']" style="padding:0px; height:23px;"><option value="">Please Select</option><option value="Son">Son</option><option value="Daughter">Daughter</option></select></td></tr></table><hr />';
				
				$("#kids").append(newKid);							
			}
			$("#children_details").fadeIn("slow");
		} else {
			$("#children_details").fadeOut("slow");
		}
	});	
	
	var telephoneCounter = 2;
	var emailCounter = 2;
	var webCounter = 1;
		
	$("#addTelephone").click(function(){
	var newTelephone = "";	
	
	newTelephone = '<input name="telephone['+telephoneCounter+']" type="text" id="telephone['+telephoneCounter+']" placeholder="enter telephone number" />&nbsp;<select name="telephone_type['+telephoneCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select> <a href="#" onclick="removeFormField(telephone['+telephoneCounter+']); return false;">Remove</a><br />';
	
	$("#telephoneNumbers").append(newTelephone);
	++telephoneCounter;
	});

	$("#addEmailAddress").click(function(){
	var newEmail = "";	
	newEmail = '<input name="email['+emailCounter+']" type="text" id="email['+emailCounter+']" placeholder="enter email address" />&nbsp;<select name="email_type['+emailCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option></select><br />';
	
	$("#emailAddresses").append(newEmail);
	++emailCounter;
	});

	$("#addURL").click(function(){
	var newWeb = "";	
	newWeb = '<input name="url['+webCounter+']" type="text" id="url['+webCounter+']" placeholder="enter web address"  />&nbsp;<select name="url_site['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Website">Website</option><option value="Twitter">Twitter</option><option value="Skype">Skype</option><option value="Xing">Xing</option><option value="Google+">Google+</option><option value="Facebook">Facebook</option><option value="YouTube">YouTube</option><option value="GitHub">GitHub</option><option value="LinkedIn">LinkedIn</option><option value="Blog">Blog</option></select>&nbsp;<select name="url_type['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Personal">Personal</option><option value="Work">Work</option></select><br />';
	
	$("#webAddressess").append(newWeb);
	++emailCounter;
	});
	
function removeFormField(id) {  
    $(id).remove();  
}

	// address finder
	$("#find_address_submit").click(function() {
		// get the number and postcode
		var findnumber = $("#find_address_1").val();
		var findpostcode = $("#find_address_2").val();
		var info = [];
		$.ajax({
			dataType: 'json',
			data: 'postcode='+findpostcode+'&number='+findnumber,
			url: '<?php echo base_url(); ?>clients/addresslookup',
			success: function(data) {
				var line;
				$("#address_lookup_results").html('<select id="address_lookup_results_list" multiple="multiple" style="width:500px;"></select>');
				for(var i in data) {
					info[i] = data[i];
					line = data[i].address+", "+data[i].town+", "+data[i].county+", "+data[i].postcode;
					$("#address_lookup_results_list").append('<option value="'+i+'">'+line+'</option>')
				}
				// select and populate form
				$("#address_lookup_results_list").click(function() {
					var address_val = (this.value);
					$("#address").val(info[address_val].address);
					$("#town").val(info[address_val].town);
					$("#county").val(info[address_val].county);
					$("#postcode").val(info[address_val].postcode);
				});
			},
			cache: false	
		});	
	});

});
</script>
<div class="row">
  <div class="span4"><button class="btn" type="submit">Save</button>&nbsp;<a class="btn" href="<?php echo base_url(); ?>clients">Cancel</a>
</div>
</div>
<input name="id" type="hidden" id="id" value="<?php echo $this->input->get('id', TRUE); ?>" />
<?php echo form_close(); ?>
</div>
<?php require("common/footer.php"); ?>
