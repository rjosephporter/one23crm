
<div id="container_top">
<h4>Email Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Edit Signature</h5>
            <form action="<?php echo base_url(); ?>settings/email_signature_save" method="post">

            <label>Signature Name:<br />
            <input type="text" name="name" id="name" class="span4" placeholder="Enter signature name" value="<?php echo $signature['name']; ?>" />
            </label>
            <label>Signature Body:<br />
            <?php /*<textarea class="wysiwygeditor" name="body" id="body" rows="10" style="width:90%;" placeholder="Enter signature body"><?php echo $signature['body']; ?></textarea>*/ ?>
            <textarea name="body" id="body" placeholder="Enter signature body"><?php echo $signature['body']; ?></textarea>
            <script>
                $(document).ready(function(){
                    $('#body').summernote();
                });
            </script>
			</label>
            <input type="hidden" name="id" value="<?php echo $signature['id']; ?>" />
            <input type="submit" value="Save"  class="btn"/>
            </form>
            </div>
        </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">Email Settings</h5>
            <p></p>
        </div>

    </div>

<?php require("common/footer.php"); ?>
