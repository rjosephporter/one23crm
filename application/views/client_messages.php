<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name =  $customer_details['company_name']; } else { $customer_name =  $customer_details['first_name'] . ' ' . $customer_details['last_name']; }

if ($this->input->get('result', TRUE)=="added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="note_added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client note has successfully been added.</div>'; } ?>

<script>
$(document).ready(function(){
	$("#sms_message_no_credits").hide();
	var smsCredit = <?php echo $sms_credits; ?>
	
	$("#message").keyup(function() {
		var smsCount = $("#message").val().length;
		var smsNeeded = Math.ceil(smsCount/160);
		$("#sms_message_counter").html("Current character count <strong>"+smsCount+"</strong>, will use "+smsNeeded+" of your "+smsCredit+" credits.");
		// if not enough credits disable the form and alert the user
		if (smsNeeded>smsCredit) {
			$("#sms_message_no_credits").show();
			$("#modalSendSMS").attr("disabled","disabled");
		} else {
			$("#sms_message_no_credits").hide();
			$("#modalSendSMS").removeAttr("disabled");
		}
		
	
	});
});
</script>

<div id="container_top">

<?php require("common/client_tags.php"); ?>

    <?php //print_r($customer_mob); 
	
	if ($customer_mob) {
	
		$mob = $customer_mob['number'];
		
		if (substr($mob, 0, 2)=="07") {
		
			$mob_number = '44' . substr($mob, 1);
		
		} else {
	
		$mob_number = "";
	
		}
	
	} else {
	
		$mob_number = "";
	
	}?>    
    <div id="sendSMS" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/send_sms" method="post" name="sendSMS" id="sendSMS">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create New SMS</h4>
    </div>
    <div class="modal-body">
    Mobile Number:<br />
 	<input type="text" id="number" name="number" value="<?php echo $mob_number; ?>" />
    <br />
    Message:<br />
    <textarea name="message" rows="6" id="message" class="span5"></textarea>
    <input type="hidden" id="client" name="client" value="<?php echo $this->input->get('id', TRUE); ?>" />    
    <p id="sms_message_counter"></p>
    <p id="sms_message_no_credits" style="color:#FF0000;">You do not have enough credits to send this message, please reduce the length of the message, or purchase additional credits.</p>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button id="modalSendSMS" class="btn">Send SMS</button>
    </form>
    </div>
    </div>  
    
    
    
<ul class="nav nav-tabs somespacefornav">
	<li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li class="active"><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>         

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li class="active"><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
      
      <div class="row-fluid">
      	<div class="span12">

		<?php if ($sms_credits==0) {
			echo '<div class="alert alert-error">You do not have any sms credits, please <a href="'. base_url() .'settings/">click here</a> to purchase additional credits.</div>';
			
			} elseif ($sms_credits<=10) {
			
				echo '<div class="alert alert-warning">You have <strong>'. $sms_credits .'</strong> sms credits remaining, please <a href="'. base_url() .'settings/account">click here</a> to purchase additional credits.</div>';
				
				echo '<p><a href="#sendSMS" class="btn" data-toggle="modal"><img class="btn_with_icon" src="'. base_url() .'img/add-icon.png" />&nbsp;&nbsp;Send Text Message</a></p>';
			
			} else {
			
				echo '<p><a href="#sendSMS" class="btn" data-toggle="modal"><img class="btn_with_icon" src="'. base_url() .'img/add-icon.png" />&nbsp;&nbsp;Send Text Message</a></p>';
			
			} ?>        
        </div>
      </div>      
      
        <div class="row-fluid">
          <div class="span12 well scrollable" style="max-height:600px;"><h5 style="margin-top:0px;">Message Centre</h5>
			<?php if ($customer_messages) {
            
				echo '<table width="100%">';
			
            	foreach($customer_messages as $message) {
				
				if ($message['direction']==1) {
					$message_type = "sent";
				} else {
					$message_type = "received";
				}
				
				if ($message['method']==2) {
					$message_method = "text-icon.png";
				} else {
					$message_method = "email-icon.png";
				}
				
				if ( ($message['read_status']==0) && ($message['direction']==2) ) {				
					$the_subject = '<strong>' . $message['subject'] . '</strong>';
				} else {
					$the_subject = $message['subject'];				
				}
				
					echo '<tr>
					<td width="2%"><img src="'. base_url() .'img/'.$message_method.'" /></td>
					<td width="20%">'. $message['sender'] .'</td>
					<td width="20%">'. $message['to'] .'</td>
					<td width="35%" align="left"><a href="'. base_url() .'messages/view?id='. $message['id'] .'&messagecentre='.$message['customer_id'].'">'. $the_subject .'</a></td>
					<td width="22%" align="right" style="text-align="right">' . $message_type . ' ' . date("d/m/y H:i", strtotime($message['added_date'])) .' | <a href="'. base_url() .'messages/view?id='. $message['id'] .'&messageclient='.$message['customer_id'].'">view</a> | <a href="'. base_url() .'messages/delete?id='. $message['customer_id'] .'&message='. $message['id'] .'" onClick="return confirm(\'Are you sure you wish to delete this message?\')">delete</a></td>
				  </tr>';
				
				}
				
				echo '</table>';     
            
            } else { echo 'The message centre is currently empty.'; } ?>         
          </div>
        </div>
        <?php /*
        <div class="row-fluid">
        	<div class="span12 well">            
            <h5 style="margin-top:0px;">Create New Email</h5>

				<form action="<?php echo base_url(); ?>clients/send_message" method="post" name="send_message" target="_self" id="send_message">
                Subject:<br />
                <select name="subject" id="subject" class="span4">
                  <option value="0">Please Select</option>
                  <option value="Quote Request">Quote Request (<?php echo strtoupper($customer_name); ?>)</option>
                  <option value="Quote Amendment">Quote Amendment</option>
                  <option value="Addition to Policy">Addition to Policy</option>
                  <option value="Amendment to Policy">Amendment to Policy</option>
                  <option value="Quote request for new member">Quote request for new member</option>
                
                </select>
                <p>
                Message:<br />
                <textarea name="body" cols="" rows="7" id="body" class="span4"></textarea>
                <input type="hidden" name="client" id="client" value="<?php echo $this->input->get('id'); ?>" />
                <input type="hidden" name="client_name" id="client_name" value="<?php echo strtoupper($customer_name); ?>" />
                </p>                
                <button type="submit" class="btn"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Send Email</button>
              </form>

          	</div>
          </div>
		  */ ?>
      	</div>
      </div>
	  
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
