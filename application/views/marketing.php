<script>
$(document).ready(function(){
	$("#tag").bind("change",function () {
		$("#filter").submit();
	});	
	
	$("#client-list").on("click", "#select_all", function() {
		$(".sms").prop('checked', this.checked);
	});
		
});
</script>

<div id="container_top">
<h4>SMS Marketing</h4>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <form class="form-inline" id="filter" action="<?php echo base_url(); ?>marketing" method="get">
              <label style="font-size:12px; margin-left:30px;">Filter by tag<br />
              <select name="tag" id="tag" style="height:23px; padding:0px;width:110px;">
              <option value="">No tag filter</option>
            <?php if ($tag_list) {
                foreach($tag_list as $tag) {
					if ($tag['id']==$this->input->get('tag')) {
						$selected = 'selected="selected"';
					} else {
						$selected = "";
					}
                    echo '<option value="'. $tag['id'] .'"'. $selected .'>' . $tag['tag'] .'</option>';	
                }
            } ?>  
              </select></label>
            </form>    
            
            <?php echo $this->session->flashdata('error'); ?>
            
            <form action="<?php echo base_url(); ?>marketing/verify" method="post">
            <table class="table" id="client-list">
            <thead>
            <tr>
            <th width="20"><input type="checkbox" value="1" name="select_all" id="select_all" /></th>
            <th>Person's Name and Mobile Number</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($client_list) {
			
				foreach($client_list as $client) {
				
					echo '<tr><td><input type="checkbox" value="'. $client['number'] .'|'. $client['first_name'] .'" name="sms[]" /></td>
					<td>'. $client['title'] .' '. $client['first_name'] .' '. $client['last_name'] .' - '. $client['number'] .'</td></tr>';
				
				}
			
			} else {
			
			echo '<tr><td colspan="2">No clients to display</td></tr>';
			
			} ?>
            </tbody>
            </table>
            <button type="submit" class="btn">Next Step</button>
            </form>

        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">SMS Marketing</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis orci nec volutpat eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec hendrerit ultricies nulla at consequat.</p>
            <p>Nunc sollicitudin eros malesuada ante egestas, dictum egestas felis venenatis. Praesent id justo dui.</p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
