<?php $attachmentcount = 0; ?>
<div id="container_top">
<h4>Email Settings</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/email">Email Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Edit Template</h5>
            <form action="<?php echo base_url(); ?>settings/email_template_save" method="post">

            <label>Template Name:<br />
            <input type="text" name="name" id="name" class="span4" placeholder="Enter template name" value="<?php echo $template['name']; ?>" />
            </label>
            <label>Template Subject:<br />
            <input type="text" name="subject" id="subject" class="span4" placeholder="Enter template subject" value="<?php echo $template['subject']; ?>" />
            </label>
            <label>Template Body:<br />
            <?php /*<textarea name="body" id="body" rows="10" style="width:90%;" placeholder="Enter template body" class="wysiwygeditor"><?php echo $template['body']; ?></textarea> */?>
            <textarea name="body" id="body" placeholder="Enter template body"><?php echo $template['body']; ?></textarea>
            <script>
            	$(document).ready(function(){
            		$('#body').summernote();
            	});
            </script>
			</label>

            <div id="attachment">
            <?php

			if ($template['attachment_1']) {
			$attachmentcount++;
			echo '<label>Attachment '. $attachmentcount .':<br />
			<select id="attachment_1" name="attachment[]" style="padding:0; height:23px;">
			<option value="0">No attachment required</option>
			' . $attachments .'></select></label>';
			}

			if ($template['attachment_2']) {
			$attachmentcount++;
			echo '<label>Attachment '. $attachmentcount .':<br />
			<select id="attachment_2" name="attachment[]" style="padding:0; height:23px;">
			<option value="0">No attachment required</option>
			' . $attachments .'></select></label>';
			}

			if ($template['attachment_3']) {
			$attachmentcount++;
			echo '<label>Attachment '. $attachmentcount .':<br />
			<select id="attachment_3" name="attachment[]" style="padding:0; height:23px;">
			<option value="0">No attachment required</option>
			' . $attachments .'></select></label>';
			}

			if ($template['attachment_4']) {
			$attachmentcount++;
			echo '<label>Attachment '. $attachmentcount .':<br />
			<select id="attachment_4" name="attachment[]" style="padding:0; height:23px;">
			<option value="0">No attachment required</option>
			' . $attachments .'></select></label>';
			}

			if ($template['attachment_5']) {
			$attachmentcount++;
			echo '<label>Attachment '. $attachmentcount .':<br />
			<select id="attachment_5" name="attachment[]" style="padding:0; height:23px;">
			<option value="0">No attachment required</option>
			' . $attachments .'></select></label>';
			}
			?>
            </div>
            <div id="add_attachment" style="cursor:pointer;"><img src="<?php echo base_url(); ?>img/add-icon.png" /> Add attachment</div>
            <p>&nbsp;</p>
            <input type="hidden" name="id" value="<?php echo $template['id']; ?>" />
            <input type="submit" value="Save"  class="btn"/>
            </form>
            </div>
        </div>

        </div>
        <div class="span4 well helpbox">
        	<h5 style=" margin-top:0px;">Email Settings</h5>
            <p></p>
        </div>


    </div>

<script>
$(document).ready(function() {

	<?php if ($template['attachment_1']) { ?>
	$('#attachment_1 option[value="<?php echo $template['attachment_1']; ?>"]').attr('selected','selected');
	<?php } ?>

	<?php if ($template['attachment_2']) { ?>
	$('#attachment_2 option[value="<?php echo $template['attachment_2']; ?>"]').attr('selected','selected');
	<?php } ?>

	<?php if ($template['attachment_3']) { ?>
	$('#attachment_3 option[value="<?php echo $template['attachment_3']; ?>"]').attr('selected','selected');
	<?php } ?>

	<?php if ($template['attachment_4']) { ?>
	$('#attachment_4 option[value="<?php echo $template['attachment_4']; ?>"]').attr('selected','selected');
	<?php } ?>

	<?php if ($template['attachment_5']) { ?>
	$('#attachment_5 option[value="<?php echo $template['attachment_5']; ?>"]').attr('selected','selected');
	<?php } ?>

	var attachmentCount = <?php echo ($attachmentcount+1); ?>;
	var attachmentCountLimit = 5;
	$("#add_attachment").click(function() {
		if (attachmentCount<=attachmentCountLimit) {
			var newAttachment = '<label>Attachment '+attachmentCount+':<br /><select name="attachment[]" style="padding:0; height:23px;"><option value="0">No attachment required</option><?php echo $attachments; ?></select></label>';
			$("#attachment").append(newAttachment);
			attachmentCount++;
		} else {
			alert("You have reached the max of 5 attachments.");
		}
	});

});
</script>

<?php require("common/footer.php"); ?>
