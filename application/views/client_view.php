<?php 

if (!$customer_details) {
redirect('clients');
}

if ($this->input->get('result', TRUE)=="added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="note_added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client note has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="email_sent") { echo '<div class="alert alert-success" style="margin-top:10px;">Email has been successfully sent.</div>'; } 

?>

<script>
$(function() {

	$.ajax({
		type: "GET",
		url: "<?php echo base_url(); ?>clients/get_tweets",
		cache: false,
		data: 'twitter='+'<?php echo $twitter; ?>',
		dataType: "html",
		beforeSend:function() {
		  $('#tweets').html('Loading...');
		},		
		success: function(data){
			if (data!="") {
				$("#tweets").html(data);
			} else {
				$("#tweets").html('No tweets found...');
			}	
		},
		error: function() {
			$("#tweets").html('Error collection latest tweets...');
		}
	});
	
});
</script>

<div id="container_top">

<?php require("common/client_tags.php"); ?>

<ul class="nav nav-tabs somespacefornav">
	<li class="active"><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
	
    <?php // show any custom tabs
	if ($custom_tabs) {
		foreach($custom_tabs as $tab) {
		
			echo '<li><a href="'. base_url() .'clients/custom?id='. $this->input->get('id') .'&custom='. $tab['tab_ref'] .'">'. $tab['name'] .'</a></li>';
		
		}
	}
	
	if ($this->session->userdata("tab_settings")==1) {
            
        if ($this->session->userdata("files_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <?php } ?>
        
        <?php if ($this->session->userdata("messages_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php } ?>
        
        <?php if (($this->session->userdata("people_tab")==1) && ($customer_details['account_type']==2)) { ?>
        	<li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>     

        <?php if ($this->session->userdata("opportunities_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <?php } ?>        
        
        <?php if ($this->session->userdata("live_tab")==1) { ?>
        	<li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
        <?php } ?>           
   
    <?php } else { ?>
        <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
        <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages (<?php echo $tab_messages; ?>)</a></li>
        <?php if ($customer_details['account_type']==2) { ?>
        <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities (<?php echo $tab_opportunities; ?>)</a></li>
        <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
    <?php } ?>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">

<?php require("common/client_left_menu.php"); ?>

      <div class="span9">
        <div class="row-fluid">
          <div class="span6 well scrollable" style="min-height:235px;max-height:235px;"><h5 style="margin-top:0px;">Activity - <a target="_self" href="#myModal" data-toggle="modal"><img src="<?php echo base_url(); ?>img/small-add-icon<?php echo $icon_set; ?>.png" /> Add New Note</a></h5>
			<?php if ($customer_notes) { 
			
			foreach($customer_notes as $notes) {
			
				if (!empty($notes['file'])) { $note_file = ' <a href="'. base_url() .'documents/'. $notes['file'] .'" target="_blank">view attached file</a>';
				} else { $note_file = ""; }
                
				if ( ($this->session->userdata("role")==1) || ($this->session->userdata("client_notes")==1) ) {
                echo '<img src="'.base_url().'/img/notes-icon'.$icon_set.'.png" />&nbsp;<span style="font-size:11px">' . date("d/m/y H:m", strtotime($notes['added_date'])) . ' by '.$notes['added_by'].'</span>&nbsp;<a href="'. base_url() .'clients/delete_note?id='. $notes['customer_id'] .'&note='. $notes['id'] .'" target="_self" onClick="return confirm(\'Are you sure you wish to delete this note?\')"><img src="'.base_url().'/img/delete-icon'.$icon_set.'.png" /></a><p>' . nl2br($notes['note']) . $note_file . '</p>';
				} else {
				echo '<img src="'.base_url().'/img/notes-icon'.$icon_set.'.png" />&nbsp;<span style="font-size:11px">' . date("d/m/y H:m", strtotime($notes['added_date'])) . ' by '.$notes['added_by'].'</span><p>' . nl2br($notes['note']) . $note_file . '</p>';				
				}
                
                } 
				
			} else { echo "No notes for this client found."; } ?>         
          </div>
          <div class="span6 well" style="min-height:235px;"><h5 style="margin-top:0px;">Client Emails In/Out - <a href="<?php echo base_url(); ?>clients/messages?id=<?php echo $this->input->get('id'); ?>"><img src="<?php echo base_url(); ?>img/small-view-icon<?php echo $icon_set; ?>.png" />&nbsp;view all</a></h5>
          <?php if ($recent_messages) {
		  
		  echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		  
		  	foreach ($recent_messages as $messages) {
			
				if ($messages['method']==2) {
					$message_method = "text-icon".$icon_set.".png";
				} else {
					$message_method = "email-icon".$icon_set.".png";
				}
				
				if ($messages['direction']==1) {
					$whichway = "sent";
				} else {
					$whichway = "received";
				}
			
				echo '<tr>
						<td width="7%"><img src="'. base_url() .'img/'. $message_method .'" /></td>
						<td width="70%"><a href="'. base_url() .'messages/view?id='. $messages['id'] .'&summary='.$this->input->get('id').'">'. $messages['subject'].'</a></td>
						<td align="right">'. $whichway . ' ' . date("d/m/y H:i", strtotime($messages['added_date'])) .'</td>
					  </tr>';
			
			}
		  
		  echo '</table>';
		  
		  
		  } else {
		  
		  	echo "No recent messages found for this client.";
		  
		  } ?>
          </div>
        </div>      
        
        <?php if ($twitter) { ?>
        <div class="row-fluid">
          <div class="span12 well" style="min-height:200px;">
          	<div style="max-height: 270px; overflow: scroll">
	          	<h5 style="margin-top:0px;">Latest Tweets</h5>
	          		<div id="tweets"></div> 
	        </div>
      	  </div>
        </div>
        <?php } else { ?>
        <div class="row-fluid">
          <div class="span12 well" style="min-height:200px;">
          <h5 style="margin-top:0px;">Latest Tweets</h5>
          You need to add Twitter account details before you are able to view any feeds. <a href="<?php echo base_url(); ?>clients/<?php if ($customer_details['account_type']==2) { echo 'edit_company'; } else { echo 'edit_client'; } ?>?id=<?php echo $this->input->get('id'); ?>">Click here</a> to add Twitter account information.
          </div>
        </div>
        <?php } ?>        
        
        <div class="row-fluid">
          <div class="span6 well" style="min-height:200px;"><h5 style="margin-top:0px;">Live Documents  - <a href="<?php echo base_url() .'clients/view_my_documents?id='. $this->input->get('id'); ?>"><img src="<?php echo base_url(); ?>img/small-view-icon<?php echo $icon_set; ?>.png" />&nbsp;view all</a></h5>
			<?php if ($shared) {
			
				echo '<table class="table" id="file-list">';
			  
				foreach($shared as $shared_file) {
				
					echo '<tr><td><img src="'. base_url() .'img/doc-icon.png" /></td><td width="70%"><a href="'. $shared_file['url'] .'" target="_blank">'. $shared_file['name'] .'</a></td><td><div class="text-right">'.  date("d/m/y H:i", strtotime($shared_file['time'])) .'</div></td></tr>';
				
				}
				
				echo '</table>';			
			
			
			} else {
			            
            	echo 'No files shared.';
				
			} ?>            
          </div>
          <div class="span6 well" style="min-height:200px;"><h5 style="margin-top:0px;">Recent Files - <a href="<?php echo base_url(); ?>clients/files?id=<?php echo $this->input->get('id'); ?>"><img src="<?php echo base_url(); ?>img/small-view-icon<?php echo $icon_set; ?>.png" />&nbsp;view all</a></h5>
          <?php if ($recent_files) {
		  
		  	echo '<table class="table" id="file-list">';
		  
			foreach($recent_files as $files) {
			
				echo '<tr><td><img src="'. base_url() .'img/doc-icon.png" /></td><td width="70%"><a href="'. base_url() .'documents/'. $files['filename'] .'" target="_blank">'. $files['name'] .'</a></td><td><div class="text-right">'.  date("d/m/y H:i", strtotime($files['added_date'])) .'</div></td></tr>';
			
			}
			
			echo '</table>';
		  
		  } else {
		  
		  	echo "No files found.";
		  
		  } ?>          
          </div>
        </div>       
      
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
