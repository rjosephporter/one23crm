
<div id="container_top">
<h4>SMS Marketing</h4>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

			 <?php echo $this->session->flashdata('error'); ?>
            
            <div class="row-fluid">
                <div class="span12 well">
                	<h5 style="margin-top:0px;">Message Recipient(s)</h5>
                    <?php if (count($recipients)>1) {
					
						$recipient_count = 1;
					
						foreach($recipients as $people) {
						
							$explode_sms = explode("|", $people);
							echo $recipient_count .') '. $explode_sms[0] . '<br />';
							
							$recipient_count++;
						
						}
					
					} else {
					
							$explode_sms = explode("|", $recipients[0]);
							echo '1) '. $explode_sms[0] . '<br />';
					
						//echo '1) ' . $recipients['number'];
					
					}
					
					echo '<hr /><p>Total Recipients: '. count($recipients) .'<br />
					Required Credits: '. $this->session->userdata('required_credits') .'</p>';
					
					?>
                </div>       
            </div>
            
            <div class="row-fluid">
                <div class="span12 well">
                	<h5 style="margin-top:0px;">Message Body</h5>
                    <pre><?php if ($this->session->userdata('personalised')==1) { echo "Hi <em>First Name</em>. " ; } 
					echo $this->session->userdata('message'); ?></pre>

                    <p><br />
                    <form action="<?php echo base_url(); ?>marketing/send" method="post">
                    <button class="btn">Send Messages</button>
                    </form>
                    </p>

                </div>       
            </div>            
        
        </div>
        <div class="span4 well helpbox">
        	<h5 style="margin-top:0px;">SMS Marketing</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mattis orci nec volutpat eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec hendrerit ultricies nulla at consequat.</p>
            <p>Nunc sollicitudin eros malesuada ante egestas, dictum egestas felis venenatis. Praesent id justo dui.</p>
        </div>
    </div>

<?php require("common/footer.php"); ?>
