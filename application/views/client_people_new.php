<?php //require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name =  $customer_details['company_name']; } else { $customer_name =  $customer_details['first_name'] . ' ' . $customer_details['last_name']; } ?>

<div id="container_top">
<h4><?php echo $customer_name; ?></h4>
    
<ul class="nav nav-tabs">
    <li class="active"><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages</a></li>
    <?php if ($customer_details['account_type']==2) { ?>
    <li><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
    <?php } ?>
    <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>    
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  		<form action="<?php echo base_url(); ?>clients/create_new_person" method="post">
        <div class="row-fluid">
            <div class="span12 well"><h5 style="margin-top:0px;">Personal Details</h5>
 				<div class="row-fluid">
                                
                <table width="50%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td><label>First Name:</label><input type="text" name="first_name" placeholder="Enter first name" /></td>
                    <td><label>Last Name:</label><input type="text" name="last_name" placeholder="Enter surname" /></td>
                    <td><label>Job Title:</label><input type="text" name="job" placeholder="Enter job title" /></td>
                  </tr>
                </table>

            
            </div>
         </div>
          
        <div class="row-fluid">
        	<div class="span12 well"><h5 style="margin-top:0px;">Contact Details</h5>
                <div class="row-fluid">
              <h6>Telephone Numbers</h6>
              <input name="telephone[1]" type="text" id="telephone[1]" placeholder="enter telephone number" />&nbsp;<select name="telephone_type[1]" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select>
              <div id="telephoneNumbers">
              </div>
              <p><a href="#" id="addTelephone"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add telephone number</a></p>
              <h6>Email Addresses</h6>
              <input name="email[1]" type="text" id="email[1]" placeholder="enter email address" />&nbsp;<select name="email_type[1]" style="padding:0px; height:23px; width:100px;">
              <option value="Home">Home</option>
              <option value="Work">Work</option></select><br />
              <div id="emailAddresses">
              </div>  
              <p><a href="#" id="addEmailAddress"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add email address</a></p>  
              <h6>Websites</h6>
              <div id="webAddressess">  
              </div> 
              <p><a href="#" id="addURL"><img src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;add web address</a></p>   
              <h6>Addresses</h6>
              <textarea name="address" cols="" rows="2" style="width:300px; padding:2px;"></textarea>&nbsp;<select name="address_type" style="padding:0px; height:23px; width:100px; margin-bottom:30px;"><option value="Home">Home</option><option value="Work">Work</option></select><br /> 
              <input name="town" type="text" style="width:160px;" placeholder="town" />&nbsp;<input name="county" type="text" style="width:120px;" placeholder="county" />&nbsp;<input name="postcode" type="text" style="width:100px;" placeholder="postcode" />
<script>
$(document).ready(function(){
	var telephoneCounter = 2;
	var emailCounter = 2;
	var webCounter = 1;
		
	$("#addTelephone").click(function(){
	var newTelephone = "";	
	
	newTelephone = '<input name="telephone['+telephoneCounter+']" type="text" id="telephone['+telephoneCounter+']" placeholder="enter telephone number" />&nbsp;<select name="telephone_type['+telephoneCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option><option value="Direct">Direct</option><option value="Mobile">Mobile</option><option value="Fax">Fax</option></select><br />';
	
	$("#telephoneNumbers").append(newTelephone);
	++telephoneCounter;
	});

	$("#addEmailAddress").click(function(){
	var newEmail = "";	
	newEmail = '<input name="email['+emailCounter+']" type="text" id="email['+emailCounter+']" placeholder="enter email address" />&nbsp;<select name="email_type['+emailCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Home">Home</option><option value="Work">Work</option></select><br />';
	
	$("#emailAddresses").append(newEmail);
	++emailCounter;
	});

	$("#addURL").click(function(){
	var newWeb = "";	
	newWeb = '<input name="url['+webCounter+']" type="text" id="url['+webCounter+']" placeholder="enter web address"  />&nbsp;<select name="url_site['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Website">Website</option><option value="Twitter">Twitter</option><option value="Skype">Skype</option><option value="Xing">Xing</option><option value="Google+">Google+</option><option value="Facebook">Facebook</option><option value="YouTube">YouTube</option><option value="GitHub">GitHub</option><option value="LinkedIn">LinkedIn</option><option value="Blog">Blog</option></select>&nbsp;<select name="url_type['+webCounter+']" style="padding:0px; height:23px; width:100px;"><option value="Personal">Personal</option><option value="Work">Work</option></select><br />';
	
	$("#webAddressess").append(newWeb);
	++emailCounter;
	});


});
</script>                
                </div>
          </div>
          <input type="hidden" name="company_id" value="<?php echo $this->input->get('id'); ?>" />
          <input type="hidden" name="company_name" value="<?php echo $customer_details['company_name']; ?>" />
          <button class="btn" type="submit">Add New Person</button>&nbsp;&nbsp;<a href="" class="btn">Cancel</a>          
		</form>         
      	</div>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
