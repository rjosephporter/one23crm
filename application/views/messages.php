<?php $tab = "messages";
//require("common/header.php"); ?>
<div id="container_top">
<h4>Messages</h4>
</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
	<div class="span12 well">
    <?php if ($messages) {
	
		echo '<table width="100%" class="table">
				  <thead>
					<tr>
					  <th>From</th>
					  <th>To</th>
					  <th>Client</th>
					  <th>Subject</th>
					  <th>Date</th>
					</tr>
				  </thead><tbody>';
	
			foreach($messages as $message) {
			
				if ( ($message['read_status']=='0') && ($message['direction']=='2') ) {
					$read = '<span class="label label-info">Unread</span> ';				
				} else {
					$read = "";
				}
			
				echo '<tr>
					<td>'. $read. $message['sender'] .'</td>
					<td>'. $message['to'] .'</td>
					<td><a href="'. base_url() .'clients/view?id='. $message['clientid'] .'">'. $message['client'] .'</a></td>
					<td><a href="'. base_url() .'messages/view?id='. $message['id'] .'&return=message">'. $message['subject'] .'</a></td>
					<td>'. date("l j M Y H:i", strtotime($message['added_date'])) .'</td>
					</tr>';
			
			}
	
		echo '</tbody></table>';
	
	} else {
	
		echo "No messages found.";
	
	} ?>    
    </div>
</div>
<?php require("common/footer.php"); ?>
