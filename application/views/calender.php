<?php //$tab = "calender"; require("common/header.php"); ?>
<script>
$(document).on("click", ".calNewTask", function () {
     var taskDate = $(this).data('id');
     $(".modal-body #task_date").val(taskDate);
});

$(function(){
	$("a[data-toggle=modaledit]").click(function() {
		var task_id = $(this).attr('id');
		$.ajax({
			type: 'post',
			cache: false,
			url: '<?php echo base_url(); ?>clients/getTaskDetails',
			data: 'task='+task_id,
			dataType: "json",
			success: function(data) {
				$("#edit_task_name").val(data.name);
				$("#edit_task_id").val(data.id);
				$("#edit_task_action").val(data.action);
				$("#edit_task_date").val(data.date);
				$("#edit_task_hour").val(data.hour);
				$("#edit_task_min").val(data.mins);
				$("#edit_task_remind").val(data.remind);
				$('#editTask').modal('show');
			},
			error: function() {
				alert("Error, can not edit task.");
			}
		});
	});

	$("#completeTask").click(function() {
		var task_id = $("#edit_task_id").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>clients/cal_complete_task",
			data: { task: task_id },
			dataType: 'text',
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not complete this task");
			}
		});
	});

	$("#deleteTask").click(function() {
		var task_id = $("#edit_task_id").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>clients/delete_task",
			data: { task: task_id },
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not delete this task");
			}
		});
	});

	$(".task_draggable").draggable({
		containment: '.calender'
		//snap: '.day_content',
		//snapMode: 'inner'
	});
	$(".day_content").droppable({
		hoverClass: "cal_hover",
		drop: function(event, ui) {;
			var taskref = ui.draggable.data("taskref");
			var taskdate = $(this).data("taskdate");
			$.ajax({
				type: "GET",
				url: "<?php echo base_url(); ?>calender/cal_update_task",
				data: { task_ref: taskref, task_date: taskdate},
				success: function() {
					location.reload(true);
					//alert(taskref+'----'+taskdate);
				},
				error: function() {
				}
			});
		}
	});

});
</script>

    <div id="createNewTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/create_task" method="post" name="createNewTask" id="createNewTask">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Create new task</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="task_name" id="task_name" class="span4" /></td>
      </tr>
      <tr>
        <td width="20%" valign="middle">Link To:</td>
        <td valign="middle">
		  <div id="link_to_client">
			  <input class="typeahead getclient" name="getclient" type="text" autocomplete="off">
			  <input type="hidden" name="customer_id" id="customer_id" value="0">
   		  </div>
        </td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="task_date" id="task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="task_hour" id="task_hour" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="task_min" id="task_min" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>End Time:</td>
        <td>
          <select name="task_hour_end" id="task_hour_end" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="task_min_end" id="task_min_end" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30" selected>30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="task_action" id="task_action" style="height:22px; padding:0px;">
          <option value="0">Please Select</option>
          <?php if( $task_setting->num_rows > 0 ){ ?>
					<?php foreach($task_setting->result() as $row){ ?>
						<option value="<?php echo $row->id;?>"><?php echo $row->action_name;?></option>
					<?php } ?>
          <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td>Reminder:</td>
        <td>
        <select name="task_remind" id="task_remind" style="height:22px; padding:0px;">
          <option value="0">Do Not Remind</option>
          <option value="5">Remind me 5 minutes before</option>
          <option value="15">Remind me 15 minutes before</option>
          <option value="30">Remind me 30 minutes before</option>
          <option value="60">Remind me 60 minutes before</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <button id="modalCreateClientTask" class="btn">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <input type="hidden" name="return" value="calender" />
    </form>
    </div>
    </div>

    <div id="editTask" class="modal hide fade">
    <form action="<?php echo base_url(); ?>clients/update_task" method="post" name="" id="">
    <input type="hidden" id="edit_task_id" name="edit_task_id" />
    <input type="hidden" name="return" value="calender" />
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4>Edit task</h4>
    </div>
    <div class="modal-body">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="20%" valign="middle">Description:</td>
        <td valign="middle"><input type="text" name="edit_task_name" id="edit_task_name" class="span4" /></td>
      </tr>
      <tr>
        <td width="20%" valign="middle">Link To:</td>
        <td valign="middle">
		  <div id="link_to_client">
			  <input class="typeahead editgetclient"name="getclient" type="text" autocomplete="off">
			  <input type="hidden" name="edit_client_task" id="edit_client_task">
			  <input type="hidden" name="edit_customer_id" id="edit_customer_id" value="0">
   		  </div>
        </td>
      </tr>
      <tr>
        <td>Date &amp; Time:</td>
        <td><input type="text" name="edit_task_date" id="edit_task_date" class="span2" value="<?php echo date("d/m/Y"); ?>" />
          <select name="edit_task_hour" id="edit_task_hour" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="edit_task_min" id="edit_task_min" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>
        </td>
      </tr>
      <tr>
        <td>End Time:</td>
        <td>
          <select name="edit_task_hour_end" id="edit_task_hour_end" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
        </select>

        <select name="edit_task_min_end" id="edit_task_min_end" class="span1" style="height:22px; padding:0px;">
          <option value="00">00</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
        </select>

        </td>
      </tr>
      <tr>
        <td>Action:</td>
        <td>
        <select name="edit_task_action" id="edit_task_action" style="height:22px; padding:0px;">
          <option value="0">Please Select</option>
          <option value="Milestone">Milestone</option>
          <option value="Send">Send</option>
          <?php if( $task_setting->num_rows > 0 ){ ?>
					<?php foreach($task_setting->result() as $row){ ?>
						<option value="<?php echo $row->id;?>"><?php echo $row->action_name;?></option>
					<?php } ?>
          <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td>Reminder:</td>
        <td>
        <select name="edit_task_remind" id="edit_task_remind" style="height:22px; padding:0px;">
          <option value="0">Do Not Remind</option>
          <option value="5">Remind me 5 minutes before</option>
          <option value="15">Remind me 15 minutes before</option>
          <option value="30">Remind me 30 minutes before</option>
          <option value="60">Remind me 60 minutes before</option>
        </select>
        </td>
      </tr>
    </table>
    </div>
    <div class="modal-footer">
    <div class="pull-left">
        <button class="btn">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	</div>
    <div class="pull-right">
        <button class="btn" id="completeTask">Complete Task</button>
        <button class="btn" id="deleteTask">Delete</button>
	</div>
    <input type="hidden" name="return" value="calender" />
    </form>
    </div>
    </div>


<div id="container_top">
<h4>Calendar and Tasks <a href="#createNewTask" class="btn pull-right" data-toggle="modal" style="font-weight:normal;"><img class="btn_with_icon" src="<?php echo base_url(); ?>img/add-icon.png" />&nbsp;&nbsp;Add Task</a></h4>

<ul class="nav nav-tabs">
    <li class="active"><a href="<?php echo base_url(); ?>calender">Calendar View</a></li>
    <li><a href="<?php echo base_url(); ?>calender/tasks">List View</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

<div row-fluid>
  <div class="span12">
    <form class="form-inline" method="get" id="action_sort">
      Action required:
      <select id="action" name="action" style="padding:0; height:23px;">
      <option value="">All</option>
      <?php foreach($sort_action as $action) {
        if ($selected_action==$action['action']) {
          $selected = 'selected="selected"';
        } else { $selected = ''; }
        echo '<option value="'.$action['action'].'" '.$selected.'>'.$action['action'].'</option>';
      } ?>
      </select>

      <?php if ($this->session->userdata("role")==1) { ?>
      &nbsp;Show tasks for:
      <select id="user" name="user" style="padding:0; height:23px;">
      <option value="all" selected="selected">All Users</option>
      <option value="<?php echo $this->session->userdata('user_id') ?>">Myself Only</option>
      <?php if ($group) {
        foreach($group as $user) {
          echo '<option value="'. $user['user_id'] .'"';

          if ($user['user_id']==$this->input->get("user")) { echo ' selected="selected"'; }

          echo '>'. $user['first_name'] .' '. $user['last_name'] .' ('. $user['username'] .')</option>';
        }
      } ?>
      </select>
      <?php } ?>
      <button type="submit" class="btn btn-mini">Apply</button>
    </form>
  </div>
</div>

<div id="fullcalendar"></div>
<?php //echo $calender; ?>
<script>
var calender_obj;
$(document).ready(function() {

    calender_obj = $('#fullcalendar').fullCalendar({
		height: 600,
		timeFormat: 'H:mm',
		editable:true,
		selectable: true,
		theme:true,
		header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay',
		},
		//events:"<?php echo base_url(); ?>calender/get_current_date",

    events: {
        url: '<?php echo base_url(); ?>calender/get_current_date',
        type: 'GET',
        data: {
            action: '<?php echo $this->input->get("action") ?>',
            user: '<?php echo $this->input->get("user") ?>'
        },
        error: function() {
            alert('there was an error while fetching events!');
        },
    },

	eventClick: function(calEvent, jsEvent, view) {
			$.ajax({
				type: 'post',
				cache: false,
				url: '<?php echo base_url(); ?>clients/getTaskDetails',
				data: 'task='+calEvent.id,
				dataType: "json",
				success: function(data) {
					$("#edit_task_name").val(data.name);
					$("#edit_task_id").val(data.id);
					$("#edit_task_action").val(data.action);
					$("#edit_task_date").val(data.date);
					$("#edit_task_hour").val(data.hour);
					$("#edit_task_min").val(data.mins);
					$("#edit_task_hour_end").val(data.end_hr);
					$("#edit_task_min_end").val(data.end_min);
					$("#edit_task_remind").val(data.remind);
					$("#edit_client_task").val(data.customer_id);
					$(".editgetclient").val(data.customer_name);
					//console.log(data);
					//console.log(data.task_setting);
					$('#editTask').modal('show');
				},
				error: function() {
					//alert("Error, can not edit task.");
				}
			});

		},
		eventDrop: function(event, delta, revertFunc) {
			if (!confirm("Are you sure about this change?")) {
				revertFunc();
			}else{
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>calender/allan_cal_update_task",
					data: { task_id: event.id, new_task_date: event.start.format('YYYY-MM-DD H:mm:ss')},
					success: function() {
						//location.reload(true);
						//alert(taskref+'----'+taskdate);
					},
					error: function() {
					}
				});
			}
		},
		eventResize: function(event, delta, revertFunc) {
			if (!confirm("Are you sure about this change?")) {
				revertFunc();
			}else{
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>calender/allan_cal_update_task",
					data: { task_id: event.id, new_task_date: event.start.format('YYYY-MM-DD H:mm:ss'), end_time : event.end.format('YYYY-MM-DD H:mm:ss')},
					success: function() {
						//location.reload(true);
						//alert(taskref+'----'+taskdate);
					},
					error: function() {
					}
				});
				//alert(event.end.format('YYYY-MM-DD H:mm:ss'));
			}
		},
		select: function(start, end, jsEvent, view) {
          if(view.name !== 'month') {
            endtime = end.format('YYYY-MM-DD H:mm:ss');
            starttime = start.format('YYYY-MM-DD H:mm:ss');

            formstartdate = start.format('DD/MM/YYYY');
            formstarthour = start.format('hh');
            formstartmin = start.format('mm');

            formenddate = end.format('DD/MM/YYYY');
            formendhour = end.format('hh');
            formendmin = end.format('mm');
            var mywhen = starttime + ' - ' + endtime;
            var myend = formendhour + ' - ' + formendmin;

            $("#task_date").val(formstartdate);
            $("#task_hour").val(formstarthour);
            $("#task_min").val(formstartmin);
            $("#task_hour_end").val(formendhour);
  		      $('#createNewTask').modal('show');
          }
    },

    dayClick: function(date, jsEvent, view) {
        if (view.name === "month") {
            $('#fullcalendar').fullCalendar('gotoDate', date);
            $('#fullcalendar').fullCalendar('changeView', 'agendaDay');
        }
    },

    eventRender: function(event, element, calEvent) {
		element.find(".fc-event-time").before($('<span class="fc-event-icons" style="margin-right:5px;"><i class="fa '+ event.icon + ' fa-2x"></i></span>'));
		if( event.customer_name != null ){
			element.find(".fc-event-title").after($('<span class="fc-event-client"> - <a style="color:#FFFFFF;text-decoration: underline;" href="' + baseURL + 'clients/view?id=' + event.customer_id + '">' + event.customer_name + '</a></span>'));
		}
    }

        // put your options and callbacks here
    })
	$('#editTask .modal').on('hidden.bs.modal', function(){
		$(this).find('form')[0].reset();
	});
	$('#createNewTask .modal').on('hidden.bs.modal', function(){
		$(this).find('form')[0].reset();
	});

  $('#action').val('<?php echo $this->input->get("action")?>');
  $('#user').val('<?php echo $this->input->get("user")?>');

});
</script>
<p></p>

<?php require("common/footer.php"); ?>
