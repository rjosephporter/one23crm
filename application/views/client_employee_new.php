<?php $tab = "clients";
//require("common/header.php");

if (!$customer_details) {
redirect('clients');
}

if (!empty($customer_details['company_name'])) { $customer_name =  $customer_details['company_name']; } else { $customer_name =  $customer_details['first_name'] . ' ' . $customer_details['last_name']; }

if ($this->input->get('result', TRUE)=="added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client has successfully been added.</div>'; }
if ($this->input->get('result', TRUE)=="note_added") { echo '<div class="alert alert-success" style="margin-top:10px;">New client note has successfully been added.</div>'; } ?>

<div id="container_top">
<h4><?php echo $customer_name; ?></h4>
    
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url() . 'clients/view?id=' . $this->input->get('id'); ?>">Summary</a></li>
    <li><a href="<?php echo base_url() . 'clients/files?id=' . $this->input->get('id'); ?>">Files</a></li>
    <li><a href="<?php echo base_url() . 'clients/messages?id=' . $this->input->get('id'); ?>">Messages</a></li>
    <li class="active"><a href="<?php echo base_url() . 'clients/people?id=' . $this->input->get('id'); ?>">People</a></li>
    <li><a href="<?php echo base_url() . 'clients/opportunities?id=' . $this->input->get('id'); ?>">Opportunities</a></li>    
    <li><a href="<?php echo base_url() . 'clients/view_my_documents?id=' . $this->input->get('id'); ?>">Live Documents</a></li>
</ul>

</div>

<br clear="all" />

<div class="container-fluid">

<div class="row-fluid">
  <div class="span12">
    <div class="row-fluid">
      <div class="span3">
        <div class="row-fluid">
          <div class="span12 well">
<?php require("common/client_left_menu.php"); ?>
      <div class="span9">
  		
        <div class="row well">
        <div class="span12">
        <?php if ($this->session->flashdata('errors')) {
		echo "<div class='alert alert-error'>" .  $this->session->flashdata('errors') . "</div>";		
		} ?>
        <form action="<?php echo base_url(); ?>clients/create_new_employee" method="post" enctype="multipart/form-data">
     	<div class="row-fluid">        
            <div class="span3" style="width:120px;">
            Title:<br />
            <select name="title" style="padding:0px; height:23px; width:120px;">
              <option value="Mr">Mr</option>
              <option value="Mrs">Mrs</option>
              <option value="Miss">Miss</option>
              <option value="Ms">Ms</option>
              <option value="Dr">Dr</option>
            </select>
            </div> 
            <div class="span3" style="width:210px;">
            First Name:<br />
            <input type="text" name="first_name" placeholder="Enter first name" value="<?php echo set_value('first_name'); ?>" />
            </div> 
            <div class="span3" style="width:210px;">
            Surname Name:<br />
            <input type="text" name="last_name" placeholder="Enter surname" value="<?php echo set_value('last_name'); ?>" />            
            </div> 
            <div class="span3" style="width:210px;">
            Date of Birth:<br />
            <input type="text" name="dob" id="dob" placeholder="Enter date of birth" value="<?php echo set_value('dob'); ?>" />
            </div>        
        </div>
        
     	<div class="row-fluid">        
            <div class="span3" style="width:210px;">
            Job Title / Position:<br />
            <input type="text" name="job" placeholder="Enter job title / position" value="<?php echo set_value('job'); ?>" />
            </div> 
            <div class="span3" style="width:210px;">
            Gender:<br />
            <select name="gender" style="padding:0px; height:23px; width:120px;">
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>            
            </div> 
            <div class="span3" style="width:210px;">
            Smoker?<br />
            <select name="smoker" style="padding:0px; height:23px; width:120px;">
              <option value="0">No</option>
              <option value="1">Yes</option>
            </select>            
            </div> 
            <div class="span3" style="width:210px;">
            </div>        
        </div>        
        
     	<div class="row-fluid">        
            <div class="span3" style="width:200px;">
            Street Address:<br />
			<input type="text" name="street" placeholder="Enter street address" value="<?php echo set_value('street'); ?>" />
            </div> 
            <div class="span3" style="width:200px;">
            Town/City:<br />
            <input type="text" name="town" placeholder="Enter town or city" value="<?php echo set_value('town'); ?>" />
            </div> 
            <div class="span3" style="width:200px;">
            County:<br />
            <input type="text" name="county" placeholder="Enter the county" value="<?php echo set_value('county'); ?>" />            
            </div> 
            <div class="span3">
            Postcode:<br />
            <input type="text" name="postcode" placeholder="Enter postcode" value="<?php echo set_value('postcode'); ?>" style="width:110px;" />
            </div>        
        </div>
        
        </div></div>
        <input type="hidden" name="client" value="<?php echo $this->input->get('id'); ?>" />      
        <button class="btn">Create New Employee</button>  
        </form>   
      	</div>
      </div>
      
    </div>
  </div>
</div>


<?php require("common/footer.php"); ?>
