<?php if ($message_detail) {


echo '<ul class="wide_list">
<li>Date: '.  date("d/m/y H:i", strtotime($message_detail['added_date'])) .'</li>
<li>Subject: '. $message_detail['subject'] .'</li>';

if ($message_attachments) {
	echo '<li>Attachments: ';
	foreach ($message_attachments as $attachment) {
		echo '<a href="'. base_url() .'documents/'. $attachment['file'] .'" target="_blank">' . $attachment['file'] . '</a><br />';
	}
	echo '</li>';
}

echo '<li>'. nl2br($message_detail['body']) .'</li>
</ul>';

} else {

redirect('messages');

}?>
