<div id="container_top">
<h4>Account Information</h4>
<ul class="nav nav-tabs">
    <li><a href="<?php echo base_url(); ?>settings">Overview</a></li>
    <li class="active"><a href="<?php echo base_url(); ?>settings/account">Account</a></li>
    <?php /*<li><a href="<?php echo base_url(); ?>settings/export">Export</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">Users</a></li>*/?>
    <li><a href="<?php echo base_url(); ?>settings/custom_fields">Custom Fields</a></li>
    <li><a href="<?php echo base_url(); ?>settings/tags">Tags</a></li>
    <li><a href="<?php echo base_url(); ?>settings/screen">Screen Settings</a></li>
    <li><a href="<?php echo base_url(); ?>settings/users">User Settings</a></li>
    <li><a href="<?php echo base_url(); ?>tasksetting">Calendar Task Settings</a></li>
</ul>
</div>

<br clear="all" />

<div class="container-fluid">

    <div class="row-fluid">
        <div class="span8">

            <div class="row-fluid">
            	<div class="span12 well">
                <?php echo $this->session->flashdata('errors'); ?>
                <h5 style="margin-top:0px;">Update Account Password</h5>
                <p>Please enter your new password below.</p>

                <form action="<?php echo base_url(); ?>settings/update_password" method="post">
                <label>Current Password:</label>
                <input type="password" name="password" />
                <label>New Password:</label>
                <input type="password" name="new_password" />
                <label>Confirm New Password:</label>
                <input type="password" name="conf_new_password" />

				<p style="margin-top:10px;">
                <input name="" type="submit" value="Save Password" class="btn" />
                </p>
				</form>
                </div>
            </div>

        </div>
        <div class="span4">
        </div>
    </div>

<?php require("common/footer.php"); ?>
