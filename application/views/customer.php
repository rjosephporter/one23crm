<?php require("common/header.php"); ?>
<div class="row-fluid">
  <div class="span9"><h2>Customers</h2></div>
  <div class="span3" style="padding-top:15px;"><button class="btn btn-primary" type="button">Create New Customer</button></div>
</div>
<?php if ($customer_list) {

	echo '<table class="table table-bordered">';
	
	echo '<thead>
                <tr>
                  <th width="10%"> Ref</th>
                  <th>Name</th>
				  <th>Email</th>
                  <th>Postcode</th>
                  <th width="15%">Active Policies</th>
                </tr>
              </thead>
			  <tbody>';

		foreach($customer_list as $customer) { 
		
		echo '<tr>
		<td><a href="'. base_url() .'customer?id='. $customer['id'] .'">' . $customer['id'] . '</a></td>
		<td>' . $customer['first_name'] . ' ' . $customer['last_name'] . '</td>
		<td>' . $customer['email'] . '</td>
		<td>' . $customer['postcode'] . '</td>
		<td><a href="'. base_url() .'policies?customer=0">0</a></td>
		</tr>';	
		
		}
		
	echo '</tbody></table>';	


} else {

	echo '<h4>Sorry no customers found.</h4>';

}

require("common/footer.php"); ?>
