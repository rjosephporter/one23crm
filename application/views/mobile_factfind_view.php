<?php if ($factfind) { ?>
<div class="title_bar">Fact Find Settings</div>
<ul class="wide_list">
<?php echo '<li><strong>Fact Find Date:</strong> '. $factfind['date'] .'</li>';
echo '<li><strong>Discussion Date:</strong> '. $factfind['discussion_date'] .'</li>';
echo '<li><strong>Sale Type:</strong> '. $factfind['sale_type'] .'</li>';
echo '<li><strong>Sales Person:</strong> '. $factfind['added_by'] .'</li>';
if ($factfind['confirm']==1) { echo '<li><strong>Forms Issused:</strong> Yes</li>'; } else { echo '<li><strong>Forms Issused:</strong> No</li>'; } ?>
</ul>

<?php if ($factfind['selected_pmi']==1) { ?>
<div class="title_bar">Private Medical Insurance</div>
<ul class="wide_list">
<?php echo '<li><strong>Cover Provided By Employer:</strong> '. $factfind['cover_employer'] .'</li>';
echo '<li><strong>Client Currently Has Cover:</strong> '. $factfind['cover_client'] .'</li>';
if ($factfind['cover_client']=="Yes") {
	echo '<li><strong>Current Provider:</strong> '. $factfind['pmi_provider'] .'</li>';
	echo '<li><strong>Renewal Premium:</strong> '. $factfind['pmi_renewal_premium'] .'</li>';
	echo '<li><strong>Plan Name:</strong> '. $factfind['pmi_plan_name'] .'</li>';
	echo '<li><strong>Who is covered:</strong> '. $factfind['pmi_covered'] .'</li>';
	echo '<li><strong>Renewal Date:</strong> '. $factfind['pmi_renewal_date'] .'</li>';
	echo '<li><strong>Underwriting:</strong> '. $factfind['pmi_underwriting'] .'</li>';
	echo '<li><strong>Underwriting Reason:</strong> '. $factfind['pmi_underwriting_reason'] .'</li>';
	echo '<li><strong>Current Cover:</strong> '. $factfind['pmi_cover'] .'</li>';
	echo '<li><strong>Current Hospital List:</strong> '. $factfind['pmi_hospital'] .'</li>';
	echo '<li><strong>Current Excess:</strong> '. $factfind['pmi_excess'] .'</li>';
}
echo '<li><strong>Desired Cover:</strong> '. $factfind['pmi_desired_cover'] .'</li>';
echo '<li><strong>Desired Excess:</strong> '. $factfind['desired_excess'] .'</li>';
?>
</ul>
<?php } ?>

<?php if ($factfind['selected_life']==1) { ?>
<div class="title_bar">Life Insurance</div>
<ul class="wide_list">
<?php echo '<li><strong>Cover Provided By Employer:</strong> '. $factfind['life_employer'] .'</li>';
echo '<li><strong>Client Currently Has Cover:</strong> '. $factfind['life_current'] .'</li>';
echo '<li><strong>Cover Details:</strong> '. $factfind['life_details'] .'</li>';
if ($factfind['life_current']=="Yes") {
	echo '<li><strong>Who is covered on the plan:</strong> '. $factfind['life_who'] .'</li>';
	echo '<li><strong>Who is your cover with:</strong> '. $factfind['life_provider'] .'</li>';
	echo '<li><strong>What is the sum assured:</strong> '. $factfind['life_assured'] .'</li>';
	echo '<li><strong>How much are you paying per month:</strong> '. $factfind['life_paying'] .'</li>';
	echo '<li><strong>What is the term of the policy:</strong> '. $factfind['life_term'] .'</li>';
	echo '<li><strong>Is the plan written in trust:</strong> '. $factfind['life_trust'] .'</li>';
	echo '<li><strong>What type of cover do you have:</strong> '. $factfind['life_cover'] .'</li>';
	echo '<li><strong>What type of plan is it:</strong> '. $factfind['life_plan'] .'</li>';
	echo '<li><strong>How is the policy written:</strong> '. $factfind['life_written'] .'</li>';
	echo '<li><strong>Cover permanent disability:</strong> '. $factfind['life_disability'] .'</li>';
	echo '<li><strong>Include any of the following:</strong> '. $factfind['life_includes'] .'</li>';
}
echo '<li><strong>Benefit Amount:</strong> '. $factfind['life_desired_amount'] .'</li>';
echo '<li><strong>Benefit Amount Reason:</strong> '. $factfind['life_desired_reason'] .'</li>';
echo '<li><strong>Term in Years:</strong> '. $factfind['life_desired_term'] .'</li>';
echo '<li><strong>Term Amount Reason:</strong> '. $factfind['life_desired_term_reason'] .'</li>';
echo '<li><strong>Lives Covered:</strong> '. $factfind['life_desired_lives'] .'</li>';
echo '<li><strong>Options:</strong> '. $factfind['life_desired_options'] .'</li>';
echo '<li><strong>Plan Type:</strong> '. $factfind['life_desired_plan'] .'</li>';
echo '<li><strong>Reason for the Policy:</strong> '. $factfind['life_reason_policy'] .'</li>';
echo '<li><strong>Reason for Rebroke:</strong> '. $factfind['life_rebroke'] .'</li>'; ?>
</ul>
<?php } ?>

<?php if ($factfind['selected_critical']==1) { ?>
<div class="title_bar">Life and Critical Illness</div>
<ul class="wide_list">
<?php echo '<li><strong>Cover Provided By Employer:</strong> '. $factfind['critical_employer'] .'</li>';
echo '<li><strong>Client Currently Has Cover:</strong> '. $factfind['critical_current'] .'</li>';
echo '<li><strong>Cover Details:</strong> '. $factfind['critical_details'] .'</li>';
if ($factfind['critical_current']=="Yes") {
	echo '<li><strong>Who is covered on the plan:</strong> '. $factfind['critical_who'] .'</li>';
	echo '<li><strong>Who is your cover with:</strong> '. $factfind['critical_provider'] .'</li>';
	echo '<li><strong>What is the sum assured:</strong> '. $factfind['critical_assured'] .'</li>';
	echo '<li><strong>How much are you paying per month:</strong> '. $factfind['critical_paying'] .'</li>';
	echo '<li><strong>How many conditions are covered:</strong> '. $factfind['critical_conditions'] .'</li>';
	echo '<li><strong>What is the term of the policy:</strong> '. $factfind['critical_term'] .'</li>';
	echo '<li><strong>Is the plan written in trust:</strong> '. $factfind['critical_trust'] .'</li>';
	echo '<li><strong>What type of cover do you have:</strong> '. $factfind['critical_cover'] .'</li>';
	echo '<li><strong>What type of plan is it:</strong> '. $factfind['critical_plan'] .'</li>';
	echo '<li><strong>How is the policy written:</strong> '. $factfind['critical_written'] .'</li>';
	echo '<li><strong>Cover permanent disability:</strong> '. $factfind['critical_disability'] .'</li>';
	echo '<li><strong>Include any of the following:</strong> '. $factfind['critical_includes'] .'</li>';
}
echo '<li><strong>Benefit Amount:</strong> '. $factfind['critical_desired_amount'] .'</li>';
echo '<li><strong>Benefit Amount Reason:</strong> '. $factfind['critical_desired_reason'] .'</li>';
echo '<li><strong>Term in Years:</strong> '. $factfind['critical_desired_term'] .'</li>';
echo '<li><strong>Term Amount Reason:</strong> '. $factfind['critical_desired_term_reason'] .'</li>';
echo '<li><strong>Lives Covered:</strong> '. $factfind['critical_desired_lives'] .'</li>';
echo '<li><strong>Options:</strong> '. $factfind['critical_desired_options'] .'</li>';
echo '<li><strong>Plan Type:</strong> '. $factfind['critical_desired_plan'] .'</li>';
echo '<li><strong>Reason for the Policy:</strong> '. $factfind['critical_reason_policy'] .'</li>';
echo '<li><strong>Reason for Rebroke:</strong> '. $factfind['critical_rebroke'] .'</li>'; ?>
</ul>
<?php } ?>

<?php if ($factfind['selected_income']==1) { ?>
<div class="title_bar">Income Protection</div>
<ul class="wide_list">
<?php echo '<li><strong>Client Currently Has Cover:</strong> '. $factfind['income_existing'] .'</li>';
 echo '<li><strong>Do you have a mortgage:</strong> '. $factfind['income_mortgage'] .'</li>';
echo '<li><strong>Receiving any state benefits:</strong> '. $factfind['income_benefits'] .'</li>';
if ($factfind['income_existing']=="Yes") {
	echo '<li><strong>Who is your cover with:</strong> '. $factfind['income_cover'] .'</li>';
	echo '<li><strong>Monthly Benefit:</strong> '. $factfind['income_monthly_benefit'] .'</li>';
	echo '<li><strong>Remaining Term:</strong> '. $factfind['income_term'] .'</li>';
	echo '<li><strong>What is the deferment period:</strong> '. $factfind['income_deferment'] .'</li>';
	echo '<li><strong>How much are you paying a month:</strong> '. $factfind['income_paying'] .'</li>';
}
echo '<li><strong>How long does your employer pay you if you are unable to work:</strong> '. $factfind['income_desired_days'] .'</li>';
echo '<li><strong>What is your declared gross monthly income:</strong> '. $factfind['income_desired_income'] .'</li>';
echo '<li><strong>Maximum long term benefit available:</strong> '. $factfind['income_desired_benefit_term'] .'</li>';
echo '<li><strong>What duties do you perform in your occupation:</strong> '. $factfind['income_desired_duties'] .'</li>';
echo '<li><strong>What deferment period do you require:</strong> '. $factfind['income_desired_deferment'] .'</li>';
echo '<li><strong>Planned age of retirement:</strong> '. $factfind['income_desired_retirement'] .'</li>';
echo '<li><strong>Term Required:</strong> '. $factfind['income_desired_term'] .'</li>';
echo '<li><strong>Monthly benefit required:</strong> '. $factfind['income_desired_benefit_required'] .'</li>';
echo '<li><strong>Reason for the term of cover:</strong> '. $factfind['income_desired_term_reason'] .'</li>';

echo '<li><strong>Options:</strong> '. $factfind['income_desired_options'] .'</li>';
echo '<li><strong>Reason for the Policy:</strong> '. $factfind['income_reason_policy'] .'</li>';
echo '<li><strong>Notes:</strong> '. $factfind['income_desired_notes'] .'</li>';
echo '<li><strong>Reason for Rebroke:</strong> '. $factfind['income_rebroke'] .'</li>'; ?>
</ul>
<?php } ?>

<?php if ($factfind['selected_asu']==1) { ?>
<div class="title_bar">Accident Sickness Unemployment</div>
<ul class="wide_list">
<?php echo '<li><strong>Client Currently Has Cover:</strong> '. $factfind['asu_existing'] .'</li>';
if ($factfind['asu_existing']=="Yes") {
	echo '<li><strong>Who is your cover with:</strong> '. $factfind['asu_cover'] .'</li>';
	echo '<li><strong>Monthly Benefit:</strong> '. $factfind['asu_benefit'] .'</li>';
	echo '<li><strong>Term of the Policy:</strong> '. $factfind['asu_term'] .'</li>';
	echo '<li><strong>What is the deferment period:</strong> '. $factfind['asu_deferment'] .'</li>';
	echo '<li><strong>How much are you paying a month:</strong> '. $factfind['asu_paying'] .'</li>';
	echo '<li><strong>Are you Covered for:</strong> '. $factfind['asu_covered'] .'</li>';
}
echo '<li><strong>Desired covered for:</strong> '. $factfind['asu_desired_covered'] .'</li>';
echo '<li><strong>How long does your employer pay you if you are unable to work:</strong> '. $factfind['asu_desired_days'] .'</li>';
echo '<li><strong>What is your declared gross monthly income:</strong> '. $factfind['asu_desired_income'] .'</li>';
echo '<li><strong>Maximum long term benefit available:</strong> '. $factfind['asu_desired_benefit_long_term'] .'</li>';
echo '<li><strong>What deferment period do you require:</strong> '. $factfind['asu_desired_deferment'] .'</li>';
echo '<li><strong>Term Required:</strong> '. $factfind['asu_desired_term'] .'</li>';
echo '<li><strong>Benefit Term Required:</strong> '. $factfind['asu_desired_benefit_term'] .'</li>';
echo '<li><strong>Monthly benefit required:</strong> '. $factfind['asu_desired_benefit_required'] .'</li>';
echo '<li><strong>Reason for the term of cover:</strong> '. $factfind['asu_desired_term_reason'] .'</li>';
echo '<li><strong>Reason for the Policy:</strong> '. $factfind['asu_reason_policy'] .'</li>';
echo '<li><strong>Reason for Rebroke:</strong> '. $factfind['asu_rebroke'] .'</li>'; ?>
</ul>
<?php } ?>

<?php if ($factfind['selected_home']==1) { ?>
<div class="title_bar">Home Insurance</div>
<ul class="wide_list">
<?php echo '<li><strong>When do you require cover from/current renewal date:</strong> '. $factfind['home_date'] .'</li>';
echo '<li><strong>Postcode:</strong> '. $factfind['home_postcode'] .'</li>';
echo '<li><strong>Is your building covered:</strong> '. $factfind['home_building_covered'] .'</li>';
echo '<li><strong>Is your contents covered:</strong> '. $factfind['home_contents_covered'] .'</li>';
echo '<li><strong>Current buildings insurer:</strong> '. $factfind['home_buildings_insurer'] .'</li>';
echo '<li><strong>Current contents insurer:</strong> '. $factfind['home_contents_insurer'] .'</li>';
echo '<li><strong>Are there any expensive valuables you need to cover:</strong> '. $factfind['home_valuables'] .'</li>';
echo '<li><strong>Notes:</strong> '. $factfind['home_notes'] .'</li>';
echo '<li><strong>Amount of away from home cover:</strong> '. $factfind['home_away'] .'</li>';
echo '<li><strong>Year property was built:</strong> '. $factfind['home_property_built'] .'</li>';
echo '<li><strong>Type of property:</strong> '. $factfind['home_property'] .'</li>';
echo '<li><strong>Number of bedrooms:</strong> '. $factfind['home_bedrooms'] .'</li>';
echo '<li><strong>Accidental damage on buildings:</strong> '. $factfind['home_accidental_buildings'] .'</li>';
echo '<li><strong>Accidental damage on contents:</strong> '. $factfind['home_accidental_contents'] .'</li>';
echo '<li><strong>Existing monthly premium:</strong> '. $factfind['home_premium'] .'</li>';
echo '<li><strong>Current building excess:</strong> '. $factfind['home_buildings_excess'] .'</li>';
echo '<li><strong>Current contents excess:</strong> '. $factfind['home_contents_excess'] .'</li>';
echo '<li><strong>Do you have legal protection:</strong> '. $factfind['home_legal'] .'</li>';
echo '<li><strong>Do you own the property:</strong> '. $factfind['home_own_property'] .'</li>';
echo '<li><strong>Who is the mortgage lender:</strong> '. $factfind['home_mortgage_lender'] .'</li>';
echo '<li><strong>Do you have home emergency cover:</strong> '. $factfind['home_emergency_cover'] .'</li>';
echo '<li><strong>Claims have you had in the last 5 years:</strong> '. $factfind['home_claims'] .'</li>';
echo '<li><strong>Details:</strong> '. $factfind['home_details'] .'</li>'; ?>
</ul>
<?php } ?>

<?php if ($factfind['selected_travel']==1) { ?>
<div class="title_bar">Travel Insurance</div>
<ul class="wide_list">
<?php echo '<li><strong>Area of cover:</strong> '. $factfind['travel_cover'] .'</li>';
echo '<li><strong>What type of policy do you require:</strong> '. $factfind['travel_policy'] .'</li>';
echo '<li><strong>Maximum days per trip:</strong> '. $factfind['travel_days'] .'</li>';
echo '<li><strong>Renewal Date:</strong> '. $factfind['travel_renewal'] .'</li>';
echo '<li><strong>Notes:</strong> '. $factfind['travel_notes'] .'</li>'; ?>
</ul>
<?php } ?>

<?php } else {
redirect('clients');
} ?>
