<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( session_status() == PHP_SESSION_NONE ) {
    session_start();
}

require_once( APPPATH . 'libraries/oauth2/Provider/Facebook.php' ); 

use League\Oauth2\Client\Provider\Facebook;
 
class Oauth2 {
    var $ci;
    var $helper;
    var $session;
    var $provider;
 
    public function __construct() {
        
        $this->provider = Facebook(array(
            'clientId'  =>  '1486594334912497',
            'clientSecret'  =>  'f1a7a118e9d5571eb15e7d2933fd9f98',
            'redirectUri'   =>  'https://sedzjctirm.localtunnel.me'
        ));
        
    }

    public function getAuthUrl() {
        return $this->provider->getAuthorizationUrl();
    }
 
}
 
/* End of file Facebook.php */
