<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf {
 
    function pdf() {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load() {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
 		
		//$param=NULL
        //if ($params==NULL) {
        //    $param = '"en-GB-x","A3","","",10,10,10,10,6,3';
        //}
 
        return new mPDF('utf-8', 'A4-L');
    }
}
