<?php

class Settings_model extends CI_Model {

	/*
    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {	
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);			
			redirect('login');			
		}	
	}
	*/		
	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getInsurerList($type) {
		$rows = array();
		$sql = "SELECT * FROM insurers WHERE belongs_to='". $this->session->userdata('user_id') ."' AND type='". $type ."' ORDER BY email ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function add_quote_email($data) {
		$this->db->insert('insurers', $data);	
	}
	
	function addProtection($data) {
		$this->db->insert('insurers_protection', $data);	
	}
	
	function removeProtection($id) {
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $id);
		$this->db->delete('insurers_protection');
	}

	function getProtectionInsurers() {
		$rows = array();
		$sql = "SELECT * FROM insurers_protection WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY insurer ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function checkPassword($pass) {
		$sql = "SELECT id FROM users WHERE id='". $this->session->userdata('user_id') ."' AND password='". md5($pass) ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}	
	}
	
	function updatePassword($data) {
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->update('users', $data);	
	}
	
	function updateTheme($theme, $icons) {
		$update = array(
			'theme_site' => $theme,
			'theme_icons' => $icons
		);
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->update('users', $update);	
	}
	
	function updateUserRecord($data) {
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->update('users', $data);	
	}
	
	function getClientTags() {
		$rows = array();
		$sql = "SELECT * FROM tags WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY tag ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getOpportunitiesTags() {
		$rows = array();
		$sql = "SELECT * FROM tags_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY tag ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function addClientTag($data) {
		$this->db->insert('tags', $data);	
	}	
		
	function addOpportunitiesTag($data) {
		$this->db->insert('tags_opportunities', $data);	
	}
	
	function removeClientTag($tag) {
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $tag);
		$this->db->delete('tags');
		return $this->db->affected_rows();	
	}
	
	function removeClientTagUsed($tag) {
		$this->db->where('tag_id', $tag);
		$this->db->delete('customer_tags');	
	}

	function removeOpportunitiesTag($tag) {
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $tag);
		$this->db->delete('tags_opportunities');
		return $this->db->affected_rows();	
	}
	
	function removeOpportunitiesTagUsed($tag) {
		$this->db->where('opp_tag', $tag);
		$this->db->delete('customer_opportunities_tags');	
	}	
	
	function getSMSCreditCount($user) {
		$sql = "SELECT SUM(credits) as sms FROM sms_credits WHERE user_id='". $user ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return 0;
		}
		
	}	
	
	function addToRecurringPaymentsLog($data) {
		$this->db->insert('recurring_payments', $data);	
	}
	
	function addEmailSignature($data) {
	$this->db->insert('email_signatures', $data);	
	}
	
	function addEmailTemplate($data) {
	$this->db->insert('email_templates', $data);	
	}	
	
	function isAccountPaymentExempt($user) {
		$sql = "SELECT payment_exempt FROM users WHERE id='". $user ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			// if is 1, then return true
			if ($row['payment_exempt']==1) {
				return true;
			} else {
				return false;
			}			
		} else {
			return false;
		}
	}
	
	function deleteInsurer($ref) {	
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $ref);
		$this->db->delete('insurers');
	}
	
	function getAccountDetails($user) {
		$sql = "SELECT * FROM users WHERE id='". $user ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}
	
	function getEmailSignatures() {
		$rows = array();
		$sql = "SELECT * FROM email_signatures WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY name ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
		
			return false;	
	
		}
	}	
	
	function getEmailTemplates() {
		$rows = array();
		$sql = "SELECT * FROM email_templates WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY name ASC";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
		
			return false;	
	
		}
	}
	
	function getThatTemplate($id) {
		$sql = "SELECT * FROM email_templates WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}
	
	function getThatSignature($id) {
		$sql = "SELECT * FROM email_signatures WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}
	
	function updateThatSignature($update, $id) {
		$this->db->where('id', $id);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('email_signatures', $update);	
	}
	
	function updateThatTemplate($update, $id) {
		$this->db->where('id', $id);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('email_templates', $update);	
	}
	
	function deleteThatTemplate($id) {
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $id);
		$this->db->delete('email_templates');	
	}	
	
	function deleteThatSignature($id) {
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('id', $id);
		$this->db->delete('email_signatures');	
	}
	
	function addAdditionalUser($data) {
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}
	
	function addUser2Group($user, $group) {
		$data = array(
			'group_id' => $group,
			'user_id' => $user,
			'role' => 2
		);
		$this->db->insert('users_to_groups', $data);
	}		
	
	function getAdditionalUsers($group, $user) {
		$rows = array();
		$sql = "SELECT users_to_groups.user_id, users.first_name, users.last_name, users.username FROM users_to_groups LEFT JOIN users on users_to_groups.user_id = users.id WHERE users_to_groups.user_id!='". $this->session->userdata('user_id') ."' AND users_to_groups.group_id='". $this->session->userdata('group_id') ."'";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;	
		}
	}
	
	function getAdditionalUserDetails($id) {
		$sql = "SELECT users_to_groups.user_id, users.username, users.first_name, users.last_name, users.email, users_permissions.* FROM users_to_groups LEFT JOIN users on users_to_groups.user_id = users.id LEFT JOIN users_permissions on users_to_groups.user_id = users_permissions.user_id WHERE users_to_groups.user_id='". $id ."' AND users_to_groups.group_id='". $this->session->userdata('group_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}	
	
	function addAdditionalPermissions($data) {
		$this->db->insert('users_permissions', $data);	
	}			

	function checkAdditionalUserInThisGroup($id) {
		$sql = "SELECT * FROM users_to_groups WHERE user_id='". $id ."' AND group_id='". $this->session->userdata('group_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function updateAdditionalUser($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('users', $data);	
	}
	
	function updateAdditionalUserPermissions($id, $data) {
		$this->db->where('user_id', $id);
		$this->db->update('users_permissions', $data);	
	}
	
	function updateTabSettings($id, $user, $data) {
		$this->db->where('id', $id);
		$this->db->where('user_id', $user);
		$this->db->update('users_tabs', $data);	
	}	
	
	function addTabSettings($data) {
		$this->db->insert('users_tabs', $data);
	}	
	
	function getTabSettings($id) {
		$sql = "SELECT * FROM users_tabs WHERE user_id='".$id ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}
	
	function getCustomTab($id) {
		$sql = "SELECT * FROM users_custom_tabs WHERE user_id='". $this->session->userdata("user_id") ."' AND id='".$id ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}	
	
	function getCustomTabsList() {
		$rows = array();
		$sql = "SELECT name, id as tab_ref FROM users_custom_tabs WHERE user_id='". $this->session->userdata("user_id") ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function addCustomTab($data) {
		$this->db->insert('users_custom_tabs', $data);
	}
	
	function updateCustomTab($id, $data) {
		$this->db->where('id', $id);
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->update('users_custom_tabs', $data);	
	}
	
	function getCustomFormsList() {
		$rows = array();
		$sql = "SELECT * FROM users_custom_forms WHERE user_id='". $this->session->userdata("user_id") ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function addCustomForm($data) {
		$this->db->insert('users_custom_forms', $data);
	}
	
	function addCustomFormItem($data) {
		$this->db->insert('users_custom_forms_build', $data);
	}
	
	function getCustomFormDetails($id) {
		$sql = "SELECT * FROM users_custom_forms WHERE id='". $id ."' AND user_id='". $this->session->userdata("user_id") ."' LIMIT 1";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function getCustomFormOptions($id) {
		$rows = array();
		$sql = "SELECT * FROM users_custom_forms_build WHERE form_id='". $id ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}	
	}
	
	function clearCustomFormOptions($id) {
		$this->db->where('form_id', $id);
		$this->db->delete('users_custom_forms_build');	
	}
	
	function deleteCustomTab($tab) {
		$this->db->where('id', $tab);
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->delete('users_custom_tabs');	
	}
	
	function deleteCustomForm($form) {
		$this->db->where('id', $form);
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->delete('users_custom_forms');		
		$this->deleteCustomFormOptions($form);
	}
	
	function deleteCustomFormOptions($form) {
		$this->db->where('form_id', $form);
		$this->db->delete('users_custom_forms_build');	
	}
	
	function getCustomFileList() {
	$sql = "SELECT files_1, files_2, files_3, files_4, files_5, files_6 FROM users WHERE id='". $this->session->userdata('user_id') ."' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}
	
	function updateCustomFiles($data) {
		$this->db->where('id', $this->session->userdata('user_id'));
		$this->db->update('users', $data);	
	}
	
	function updateCompanyLogo($image) {
		$data = array(
			'logo' => $image
		);
		$this->db->where('manager_id', $this->session->userdata('user_id'));
		$this->db->where('group_id', $this->session->userdata('group_id'));
		$this->db->update('users_groups', $data);	
	}
	
	function getCompanyLogo() {
		$sql = "SELECT logo FROM users_groups WHERE group_id='". $this->session->userdata("group_id") ."' LIMIT 1";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row['logo'];
		} else {
			return false;
		}
	}
	
	function saveSMSPurchase($data) {
		$this->db->insert('sms_purchase_history', $data);
	}			

	function saveSMSPurchaseStatus($id, $status) {
		$data = array('status' => $status);
		$this->db->where('id', $id);
		$this->db->update('sms_purchase_history', $data);		
	}
	
	function getSMSPurchase($token) {
		$sql = "SELECT * FROM sms_purchase_history WHERE paypal_token='".$token ."' AND status='0' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}	
	
	function addSMSCredits($user, $credits) {
		// if there isnt a record for the credits we need to created on .... or we can update it
		$sql = "SELECT * FROM sms_credits WHERE user_id='". $user ."' LIMIT 1";
		$query = $this->db->query($sql);
		if ($query->num_rows>0) {
			$sql2 = "UPDATE sms_credits SET credits=credits+". $credits ." where user_id='". $user ."' LIMIT 1";
			$this->db->query($sql2);
		} else {
			$data = array('user_id' => $user, 'credits' => $credits);
			$this->db->insert('sms_credits', $data);
		}
	}
	
	function transferSMSCredits($user, $credits) {
		$this->curl->create("https://api.txtlocal.com/transfer_credits/");
		$topost = array(
			'username' => 'steve.warden1@btopenworld.com',
			'hash' => 'eeb7b8ed5a3665863a492baecd6923dca108c429',
			'user_id' => $user,
			'credits' => $credits
		);		
		$this->curl->post($topost);
		$yes = $this->curl->execute();
		
		print_r($yes);
		
	}	

} ?>
