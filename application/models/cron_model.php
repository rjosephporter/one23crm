<?php

class Cron_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function listUsers() {
		$rows = array();
		$sql = "SELECT * FROM users WHERE send_task_reminder='1' AND active='1'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;	
	}

	function taskByUser($user, $today) {
		$rows = array();
		$sql = "SELECT name, action, customer_id, DATE_FORMAT(date,'%l:%i%p') as due_time, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name FROM customer_tasks LEFT JOIN customer ON customer_id=customer.id WHERE date LIKE '". $today ."%' AND customer_tasks.belongs_to='". $user ."' AND customer_tasks.status='1' ORDER BY date ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getQuoteDetails($quote) {
		$sql = "SELECT * FROM webline_quotes WHERE id='". $quote ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}
	
	function getQuoteDetailsMulti($quote) {
		$sql = "SELECT * FROM webline_multiproduct_quotes WHERE id='". $quote ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}	

	function getWeblineListQuotes() {
		$poll = date('Y-m-d H:i:s');	
		$rows = array();
		$sql = "SELECT id, webline_ref, collection_type, belongs_to FROM webline_quotes WHERE polltime <= '". $poll ."' AND status='0'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}	
		return $rows;	
	}
	
	function getWeblineListQuotesMulti() {
		$poll = date('Y-m-d H:i:s');	
		$rows = array();
		$sql = "SELECT id, webline_ref, collection_type, belongs_to, multiproduct FROM webline_multiproduct_quotes WHERE polltime <= '". $poll ."' AND status='0'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}	
		return $rows;	
	}	

	function addQuoteResults($data) {
		$this->db->insert('webline_quotes_results', $data);	
	}
	
	function addQuoteResultsMulti($data) {
		$this->db->insert('webline_multiproduct_quotes_results', $data);	
	}	
	
	function setQuoteStatus($quote, $status) {
		$data = array('status' => $status);
		$this->db->where('id', $quote);
		$this->db->update('webline_quotes', $data);
	}	
	
	function setQuoteStatusMulti($quote, $status) {
		$data = array('status' => $status);
		$this->db->where('id', $quote);
		$this->db->update('webline_multiproduct_quotes', $data);
	}
	
	function getWeblineLoginDetails($user) {
		//$sql = "SELECT webline_num, webline_user, webline_pass FROM users WHERE id='". $user ."' LIMIT 1";
		$sql = "SELECT webline_num, webline_user, webline_pass FROM users_groups WHERE group_id='". $user ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}		
	}
	
	function getQuoteBestPrice($id) {
		$sql = "SELECT provider, premium, product, benefit, death_benefit, cic_benefit FROM webline_quotes_results WHERE premium>0 AND quote_id='". $id ."' ORDER BY premium LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}		
	}
	
	function setQuoteBestPrice($quote, $provider, $benefit, $price, $product, $deathcic) {
		$data = array('bestprice_provider' => $provider, 'bestprice_benefit' => $benefit, 'bestprice_price' => $price, 'bestprice_product' => $product, 'bestprice_deathcic' => $deathcic);
		$this->db->where('id', $quote);
		$this->db->update('webline_quotes', $data);
	}
	
	function setQuoteProcessedMulti($ref) {
		$data = array('ref' => $ref);	
		$this->db->insert('webline_multiproduct_quotes_processed', $data);
	}	
		
	
} ?>
