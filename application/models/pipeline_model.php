<?php

class Pipeline_model extends CI_Model {

	/*
    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {	
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);			
			redirect('login');			
		}	
	}
	*/	
	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}			

	function pipelineByMilestone() {	
		$rows = array();
		$sql = "SELECT milestone, count(milestone) as milestonecount FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' GROUP BY milestone";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
	
		return $rows;	
	}
	
	function pipelineByMilestoneUser($users) {	
		$rows = array();
		$sql = "SELECT milestone, count(milestone) as milestonecount FROM customer_opportunities WHERE belongs_to IN (". $users .") GROUP BY milestone";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
	
		return $rows;	
	}	

	function pipelineForecast() {	
		$rows = array();
		$sql = "SELECT SUM(value_calc) as thecash, SUM(value) as maxcash, DATE_FORMAT(close_date,'%m/%Y') as themonth FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' AND status='0' GROUP BY themonth ORDER BY themonth";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
	
		return $rows;	
	}
	
	function pipelineForecastUser($users) {	
		$rows = array();
		$sql = "SELECT SUM(value_calc) as thecash, SUM(value) as maxcash, DATE_FORMAT(close_date,'%m/%Y') as themonth FROM customer_opportunities WHERE belongs_to IN (". $users .") AND status='0' GROUP BY themonth ORDER BY themonth";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
	
		return $rows;	
	}	
	
	function pipelineStats() {
		$sql = "SELECT SUM(value_calc) as pipeline, SUM(value) as total FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' AND status='0'";		
		$query = $this->db->query($sql);
				
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}
	
	function pipelineStatsUser($users) {
		$sql = "SELECT SUM(value_calc) as pipeline, SUM(value) as total FROM customer_opportunities WHERE belongs_to IN (". $users .") AND status='0'";		
		$query = $this->db->query($sql);
				
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}	
	
	function pipelineConversion($date_from, $date_to) {
		$sql = "SELECT COUNT(*) as total, COALESCE(sum(milestone='Won'), 0) as won FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' AND close_date>='". $date_from ."' AND close_date<='". $date_to ."'";		
		$query = $this->db->query($sql);
				
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}
	
	function pipelineConversionUser($date_from, $date_to, $users) {
		$sql = "SELECT COUNT(*) as total, COALESCE(sum(milestone='Won'), 0) as won FROM customer_opportunities WHERE belongs_to IN (". $users .") AND close_date>='". $date_from ."' AND close_date<='". $date_to ."'";		
		$query = $this->db->query($sql);
				
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}	
	
	function pipelineSales($date_from, $date_to) {
		$sql = "SELECT SUM(value_calc) as total FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' AND close_date>='". $date_from ."' AND close_date<='". $date_to ."' AND milestone='Won'";		
		$query = $this->db->query($sql);
						
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}
	
	function pipelineSalesUser($date_from, $date_to, $users) {
		$sql = "SELECT SUM(value_calc) as total FROM customer_opportunities WHERE belongs_to IN (". $users .") AND close_date>='". $date_from ."' AND close_date<='". $date_to ."' AND milestone='Won'";		
		$query = $this->db->query($sql);
						
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}	

	function pipelineSalesMonth($date_from, $date_to, $month) {
		$sql = "SELECT IFNULL(SUM(value),0) as total, IFNULL(DATE_FORMAT(close_date,'%m/%Y'),'".$month."') as themonth FROM customer_opportunities WHERE belongs_to='". $this->session->userdata('user_id') ."' AND close_date>='". $date_from ."' AND close_date<='". $date_to ."' AND milestone='Won'";		
		
		$query = $this->db->query($sql);
								
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}
	
	function pipelineSalesMonthUser($date_from, $date_to, $month, $users) {
		$sql = "SELECT IFNULL(SUM(value),0) as total, IFNULL(DATE_FORMAT(close_date,'%m/%Y'),'".$month."') as themonth FROM customer_opportunities WHERE belongs_to IN (". $users .") AND close_date>='". $date_from ."' AND close_date<='". $date_to ."' AND milestone='Won'";		
		
		$query = $this->db->query($sql);
								
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
			
	}	
	
	function pipelineList($status, $tag) {
		$rows = array();
		$sql = "SELECT customer_opportunities.*, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client FROM customer_opportunities LEFT JOIN customer on customer_opportunities.customer_id=customer.id";
		if ($tag!="") {		
		$sql .= " LEFT JOIN customer_opportunities_tags on customer_opportunities_tags.opp_id=customer_opportunities.id";
		}		
		$sql .= " WHERE customer_opportunities.belongs_to='". $this->session->userdata('user_id') ."'";
		// todays date
		$todays_is = date("Y-m-d H:i:s");	
		if ($status=="open") {
			$sql .=" AND customer_opportunities.status='0' ";
		} elseif ($status=="closed") {
			$sql .=" AND customer_opportunities.status='1' ";
		} elseif ($status=="closed30") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -30 days"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="closed90") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -90 days"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="closedyear") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -1 year"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="suspect") {
			$sql .=" AND customer_opportunities.milestone='Suspect' ";
		} elseif ($status=="prospect") {
			$sql .=" AND customer_opportunities.milestone='Prospect' ";
		} elseif ($status=="champion") {
			$sql .=" AND customer_opportunities.milestone='Champion' ";
		} elseif ($status=="opportunity") {
			$sql .=" AND customer_opportunities.milestone='Opportunity' ";
		} elseif ($status=="proposal") {
			$sql .=" AND customer_opportunities.milestone='Proposal' ";
		} elseif ($status=="verbal") {
			$sql .=" AND customer_opportunities.milestone='Verbal' ";
		} elseif ($status=="lost") {
			$sql .=" AND customer_opportunities.milestone='Lost' ";
		} elseif ($status=="won") {
			$sql .=" AND customer_opportunities.milestone='Won' ";					
		} elseif ($status=="wonthismonth") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y-m")."-01 00:00:00' AND customer_opportunities.close_date<='".date("Y-m-t"). " 23:59:59' ";	
		} elseif ($status=="won90days") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='". date("Y-m-d H:i:s", strtotime($todays_is . " -90 days")) ."' AND customer_opportunities.close_date<='". $todays_is ."' ";		
		} elseif ($status=="wonthisyear") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y")."-01-01 00:00:00' AND customer_opportunities.close_date<='".date("Y")."-12-31 23:59:59' ";
		} elseif ($status=="conversionthismonth") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y-m")."-01 00:00:00' AND customer_opportunities.close_date<='".date("Y-m-t"). " 23:59:59' ";	
		} elseif ($status=="conversion90days") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='". date("Y-m-d H:i:s", strtotime($todays_is . " -90 days")) ."' AND customer_opportunities.close_date<='". $todays_is ."' ";		
		} elseif ($status=="conversionthisyear") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y")."-01-01 00:00:00' AND customer_opportunities.close_date<='".date("Y")."-12-31 23:59:59' ";
		}	
		
		if ($tag!="") {
			$sql .= "AND customer_opportunities_tags.opp_tag='".$tag."'";
		}
		
		$sql .= "ORDER BY customer_opportunities.close_date DESC";

		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}	
		return $rows;	
	}	

	function pipelineListGroupUser($status, $tag, $user) {
		$rows = array();
		$sql = "SELECT customer_opportunities.*, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client FROM customer_opportunities LEFT JOIN customer on customer_opportunities.customer_id=customer.id";
		if ($tag!="") {		
		$sql .= " LEFT JOIN customer_opportunities_tags on customer_opportunities_tags.opp_id=customer_opportunities.id";
		}		
		$sql .= " WHERE customer_opportunities.belongs_to IN (". $user .")";
		// todays date
		$todays_is = date("Y-m-d H:i:s");	
		if ($status=="open") {
			$sql .=" AND customer_opportunities.status='0' ";
		} elseif ($status=="closed") {
			$sql .=" AND customer_opportunities.status='1' ";
		} elseif ($status=="closed30") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -30 days"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="closed90") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -90 days"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="closedyear") {
			$todate = date("Y-m-d H:i:s", strtotime($todays_is . " -1 year"));
			$sql .=" AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".$todate."' AND customer_opportunities.close_date<='".$todays_is."' ";
		} elseif ($status=="suspect") {
			$sql .=" AND customer_opportunities.milestone='Suspect' ";
		} elseif ($status=="prospect") {
			$sql .=" AND customer_opportunities.milestone='Prospect' ";
		} elseif ($status=="champion") {
			$sql .=" AND customer_opportunities.milestone='Champion' ";
		} elseif ($status=="opportunity") {
			$sql .=" AND customer_opportunities.milestone='Opportunity' ";
		} elseif ($status=="proposal") {
			$sql .=" AND customer_opportunities.milestone='Proposal' ";
		} elseif ($status=="verbal") {
			$sql .=" AND customer_opportunities.milestone='Verbal' ";
		} elseif ($status=="lost") {
			$sql .=" AND customer_opportunities.milestone='Lost' ";
		} elseif ($status=="won") {
			$sql .=" AND customer_opportunities.milestone='Won' ";					
		} elseif ($status=="wonthismonth") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y-m")."-01 00:00:00' AND customer_opportunities.close_date<='".date("Y-m-t"). " 23:59:59' ";	
		} elseif ($status=="won90days") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='". date("Y-m-d H:i:s", strtotime($todays_is . " -90 days")) ."' AND customer_opportunities.close_date<='". $todays_is ."' ";		
		} elseif ($status=="wonthisyear") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y")."-01-01 00:00:00' AND customer_opportunities.close_date<='".date("Y")."-12-31 23:59:59' ";
		} elseif ($status=="conversionthismonth") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y-m")."-01 00:00:00' AND customer_opportunities.close_date<='".date("Y-m-t"). " 23:59:59' ";	
		} elseif ($status=="conversion90days") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='". date("Y-m-d H:i:s", strtotime($todays_is . " -90 days")) ."' AND customer_opportunities.close_date<='". $todays_is ."' ";		
		} elseif ($status=="conversionthisyear") {
			$sql .=" AND customer_opportunities.milestone='Won' AND customer_opportunities.status='1' AND customer_opportunities.close_date>='".date("Y")."-01-01 00:00:00' AND customer_opportunities.close_date<='".date("Y")."-12-31 23:59:59' ";
		}	
		
		if ($tag!="") {
			$sql .= "AND customer_opportunities_tags.opp_tag='".$tag."'";
		}
		
		$sql .= "ORDER BY customer_opportunities.close_date DESC";

		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}	
		return $rows;	
	}

	function getOpportunitiesTags() {
		$rows = array();
		$sql = "SELECT * FROM tags_opportunities WHERE belongs_to='".$this->session->userdata('user_id')."' ORDER BY tag ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}

	function getGroupUsersList() {
		$rows = array();
		$sql = "SELECT users_to_groups.*, users.first_name, users.last_name, users.username FROM users_to_groups LEFT JOIN users ON users_to_groups.user_id=users.id WHERE users_to_groups.group_id='". $this->session->userdata("group_id") ."' AND user_id!='". $this->session->userdata("user_id") ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getFullGroupUsersList() {
		$rows = array();
		$sql = "SELECT user_id FROM users_to_groups WHERE group_id='". $this->session->userdata("group_id") ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row['user_id'];
			}
		}		
		return $rows;
	}	
	
	function getUserMemeberOfGroup($user) {
		$sql = "SELECT * FROM users_to_groups WHERE group_id='". $this->session->userdata("group_id")."' AND user_id='". $user ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
		
}
