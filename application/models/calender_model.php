<?php

class Calender_model extends CI_Model {
	private $action_task_label;
	protected $data_task_setting;
	protected $color_label;
	protected $icons_label;

    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
		$this->load->model('Tasksetting_model');
		$this->getTaskSettingData();
		$this->getColorLabel();
		$this->getIconLabel();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);
			redirect('login');
		}
	}


	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	public function getTaskSettingData(){
		$this->data_task_setting = $this->Tasksetting_model->getAllTaskLabel('',true);
	}

	public function getTaskSettingLabel(){
		$array_label = array();
		$label = $this->data_task_setting;
		foreach($label->result() as $row){
			$array_label[$row->id] = $row->color;
		}
		return $array_label;
	}
	public function getTaskSettingIcons(){
		$array_label = array();
		$label = $this->data_task_setting;
		foreach($label->result() as $row){
			$array_label[$row->id] = $row->icons;
		}
		return $array_label;
	}

	public function getColorLabel(){
		$task_setting_color = $this->getTaskSettingLabel();
		$label = array(
			'Call'=>'#FF7400',
			'Email'=>'#FF1A00',
			'Follow-Up'=>'#D01F3C',
			'Home Insurance Renew'=>'#36393D',
			'Meeting'=>'#428bca',
			'Milestone'=>'#5cb85c',
			'Out of Indemnity'=>'#5bc0de',
			'PMI Renewal'=>'#f0ad4e',
			'Send'=>'#d9534f',
		);
		$this->color_label = ($task_setting_color + $label);
	}
	public function getIconLabel(){
		$this->icons_label = $this->getTaskSettingIcons();
	}

	public function get_task_action_icon($action){
		if( !array_key_exists($action, $this->icons_label) ){
			$this->icons_label[$action] = '';
		}
		return $this->icons_label[$action];
	}

	function get_task_action_label($action){
		if( !array_key_exists($action, $this->color_label) ){
			$this->color_label[$action] = '#000000';
		}
		return $this->color_label[$action];
	}

	function allan_get_cal_task($start_date, $end_date){
		$sql = "SELECT *, customer_tasks.id as task_id,
				customer.id as customer_id,
				CONCAT(customer.title,' ',customer.first_name,' ',customer.last_name,' ') as customer_name
				FROM customer_tasks LEFT JOIN customer
				ON customer_tasks.customer_id = customer.id
				WHERE
				customer_tasks.date >= '$start_date'
				AND customer_tasks.date <= '$end_date'
				AND customer_tasks.belongs_to in (select user_id from users_to_groups where group_id = '".$this->session->userdata('user_id')."')
				AND customer_tasks.completed_date = '0000-00-00 00:00:00'
				";
		//echo $sql;
		$action = $this->input->get('action');
		$user = $this->input->get('user');
		if(!empty($action) && $action !== 'all')
			$sql .= ' AND action = "'.$action.'"';
		if(!empty($user) && $user !== 'all')
			$sql .= ' AND belongs_to = "'.$user.'"';

		$query = $this->db->query($sql);
		$task = array();

		foreach($query->result() as $row){
			$task[] = array(
				 'id' => $row->task_id,
				 'title' => $row->name,
				 'start' => $row->date,
				 'end' => $row->end_time,
				 'url' => "",
				 'color'=>$this->get_task_action_label( $row->task_setting == 0 ? $row->action:$row->task_setting ),
				 'icon' =>$this->get_task_action_icon($row->task_setting == 0 ? $row->action:$row->task_setting),
				 'customer_id' =>$row->customer_id,
				 'customer_name' =>$row->customer_name
			);
		}

		echo json_encode($task);
	}

	function get_cal_data($year, $month, $client=null) {
			$sql = "SELECT DISTINCT DATE_FORMAT(date, '%Y-%m-%d') AS date FROM customer_tasks WHERE date LIKE '$year-$month%' AND belongs_to='". $this->session->userdata('user_id') ."'";
			if (!empty($client)) { $sql .= " AND customer_id='". $client ."'"; }

			$query = $this->db->query($sql);
			$cal_data = array();
			foreach ($query->result() as $row) { //for every date fetch data
			$a = array();
			$i = 0;
			//$sql2 = "SELECT name FROM customer_tasks WHERE date LIKE DATE_FORMAT('$row->date', '%Y-%m-%d') AND belongs_to='". $this->session->userdata('user_id') ."'";
			$sql2 = "SELECT customer_tasks.id as taskid, customer_tasks.action, DATE_FORMAT(customer_tasks.date, '%H:%i') as taskdate, customer_tasks.name, customer_tasks.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client FROM customer_tasks LEFT JOIN customer ON customer.id=customer_tasks.customer_id WHERE customer_tasks.date like '". substr($row->date,0,10) ."%' AND customer_tasks.status='1' AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."'";
			if (!empty($client)) { $sql2 .= " AND customer_tasks.customer_id='". $client ."'"; }
			$sql2 .= " ORDER BY customer_tasks.date ASC";
			$query2 = $this->db->query($sql2);
			//echo substr($row->date,0,10) . ' ' . $query2->num_rows() . '<br>';
			foreach ($query2->result() as $r) {
			 $a[$i] = '<div class="task_draggable" data-taskref="'.$this->session->userdata('user_id').'_'.$r->taskid.'"><span class="label label-info">' . $r->action . '</span> <small><a href="#" id="'. $r->taskid .'" data-toggle="modaledit" >edit task</a></small><br /><small>'. $r->taskdate . ' - ' . $r->name .' <a href="'. base_url() .'clients/view?id='. $r->customer_id .'">'. $r->client .'</a></small><div style="border-bottom:#999 1px soild;"></div></div>';     //make data array to put to specific date
			 $i++;

			}
			$cal_data[ltrim(substr($row->date,8,2),'0')] = $a;

			//echo substr($row->date,0,10) .'<br>';

			}
	//echo '<pre>';
	//print_r($cal_data);
	//echo '</pre>';

	return $cal_data;

	}


	function show_cal($year, $month, $events) {

		$conf = array(
			'start_day' => 'monday',
			'show_next_prev' => true,
			'next_prev_url' => base_url() . 'calender/index',
			'day_type' => 'long'
		);

		$conf['template'] = '{table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="{previous_url}" class="btn">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}" style="font-size:20px;">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="{next_url}" class="btn">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr class="day_name">{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr class="days">{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<div class="day_num">{day}</div><div class="day_content" data-taskdate="'.$year.'-'.$month.'-{day}">{content}</div>{/cal_cell_content}
		   {cal_cell_content_today}<div class="day_num highlight">{day}</div><div class="day_content" data-taskdate="'.$year.'-'.$month.'-{day}">{content}</div>{/cal_cell_content_today}

		   {cal_cell_no_content}<div class="day_num">{day}</div><div class="day_content" data-taskdate="'.$year.'-'.$month.'-{day}"></div>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="day_num highlight">{day}</div><div class="day_content" data-taskdate="'.$year.'-'.$month.'-{day}"></div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}';

		$this->load->library('calendar', $conf);

		return $this->calendar->generate($year, $month, $events);

	}

	function taskList($sort) {
		$rows = array();
		$sql = "SELECT
					customer_tasks.*,
					customer.company_name,
					CONCAT(customer.company_name, ' ',customer.first_name, ' ', customer.last_name) as client_name,
					customer.id as client, users.username,
					task_settings.action_name
				FROM customer_tasks
				LEFT JOIN customer
					ON customer_tasks.customer_id=customer.id
				LEFT JOIN users
					ON customer_tasks.belongs_to=users.id
				LEFT JOIN task_settings
					ON customer_tasks.customer_id = task_settings.user_id
						OR customer_tasks.task_setting = task_settings.id
				WHERE customer_tasks.status='1'
					AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."'";

		if ($sort!="") {
			$sql .= " AND action='".mysql_real_escape_string($sort)."'";
		}

		$sql .= " ORDER BY customer_tasks.date ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function taskListGroupUser($sort, $user) {
		$rows = array();
		$sql = "SELECT
					customer_tasks.*,
					customer.company_name,
					CONCAT( customer.company_name, ' ', customer.first_name, ' ',  customer.last_name) as client_name,
					customer.id as client, users.username,
					task_settings.action_name
				FROM customer_tasks
				LEFT JOIN customer
					ON customer_tasks.customer_id=customer.id
				LEFT JOIN users
					ON customer_tasks.belongs_to=users.id
				LEFT JOIN task_settings
					ON customer_tasks.customer_id = task_settings.user_id
						OR customer_tasks.task_setting = task_settings.id
				WHERE customer_tasks.status='1'
					AND customer_tasks.belongs_to IN (". $user .")
		";

		if ($sort!="") {
			$sql .= " AND action='".mysql_real_escape_string($sort)."'";
		}

		$sql .= " ORDER BY customer_tasks.date ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function taskListActions() {
		$rows = array();
		$sql = "SELECT action FROM customer_tasks WHERE action!='0' AND belongs_to='". $this->session->userdata('user_id') ."' GROUP by action";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getTaskDetails($id) {
		$sql = "SELECT * FROM customer_tasks WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function updateTaskDate($task, $date, $end_date) {
		$data = array('date' => $date,'end_time' => $end_date);
		$this->db->where('id', $task);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('customer_tasks', $data);
	}

	function getGroupUsersList() {
		$rows = array();
		$sql = "SELECT users_to_groups.*, users.first_name, users.last_name, users.username FROM users_to_groups LEFT JOIN users ON users_to_groups.user_id=users.id WHERE users_to_groups.group_id='". $this->session->userdata("group_id") ."' AND user_id!='". $this->session->userdata("user_id") ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getFullGroupUsersList() {
		$rows = array();
		$sql = "SELECT user_id FROM users_to_groups WHERE group_id='". $this->session->userdata("group_id") ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row['user_id'];
			}
		}
		return $rows;
	}

	function getUserMemeberOfGroup($user) {
		$sql = "SELECT * FROM users_to_groups WHERE group_id='". $this->session->userdata("group_id")."' AND user_id='". $user ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

} ?>
