<?php

class Dashboard_model extends CI_Model {

	/*
    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {	
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);			
			redirect('login');			
		}	
	}
	*/
		
	function getTaskListOverdue() {
		$rows = array();
		$sql = "SELECT customer_tasks.*, customer.company_name, CONCAT(first_name, ' ', last_name) as client_name, customer.id as client FROM customer_tasks LEFT JOIN customer ON customer_tasks.customer_id=customer.id WHERE customer_tasks.status='1' AND customer_tasks.date < NOW() AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."' ORDER BY customer_tasks.date ASC";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}

	function getTaskList($start, $end) {
		$rows = array();
		$sql = "SELECT customer_tasks.*, customer.company_name, CONCAT(first_name, ' ', last_name) as client_name, customer.id as client FROM customer_tasks LEFT JOIN customer ON customer_tasks.customer_id=customer.id WHERE customer_tasks.date BETWEEN '". $start ."' AND '". $end ."' AND customer_tasks.status='1' AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."' ORDER BY customer_tasks.date ASC";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}	
		
	function getUpdatesList() {
		$rows = array();
		$sql = "SELECT updates.*, updates.customer_id as the_customer, updates.added_date as update_date, customer.first_name, customer.last_name, customer.company_name, customer_tasks.name as task_name, customer_tasks.action as task_action, customer_notes.* FROM updates LEFT JOIN customer ON updates.customer_id=customer.id
		LEFT JOIN customer_tasks ON updates.task_id=customer_tasks.id
		LEFT JOIN customer_notes ON updates.note_id=customer_notes.id
		WHERE updates.belongs_user='". $this->session->userdata('user_id') ."' ORDER BY updates.added_date DESC LIMIT 30";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getUpdatesListGroup($user) {
		$rows = array();
		$sql = "SELECT updates.*, updates.customer_id as the_customer, updates.added_date as update_date, customer.first_name, customer.last_name, customer.company_name, customer_tasks.name as task_name, customer_tasks.action as task_action, customer_notes.* FROM updates LEFT JOIN customer ON updates.customer_id=customer.id
		LEFT JOIN customer_tasks ON updates.task_id=customer_tasks.id
		LEFT JOIN customer_notes ON updates.note_id=customer_notes.id
		WHERE updates.belongs_to='". $this->session->userdata('group_id') ."' AND updates.belongs_user='". $user ."' ORDER BY updates.added_date DESC LIMIT 30";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}	
	
	function getUpdatesListAll() {
		$rows = array();
		$sql = "SELECT updates.*, updates.customer_id as the_customer, updates.added_date as update_date, customer.first_name, customer.last_name, customer.company_name, customer_tasks.name as task_name, customer_tasks.action as task_action, customer_notes.* FROM updates LEFT JOIN customer ON updates.customer_id=customer.id
		LEFT JOIN customer_tasks ON updates.task_id=customer_tasks.id
		LEFT JOIN customer_notes ON updates.note_id=customer_notes.id
		WHERE updates.belongs_to='". $this->session->userdata('group_id') ."' ORDER BY updates.added_date DESC LIMIT 30";		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}	
	
	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getNoteDetails($id) {
		$sql = "SELECT n.note, n.added_date FROM customer_notes n, customer c WHERE n.id='". $id ."' AND (n.customer_id=c.id) AND c.belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}	
	}
	
	function getDocumentCategory() {
		$rows = array();
		$sql = "SELECT * FROM document_library_category WHERE active='1' ORDER BY name ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}	

	function getDocumentProviders() {
		$rows = array();
		$sql = "SELECT * FROM document_library_provider WHERE active='1' ORDER BY name ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function getDocumentList($cat, $provider) {
		$rows = array();
		$sql = "SELECT * FROM document_library_doc WHERE category='".$cat."' AND provider='".$provider."' AND active='1' ORDER BY name ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function getUserUploadedDocs() {
		$rows = array();
		$sql = "SELECT * FROM document_library_own WHERE belongs_to='".$this->session->userdata('user_id')."' AND active='1' ORDER BY name ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function getClientBirthDays($from, $to) {
		$rows = array();
		$sql = "SELECT id, CONCAT(company_name, ' ', first_name, ' ', last_name) as customer_name, DATE_FORMAT(dob, '%d %M') as birtday FROM customer WHERE DATE_FORMAT(dob, '%c-%d') BETWEEN DATE_FORMAT('". $from ."', '%c-%d') AND DATE_FORMAT('". $to ."', '%c-%d') AND type='1' AND belongs_to='". $this->session->userdata('user_id') ."' ORDER BY birtday ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}
	
	function removeUserDoc($file) {
		$this->db->where('id', $file);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->delete('document_library_own');	
	}
	
	function addUserDoc($data) {	
		$this->db->insert('document_library_own', $data);	
	}
	
	function dueTasks() {
		$rows = array();
		$sql = "SELECT id, CONCAT(company_name, ' ', first_name, ' ', last_name) as customer_name, DATE_FORMAT(dob, '%d %M') as birtday FROM customer WHERE DATE_FORMAT(dob, '%c-%d') BETWEEN DATE_FORMAT('". $from ."', '%c-%d') AND DATE_FORMAT('". $to ."', '%c-%d') AND type='1' AND belongs_to='". $this->session->userdata('user_id') ."' ORDER BY birtday ASC";	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}	
	}
	
	function getTaskNotificationList() {
		$rows = array();
		$sql = "SELECT customer_tasks.*, customer.company_name, CONCAT(first_name, ' ', last_name) as client_name, customer.id as client FROM customer_tasks LEFT JOIN customer ON customer_tasks.customer_id=customer.id WHERE customer_tasks.remind!='0000-00-00 00:00:00' AND customer_tasks.status='1' AND customer_tasks.remind < NOW() AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."' ORDER BY customer_tasks.date ASC";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}	
	
	function updateTaskTime($task, $time) {
		$data = array(
			'remind' => $time
		);
		$this->db->where('id', $task);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('customer_tasks', $data);		
	}
	
	function getGroupUsersList() {
		$rows = array();
		$sql = "SELECT users_to_groups.*, users.first_name, users.last_name, users.username FROM users_to_groups LEFT JOIN users ON users_to_groups.user_id=users.id WHERE users_to_groups.group_id='". $this->session->userdata("group_id") ."' AND user_id!='". $this->session->userdata("user_id") ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
			
	
	
} ?>
