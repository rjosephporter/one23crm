<?php

class Addresslookup_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function addressLookup($lookup_postcode, $lookup_number) {	
	
	require("OAuth.php");
	
	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

	$postcode = strtoupper(trim(urlencode($lookup_postcode)));
	$number = $lookup_number;
	$postcode = str_replace("+", "", $postcode);
	$postcode = str_replace("%20", "", $postcode);
	$post_left = substr($postcode,0,strlen($postcode)-3);
	$post_right = substr($postcode,strlen($postcode)-3);
	$postcode = $post_left."+".$post_right;
	$original_postcode = $post_left." ".$post_right;

	if(preg_match('/[A-Z]{1,2}[0-9]{1,2}[A-Z]{0,1}\s[0-9]{1,2}[A-Z]{1,2}/i', $original_postcode)) {
		
		
            $cc_key  = "dj0yJmk9RUdlMXFSRkFQRVFZJmQ9WVdrOU1XeFFabWRUTjJFbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD00MQ--";
            $cc_secret = "3283de1a11bd2dc65d93b31514e9b63eb3b41a3c";
            $url = "http://yboss.yahooapis.com/ysearch/web";
            $args = array();
            $args["q"] = $original_postcode;
            $args["format"] = "json";
             
            $consumer = new OAuthConsumer($cc_key, $cc_secret);
            $request = OAuthRequest::from_consumer_and_token($consumer, NULL,"GET", $url, $args);
            $request->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, NULL);
            $url = sprintf("%s?%s", $url, OAuthUtil::build_http_query($args));
            $ch = curl_init();
            $headers = array($request->to_header());
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $rsp = curl_exec($ch);
			
			$results = json_decode($rsp);		
            
            $arrayus = objectToArray($results);
			
			//print_r($arrayus);
			

			/*   
            foreach($arrayus['bossresponse']['web']['results'] as $i=>$results_array) {
                    $summary = $results_array['abstract'];
                    $newSummaries = explode("...", $summary);
                    $arrayus['bossresponse']['web']['results'][$i] = $newSummaries[0];

                    foreach($newSummaries as $newSummary) {
                            $arrayus['bossresponse']['web']['results'][] = array("abstract"=>$newSummary);
                    }
            }
			*/
    
            foreach($arrayus['bossresponse']['web']['results'] as $results_array) {
				$address = "";
				$doBreak = false;
			
                $summary = $results_array['abstract'];
                
				$summary = str_replace("<b>","",$summary);
                $summary = str_replace("</b>","",$summary);
                
                $summary = str_ireplace($original_postcode, strtoupper($original_postcode), $summary);
                preg_match('/(.*\s'.$original_postcode.')/i', $summary, $matches);
				
				if (!isset($matches[1])) {
				continue;
				}
				
				$data = trim($matches[1]);
                $data = str_replace($original_postcode, ", ".$original_postcode, $data);
                $data = str_replace("  ", " ", $data);
                $data = str_replace(" , ", ",", $data);
                $data = str_replace(",,", ",", $data);
                $data = str_replace(", ", ",", $data);
                $data = str_replace(",", ", ", $data);
                $data = str_replace("amp;", "", $data);
                $data = str_replace($original_postcode.", ".$original_postcode, ", ".$original_postcode, $data);
                $data = str_replace(", ,", ",", $data);
                
                $addressParts = explode(", ", $data);
                $addressParts = array_unique($addressParts);
                                                        
                $addressParts[0] = preg_replace("/[0-9]{1,4}\s/", $number." ", $addressParts[0]);
                $addressParts[0] = preg_replace("/\s[0-9]{1,4}/", " ".$number, $addressParts[0]);
                
                $addressParts = array_reverse($addressParts);

                $postcodeLine = array_shift($addressParts);
                
                foreach($addressParts as $addressLine) {
                    if(substr_count($addressLine, " ")<4) {

                    } else {
                            preg_match('/[0-9]{1,3}([^0-9]*)$/', $addressLine, $matches);
                            
							if (!isset($matches[1])) {
							continue;
							}
							
							$addressLine = $matches[1];
                            
                            if(substr_count($addressLine, " ")>4) {
                                    preg_match('/([A-Z]{2,10}\s[A-Z]{2,10})$/i', $addressLine, $matches);
                                    //$addressLine = $matches[1];
									$addressLine = "";
                            }
							
                            $doBreak = true;
                            
                    }
                    $addressLine = preg_replace("/.*Address:/i", "", $addressLine);
                    $addressLine = preg_replace("/^in\s/i", "", $addressLine);
                    $addressLine = str_replace("&amp;", "", $addressLine);
                    $addressLine = str_replace("&quot;", "", $addressLine);
                    $addressLine = str_replace("amp;quot;", "", $addressLine);
                    $addressLine = str_replace("e.g.", "", $addressLine);
                    
                    if($addressLine && !substr_count(strtoupper($addressLine), "FOR SALE") && !substr_count(strtoupper($addressLine), "?") && !substr_count(strtoupper($addressLine), $post_left)) {
                            $address = $addressLine.", ".$address;
                    }
                    if($doBreak) {
                            $doBreak = false;
                            break;
                    }
                }
                if($number && !substr_count($address, $number)) {
                        $address = preg_replace("/^[0-9]{1,4},/", "", $address);
                        $address = $number." ".$address;
                }

                $address = str_replace("  ", " ", $address);
                $address = ucwords(strtolower($address));
                
                if(substr_count($address, ",")>=1 && !substr_count($address, $post_left) && preg_match_all("/[0-9]/i", $address, $dummy)<20) {
                        $addresses[] = trim($address.$postcodeLine);
                }
                if(substr_count($address, ",")>=2 && preg_match_all("/[0-9]/i", $address, $dummy)<20) {
                        $addressParts = explode(", ", $address);
                        array_shift($addressParts);
                        
                        $addressParts[0] = preg_replace("/[0-9]{1,4}\s/", $number." ", $addressParts[0]);
                        
                        $address = implode(", ", $addressParts);
                        $addresses[] = trim($address.$postcodeLine);
                }
                unset($address);
            }
            function sortByLength($a,$b){
                    return strlen($b)-strlen($a);
            }
            if (sizeof($addresses)) {
                usort($addresses,'sortByLength');
                $addresses = array_unique($addresses);
                
                $selectSize = sizeof($addresses)+1;
                if($selectSize>6) {
                        $selectSize = 6;
                }
                foreach($addresses as $i=>$address) {
                    if(sizeof($addresses)>6 && $i>6 && substr_count($address, ", ")==1) {
                            break;
                    }
                    $crap = explode(", ", $address);
                    $crap_size = sizeof($crap);
                    $crap = array_reverse($crap);
                    $labels = array("postcode","county","town");
                    $crap[0] = $original_postcode;
                    foreach($crap as $j=>$plop) {
						$first_line = "";
                        $plop_part[array_shift($labels)] = array_shift($crap);
                        if($j==2) {
                            $crap = array_reverse($crap);
                            if(sizeof($crap)) {
                                $first_line = array_shift($crap);
                            }
                            while(sizeof($crap)) {
                                $first_line .= ", ".array_shift($crap);
                            }

                            $plop_part["address"] = $first_line;
                            break;
                        }
                    }			
					
                    if(!isset($plop_part["address"])) {
                            $plop_part["address"]="";
                    }
                    if(!isset($plop_part["county"])) {
                            $plop_part["county"]="";
                    }
                    if(!isset($plop_part["town"])) {
                            $plop_part["town"]="";
                    }
                    $raw_data[] = $plop_part;
                    unset($plop_part);
                    unset($first_line);
                }
                foreach($raw_data as $w=>$raw_data_item) {
                        $raw_data[$w] = array_reverse($raw_data[$w]);
                }


                //ob_start(); 
                //var_dump($addresses); 
                //$save = ob_get_contents(); 
                //ob_end_clean();
            } else {
                $raw_data[] = array("address" => "Sorry, no results, just enter your address manually", "town" => "", "county" => "", "postcode" => "");
            }
	} else {
		$raw_data[] = array("address" => "Please enter a valid postcode", "town" => "", "county" => "", "postcode" => "");
	
	}

	return $json_data = json_encode($raw_data);
	//$test = json_decode($json_data, true);	
	
	}

} ?>
