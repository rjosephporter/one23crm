<?php

class Messages_model extends CI_Model {

	/*
    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!isset($is_logged_in)) || ($is_logged_in != true) ) {	
			$this->session->set_flashdata('loginRedirect', current_url(). '?' . $_SERVER['QUERY_STRING']);			
			redirect('login');			
		}	
	}
	*/	
	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}			
	
	function listAllMessages() {
		$rows = array();
		$sql = "SELECT m.id, m.sender, m.to, m.subject, m.added_date, m.read_status, m.direction, c.belongs_to, CONCAT(c.company_name, ' ', c.first_name, ' ', c.last_name) as client, c.id as clientid FROM messages m, customer c WHERE m.customer_id=c.id AND c.belongs_to='". $this->session->userdata('group_id') ."' ORDER BY m.added_date DESC";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getMessageDetails($id) {
		$rows = array();
		//$sql = "SELECT * FROM messages WHERE id='". $id ."' LIMIT 1";		
		$sql = "SELECT m.*, m.added_date as messagedate FROM messages m, customer c WHERE m.customer_id=c.id AND c.belongs_user='". $this->session->userdata('user_id') ."' AND m.id='". $id ."' LIMIT 1";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
				return $row;
		}		
		return $rows;
	}	
	
	function getMessageAttachments($message) {
		$rows = array();
		$sql = "SELECT * FROM messages_attachments WHERE message_id='". $message ."'";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function deleteMessage($client, $id) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $id);
		$this->db->delete('messages');
	}
	
	function messageRead($message) {
		$update_array = array(
			'read_status' => 1
		);
		
		$this->db->where('id', $message);
		$this->db->update('messages', $update_array);		
	}	

	function getUnreadMessagesCount() {
		$sql = "SELECT * FROM messages m, customer c WHERE m.customer_id=c.id AND c.belongs_user='". $this->session->userdata('user_id') ."' AND m.read_status='0' AND direction='2'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

} ?>
