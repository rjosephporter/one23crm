<?php

class Client_model extends CI_Model {

	function tagSetForCustomer($id) {
		$rows = array();
		$sql = "SELECT c.tag_id as tag_id, t.tag as tag_name FROM customer_tags c, tags t WHERE c.customer_id='".$id."' AND c.tag_id=t.id ORDER BY c.id ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function GetAllTags() {
		$rows = array();
		$sql = "SELECT * FROM tags WHERE belongs_to='".$this->session->userdata('user_id')."' ORDER BY id ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getOpportunitiesTags() {
		$rows = array();
		$sql = "SELECT * FROM tags_opportunities WHERE belongs_to='".$this->session->userdata('user_id')."' ORDER BY tag ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getTagsForOpportunity($opp) {
		$rows = array();
		$sql = "SELECT * FROM customer_opportunities_tags WHERE opp_id='". $opp ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row['opp_tag'];
			}
		}
		return $rows;
	}

	function clearTagOpportunity($opp) {
		$this->db->where('opp_id', $opp);
		$this->db->delete('customer_opportunities_tags');
	}

	function tabMessages($id) {
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' AND read_status='0' AND direction='2'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function tabPolicies($id) {
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $id ."' AND status='1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function tabOpportunities($id) {
		$sql = "SELECT * FROM customer_opportunities WHERE customer_id='". $id ."' AND status='0'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function addRecentlyViewed($id) {
		$data = array(
			'customer_id' => $id,
			'time' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('user_id')
		);
		$this->db->insert('recently_viewed', $data);
	}

	function getRecentlyViewed($limit) {
		$rows = array();
		//$sql = "SELECT count(recently_viewed.customer_id) as count, recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY count DESC LIMIT 6";
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}


	function getCustomerData($id) {
		$rows = array();
		$sql = "SELECT customer.type as account_type, customer.*, customer_address.* FROM customer LEFT JOIN customer_address on customer.address_id = customer_address.id WHERE customer.id='". $id ."' AND belongs_to='". $this->session->userdata('group_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function doesClientHaveAddress($id) {
		$rows = array();
		$sql = "SELECT address_id FROM customer WHERE address_id IS NOT NULL AND id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function getCustomerList($keywords, $tags) {
		$rows = array();
		$sql = "SELECT customer.*, CONCAT(customer.company_name, '', customer.first_name, ' ', customer.last_name) as customer_name, customer_address.address_line_1, customer_address.town, customer_address.postcode, CONCAT(customer_address.address_line_1, ', ', customer_address.town, ', ', customer_address.postcode) as address, customer_profile_images.image as profile_picture
		FROM customer
		LEFT JOIN customer_address on customer.address_id = customer_address.id
		LEFT JOIN customer_profile_images on customer.profile_image=customer_profile_images.id WHERE ";
		$where = "(customer.type='1' OR customer.type='2' OR customer.type='3')";

		  // if we have keywords, then we have to search
		  if ($keywords) {
		  	$keyword = mysql_real_escape_string($keywords);
		   $where .= " AND (customer.first_name LIKE '%%$keyword%%' OR customer.last_name LIKE '%%$keyword%%' OR customer.company_name LIKE '%%$keyword%%' OR customer_address.postcode LIKE '%%$keyword%%')";
		  }

		  if ($tags) {
		  $tag = mysql_real_escape_string($tags);
		  $where .= " AND customer_tags.tag_id='". $tag ."' AND customer_tags.customer_id=customer.id ";
		  }

		$sql_run = $sql . $where . " AND customer.belongs_to='". $this->session->userdata('group_id') ."' GROUP BY customer.id ORDER BY customer_name";

		$query = $this->db->query($sql_run);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	/*
	function getCustomerList($keywords, $tags) {
		$rows = array();
		$sql = "SELECT customer.*, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as customer_name, customer_address.address_line_1, customer_address.town, customer_address.postcode, CONCAT(customer_address.address_line_1, ', ', customer_address.town, ', ', customer_address.postcode) as address, customer_email.email, customer_telephone.number, customer_profile_images.image as profile_picture FROM (customer, customer_tags)
		LEFT JOIN customer_address on customer.address_id = customer_address.id
		LEFT JOIN customer_email on customer.id=customer_email.customer_id
		LEFT JOIN customer_telephone on customer.id=customer_telephone.customer_id
		LEFT JOIN customer_profile_images on customer.profile_image=customer_profile_images.id WHERE ";
		$where = "(customer.type='1' OR customer.type='2' OR customer.type='3')";

		  // if we have keywords, then we have to search
		  if ($keywords) {
		  	$keyword = mysql_real_escape_string($keywords);
		   $where .= " AND (customer.first_name LIKE '%%$keyword%%' OR customer.last_name LIKE '%%$keyword%%' OR customer_email.email LIKE '%%$keyword%%' OR customer_telephone.number LIKE '%%$keyword%%' OR customer.company_name LIKE '%%$keyword%%' OR customer_address.address_line_1 LIKE '%%$keyword%%' OR customer_address.postcode LIKE '%%$keyword%%' OR customer.organisation LIKE '%%$keyword%%' OR customer.job_title LIKE '%%$keyword%%')";
		  }

		  // if we have keywords, then we have to search
		  if ($tags) {
		  $tag = mysql_real_escape_string($tags);
		  $where .= " AND customer_tags.tag_id='". $tag ."' AND customer_tags.customer_id=customer.id ";
		  }

		$sql_run = $sql . $where . " AND customer.belongs_to='". $this->session->userdata('group_id') ."' GROUP BY customer.id ORDER by customer_name ASC LIMIT 250";

		$query = $this->db->query($sql_run);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}
	*/

	function getCustomerListName($keyword) {
		$rows = array();
		$sql = "SELECT id, CONCAT(company_name, ' ', first_name, ' ', last_name) as customer_name FROM customer WHERE belongs_to='". $this->session->userdata("user_id") ."' AND (first_name LIKE '%%$keyword%%' OR last_name LIKE '%%$keyword%%' OR company_name LIKE '%%$keyword%%') LIMIT 20";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}

			return $rows;

		} else {

			return false;

		}
	}


	function getPartnerInfo($partner) {
		$rows = array();
		$sql = "SELECT CONCAT(first_name, ' ', last_name) as partner_name, id as partner_id FROM customer WHERE id='". $partner ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function insertData($data) {
		$this->db->insert('customer', $data);
	}

	function deleteMainClient($client) {
		$this->db->where('id', $client);
		$this->db->where('belongs_user', $this->session->userdata('user_id'));
		$this->db->delete('customer');
	}

	function deleteClient($client, $person) {
		$this->db->where('id', $person);
		$this->db->where('associated', $client);
		$this->db->where('belongs_user', $this->session->userdata('user_id'));
		$this->db->delete('customer');
	}

	function deleteFile($client, $file) {
		$this->db->where('id', $file);
		$this->db->where('customer_id', $client);
		//$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->delete('customer_files');
	}

	function insertAddressData($data, $clientid) {
		$this->db->insert('customer_address', $data);
		$address_id = $this->db->insert_id();

		// update the customer with the address id
		$updatedata = array('address_id' => $address_id);
		$this->db->where('id', $clientid);
		$this->db->update('customer', $updatedata);

	}

	function updateBackgroundInfo($client, $data) {
		$this->db->where('id', $client);
		$this->db->update('customer', $data);
	}

	function getCustomerNotes($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_notes WHERE customer_id='". $id ."' ORDER BY added_date DESC";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;

	}

	function addDashUpdate($data) {
		$this->db->insert('updates', $data);
	}

	function addCustomerNotes($data) {
		$this->db->insert('customer_notes', $data);
	}

	function deleteCustomerNotes($client, $note) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $note);
		$this->db->delete('customer_notes');
	}

	function deleteTask($task) {
		//$this->db->where('customer_id', $client);
		$this->db->where('id', $task);
		$this->db->delete('customer_tasks');
	}

	function addClientTask($data) {
		$this->db->insert('customer_tasks', $data);
	}

	function completeTask($update, $task, $owner) {
		$this->db->where('belongs_to', $owner);
		$this->db->where('id', $task);
		$this->db->update('customer_tasks', $update);
		return true;
	}

	function updateTask($update, $task, $owner) {
		$this->db->where('belongs_to', $owner);
		$this->db->where('id', $task);
		$this->db->update('customer_tasks', $update);
	}

	function addNewCustomerQuote($data) {
		$this->db->insert('quotes', $data);
		return $this->db->insert_id();
	}

	function addNewCustomerQuoteEmployees($data) {
		$this->db->insert('quotes_employees', $data);
	}

	function addNewCustomerQuoteFiles($data) {
		$this->db->insert('quotes_files', $data);
	}

	function deleteQuote($client, $quote) {
		$this->db->where('ref', $quote);
		$this->db->where('customer_id', $client);
		$this->db->delete('quotes');
	}

	function getCustomerTasks($id) {
		$rows = array();
		$sql = "SELECT * , task_settings.action_name
					FROM customer_tasks
					LEFT JOIN task_settings
					ON
						customer_tasks.customer_id = task_settings.user_id
						OR customer_tasks.task_setting = task_settings.id
					WHERE customer_id='". $id ."' AND status='1' ORDER BY date ASC
		";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getCustomerMessages($id) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' ORDER BY added_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getMessagesAttachments($message) {
		$rows = array();
		$sql = "SELECT * FROM messages_attachments WHERE message_id='". $message ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function sendMessage($client, $client_name, $data) {

		$ref = rand(5,15) . time();

		$type = 0;

		// if the message subject is quote request, then set the type as 1
		if ($this->input->post('subject')=="Quote Request") {
		$type = 1;
		}

		$message_data = array(
				'subject' => $this->input->post('subject') . ' (' . $client_name . ')' . ' [REF:'. $ref .':' . $type . ']',
				'body' => $this->input->post('body'),
				'customer_id' => $client,
				'ref' => $ref,
				'direction' => '1',
				'added_date' => date('Y-m-d H:i:s')
		);

		$this->db->insert('messages', $message_data);
		//$config['smtp_host'] = 'smtp.123-reg.co.uk';
		//$config['smtp_user'] = 'no-reply@123-insureme.co.uk';
		//$config['smtp_pass'] = 'ce3U3pug';
		//$config['smtp_port'] = '25';
		//$config['protocol'] = 'smtp';
		$config['protocol'] = 'sendemail';

		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from('dropbox.13554456@one23.co.uk', 'One23');
		//$this->email->to('stunnin008@yahoo.com');
		//$this->email->cc('steve.warden1@btopenworld.com');
		$this->email->to('jim@le9designs.co.uk');
		$this->email->subject($data['subject'] . ' (' . $client_name . ')' . ' [REF:'. $ref . ':' . $type . ']');
		$this->email->message($data['body']);
		$this->email->send();

	}

	function sendMessageAttachment($data) {
		$message_data = array(
				'subject' => $data['subject'],
				'body' => $data['body'],
				'sender' => $this->session->userdata('name'),
				'direction' => '1',
				'type' => '0',
				'added_date' => date('Y-m-d H:i:s'),
				'customer_id' => $data['client'],
				'to' => $data['to']
		);

		$this->db->insert('messages', $message_data);
		$message_id = $this->db->insert_id();

		$attachment_data = array(
			'message_id' => $message_id,
			'file' => $data['attachment']
		);

		$this->db->insert('messages_attachments', $attachment_data);

		$config['protocol'] = 'sendemail';
		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from('dropbox.13554456@one23.co.uk', $this->session->userdata('name'));
		$this->email->to($data['to']);
		$this->email->subject($data['subject']);
		$this->email->message($data['body']);
		$this->email->attach('./documents/'.$data['attachment']);
		$this->email->send();

	}


	function sendReplyMessage($data) {
		$message_data = array(
				'subject' => $data['subject'],
				'body' => $data['body'],
				'sender' => $this->session->userdata('name'),
				'customer_id' => $data['client'],
				'to' => $data['to'],
				'direction' => '1',
				'type' => '1',
				'added_date' => date('Y-m-d H:i:s')
		);

		$this->db->insert('messages', $message_data);
		$config['protocol'] = 'sendemail';

		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from('dropbox.13554456@one23.co.uk', $this->session->userdata('name'));
		$this->email->to($data['to']);
		$this->email->subject($data['subject']);
		$this->email->message($data['body']);
		$this->email->send();

	}

	function sendQuoteMessage($client_id, $ref, $body, $subject, $to, $sender, $company, $from_email) {

		$message_data = array(
				'subject' => $subject,
				'body' => $body,
				'to' => $company,
				'customer_id' => $client_id,
				'ref' => $ref,
				'direction' => '1',
				'type' => '1',
				'sender' => $sender,
				'added_date' => date('Y-m-d H:i:s')
		);

		// put the message data into database
		$this->db->insert('messages', $message_data);

		//$config['smtp_host'] = 'smtp.123-reg.co.uk';
		//$config['smtp_user'] = 'no-reply@123-insureme.co.uk';
		//$config['smtp_pass'] = 'ce3U3pug';
		//$config['smtp_port'] = '25';
		//$config['protocol'] = 'smtp';
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';

		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from($from_email, $sender);
		$this->email->reply_to('dropbox.13554456@one23.co.uk', $sender);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($body);
		$this->email->send();

	}

	function getIncomingQuotes($id) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' AND direction='2' AND type='1' AND method='1' ORDER BY added_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getOutgoingQuotes($id) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' AND direction='1' AND type='1' AND method='1' ORDER BY added_date DESC LIMIT 10";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getRecentQuotes($id) {
		$rows = array();
		$sql = "SELECT * FROM quotes WHERE customer_id='". $id ."' ORDER BY added_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getQuoteData($ref) {
		$rows = array();
		$sql = "SELECT * FROM quotes WHERE ref='". $ref ."' LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getTelephoneNumberList($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_telephone WHERE customer_id='". $client ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getCustomerURLs($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_url WHERE customer_id='". $client ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getCustomerProfileImage($client) {
		$sql = "SELECT c.profile_image, i.image as image_name FROM customer c, customer_profile_images i WHERE c.id='". $client ."' AND c.belongs_to='". $this->session->userdata('user_id') ."' AND (c.profile_image=i.id) LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getClientMobNumber($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_telephone WHERE customer_id='". $client ."' AND type='Mobile' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getEmailAddressList($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_email WHERE customer_id='". $client ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getWebsiteAddressList($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_url WHERE customer_id='". $client ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getEmployeeQuoteData($id) {
		$rows = array();
		$sql = "SELECT * FROM quotes_employees WHERE quote_id='". $id ."'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getInsurerQuoteList($type) {
		$rows = array();
		$sql = "SELECT * FROM insurers WHERE type='".$type."' AND belongs_to='". $this->session->userdata('user_id') ."'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function updateCustomerQuote($data, $quote) {
		$this->db->where('id', $quote);
		$this->db->update('quotes', $data);
	}

	function removeQuoteEmployees($quote) {
		$this->db->where('quote_id', $quote);
		$this->db->delete('quotes_employees');
	}

	function getQuoteFileData($id) {
		$rows = array();
		$sql = "SELECT * FROM quotes_files WHERE quote_id='". $id ."'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function summaryRecentQuotes($id) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' AND type='1' AND direction='2' AND method='1' ORDER BY added_date DESC LIMIT 5";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function summaryEvents($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_tasks WHERE customer_id='". $id ."' AND status='1' ORDER BY date ASC LIMIT 5";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function summaryLivePolicies($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $id ."' AND status='1' LIMIT 3";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function summaryRecentMessages($id) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $id ."' AND type='0' ORDER BY added_date DESC LIMIT 4";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getAssociatedPeople($id) {
		$rows = array();
		$sql = "SELECT * FROM customer WHERE associated='". $id ."'";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getFamilyForQuote($id) {
		$rows = array();
		$sql = "SELECT * FROM customer WHERE (associated='". $id ."' OR id='". $id ."') AND type!='4'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getClientsPartner($id) {
		$sql = "SELECT * FROM customer WHERE associated='". $id ."' AND relationship='Spouse/Partner' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function updateClient($updated_data, $id) {
		$this->db->where('id', $id);
		$this->db->update('customer', $updated_data);
	}

	function updateClientAddress($updated_data, $id) {
		$this->db->where('customer_id', $id);
		$this->db->update('customer_address', $updated_data);
	}

	function addNewFileToDb($data, $client) {
		$this->db->insert('customer_files', $data);
	}

	function addNewMessageToDb($data, $client) {
		$this->db->insert('messages', $data);
	}

	function addNewMessageToDbAttachments($data) {
		$this->db->insert('messages_attachments', $data);
	}

	function addNewTelephoneToDb($data) {
		$this->db->insert('customer_telephone', $data);
	}

	function clearAllTelephoneNumbers($client) {
		$this->db->where('customer_id', $client);
		$this->db->delete('customer_telephone');
	}

	function addNewEmailToDb($data) {
		$this->db->insert('customer_email', $data);
	}

	function clearAllEmailAddresses($client) {
		$this->db->where('customer_id', $client);
		$this->db->delete('customer_email');
	}

	function addNewUrlToDb($data) {
		$this->db->insert('customer_url', $data);
	}

	function clearAllUrls($client) {
		$this->db->where('customer_id', $client);
		$this->db->delete('customer_url');
	}

	function getFilesForClient($client, $type, $limit) {
		$rows = array();
		$sql = "SELECT * FROM customer_files WHERE customer_id='". $client ."'";

		if ($type) {
		$sql .= " AND type='". $type ."'";
		}

		$sql .= "ORDER BY added_date DESC LIMIT ". $limit ."";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getFilesAllForClient($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_files WHERE customer_id='". $client ."' ORDER BY added_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getFilesPolicyDocuments($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $client ."' AND status='1' AND file!='' ORDER BY renewal_date ASC LIMIT 5";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getQuoteAttachmentFiles($client) {
		$rows = array();
		$sql = "SELECT messages.*, message_attachments.* FROM messages LEFT JOIN message_attachments on customer.id = message_attachments.message_id WHERE direction='2' LIMIT 10";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function addNewEmployeeToDb($data) {
		$this->db->insert('customer_employees', $data);
	}

	function deleteEmployee($client, $employee) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $employee);
		$this->db->delete('customer_employees');
	}

	function deleteFamilyMember($client, $family) {
		$this->db->where('associated', $client);
		$this->db->where('id', $family);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->delete('customer');
	}

	function updateFamilyMember($client, $family, $data) {
		$this->db->where('id', $family);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->where('associated', $client);
		$this->db->update('customer', $data);
	}

	function getFamilyMemberDetails($client, $family) {
		$sql = "SELECT * FROM customer WHERE id='". $family ."' AND belongs_to='". $this->session->userdata('user_id') ."' AND associated='". $client ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getListMainEmployees($id) {
		$rows = array();
		$sql = "SELECT customer.id as customer_id, customer.title, customer.first_name, customer.last_name, customer.dob, customer.job_title, customer_address.address_line_1 as address, customer_address.town as town, customer_address.county as county, customer_address.postcode as postcode FROM customer LEFT JOIN customer_address on customer.address_id = customer_address.id WHERE customer.associated='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getListEmployees($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_employees WHERE customer_id='". $id ."' ORDER BY first_name ASC, last_name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function get_cal_data($year, $month, $client=null) {
			$sql = "SELECT DISTINCT DATE_FORMAT(date, '%Y-%m-%d') AS date FROM customer_tasks WHERE date LIKE '$year-$month%' AND belongs_to='". $this->session->userdata('user_id') ."'";
			if (!empty($client)) { $sql .= " AND customer_id='". $client ."'"; }

			$query = $this->db->query($sql);
			$cal_data = array();

			foreach ($query->result() as $row) { //for every date fetch data
			$a = array();
			$i = 0;
			//$sql2 = "SELECT name FROM customer_tasks WHERE date LIKE DATE_FORMAT('$row->date', '%Y-%m-%d') AND belongs_to='". $this->session->userdata('user_id') ."'";
			$sql2 = "SELECT action, name FROM customer_tasks WHERE date like '". substr($row->date,0,10) ."%' AND customer_tasks.status='1' AND belongs_to='". $this->session->userdata('user_id') ."'";
			if (!empty($client)) { $sql2 .= " AND customer_id='". $client ."'"; }
			$query2 = $this->db->query($sql2);

			foreach ($query2->result() as $r) {
			  $a[$i] = '<span class="label label-info">' . $r->action . '</span><br /><small>'. $r->name .'</small><div style="border-bottom:#999 1px soild;"></div>';     //make data array to put to specific date
			 $i++;

			}
			$cal_data[ltrim(substr($row->date,8,2),'0')] = $a;

			}

	return $cal_data;

	}

	function show_cal($year, $month, $client, $data) {

		$conf = array(
			'start_day' => 'monday',
			'show_next_prev' => true,
			'next_prev_url' => base_url() . 'clients/calender',
			'day_type' => 'long'
		);

		$conf['template'] = '{table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="{previous_url}?id='.$client.'" class="btn">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}" style="font-size:20px;">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="{next_url}?id='.$client.'" class="btn">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr class="day_name">{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr class="days">{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<div class="day_num">{day}</div><div class="day_content">{content}</div>{/cal_cell_content}
		   {cal_cell_content_today}<div class="day_num highlight">{day}</div><div class="day_content">{content}</div>{/cal_cell_content_today}

		   {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}';

		$this->load->library('calendar', $conf);

		return $this->calendar->generate($year, $month, $data);

	}

	function getLivePolicyList($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $id ."' AND status='1'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getPolicyDetails($client, $policy) {
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $client ."' AND id='". $policy ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getArchivedPolicyList($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $id ."' AND status='2'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getAllPolicyList($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies WHERE customer_id='". $id ."' AND status='1' ORDER BY renewal_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getAllPolicyNotes($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_policies_notes WHERE customer_id='". $id ."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function updatePolicy($id, $client, $data) {
		$this->db->where('id', $id);
		$this->db->where('customer_id', $client);
		$this->db->update('customer_policies', $data);
	}

	function getOpportunityList($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_opportunities WHERE customer_id='". $id ."' ORDER BY close_date ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getOpportunity($id) {
		$rows = array();
		$sql = "SELECT * FROM customer_opportunities WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function addNewOpportunity($data) {
		$this->db->insert('customer_opportunities', $data);
	}

	function updateOpportunity($updated_data, $id) {
		$this->db->where('id', $id);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('customer_opportunities', $updated_data);
	}

	function deleteOpportunity($opportunity) {
		$this->db->where('id', $opportunity);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->delete('customer_opportunities');
	}

	function getRecentQuoteAttachments($client) {
		$rows = array();
		$sql = "SELECT messages.*, messages_attachments.* FROM messages LEFT JOIN messages_attachments on messages.id = messages_attachments.message_id WHERE messages.customer_id='". $client ."' AND messages.direction='2' AND messages.type='1' AND messages_attachments.file IS NOT NULL AND messages_attachments.file LIKE '%.pdf%' ORDER BY messages.added_date DESC LIMIT 5";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function addNewPolicy($data) {
		$this->db->insert('customer_policies', $data);
	}

	function addNewClientPartner($data) {
		$this->db->insert('customer', $data);
	}

	function deletePolicy($policy, $client) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $policy);
		$this->db->delete('customer_policies');
	}

	function archivePolicy($policy, $client, $data) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $policy);
		$this->db->update('customer_policies', $data);
	}

	function addPolicyNote($data) {
		$this->db->insert('customer_policies_notes', $data);
	}

	function createMedical($data) {
		$this->db->insert('customer_medical', $data);
	}

	function deleteMedicalReport($client, $report) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $report);
		$this->db->delete('customer_medical');
	}

	function updateMedical($med, $client, $data) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $med);
		$this->db->update('customer_medical', $data);
	}

	function checkMedical($id) {
		$sql = "SELECT id FROM customer_medical WHERE customer_id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getMedical($id, $client) {
		$sql = "SELECT * FROM customer_medical WHERE id='".$id."' AND customer_id='". $client ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getMedicalRecords($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_medical WHERE customer_id='". $client ."' AND belongs_to='". $this->session->userdata('user_id') ."' ORDER BY added_date";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getTask($id) {
		$sql = "SELECT customer_tasks.id,
						customer_tasks.customer_id,
						customer_tasks.task_setting,
						customer_tasks.name,
						customer_tasks.date,
						customer_tasks.end_time,
						customer_tasks.action,
						customer_tasks.remind_mins,
						CONCAT(customer.title,' ',customer.first_name,' ',customer.last_name,' ') as customer_name
				FROM customer_tasks LEFT JOIN customer
				ON customer_tasks.customer_id = customer.id
				WHERE customer_tasks.id='". $id ."'
				AND customer_tasks.belongs_to='". $this->session->userdata('user_id') ."'
				LIMIT 1";
		//echo $sql;
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getIncomeReportList($client) {

		$sql = "SELECT * FROM customer_income_expenditure WHERE customer_id='". $client ."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}

	}

	function getIncomeReport($client, $report) {
		$sql = "SELECT * FROM customer_income_expenditure WHERE id='". $report ."' AND customer_id='". $client ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}

	}

	function addIncome($data) {
		$this->db->insert('customer_income_expenditure', $data);
	}

	function deleteIncomeExpenditure($client, $report) {
		$this->db->where('customer_id', $client);
		$this->db->where('id', $report);
		$this->db->delete('customer_income_expenditure');
	}

	function updateIncome($client, $id, $data) {
		$this->db->where('id', $id);
		$this->db->where('customer_id', $client);
		$this->db->update('customer_income_expenditure', $data);
	}

	function addFactFind($data) {
		$this->db->insert('customer_factfind', $data);
	}

	function getFactFindList($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_factfind WHERE customer_id='". $client ."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getFactFindDetails($client, $report) {
		$sql = "SELECT * FROM customer_factfind WHERE id='". $report ."' AND customer_id='". $client ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getComplianceList($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_compliance WHERE customer_id='". $client ."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function addTagClient($data) {
		$this->db->insert('customer_tags', $data);
	}

	function checkTagClient($tag, $client) {
		$sql = "SELECT * FROM customer_tags WHERE customer_id='". $client ."' AND tag_id='". $tag ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	function RemoveTagClient($client, $tag) {
		$this->db->where('customer_id', $client);
		$this->db->where('tag_id', $tag);
		$this->db->delete('customer_tags');
	}

	function getSMSCredits() {
		$sql = "SELECT * FROM sms_credits WHERE user_id='". $this->session->userdata('user_id') ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row['credits'];
		} else {
			return 0;
		}
	}

	function removeSMSCredits($credits, $used) {
		$data = array('credits' => ($credits-$used));
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->update('sms_credits', $data);
	}

	function getWeblineQuotes($client) {
		$rows = array();
		$sql = "SELECT * FROM webline_quotes WHERE customer_id='". $client ."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getWeblineQuotesMobile($client) {
		$rows = array();
		$sql = "SELECT * FROM webline_quotes WHERE customer_id='". $client ."' ORDER BY id DESC LIMIT 15";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function weblineSaveQuote($data) {
		$this->db->insert('webline_quotes', $data);
		return $this->db->insert_id();
	}

	function getWeblineQuotesResults($quote) {
		$rows = array();
		$sql = "SELECT * FROM webline_quotes_results WHERE quote_id='". $quote ."' AND error!='ERROR' AND premium>'0' ORDER BY premium ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}

		return $rows;
	}

	function getWeblineQuotesDetails($quote) {
		$sql = "SELECT * FROM webline_quotes WHERE id='". $quote ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getWeblineQuotesResult($resp) {
		$sql = "SELECT * FROM webline_quotes_results WHERE response_ref='". $resp ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function deleteWebline($quote, $client, $belongs) {
		$this->db->where('id', $quote);
		$this->db->where('customer_id', $client);
		$this->db->where('belongs_to', $belongs);
		$this->db->delete('webline_quotes');
	}

	function getWeblineQuoteStatus($quote) {
		$sql = "SELECT * FROM webline_quotes WHERE id='". $quote ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row['status'];
		} else {
			return 0;
		}
	}

	function getEmployeeData($id, $employee) {
		$sql = "SELECT * FROM customer_employees WHERE customer_id='". $id ."' AND id='". $employee ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function updateEmployeeData($client, $employee, $data) {
		$this->db->where('id', $employee);
		$this->db->where('customer_id', $client);
		$this->db->update('customer_employees', $data);
	}

	function clientProfileImage($data) {
		$this->db->insert('customer_profile_images', $data);
	}

	function addOpportunityTag($data) {
		$this->db->insert('customer_opportunities_tags', $data);
	}

	function clientProfileImageSet($image, $client) {
		$data = array('profile_image' => $image);
		$this->db->where('id', $client);
		$this->db->where('belongs_to', $this->session->userdata('user_id'));
		$this->db->update('customer', $data);
	}

	function duedilCompanySearch($name) {
		$duedil_url = "http://api.duedil.com/open/search?q=". urlencode($name) ."&api_key=wtxqempevsm84r9tdc362v75";
		//$duedil_url = "http://api.duedil.com/open/uk/company/02341082.json?api_key=wtxqempevsm84r9tdc362v75";
		$response = $this->curl->simple_get($duedil_url);
		if ($response) {
			$json = json_decode($response, true);
			//print_r($json);
			$return = array();
			foreach($json['response']['data'] as $result) {
				$return[] = array('name'=> $result['name'], 'number' => $result['company_number']);
			}
			return json_encode($return);
		} else {
			return false;
		}

	}

	function duedilCompanyInfo($number) {
		$duedil_url = "http://api.duedil.com/open/uk/company/". urlencode($number) .".json?api_key=wtxqempevsm84r9tdc362v75";
		$response = $this->curl->simple_get($duedil_url);
		if ($response) {
			return json_encode($response);
		} else {
			return false;
		}

	}

	function complianceCustomerEmails($client) {
		$rows = array();
		$sql = "SELECT * FROM messages WHERE customer_id='". $client ."' AND type='0' AND method='1'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getFileDetails($id, $client) {
		$sql = "SELECT * FROM customer_files WHERE customer_id='". $client ."' AND id='". $id ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getMessageDetails($id, $client) {
		$sql = "SELECT * FROM messages WHERE customer_id='". $client ."' AND id='". $id ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function createComplianceReport($data) {
		$this->db->insert('customer_compliance', $data);
	}

	function createComplianceFactfind($data) {
		$this->db->insert('customer_compliance_factfind', $data);
	}

	function createComplianceIncome($data) {
		$this->db->insert('customer_compliance_income', $data);
	}

	function createComplianceMedical($data) {
		$this->db->insert('customer_compliance_medical', $data);
	}

	function createComplianceFile($data) {
		$this->db->insert('customer_compliance_files', $data);
	}

	function createComplianceEmail($data) {
		$this->db->insert('customer_compliance_emails', $data);
	}

	function getComplianceReport($client,$report) {
		$sql = "SELECT * FROM customer_compliance WHERE customer_id='". $client ."' AND id='". $report ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getComplianceReportItems($client, $report, $table) {
		$rows = array();
		$sql = "SELECT * FROM ". $table ." WHERE customer_id='". $client ."' AND report_id='".$report."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getComplianceReportFiles($client, $report, $doc) {
		$rows = array();
		$sql = "SELECT * FROM customer_compliance_files WHERE customer_id='". $client ."' AND report_id='".$report."' AND type='". $doc ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getMultiquoteComparison($client, $batch) {
		$rows = array();
		$sql = "SELECT product_line, product, bestprice_benefit, bestprice_product, bestprice_provider, bestprice_price, life1, life2 FROM webline_quotes WHERE customer_id='". $client ."' AND multiquote='".$batch."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getMultiquoteComparisonByTerm($client, $batch, $term) {
		$rows = array();
		$sql = "SELECT product_line, product, life1, life2, bestprice_provider, bestprice_price, bestprice_benefit, bestprice_deathcic, bestprice_product FROM webline_quotes WHERE customer_id='". $client ."' AND multiquote='".$batch."' AND term_years='". $term ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getMultiquoteComparisonTerms($client, $batch) {
		$rows = array();
		$sql = "SELECT DISTINCT term_years FROM webline_quotes WHERE customer_id='". $client ."' AND multiquote='".$batch."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function saveMultiQuteRef($data) {
		$this->db->insert('webline_multi_quote_ref', $data);
	}

	function getMultiquoteDetails($ref) {
		$sql = "SELECT * FROM webline_multi_quote_ref WHERE ref='". $ref ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function doVMDLogin($user, $pass) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://www.viewmydocuments.co.uk/api/providerAuth");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "provider_user=". $user ."&provider_pass=". $pass);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		if ($response) {
			$results = json_decode($response, true);
			return $results['token'];
		} else {
			return false;
		}
	}

	function getVMD() {
		$sql = "SELECT vmd_user, vmd_pass FROM users_groups WHERE group_id='". $this->session->userdata('group_id') ."' AND vmd_user!='' and vmd_pass!='' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getVMDAccount($client) {
		$sql = "SELECT vmd, vmd_pin FROM customer WHERE id='". $client ."' AND belongs_to='". $this->session->userdata('group_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function createVMDAccount($client, $title, $first_name, $last_name, $email, $postcode) {
		// log in first
		$login_details = $this->getVMD();
		if (!$login_details) {
			exit('No view my documents login details found');
		}
		$login = $this->doVMDLogin($login_details['vmd_user'], $login_details['vmd_pass']);
		// send over request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://www.viewmydocuments.co.uk/api/searchClient");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "token=". $login ."&client_title=". $title ."&client_fname=". $first_name ."&client_sname=". $last_name ."&client_email=". $email ."&client_postcode=". $postcode);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		if ($response) {
			$results = json_decode($response, true);
			return $results;
		} else {
			return false;
		}
	}

	function updateVMDRecord($client, $ref, $pin) {
		$data = array('vmd' => $ref, 'vmd_pin' => $pin);
		$this->db->where('id', $client);
		$this->db->update('customer', $data);
	}

	function updateVMDRecordRemove($client) {
		$data = array('vmd' => '', 'vmd_pin' => '');
		$this->db->where('id', $client);
		$this->db->update('customer', $data);
	}

	function uploadVMDDocument($client, $file, $name, $notes) {
		// log in first
		$login_details = $this->getVMD();
		if (!$login_details) {
			exit('No view my documents login details found');
		}
		$login = $this->doVMDLogin($login_details['vmd_user'], $login_details['vmd_pass']);
		// get the file location and create array
		$post = array(
			'token' => $login,
			'client_ref' => $client['vmd'],
			'file_name' => $name,
			'file_text' => $notes,
			'file' => '@/home/onecouk/www/123crm.co.uk/documents/' . $file
		);
		// send over request
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.viewmydocuments.co.uk/api/uploadDocument");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		if ($response) {
			$response = json_decode($response, true);
			return array('ref' => $response['ref'], 'url' => $response['url']);
		} else {
			return false;
		}
	}

	function createVMDShared($data) {
		$this->db->insert('view_my_docs_shared', $data);
	}

	function getViewMyDocsSharedList($user) {
		$rows = array();
		$sql = "SELECT * FROM view_my_docs_shared WHERE user_id='". $user ."' ORDER BY time DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getViewMyDocsUploadedList($ref) {
		$vmd = $this->getVMD();
		$rows = array();
		$sql = "SELECT * FROM view_my_docs_uploaded WHERE ref='". $ref ."' AND provider='". $vmd['vmd_user'] ."' ORDER BY time DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getEmailTemplates() {
		$rows = array();
		$sql = "SELECT * FROM email_templates WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getEmailTemplate($id) {
		$sql = "SELECT * FROM email_templates WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getEmailSignature($id) {
		$sql = "SELECT body FROM email_signatures WHERE id='". $id ."' AND belongs_to='". $this->session->userdata('user_id') ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getEmailSignatures() {
		$rows = array();
		$sql = "SELECT * FROM email_signatures WHERE belongs_to='". $this->session->userdata('user_id') ."' ORDER BY name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getEmailAttachmentsListDocLibOwn() {
		$rows = array();
		$sql = "SELECT CONCAT('documents/library/own/', filename) AS file, name FROM document_library_own WHERE belongs_to='".$this->session->userdata('user_id')."' AND active='1' ORDER BY name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getEmailAttachmentsListDocLib() {
		$rows = array();
		$sql = "SELECT document_library_doc.*, document_library_category.name AS cat FROM document_library_doc LEFT JOIN document_library_category ON document_library_doc.category=document_library_category.id WHERE document_library_doc.active='1' ORDER BY cat ASC, document_library_doc.name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getEmailAttachmentsList($client) {
		$list = ""; // empty

		if (isset($client)) {

		// get list of files
		$files = $this->getFilesAllForClient($client);
		if ($files) {
			$list .= '<optgroup label="Client Files">';
			foreach ($files as $file) {
				$list .=  '<option value="documents/'. $file['filename'] .'">'. $file['name'] .'</option>';
			}
			$list .= '</optgroup>';
		}

		}

		// get list of own docs in library
		$doc_lib_own = $this->getEmailAttachmentsListDocLibOwn();
		if ($doc_lib_own) {
			$list .= '<optgroup label="Document Library">';
			foreach ($doc_lib_own as $dlo) {
				$list .=  '<option value="'. $dlo['file'] .'">'. $dlo['name'] .'</option>';
			}
			$list .= '</optgroup>';
		}

		return $list;
	}

	function weblineMultiProductSaveQuote($data) {
		$this->db->insert('webline_multiproduct_quotes', $data);
		return $this->db->insert_id();
	}

	function weblineMultiProductQuotes($ref) {
		$rows = array();
		$sql = "SELECT * FROM webline_multiproduct_quotes WHERE multiproduct='". $ref ."' ORDER BY product_line ASC";
		$query = $this->db->query($sql);
		$quote_counter = 0;
		$result_counter = 0;
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = array('quote_product' => $row['product_line'], 'quote_id' => $row['id'], 'collection_type' => $row['collection_type']);
				$sql_results = "SELECT * FROM webline_multiproduct_quotes_results WHERE quote_id='". $row['id'] ."' AND error!='ERROR' AND premium>0 ORDER BY premium ASC";
				$query_results = $this->db->query($sql_results);
				if($query_results->num_rows() > 0) {
					foreach($query_results->result_array() as $row_results) {
						$rows[$quote_counter]['quotes'][] = array('result_id' => $row_results['result_id'], 'product' => $row_results['product'], 'provider' => $row_results['provider'], 'premium' => $row_results['premium'], 'benefit' => $row_results['benefit'], 'rates' => $row_results['rates']);
					}
				}
			$quote_counter++;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function weblineMultiProductQuote($id) {
		$sql = "SELECT * FROM webline_multiproduct_quotes_results WHERE result_id='". $id ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function saveWeblineMultiProductQuote($data) {
		$this->db->insert('webline_multiproduct_quotes_saved', $data);
	}

	function weblineMultiProductQuotesView($quote) {
		$rows = array();
		$counter = 0;
		$counter2 = 0;
		$sql = "SELECT multiquote_id FROM webline_multiproduct_quotes_saved WHERE quote_id='". $quote ."' GROUP BY multiquote_id";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$quote_sql = "SELECT * FROM webline_multiproduct_quotes WHERE id='". $row['multiquote_id'] ."'";
				$quote_query = $this->db->query($quote_sql);
				//echo $quote_query->num_rows();
				foreach($quote_query->result_array() as $quote_row) {

					$rows[$counter] = array('product_line' => $quote_row['product_line'], 'life1' => $quote_row['life1'], 'life2' => $quote_row['life2'], 'date' => $quote_row['added_date']);

					$result_sql = "SELECT * FROM webline_multiproduct_quotes_saved WHERE multiquote_id='". $quote_row['id'] ."'";
					$result_query = $this->db->query($result_sql);
					foreach($result_query->result_array() as $result_row) {

						$rows[$counter]['quotes'][] = array('product' => $result_row['product'], 'provider' => $result_row['provider'], 'provider_logo' => $result_row['provider_logo'], 'premium' => $result_row['premium'], 'response_ref' => $result_row['response_ref'], 'benefit' => $result_row['benefit'], 'rates' => $result_row['rates'], 'apply_online' => $result_row['apply_online'], 'response_guid' => $result_row['response_guid'], 'product_id' => $result_row['product_id'], 'collection_type' => $result_row['collection_type'], 'notes' => $result_row['notes']);

					}


				}
				$counter++;
			} // foreach group
			return $rows;
		} else {
			return false;
		}
	}

	function getWeblineMultiProductQuoteWithRef($ref) {
		$sql = "SELECT multiquote_id FROM webline_multiproduct_quotes_saved WHERE response_ref='". $ref ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			$quote_sql = "SELECT * FROM webline_multiproduct_quotes WHERE id='". $row['multiquote_id'] ."' LIMIT 1";
			$quote_query = $this->db->query($quote_sql);
			if($quote_query->num_rows() > 0) {
				$quote_row = $quote_query->row_array();
				return $quote_row;
			}
		} else {
			return false;
		}
	}

	function getWeblineMultiQuotesResultRef($ref) {
	$sql = "SELECT * FROM webline_multiproduct_quotes_saved WHERE response_ref='". $ref ."' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getMultiQuoteStatus($quote) {
		$sql = "SELECT ref FROM webline_multiproduct_quotes_processed WHERE ref='". $quote ."'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function updateFilesName($id, $client, $data) {
		$this->db->where('id', $id);
		$this->db->where('customer_id', $client);
		$this->db->update('customer_files', $data);
	}

	function getBMILoadingRates($bmi, $age) {
		$rows = array();
		$sql = "SELECT * FROM bmi_loading_data LEFT JOIN bmi_loading_provider on bmi_loading_data.provider = bmi_loading_provider.id WHERE bmi_loading_data.bmi='". $bmi ."' AND ".$age." BETWEEN age_from AND age_to ORDER BY bmi_loading_provider.provider";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getGroupUsersList() {
		$rows = array();
		$sql = "SELECT users_to_groups.*, users.first_name, users.last_name, users.username FROM users_to_groups LEFT JOIN users ON users_to_groups.user_id=users.id WHERE users_to_groups.group_id='". $this->session->userdata("group_id") ."' AND user_id!='". $this->session->userdata("user_id") ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}
		return $rows;
	}

	function getCustomTabsList() {
		$rows = array();
		$sql = "SELECT name, id as tab_ref FROM users_custom_tabs WHERE user_id='". $this->session->userdata("user_id") ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getCustomTab($user, $tab) {
	$sql = "SELECT * FROM users_custom_tabs WHERE user_id='". $user ."' AND id='". $tab ."' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getCustomTabData($custom, $customer, $section) {
		$rows = array();
		$sql = "SELECT * FROM users_custom_tabs_data WHERE custom='". $custom ."' AND customer_id='". $customer ."' AND section='". $section ."' ORDER BY added_date DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function addCustomNotes($data) {
		$this->db->insert('users_custom_tabs_data', $data);
	}

	function removeCustomEntry($id, $custom) {
		$this->db->where('id', $id);
		$this->db->where('custom', $custom);
		$this->db->delete('users_custom_tabs_data');
	}

	function getCustomTabForm($user, $form) {
		$rows = array();
		$sql = "SELECT * FROM users_custom_forms_build WHERE form_id='". $form ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getProtectionCalcReports($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_protection_calculations WHERE customer_id='". $client ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function fetchDetailsFromIncomeExpend($client) {
	$sql = "SELECT * FROM customer_income_expenditure WHERE customer_id='". $client ."' ORDER BY id DESC LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function saveProtectionCalc($data) {
		$this->db->insert('customer_protection_calculations', $data);
		return $this->db->insert_id();
	}

	function saveProtectionCalcQuote($data) {
		$this->db->insert('customer_protection_calculations_quotes', $data);
	}

	function getProtectionCalcReport($client, $report) {
	$sql = "SELECT * FROM customer_protection_calculations WHERE customer_id='". $client ."' AND id='". $report ."' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getProtectionCalcQuotes($report) {
		$rows = array();
		$sql = "SELECT * FROM customer_protection_calculations_quotes WHERE calc_ref='". $report ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return false;
		}
	}

	function getCustomFileSections() {
	$sql = "SELECT files_1, files_2, files_3, files_4, files_5, files_6 FROM users WHERE id='". $this->session->userdata('user_id') ."' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function deleteProtectionCalcReport($client, $report) {
		$this->db->where('id', $report);
		$this->db->where('customer_id', $client);
		$this->db->delete('customer_protection_calculations');
	}

	function getClientTwitterAccount($client) {
	$sql = "SELECT url FROM customer_url WHERE customer_id='". $client ."' AND website='Twitter' LIMIT 1";
	$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			$row = $row['url'];
			$row = rtrim($row, '/');
			$explode = explode('/', $row);
			return end($explode);
		} else {
			return false;
		}
	}

	function getCompanyLogo() {
		$sql = "SELECT logo FROM users_groups WHERE group_id='". $this->session->userdata("group_id") ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row['logo'];
		} else {
			return false;
		}
	}

	public function clientTypeAheadFormatJson()
	{
		$parse 		= $this->getCustomerList('','');
		$typehead 	= array();

		foreach($parse as $parse_key => $parse_val)
		{
			$typehead[] = array('id'=>$parse_val['id'],'Name'=>$parse_val['customer_name']);
		}
		return json_encode($typehead);
	}

}
