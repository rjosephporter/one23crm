<?php

class Admin_model extends CI_Model {

	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function clientList() {
		$rows = array();
		$sql = "SELECT users.*, subscriptions.expiry FROM users LEFT JOIN subscriptions on users.id=subscriptions.user_id ORDER BY first_name";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;	
	}
	
	function getClientDetails($id) {
		$sql = "SELECT * FROM users WHERE id='". $id ."' LIMIT 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row;
		} else {
			return false;
		}
	}
	
	function clientsClientList($id) {
		$rows = array();
		$sql = "SELECT * FROM customer WHERE belongs_user='". $id ."' ORDER BY first_name";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;	
	}	
	
	
} ?>
