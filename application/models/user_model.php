<?php

class User_model extends CI_Model {

	/*
    function __construct()
    {
        parent::__construct();
		$this->is_logged_in();
    }

	function is_logged_in() {
		$is_logged_in = $this->session->userdata('is_logged_in');		
		if ( (!empty($is_logged_in)) && ($is_logged_in = true) ) {				
			redirect('dashboard');			
		}	
	}
	*/

	function checkSubscription($id) {
		$sql = "SELECT * FROM subscriptions WHERE user_id='". $id ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}
	
	function checkGroupSubscription($group) {
		// get the manager id from the group
		$sql_group_id = "SELECT * FROM users_groups WHERE group_id='". $group ."' LIMIT 1";
		$group_query = $this->db->query($sql_group_id);
		if($group_query->num_rows() > 0) {
			$group_details = $group_query->row_array();
			$sql = "SELECT * FROM subscriptions WHERE user_id='". $group_details['manager_id'] ."' LIMIT 1";
			$query = $this->db->query($sql);		
			if($query->num_rows() > 0) {
				return $query->row_array();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}	

	function doauth() {
		//$this->db->where('username', $this->input->post('username'));

		/* 08.15.2014 @rjosephporter */
		$username = $this->db->escape_str($this->input->post('username'));
		$this->db->where('(username = "'.$username.'" OR email = "'.$username.'")');

		$this->db->where('password', md5($this->input->post('password')));
		$this->db->where('active', 1);
		$query = $this->db->get('users');
		
		$num = $query->num_rows;
	
		if ($query->num_rows == 1) {		
			$row = $query->row_array();
			return $row;		
		} else {
			return false;
		}	
	
	}
	
	function recordLogin($data) {
		$this->db->insert('logins', $data);
	}
	
	function createNewUser($details) {
		$this->db->insert('users', $details);
		return $this->db->insert_id();
	}
	
	function addNewGroup($user) {
		$data = array(
			'manager_id' => $user,
			'added_date' => date('Y-m-d H:i:s')
		);
		$this->db->insert('users_groups', $data);
		return $this->db->insert_id();
	}
	
	function addUser2Group($user, $group) {
		$data = array(
			'group_id' => $group,
			'user_id' => $user,
			'role' => 1
		);
		$this->db->insert('users_to_groups', $data);
	}		
	
	function getGroupPermissions($user) {
		$sql = "SELECT * FROM users_permissions WHERE user_id='". $user ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}
	
	/*
	function sendConfirmMessage($to, $body) {
	
		//$config['smtp_host'] = 'smtp.123-reg.co.uk';
		//$config['smtp_user'] = 'no-reply@123-insureme.co.uk';
		//$config['smtp_pass'] = 'ce3U3pug';
		//$config['smtp_port'] = '25';	
		//$config['protocol'] = 'smtp';
		$config['protocol'] = 'sendmaik';
		$config['mailtype'] = 'html';	
	
		$this->load->library('email');
		$this->email->initialize($config);		
		$this->email->from('no-reply@one23.co.uk', 'no-reply@one23.co.uk');
		$this->email->to($to); 
		$this->email->subject('Please confirm your account');
		$this->email->message($body);	
		$this->email->send();	
	} */
	
	function checkCode($code) {
		$sql = "SELECT id FROM users WHERE confirm_code='". mysql_real_escape_string($code) ."' AND active='2' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function getGroupDetails($id) {
		$sql = "SELECT * FROM users_to_groups WHERE user_id='".$id ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function getClientDetails($id) {
		$sql = "SELECT * FROM users WHERE id='". mysql_real_escape_string($id) ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}		
	
	function getTabSettings($id) {
		$sql = "SELECT * FROM users_tabs WHERE user_id='".$id ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}	
	}
		
	function activateUser($user) {
		$data = array(
			'active' => 1,
			'confirmed_date' => date('Y-m-d H:i:s')
		);
		$this->db->where('id', $user);
		$this->db->update('users', $data);
	}
	
	function createSubscription($id, $expiry) {
		$data = array(
			'user_id' => $id,
			'date' => date('Y-m-d H:i:s'),
			'expiry' => $expiry
		);
		$this->db->insert('subscriptions', $data);
	}
	
	function addSMSCredits($id, $credits) {
		$data = array(
			'user_id' => $id,
			'credits' => $credits
		);
		
		// insert
		$this->db->insert('sms_credits', $data);
	}	
	
	function updatePassword($user, $password) {
		$data = array('password' => md5($password));
		$this->db->where('id', $user);
		$this->db->update('users', $data);
	}
	
	function checkEmail($email) {
		$sql = "SELECT id, email FROM users WHERE email='". mysql_real_escape_string($email) ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}	

	function sendNewPassword($to, $emailbody) {	
		$config['smtp_host'] = 'mail.123crm.co.uk';
		$config['smtp_user'] = 'no-reply@123crm.co.uk';
		$config['smtp_pass'] = '}T_!ydb-RQ3a';
		$config['smtp_port'] = '26';	
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';	
		$this->load->library('email');
		$this->email->initialize($config);		
		$this->email->from('no-reply@123crm.co.uk', 'no-reply@123crm.co.uk');
		$this->email->to($to); 
		$this->email->subject('Your new password');
		$this->email->message($emailbody);	
		$this->email->send();
	}
	
	function sendNormalEmail($to, $subject, $emailbody) {	
		$config['smtp_host'] = 'mail.123crm.co.uk';
		$config['smtp_user'] = 'no-reply@123crm.co.uk';
		$config['smtp_pass'] = '}T_!ydb-RQ3a';
		$config['smtp_port'] = '26';	
		$config['protocol'] = 'smtp';
		$config['mailtype'] = 'html';	
		$this->load->library('email');
		$this->email->initialize($config);		
		$this->email->from('no-reply@123crm.co.uk', 'no-reply@123crm.co.uk');
		$this->email->to($to); 
		$this->email->subject($subject);
		$this->email->message($emailbody);	
		$this->email->send();
	}	
	
	function getFullGroupDetails($groupid) {
		$sql = "SELECT * FROM users_groups WHERE group_id='". $groupid ."' LIMIT 1";
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}

	/**
	 * Check if sns user exists
	 *
	 * @author 	rjosephporter
	 * @date 	08.08.2014
	 * @param 	string 		sns network (facebook, twitter, linkedin, google)
	 * @param 	string 		sns id
	 */
	function isExistSnsAccount($network, $id) 
	{
		$result = array();
		/* 08.11.2014 @rjosephporter
		$query = $this->db->select('*')
							->from('users')
							->where('sns_type', $network)
							->where('sns_id', $id)
							->limit(1)
							->get()
							->row_array();
		*/
		$query = $this->db->select('*')
							->from('users_sns')
							->where('sns_type', $network)
							->where('sns_id', $id)
							->limit(1)
							->get()
							->row_array();

		$result['count'] = count($query);
		//$result['user_data']  = $query; 08.11.2014 @rjosephporter
		if(count($query) > 0)
			$result['user_data'] = $this->getClientDetails($query['user_id']);

		return $result;
	}

	/**
	 * Create new sns account for user
	 *
	 * @author 	rjosephporter
	 * @date 	08.11.2014
	 *
	 */
	function createSnsAccount($user_id, $sns_type, $sns_id)
	{
		$isExist = $this->isExistSnsAccount($sns_type,$sns_id);
		if($isExist['count'] == 0) {
			$data = array(
				'user_id'	=> $user_id,
				'sns_type'	=> $sns_type,
				'sns_id'	=> $sns_id
			);
			$query = $this->db->insert('users_sns',  $data);

			return $query;
		}
	}

	function getUserSnsAccounts($user_id)
	{
		$query = $this->db->from('users_sns')->where('user_id', $user_id)->get();

		return $query->result_array();
	}

	function isUserParent($user_id)
	{
		$query = $this->db->from('users_to_groups')->where('user_id', $user_id)->limit(1)->get();

		$result = $query->row_array();

		return ($result['role'] == 1) ? true : false;
	}

} ?>
