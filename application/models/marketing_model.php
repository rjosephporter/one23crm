<?php

class Marketing_model extends CI_Model {
	
	function getRecentlyViewed($limit) {
		$rows = array();
		$sql = "SELECT recently_viewed.customer_id, CONCAT(customer.company_name, ' ', customer.first_name, ' ', customer.last_name) as client_name, MAX(time) as time FROM recently_viewed LEFT JOIN customer on recently_viewed.customer_id = customer.id WHERE customer.belongs_to='". $this->session->userdata('group_id') ."' AND recently_viewed.user_id='". $this->session->userdata('user_id') ."' GROUP BY recently_viewed.customer_id ORDER BY time DESC LIMIT ". $limit;		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}
	
	function getClientsWithSMS($tag) {
		$clients = array();
		$sql = "SELECT customer.id, customer.title, customer.first_name, customer.last_name FROM (customer, customer_tags) WHERE";
		
		  if ($tag) {
		  $sql .= " customer_tags.tag_id='". $tag ."' AND customer_tags.customer_id=customer.id AND ";		 
		  }		
		
		$sql .= " customer.belongs_user='".$this->session->userdata('user_id')."' AND customer.type='1' GROUP BY customer.id ORDER BY customer.last_name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
			
				// get number
				$mob = $this->getClientMobNumber($row['id']);
				if ($mob) {				
					$clients[] = array(
						'id' => $row['id'],
						'title' => $row['title'],
						'first_name' => $row['first_name'],
						'last_name' => $row['last_name'],
						'number' => $mob
					);
				}
			}
		}		
		return $clients;		
	}
	
	function getClientMobNumber($client) {
		$rows = array();
		$sql = "SELECT * FROM customer_telephone WHERE customer_id='". $client ."' AND type='Mobile' LIMIT 1";		
		$query = $this->db->query($sql);		
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return '44' .substr($row['number'], 1);
		} else {
			return false;
		}
	}	
	
	function GetAllTags() {
		$rows = array();
		$sql = "SELECT * FROM tags WHERE belongs_to='".$this->session->userdata('user_id')."' ORDER BY id ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			foreach($query->result_array() as $row) {
				$rows[] = $row;
			}
		}		
		return $rows;
	}

	function getSMSCreditCount() {
		$sql = "SELECT SUM(credits) as sms FROM sms_credits WHERE user_id='". $this->session->userdata('user_id') ."'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0) {
			$row = $query->row_array();
			return $row['sms'];
		} else {
			return 0;
		}
	}
	
	function spendCredit($credits) {
		$this->db->set('credits', 'credits-'.$credits, FALSE);
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$this->db->update('sms_credits');
	}
	
	function sendSms($number, $message) {
		// number of message characters
		$characters = strlen($message);
		$used_credits = ceil($characters/160);
		
		// remove credit(s)
		$this->spendCredit($used_credits);
		
		// send message
		$this->curl->create("http://api.txtlocal.com/send/");
		$topost = array(
			'username' => 'steve.warden1@btopenworld.com',
			'hash' => 'eeb7b8ed5a3665863a492baecd6923dca108c429',
			'message' => $message,
			'sender' => '123InsureMe',
			'numbers' => $number,
			'test' => true	
		);		
		$this->curl->post($topost);
		$response = $this->curl->execute();	
	}

			
	
	
} ?>
