/*
 *
 * This is not a good solution
 * Check for the revise code on /common/main_header.php
 * - Allan
pathArray = window.location.href.split( '/' );
protocol = pathArray[0];
host = (pathArray[2].indexOf("localhost") > -1) ? pathArray[2] + '/' + pathArray[3] : pathArray[2] ;
var baseURL = protocol + '//' + host + '/';
*/
$(document).ready(function(){

	//var baseURL = "http://localhost/123crm/";
	//var baseURL = "http://www.123crm.co.uk/";
	//var baseURL = "http://123fs.co.uk/";
	$("#cssmenu,#cssmenu2").tooltip({
		'selector': "a[rel=tooltip]"
	});

	$('#modalCreateClientSelect').click(function() {

		if ($("#type").val()==0) {
			return false;
		}

	});

	$('#modalCreateClientNote').click(function() {

		if ($("#note").val()==0) {
			return false;
		}

	});

	$('#modalUploadNewFile').click(function() {
		if ($("#name").val()==0) {
			return false;
		}
	});

	$('#modalSendSMS').click(function() {
		if ($("#number").val()==0) {
			return false;
		}
	});

	$('#modalCreateClientTask').click(function() {
		if ($("#task_action").val()==0) {
			return false;
		}
	});

	$('#dob,#dob2,#partner_dob,#lifea_dob,#lifeb_dob').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '1920:+0'
	});

	$('#task_date,#edit_task_date').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '2013:+3'
	});

	$('#close_date').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '2013:+3'
	});

	$('#date,#discussiondate,#pmi_renewal_date,#home_date, #travel_renewal').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '2013:+1'
	});

	$('#start').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '2000:+0'
	});

	$('#renewal').datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: '2013:+10'
	});

	$("a[data-toggle=modaledit]").click(function() {
		var task_id = $(this).attr('id');
		$.ajax({
			type: 'post',
			cache: false,
			url: baseURL+'clients/getTaskDetails',
			data: 'task='+task_id,
			dataType: "json",
			success: function(data) {
				/*$("#edit_task_name").val(data.name);
				$("#edit_task_id").val(data.id);
				$("#edit_task_action").val(data.action);
				$("#edit_task_date").val(data.date);
				$("#edit_task_hour").val(data.hour);
				$("#edit_task_min").val(data.mins);

				$("#edit_task_hour_end").val(data.end_hr);
				$("#edit_task_min_end").val(data.end_min);*/
				$("#edit_task_name").val(data.name);
				$("#edit_task_id").val(data.id);
				$("#edit_task_action").val(data.action);
				$("#edit_task_date").val(data.date);
				$("#edit_task_hour").val(data.hour);
				$("#edit_task_min").val(data.mins);
				$("#edit_task_hour_end").val(data.end_hr);
				$("#edit_task_min_end").val(data.end_min);
				$("#edit_task_remind").val(data.remind);
				$("#edit_client_task").val(data.customer_id);
				$(".editgetclient").val(data.customer_name);
				//console.log(data);
				$('#editTask').modal('show');
			},
			error: function() {
				alert("Error, can not edit task.");
			}
		});
	});

	$("#completeTask").click(function() {
		var task_id = $("#edit_task_id").val();
		$.ajax({
			type: "POST",
			url: baseURL+"clients/cal_complete_task",
			data: { task: task_id },
			dataType: 'text',
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not complete this task");
			}
		});
	});

	$("#deleteTask").click(function() {
		var task_id = $("#edit_task_id").val();
		$.ajax({
			type: "POST",
			url: baseURL+"clients/delete_task",
			data: { task: task_id },
			success: function() {
				location.reload(true);
			},
			error: function() {
				//alert("can not delete this task");
			}
		});
	});

	function getUnreadMessages() {
		$.ajax({
			type: "GET",
			url: baseURL+"messages/messageUnread",
			dataType: "text",
			cache: false,
			success: function(data) {
				//alert(data);
				if (data=="0") {
					$("#messageUnread").html("");
					$("#mini_header_message").html("");
				} else {
					$("#messageUnread").html(data);
					$("#mini_header_message").html(data);
				}
			},
			error: function(error) {
				//console.log(error);
			}
		});
	}

	// run on page load
	getUnreadMessages();
	// run every min
	setInterval(getUnreadMessages, 60000);

	// hide tag selection of page load
	$("#client_tag_selection").hide();
	$("#client_tag_area_edit").hide();

	// if edit tag button clicked show the selecton area
	$("#showEditClientTags").click(function() {
	$("#client_tag_selection").show();
	$("#client_tag_area").hide();
	$("#client_tag_area_edit").show();
	});

	// if close tag button clicked show the selecton area
	$("#closeClientTags").click(function() {
	$("#client_tag_selection").hide();
	//$("#showEditClientTags").show();
	$("#client_tag_area").show();
	$("#client_tag_area_edit").hide();
	});

	// if the add tag is sumbitted
	$("#add_client_tag").submit(function() {
		var tag = $("#add_client_tag_ref").val();

		if (tag!="") {
			$.ajax({
			  url: baseURL+"clients/add_tag_client",
			  type: "POST",
			  data: $("#add_client_tag").serialize(),
			  success: function(msg){
				  location.reload(true);
				},
			  error:function(){
				  alert("Error adding tag, please try again");
				}
			});
		}
	return false;

	});

	$(".removeClientTag").click(function() {
		var tag = $(this).data('tag');
		$.ajax({
		  url: baseURL+"clients/remove_tag_client",
		  type: "POST",
		  data: {tag: tag},
		  success: function(msg){
			  location.reload(true);
			},
		  error:function(){
			  alert("Error removing tag, please try again");
			}
		});
	});

	$("#search_keyword").focus(function(){
		$("#globalSearchResults").removeClass("hide");
	});

	$("#globalSearchResults").click(function(){
		$("#globalSearchResults").addClass("hide");
	});

	$("#search_keyword").keyup(function () {
		var globalSearchMinLength = 2;
		var globalsearch = $("#search_keyword").val();
		if (globalsearch.length > globalSearchMinLength) {
            $.ajax({
                type: "GET",
                url: baseURL+"clients/ajax_global_search",
				cache: false,
                data: {
                    'keywords' : globalsearch,
                },
                dataType: "html",
                success: function(results){
					$('#globalSearchResults').html(results);
                }
            });
		} else {
			$('#globalSearchResults').html("Search for a client, simply enter a first name, last name or company name");
		}
	});

	$("#clientImageFb").click(function() {
		var url = $(this).data('image');
		var client = $(this).data('client');
            $.ajax({
                type: "POST",
                url: baseURL+"clients/client_new_image_url",
				cache: false,
                data: { 'image' : url, 'client' : client },
                dataType: "text",
                success: function(results){
					location.reload(true);
                }
            });
	});

	$("#clientImageDefault").click(function() {
		var client = $(this).data('client');
            $.ajax({
                type: "POST",
                url: baseURL+"clients/client_blank_image",
				cache: false,
                data: { 'client' : client },
                dataType: "text",
                success: function(results){
					location.reload(true);
                }
            });
	});

	$("#sendEmailToClient").click(function() {
	//alert(1);
	});

	var attachmentCount = 0;

	$("#attach_files_button").hide();

	$("#attach_files_list").change(function() {
		var listValue = $(this).val();
		if (listValue!=0) {
			$("#attach_files_button").show();
		} else {
			$("#attach_files_button").hide();
		}
	});

	$("#attach_files_button").click(function() {
		// get the value of the select option
		var selectedFile = $("#attach_files_list").val();
		var selectedFileName = $("#attach_files_list option:selected").text();
		if (selectedFile!="") {
			var attachmentFile = "";
			attachmentFile = '<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+selectedFile+'" />'+selectedFileName+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>';
			$("#attached_files").append(attachmentFile);
			attachmentCount++;
			$("#attach_files_list").val(0);
			$("#attach_files_button").hide();
		}
	});

	$(document).on('click', '.attach_files_remove', function() {
		var attachmentRemove = $(this).data("removeattach");
		if (attachmentRemove>=0) {
			var toRemove = "#attach_"+attachmentRemove;
			$(toRemove).remove();
		}
	});

	$("#sendemail_template").change(function() {
		var template = $(this).val();
		if (template>0) {
            $.ajax({
                type: "GET",
                url: baseURL+"clients/ajax_email_template",
				cache: false,
                data: { 'template' : template },
                dataType: "json",
                success: function(data){
					$("#sendemail_subject").val(data['subject']);
					$("#sendemail_body").val(data['body']);
					$("#sendemail_body").code(data['body']); //summernote function to insert content to editor
					//CKEDITOR.instances.sendemail_body.insertHtml(data['body']);

					if (data['attachment_1']!="") {
						$("#attached_files").append('<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+data['attachment_1']+'" />'+data['attachment_1_name']+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>');
						attachmentCount++;
					}

					if (data['attachment_2']!="") {
						$("#attached_files").append('<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+data['attachment_2']+'" />'+data['attachment_2_name']+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>');
						attachmentCount++;
					}

					if (data['attachment_3']!="") {
						$("#attached_files").append('<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+data['attachment_3']+'" />'+data['attachment_3_name']+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>');
						attachmentCount++;
					}

					if (data['attachment_4']!="") {
						$("#attached_files").append('<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+data['attachment_4']+'" />'+data['attachment_4_name']+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>');
						attachmentCount++;
					}

					if (data['attachment_5']!="") {
						$("#attached_files").append('<div id="attach_'+attachmentCount+'"><input type="hidden" name="attach['+attachmentCount+']" value="'+data['attachment_5']+'" />'+data['attachment_5_name']+' <small>(<a href="#" class="attach_files_remove" data-removeattach="'+attachmentCount+'">remove</a>)</small></div>');
						attachmentCount++;
					}

                }
            });
		}

	});

	function taskNotification() {
		$.ajax({
			type: "GET",
			url: baseURL+"dashboard/taskNotification",
			dataType: "html",
			cache: false,
			success: function(data) {
				if (data.length!=0) {
				$("#taskNotifications").modal('show');
				$("#task_notification_list").html(data);
				} else {
				$("#task_notification_list").html("");
					$("#taskNotifications").modal('hide');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}

	// check notification
	taskNotification();
	setInterval(taskNotification, 60000);

	function taskNotificationHide(time) {
		$.ajax({
			type: "GET",
			url: baseURL+"dashboard/taskNotificationHide",
			data: {'time': time},
			dataType: "text",
			cache: false,
			//success: function() {
			//},
			error: function(error) {
				console.log(error);
			}
		});
	}

	$("#taskNotificationHide30").click(function() {
		taskNotificationHide(1);
		$("#taskNotifications").modal('hide');
	});

	$("#taskNotificationHide60").click(function() {
		taskNotificationHide(2);
		$("#taskNotifications").modal('hide');
	});

	$("#taskNotificationHide300").click(function() {
		taskNotificationHide(3);
		$("#taskNotifications").modal('hide');
	});

	function taskNotificationReminder(task,time) {
		$.ajax({
			type: "GET",
			url: baseURL+"dashboard/taskNotificationRemind",
			data: {'task': task, 'time': time},
			dataType: "text",
			cache: false,
			success: function() {
				// refresh list
				taskNotification();
			},
			error: function(error) {
				console.log(error);
			}
		});
	}

	$(document).on('click', '.taskNotificationRemind', function() {
		var task = $(this).data('task');
		var time = $(this).data('time');
		taskNotificationReminder(task,time);
	});

	function taskNotificationComplete(task) {
		$.ajax({
			type: "POST",
			url: baseURL+"clients/cal_complete_task",
			data: { task: task },
			dataType: 'text',
			success: function() {
				taskNotification();
			},
			error: function() {
				console.log(error);
			}
		});
	}

	$(document).on('click', '.taskNotificationComplete', function() {
		var task = $(this).data('task');
		taskNotificationComplete(task);
	});

	//wysiwyg summernote editor
	if ($.isFunction($.fn.summernote)) {
		$('.wysiwygeditor').summernote({
			height:120,
			toolbar: [
    	    	['style', ['style']],
    	    	['color', ['color']],
    	    	['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
    	    	['fontname', ['fontname']],
    	    	['para', ['ul', 'ol', 'paragraph']],
    	    	['height', ['height']],
    	    	['table', ['table']],
    	    	['insert', ['link', 'picture', 'video', 'hr']],
    	    	['view', ['fullscreen', 'codeview']],
    	  	],
		}).parents('form:first').submit(function(e){
			//e.preventDefault();
			var _this = $(this);

			_this.find('.wysiwygeditor').each(function(){
				$(this).val($(this).code()) //summernote function that retrieves editor content
					   .inlineStyler(); //css inline styler
				//console.log($(this).val());
				$.ajax({
					async: false,
					type: 'post',
					cache: false,
					url: baseURL+'clients/clean_wysiwyg_content',
					data: 'dirty_html='+$(this).val(),
					dataType: "json",
					success: function(data) {
						$(this).val(data.clean_html);
					}
				});
			});
		});
	}
});
