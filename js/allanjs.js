var GetClient = (function(){

	var GetClient = function($name, $selector, $url, $input_id, $displayKey){

		var getClients = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.obj.whitespace($displayKey),
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  prefetch: $url
		});

		getClients.initialize();

		jQuery($selector).typeahead({
		  hint: false,
		  highlight: true,
		  minLength: 1,
		},
		{
		  name: $name,
		  displayKey: $displayKey,
		  source: getClients.ttAdapter()
		});

		jQuery($selector).on('typeahead:selected', function (object, datum) {
			jQuery($input_id).val(datum.id);
			//jQuery($selector).val(datum.Name);
			//console.log(datum);
		});
	};

	return{
		init:function($name, $selector, $url, $input_id, $displayKey){
			GetClient($name, $selector, $url, $input_id, $displayKey);
		}
	};

})();
var EditGetClient = (function(){

	var GetClient = function($name, $selector, $url, $input_id, $displayKey){

		var getClients = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.obj.whitespace($displayKey),
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  prefetch: $url
		});

		getClients.initialize();

		jQuery($selector).typeahead({
		  hint: false,
		  highlight: true,
		  minLength: 1,
		},
		{
		  name: $name,
		  displayKey: $displayKey,
		  source: getClients.ttAdapter()
		});

		jQuery($selector).on('typeahead:selected', function (object, datum) {
			jQuery($input_id).val(datum.id);
			//jQuery($selector).val(datum.Name);
			//console.log(datum);
		});
	};

	return{
		init:function($name, $selector, $url, $input_id, $displayKey){
			GetClient($name, $selector, $url, $input_id, $displayKey);
		}
	};

})();


$(document).ready(function() {
	var url = baseURL + 'clients/get_typeahead_client';
	GetClient.init('get-clients', '.getclient', url, '#customer_id', 'Name');
	EditGetClient.init('edit-get-clients', '.editgetclient', url, '#edit_customer_id', 'Name');

	var _notify_task_sound = $(this).find('audio').get(0);


	$('#taskNotifications').on('shown.bs.modal', function (e) {
		_notify_task_sound.play();
	});
	//addNew
	$('#createNewTask').on('shown.bs.modal', function (e) {
		$('#addNew').modal('hide');
	});
});
